---
layout: blog
title: POSSE with Nostr and Drupal
date: 2024-01-02T13:13:32.76Z
description: To crosspost (POSSE - Publish on your Own Site & Syndicate Elsewhere) from your site to other platforms we need API’s from those platforms. With Nostr, the syndication is baked in on the protocol level. This is next level web stuff.
categories: POSSE
comments: "true"
---

Here is an example with long-form content crossposted to Nostr from stacker.news:


With Nostr the data interoperability property is moving from the application (API) layer to the protocol layer.
It’s not only the content that’s portable, also related comments and other kind of reactions are.
Your social graph is interoperable as well and this is pretty unique with Nostr compared with other open protocols focused on building social networking applications.  
## WordPress and the Fediverse
In the Fediverse (based on the ActivityPub protocol) POSSE is also buzzing around. If you have a WordPress website, you can use the plugin [ActivityPub](https://wordpress.org/plugins/activitypub/) (acquired by Automatic in March 2023) to federate (publish) your content to the fediverse network.
Your website will perform as an Actor so it can be followed by other accounts in the Fediverse.
![](Screen-Shot-2023-12-30-15-28-51.72.png)
We could build the same for Nostr: integrate a following feature on a website so when new content is published on that site it pops up in the feed in your Nostr client.  

## POSSE with Drupal to Nostr
For Drupal I’m developing a module Nostr long-form content NIP-23 which makes it possible to publish your (long-form) content (formatted in Markdown) to the Nostr network.

For short text notes, I'm also a maintainer of the module Nostr Simple Publish.
Please read my blog [Nostr integration for CCHS.social](https://sebastix.nl/blog/nostr-integration-for-cchs-social-drupal-cms/) how this module is used to post short text notes to Nostr from the Drupal CMS.

**Resources**
- https://indieweb.org/POSSE 
- https://www.theverge.com/2023/10/23/23928550/posse-posting-activitypub-standard-twitter-tumblr-mastodon 
- https://diggingthedigital.com/how-do-i-posse-to-mastodon-now/ 
- https://publicspaces.net/2022/11/25/de-posse-strategie-pragmatisch-principieel-omgaan-met-big-tech/ (Dutch)
- https://hammyhavoc.com/setting-up-my-blog-within-the-fediverse-via-activitypub/
- https://brid.gy/about#publish
- https://techcrunch.com/2023/09/14/wordpress-blogs-can-now-be-followed-in-the-fediverse-including-mastodon/?guccounter=1 
- https://activitypub.ghost.org/ 
- https://writefreely.org/ 