---
layout: Using a JavaScript singleton in Drupal
title: insert_title_here
date: 2024-01-17T13:43:38.299Z
description: ...description here
categories: tag1, tag2
comments: "true"
---

## What is a JavaScript singleton?



## Drupal does not support Javascript ES6 imports yet

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import

https://www.drupal.org/project/drupal/issues/3398525

## My hacky solution

Let's imagine we have two modules:
- Module A
- Module B

This is the instance Javascript class:

```javascript
class NdkStore {
  /**
   * Constructor.
   *
   * @returns {NdkStore}
   */
  constructor() {
    if(!NdkStore.instance) {
      this._data = {};
      NdkStore.instance = this;
    }
    return NdkStore.instance;
  }

  /**
   * Setter.
   *
   * @param key
   * @param value
   */
  set(key, value){
    this._data[key] = value;
  }

  /**
   * Getter.
   *
   * @param key
   * @returns {*}
   */
  get(key){
    return this._data[key];
  }

  /**
   * Delete an entry.
   *
   * @param key
   */
  delete(key){

  }
  /**
   * Return all keys in an array.
   * @returns {*[]}
   */
  getAllKeys(){
    const keys = [];
    for (const key in this._data) {
      keys.push(key)
    }
    return keys;
  }
}
```



Resources:
* https://code.tutsplus.com/how-to-implement-the-singleton-pattern-in-javascript-es6--cms-39927t
* https://www.drupal.org/project/importmaps
* https://www.previousnext.com.au/blog/using-es6-your-drupal-components