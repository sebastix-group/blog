---
layout: blog
title: Very humbled with an OpenSats grant for the nostr-php library development
date: 2024-04-25T08:04:15
description: In January 2024 I submitted an application for a grant from the Nostr funds. Three months later I received the message my application got approved!
categories: nostr, opensats, php
comments: "true"
---
OpenSats is founded in 2020 by Ben Price. Here he explained what OpenSats is: [https://piped.r4fo.com/watch?v=UmFVC8Ap0pg&t=4230](https://piped.r4fo.com/watch?v=UmFVC8Ap0pg&t=4230). Till the end of 2022 this non-profit organisation had a really hard time on getting donations for the fund for FOSS projects and developers. This changed when they received a $10M donation from Jack Dorsey (former CEO of Twitter) for the general fund and a new created operations budget fund with paid employees. Before that there was no one did paid work for OpenSats and all the work was voluntary. They didn’t received many donations and where struggling setting up a ‘business model’ and raising money for the FOSS funds. Since early 2023 OpenSats received more than 600 applications and they granted to more than 100 projects (with an allocation close to $10M for these grants). What is the source of this info? Listen to this Citadel Dispatch podcast episode with Matt Odell (one of the first board members) and Will Casarin (founder of the Damus Nostr client) where he shared the history of OpenSats and what their current challenges are: [https://overcast.fm/+BHdPJSxlbQ/1:21:32](https://overcast.fm/+BHdPJSxlbQ/1:21:32) 

![Open Sats](open_sats_cover.jpeg)

With this background info I’m even more humbled as a OpenSats grantee receiving sats from the Nostr funds.

### Onwards with nostr-php 
- Github
- Telegram group

### More donations
April 2, 2024: A $1M donation from the Reynolds foundation
May 4, 2024: An additional $21M donation from [#startsmall](https://startsmall.llc/)
With these new donations I'm convinced that OpenSats is heading into the right direction with a lot of critical wisdom on the team. I can imagine it's not easy to "spent" those amounts of money. It's not just the spending, but all the tradeoffs you have to make before making a decision. 
