# What are the best WYSIWYG Markdown editors?

### For Nostr
What are the long-form content Nostr clients on currently using?
* blogstack.io: [Markdown Editor for React](https://uiwjs.github.io/react-md-editor/) 
* habla.news: [Markdown Editor for React](https://uiwjs.github.io/react-md-editor/) 
* YakiHonne: 
* Highlighter.com: [Quilljs](https://quilljs.com/) & [Markdown extension](https://cloverhearts.github.io/quilljs-markdown/)
### For Drupal
CKEditor5
- https://www.drupal.org/project/ckeditor5_markdown_editor 
### Other


**Resources**
- [GitHub - mundimark/awesome-markdown-editors: A collection of awesome markdown editors &amp; (pre)viewers for Linux, Apple OS X, Microsoft Windows, the World Wide Web &amp; more](https://github.com/mundimark/awesome-markdown-editors)
- https://www.wysimark.com/ 
- [The 5 Best Markdown Editors for WordPress Power Users](https://kinsta.com/blog/markdown-editor/) 
- [The best Open Source Markdown editor - CKEditor 5](https://ckeditor.com/blog/CKEditor-5-the-best-open-source-Markdown-editor/) 
-  [GitHub - mundimark/awesome-markdown: A collection of awesome markdown goodies (libraries, services, editors, tools, cheatsheets, etc.)](https://github.com/mundimark/awesome-markdown) 
