---
layout: blog
title: Entix - a Nostr search concept using labels described in NIP-32
date: 2021-01-02T13:13:32.76Z
description: Back in 2008 I worked out a concept for a search engine which would connect any API from the big platform those days.
categories: nostr, search
comments: "true"
---

In 2008 these were the big platforms dominating the web:
* Google
* YouTube (which was still independent)
* Flickr
* Delicious
* Digg
* Ebay
* Amazon
* Technorati
* LinkedIn

Each of them had an API, an application programming interface. You could talk with those API's to send and request data. 

[2find web search api concept](2find-search-api-concept)
![](../2find-search-api-concept/find_honda_b16_build.jpg)

Now 15 years later while I was exploring NIP-32 I had a clear moment last week how I could reuse this model for a Nostr search client.
With Nostr the data interoperabelity has moved away from API's to the protocol layer. This mean we have to figure out a new way of discovering content which is cluttered all over different relays.
There is no single source of truth. NIP-32 is a constructive way how we could add context to events, which is needed if we would like to search for stuff which matches our search query or prompts.

![](entix-concept.png)

All these different kinds already represent a kind of taxonomy with categories. What categories could we use based on the possible provided namespaces with these events?
* Hashtags
* Dates
* Locations
* Products
* Files

## Challenges

### Where to find the events with kind 1985

I'm using nostrdebug.com to find out where all the events live with kind 1985. There is a tool Ontolo (launched November 21) where you can label events. Each time someone labels an event, an event is published to the most used relays.
I searched for events on the following relays (Ontolo is publishing to these relays):
```shell
wss://purplepag.es
wss://relay.nostr.band
wss://nos.lol
wss://relay.snort.social
wss://relay.damus.io
wss://relay.primal.net
```
https://shares.sebastix.dev/lV0E2PsZ.png

I retrieved those event and found 1500+ items from 04-05-2023 till now. That's not a lot.
Most events can be found on relay.nostr.band, relay.damus.io and nos.lol.

### Indexing events

Due to the decentralized characteristics of Nostr, it's hard to query against one source. I can imagine there will be relays who are going to fix this.
There will be very specific relays which you can use, comparable how the relays nostr.band and purplepag.es is already fulfilling specific tasks to clients.

### The real valued information is inside the content of an event

I think LLM's could help us out here. Based on the provided content of an event, a LLM could automatically detect which labels could be applied for the analyzed event. 

### NIP-51
Things could really start shining when NIP-32 and NIP-51 are integrated in some way. 

**Other resources**
* [Why The Five Star Rating Paradigm is Garbage, and One Way to Fix It With NIP-32](https://habla.news/u/arkinox@arkinox.tech/DLAfzJJpQDS4vj3wSleum) 
* [Nostrdebug.com](https://nostrdebug.com)
* [Ontolo](https://www.ontolo.social/)

