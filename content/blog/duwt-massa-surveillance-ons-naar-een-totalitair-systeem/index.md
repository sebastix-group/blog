---
layout: blog
title: Duwt massa surveillance ons naar een totalitair systeem?
date: 2021-01-23T12:09:49.955Z
description: Mijn zorgen over hoe we massa surveillance in ons leven toelaten in
  strijd tegen een onzichtbare vijand.
categories: Online privacy, democratie
comments: false
---
Eerder deelde ik de blog: "[De schaduwzijde van techno-optimisme](https://sebastix.nl/blog/de-schaduwzijde-van-het-techno-optimisme/)". Hierin deel ik een link naar een podcast waarin besproken wordt hoe Big Tech met persoonlijke data omgaat. Ik wil daar even dieper op ingaan. Al sinds 2009 volg ik [Big Tech](https://en.wikipedia.org/wiki/Big_Tech) met een kritische blik. Een bedrijf als Facebook of Google ziet zijn gebruikers als een product bestaande uit data. Met deze data verdienen ze geld. Waarom ik kritisch ben? Omdat de meeste mensen dit niet weten. Of lees jij misschien wél altijd de algemene voorwaarden voordat je die accepteert?\
Handel in persoonlijke data is in mijn ogen niet correct. Orgaanhandel is bij wet verboden. Handel in persoonsgegevens zou dit ook moeten zijn.\
De onwetendheid van de massa geeft digitale bedrijven vrij spel en een ongekende machtspositie. \
Sinds we in gezondheidscrisis beland zijn, maak ik me nog meer zorgen over het gemak waarmee de massa persoonlijke data op straat gooit. Digitale veranderingen volgen elkaar razendsnel op en we lijken blind te zijn voor de maatschappelijke gevolgen. We worden volledig onderworpen aan de macht van deze Big Tech en sinds kort is de overheid daar ook bijgekomen.

### Massa hypnose

Het Covid-virus is levensgevaarlijk. We kunnen er allemaal dood aan gaan. Nietwaar?\
Experts geven adviezen over wat overheden moeten doen om het virus onder controle te krijgen. De maatregelen die daaruit volgen zijn gericht op ons gedrag. Uit angst voor een virus worden vrijheden ingeperkt ten koste van sociale, economische, mentale en fysieke interacties. De massa accepteert dit. Dus ook wij. De overheid heeft de plicht om onze collectieve gezondheid te beschermen. Onze volksgezondheid is echter meer dan alleen de klinische patiënten die we tellen.\
Het is alsof we als volk in hypnose zijn. We lijken onszelf bewust van wat er om ons heen gebeurt, maar onbewust wordt onze manier van denken en doen stapje voor stapje veranderd door allerlei maatregelen. Het lijkt alsof we blind zijn voor de nevenschade die hierdoor aanricht wordt. Wie denkt dat dit allemaal maar tijdelijk is, komt misschien wel bedrogen uit. Er wordt immers gesproken over het nieuwe normaal en vaccinaties lijken ook niet de oplossing te zijn voor de huidige gezondheidscrisis (WHO: [A vaccine by itself will not stop the pandamic](https://www.cbsnews.com/news/who-chief-warns-that-vaccine-alone-wouldnt-end-covid-pandemic/)). Ik denk dat we moeten aannemen dat de gemeenschappelijke vijand, het virus, altijd in ons leven zal blijven. We moeten er dus mee leren leven. Geldt dat dan ook voor *massa surveillance*? Moeten we accepteren dat we met z'n allen in één groot Big Brother huis leven? 

### Democratie? Technocratie!

Maar hoe moeten we er dan mee leren leven? Veel oplossingen worden gevonden in digitale technologie. Bedrijven die als techno-optimisten in het leven staan, hebben de volgende overtuigingen: 

> Met de huidige techniek kunnen we alles onder controle krijgen! \
> Hoe meer data we verzamelen, hoe beter digitale oplossingen worden! \
> Hoe meer data we hebben, hoe beter we de toekomst kunnen voorspellen!

Twee voorbeelden en een visueel overzicht waarin dit zich uit:

* Contact tracing applicaties (CoronaMelder)
* Health passport applicaties om aan te tonen dat je gezond bent

![](infographic_data-tijdens-corona-bits-of-freedom-2-scaled.jpg)

Onze overheid is er niet vies van verschillende digitale technieken te gebruiken. Zij verzamelt op grote schaal zoveel mogelijk data van haar burgers om vervolgens een zo goed mogelijke beslissing te kunnen nemen. Wat volgt is besluitvorming op basis van deze data die in allerlei rekenmodellen wordt gestopt. Het is wetenschappelijke besluitvorming. Echter wat als die data niet klopt of een totaal ander beeld weergeeft dan de realiteit? Die data is dan waardeloos, evenals de genomen besluiten. [Garbage in, garbage out](https://en.wikipedia.org/wiki/Garbage_in,_garbage_out). Een goed voorbeeld is de toeslagenaffaire.\
Ik heb het gevoel dat we in een *[technocratie](https://nl.wikipedia.org/wiki/Technocratie)* leven waarin geen ruimte is voor ethische afwegingen bij besluitvorming. Dat zien we ook terug in veel digitale oplossingen. Ze dienen vaak een primitief individueel doel gebaseerd op aannames. Een Corona Melder applicatie die continue volgt bij wie je in de buurt ben geweest, heeft als doel om je te waarschuwen dat je in contact bent geweest met iemand die positief is getest. In mijn ogen heeft die applicatie een schaduwzijde. Je wordt gemanipuleerd omdat je wordt gevolgd. Als je gevolgd wordt, pas je onbewust je gedrag aan. De Corona Melder app doet ook de aanname dat anderen een potentieel besmettingsgevaar zijn voor jou en daar gedraag je je dus ook naar. 

### Willen we een totalitaire samenleving?

Mijn grootste zorg op dit moment is dat we blind zijn voor de nevenschade die we aanrichten. Zelfs als dat op langere termijn meer mensenlevens kost dan dat er mensen zullen overlijden aan het virus zelf. De vijand moet ten koste van alles verslagen worden. \
Zoals gezegd: vaccinaties zijn blijkbaar niet de oplossing voor de huidige gezondheidscrisis. De oplossing moet dus ergens anders gevonden worden: s*urveillance*. Permanente controle op ons gedrag. Zolang iedereen zich correct gedraagt, zal het virus onder controle zijn. Dat is de aanname. Dat is wat experts met data en hun rekenmodellen ons vertellen. Het is hun heilige geloof in technologie en data. 

Het volk gaat hierin mee en vertrouwt zijn data ook toe aan deze experts. Ik merk dat de sociale druk en controle toeneemt. Voortkomend uit de angst voor het virus. We controleren of de medemens zich wel aan de maatregelen houdt. De bemoeienis neemt dus toe. Ik vind dat we kritisch moeten blijven en de maatschappelijke gevolgen in de breedte moeten blijven afwegen. Je kunt jezelf  afvragen of je in een totalitair systeem zou willen leven. Is dit enige oplossing om het virus de baas te blijven?

Volgens mij is het tijd voor meer realisme. Zelf óók nadenken in plaats van het denkwerk altijd aan anderen overlaten. Dus niet zomaar die algemene voorwaarden accepteren. Wees kritisch, luister aandachtig en stel vragen. Bemoei je niet met de ander maar neem je eigen verantwoordelijkheid.

Ik sluit me aan bij [deze rechterlijke uitspraak](https://openjur.de/u/2316798.html) in Duitsland (vertaald naar het Nederlands door [The Fire Online](https://thefireonline.com/duitse-rechter-coronabeleid-catastrofaal-verkeerde-politieke-beslissing-met-dramatische-gevolgen-voor-menselijk-leven/)):

> *Als elke burger wordt gezien als een bedreiging waartegen anderen moeten worden beschermd, wordt hem ook de mogelijkheid ontnomen om te beslissen aan welke risico’s hij zichzelf blootstelt, wat een fundamentele vrijheid is. Of de burger nu ‘s avonds een café of een bar bezoekt en het risico van infectie met een respiratoir virus accepteert omwille van gezelligheid en levensvreugde, of dat ze voorzichtiger is omdat ze een verzwakt immuunsysteem heeft en daarom liever thuis blijft, is onder een algemeen contactverbod niet meer te beslissen.*
>
> *De vrije subject die de verantwoordelijkheid neemt voor zijn eigen gezondheid en die van zijn medemensen wordt hierbij geschorst. Alle burgers worden door de staat gezien als potentiële bronnen van gevaar voor anderen en dus als objecten die door staatsdwang moeten worden “op afstand” gehouden.*

*Mijn bronnen? Of wil je meer weten?*

Future Shock podcast - Dataïsme met Mirjam Rasch
<audio controls style="width: 100%">

<https://www.bitsoffreedom.nl/2020/12/16/body-surveillance-niet-het-nieuwe-normaal/>

<source src="https://content1a.omroep.nl/urishieldv2/l27m46f663b70f26016200600c23d9000000.854b6b55a939e90e5f9896524773d424/vpro/algemeen/tegenlicht/podcast/futureshockmiriamrascheindmix.mp3"</source>
</audio>

<https://www.dewereldmorgen.be/community/mattias-desmet-professor-klinische-psychologie-coronamaatregelen-onthullen-totalitaire-trekken/>

[Techno realisme](https://www.technorealism.org/)

<https://kompanje.org/>

<https://www.bitsoffreedom.nl/2020/11/25/een-testsamenleving-mag-niet-uitmonden-in-een-douane-en-controlesamenleving/>

[VPRO Tegenlicht - Technologie als religie](https://www.vpro.nl/programmas/tegenlicht/kijk/afleveringen/2020-2021/technologie-als-religie.html)

VPRO Tegenlicht - Overleven in de Chaos\
<https://www.npostart.nl/vpro-tegenlicht/29-09-2019/VPWON_1295415>

Literatuur: [Frictie van Miriam Rasch](https://www.debezigebij.nl/boek/frictie/)