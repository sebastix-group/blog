---
layout: blog
title: Maak BigTech interoperabel
date: 2021-02-16T13:51:28.942Z
description: Bij VPRO's podcast serie De Slag om het internet was Toon Toetenel
  te gast met een pleidoor om grote social media platformen meer interoperabel
  te maken via wet- en regelgeving..
categories: open internet
comments: "true"
---
Luister hier de podcast:

<audio controls style="width:100%">
    <source src="/blog/audio/Podcast_Toon_Toetenel_internetpionier_mp3.mp3" type="audio/mpeg">
</audio>

Hij pleit voor een decentraal, gefedereerd internet zodat de macht niet kan worden gecentraliseerd. Hiervoor kunnen we verschillende protocollen inzetten als standaard. Zoals bijvoorbeeld [ActitityPub](https://en.wikipedia.org/wiki/ActivityPub) voor social media feeds (timelines). Voor chats heb je bijvoorbeeld XMPP of [Matrix](https://matrix.org/). Applicaties die van deze standaarden gebruikmaken, zorgen ervoor dat data vrij kan bewegen tussen systemen en applicaties. Data zoals bijvoorbeeld chatberichten. Stel dat zowel Whatsapp als Telegram beide de [XMPP](https://xmpp.org/) standaard integreren, dan is het mogelijk om berichten tussen beide applicaties uit te wisselen. Dan kan ik met  de Telegram app chatten met een vriend die gebruikt maakt van Whatsapp. Vroeger werkte dit prima, toen gebruikte ik de applicatie [Adium](https://adium.im/) waar ik via MSN chatte met schoolvrienden en via ICQ berichten stuurde naar online gamevrienden. 

**Interoperabiliteit**

Al jaren ben ik groot voorstander van meer interoperabiliteit. 

> Producten, systemen of organisaties zijn **interoperabel** als ze zonder beperkingen samen kunnen werken. De term wordt vaak gebruikt in de IT, bij de spoorwegen, de medische sector en bij overheidsorganisaties bij onderlinge samenwerking en bij samenwerking met het bedrijfsleven, maatschappelijke organisaties en burgers.\
> Een woord voor product, systeem, organisatie, enzovoorts, is entiteit. Voor **interoperabiliteit** van entiteiten zijn standaarden, protocollen en procedures nodig voor de wederzijdse  afstemming van de entiteiten. Bij fysieke entiteiten, zoals producten, apparaten en communicatiemiddellen, impliceert interoperabiliteit ook dat de gebruiker geen speciale moeite hoeft te doen om de entiteit interoperabel te laten zijn.\
> Interoperabiliteit is noodzakelijk als de samenwerking van entiteiten noodzakelijk is en als de entiteiten autonoom of heterogeen zijn. Netwerken zijn daar een voorbeeld van. Zonder interoperabiliteit functioneert een netwerk niet.\
> Tekst van *[https://nl.wikipedia.org/wiki/Interoperabiliteit](<﻿ https://nl.wikipedia.org/wiki/Interoperabiliteit>)*

Wie mijn drieluik [we geven alles weg](https://lifehacking.nl/we-geven-alles-weg/), [persoonlijke data zijn vogelvrij](https://lifehacking.nl/persoonlijke-data-zijn-vogelvrij/) en [jouw personal graph](https://sebastix.nl/blog/io/jouw-personal-graph/) heeft gelezen in 2012, zal inzien dat er in bijna 10 jaar tijd weinig is veranderd als het gaat om het creëren van een meer open internet. Sterker nog, het internet is veel gecentraliseerder geworden met de huidige BigTech partijen. Onze data zit opgesloten in *closed gardens* en het is verdomd lastig om eruit te stappen mét je data naar een ander open platform. Toon Toetenel pleit ervoor dat die huidige BigTech via wet- en regelgeving verplicht worden om open standaarden op te nemen op hun platform, zodat we eenvoudig kunnen kiezen welke applicaties en platformen we gebruiken om onderling met elkaar te communiceren. Daar sluit ik me bij aan want via marktwerking is het vooralsnog niet gelukt om social media platformen meer open te maken. 

Check ook eens [deze vier maatregelen](https://www.bitsoffreedom.nl/2021/02/11/vier-maatregelen-om-de-dominantie-van-google-en-facebook-te-ondermijnen/?utm_source=mailpoet&utm_medium=email&utm_campaign=nieuwe-regels-over-jouw-online-privacy-en-platforms-en-wie-help-jij-vandaag-hun-privacy-te-fixen_330) die Bits of Freedom stelt om BigTech meer interoperabel te maken.

Bron: <https://www.vpro.nl/programmas/tegenlicht/kijk/podcast/2021/Een-internetprogrammeur--off-the-record-.html>