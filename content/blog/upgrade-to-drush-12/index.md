---
layout: blog
title: Upgrade to Drush 12
date: 2023-10-05T10:24:24.748Z
description: Drush version 12 is out since June 2023 and is now the new recommended version using Drupal 10. While I was upgrading to this version, I needed to adjust my $PATH for using the drush command.
categories: Drupal, Drush
comments: "true"
---

I ran into the issue that my `drush` command was broken after I upgraded Drush to version 12 on the Drupal 10 websites I maintain.

```bash
$ drush cache-rebuild
The Drush launcher could not find a local Drush in your Drupal site.
Please add Drush with Composer to your project.
Run 'cd "/var/www" && composer require drush/drush'
```

On some other environments this error could pop up:  
`PHP Fatal error:  Uncaught RuntimeException: Drush Launcher is not compatible with Drush 12+. See https://github.com/drush-ops/drush-launcher/issues/105. in /home/user/domains/yourdomain.com/vendor/drush/drush/includes/preflight.inc:23`

## Removing Drush Launcher
I always installed Drush globally with a wrapper for my global `$PATH` with [Drush Launcher](https://github.com/drush-ops/drush-launcher). This project is now archived and deprecated with Drush 12. To uninstall Drush Launcher, I simply executed this command:
`rm /usr/local/bin/drush`.  
You can find your Drush location by using the `whereis drush` command.

## Using the local Drush in your project
The current recommended installation for Drush 12 is to use the Drush binary which you can find in the `vendor/bin` directory of your packages installed by Composer within your project: `vendor/bin/drush`.   
So, to just use `drush` again instead of the command `vendor/bin/drush`, I added this line of code to my `~/.bashrc` file:

```bash
export PATH="$PATH:./vendor/bin:/usr/local/bin"
```

It also meant I needed to update my Docker images, server environments and some CI/CD scripts where I'm using `drush`. 