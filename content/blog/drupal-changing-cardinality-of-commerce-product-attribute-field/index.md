---
layout: blog
title: "[Drupal] Changing cardinality of commerce product attribute field"
date: 2021-02-12T20:47:11.382Z
description: Change the number of accepted values of a commerce product attribute.
categories: Drupal Commerce
comments: "true"
---
This blog is a note to myself. I was looking to change the cardinality of Drupal Commerce attribute field in in the configuration code. It is missing in the user interface of the CMS.

I've found it in the file `field.storage.commerce_product_variation.attribute_<your_field_name>.yml`

Change `cardinality: 1` to `cardinality: -1` to set the options to unlimited.