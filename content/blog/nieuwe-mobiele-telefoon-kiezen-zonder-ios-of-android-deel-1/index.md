---
layout: blog
title: Nieuwe mobiele telefoon kiezen zonder iOS of Google Android - deel 1
date: 2021-08-25T07:44:45.493Z
description: Vanaf het moment dat de Apple iPhone in Nederland beschikbaar was
  (iPhone 3G), ben ik altijd een iPhone bezitter geweest. Ongeveer elke 3 à 4
  jaar kocht ik een nieuwe iPhone, want dan was de vernieuwing vaak
  aantrekkelijk genoeg voor mij. Toch slijt ik nu al meer dan 5 jaar een 4inch
  iPhone SE (2016) omdat er maar geen nieuwe 4inch variant op de markt kwam.
  Inmiddels ben ik toch echt van plan om een andere mobiele telefoon aan te
  schaffen, echter ben ik op zoek naar een toestel zonder iOS of zonder Google
  Android. Een uitdaging!
categories: Apple, Google, iOS, Android, deGoogle, privacy, opensource
comments: "true"
---
Al langere tijd ben ik bezig om van andere producten en diensten gebruik te maken dan die van de machtige Big Tech die in mijn ogen een te grote monopolie hebben. Alternatieven zijn vaak open source toepassingen die minstens net zo goed werken of zelfs beter. Ik maak bijvoorbeeld reeds gebruik van Matomo ([zie deze blog](https://sebastix.nl/blog/terug-naar-eenvoudige-statistieken-met-matomo-bye-bye-google-analytics/)) in plaats van Google Analytics, Protonmail in plaats van Gmail, Firefox in plaats van Google Chrome, DuckDuckGo in plaats van Google Search en Notion / LibreOffice in plaats van Google Docs. Websites als [No More Google](https://nomoregoogle.com/) en [Good Reports](https://goodreports.com/) bevatten een actueel en uitgebreid overzicht met dergelijke alternatieven. 

Een nieuwe mobiele telefoon met een *open source operating system*, idealiter zonder koppeling naar Apple en Google, is voor mij een logische stap om minder afhankelijk te worden van Big Tech.

### Meer vrijheid door beperkingen

Door de keuze voor een telefoon zonder Apple of Google leg ik mezelf wel de nodige beperkingen op, maar dankzij deze beperkingen kan ik me losmaken van een systeem waarin steeds meer (ongewenst) voor mij wordt uitgedacht en uitgevoerd.

Ik besef me dat beperkingen zorgen voor gedoe, weerstand en frictie, daarom doe ik een beroep op mijn zelfbeschikking. Ik lever een stuk comfort, gemak en eenvoud in, maar ik zie het als een uitdaging om toch de noodzakelijke dingen te gaan vinden. Dingen die ik zelf moet fixen en niet standaard zijn. Desnoods bouw ik het zelf. Dat kost extra moeite, maar daar krijg ik juist energie van, omdat ik geloof dat mijn keuze voor zo'n telefoon het internet een stukje beter maakt. Ik sta voor een internet met minder centralisatie en macht bij één organisatie zoals Apple, Facebook of Google. Ik wil minder Big Tech.

**Wat betekenen die beperkingen mogelijk in de praktijk?**

* Geen social media zoals Instagram, TikTok of Facebook
* Geen Google account

  * Geen Gmail
  * Geen Google Drive
  * Geen Google agenda
  * Geen Playstore
* Geen Apple account

  * Geen iCloud
  * Geen Apple app store
* Verschillende apps die misschien niet zullen werken

![Volla Phone](gjaad6qe0okcrrkgvggs.jpg "Volla Phone")

### Alternatieve mobile operating systems

* [PureOS](https://pureos.net/)
* [Ubuntu Touch](https://ubuntu-touch.io/nl/)
* [LineageOS (Android ROM)](https://lineageos.org/)
* [Replicant](https://replicant.us/)
* [GrapheneOS (Android ROM)](https://grapheneos.org/)
* [openSUSE](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions)
* [pmOS](https://postmarketos.org/)
* [/e/](https://e.foundation/)
* [CalyxOS](https://calyxos.org/)
* [Sailfish OS](https://sailfishos.org/)
* [Plasma Mobile](https://www.plasma-mobile.org/)
* [Volla OS](https://volla.online/en/downloads/)

Hoeveel blogs ik over dit avontuur ga schrijven, weet ik niet. Dit is in ieder geval het eerste deel. De volgende stap is voor mij het vinden van ervaringen van anderen over bovengenoemde systemen en het inventariseren van verschillende geschikte telefoons. Waar ik zelf al een korte, goede eerste indruk van heb zijn de volgende telefoons / operating systems:

* [PinePhone](https://www.pine64.org/)
* [/e/](https://e.foundation/leaving-apple-google-pre-order-your-e-t2e-dark-mode-is-coming-and-e-is-hiring/)
* [Volla Phone](https://volla.online/en/index.html)
* [Purism Librem 5](https://puri.sm/products/librem-5/)
* [Ubuntu Touch](https://ubuntu-touch.io/nl/)

Heb je zelf ervaring, tips of andere suggesties? Let me know!