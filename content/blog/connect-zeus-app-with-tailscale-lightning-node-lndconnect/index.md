---
layout: blog
title: Connect Zeus wallet app with Tailscale to your lightning node
date: 2023-09-15T14:28:19.389Z
description: This is a short explanation how I connected my Zeus wallet using Tailscale instead of Tor with lndconnect. The Tor connection between my Zeus wallet and Lightning was always very slow. Now my Zeus wallet loads instant without the hassle waiting for an active connection.  
categories: zeus, tailscale, lightning network
comments: "true"
---

Back in 2021 I discovered Tailscale via the [Selfhosted podcast](https://selfhosted.show/). Tailscale is an application and service which creates one (private) network which connects all your devices on different networks. Each device has its own IP address within this network.
When connected to this network, each device can talk to each other. This means I can connect to my node device at my office from my laptop. It does not matter how my laptop is connected as it has least a working internet connection.
The same goes up for my mobile device where I use Tailscale to connect Zeus to my node.

In the documentation of Zeus you cannot find how to connect your Zeus app to your Tailscale network. There is only a section how to connect your Umbrel node with Zeus and Tailscale.

Before we can connect Zeus to your node with Tailscale:
1. Create an account on [Tailscale.com](https://tailscale.com)
2. Install Tailscale on your mobile device   
-> [iOS](https://apps.apple.com/us/app/tailscale/id1470499037?ls=1) or [Android](https://play.google.com/store/apps/details?id=com.tailscale.ipn) or [F-Droid](https://f-droid.org/packages/com.tailscale.ipn/)
3. Install Tailscale on your node device - [Linux](https://tailscale.com/download/linux)
4. Copy the private IP address of your node
5. Install [lndconnect](https://github.com/LN-Zap/lndconnect) on your node device
6. Install [Zeus](https://zeusln.app/) on your mobile device

Open your Zeus wallet app on your phone and go to **Settings** -> **Connect a node** and click on the **+** and this screen will be opened:
![](IMG_CD8752550F65-1.jpeg)

Here you select the **Node interface** depending on which Lightning Node implementation you are using. In my case it's LND (REST).  
Make sure you also **uncheck** the **Use Tor** and **Certificate Verification** option.

Now it's time to open your terminal and run this lndconnect command on your node device:
```
lndconnect --host=<your_private_ip_address_from_tailscale> --port=8080 --nocert
```
Depending on your terminal, you maybe have to zoom out to see the full QR code before scanning it with your Zeus app.

Back to your Zeus application, press **Scan LN node QR**.
Now scan the QR code with Zeus.

Before hit save config, remove the Use Tor option and replace all the onion address with that Tailscale IP of your node (see point 4).
When you hit the save config button, Zeus will warn you about not verifying your connection with a certificate. As we are using a private network, this is not needed so click the button **I understand, save node config**.

Now you should be able to see your lightning and on-chain balances, send and receive payments and see a list of your lightning channels.

_Used resources_
* https://community.umbrel.com/t/how-to-use-tailscale-with-umbrel/6782
* https://docs.zeusln.app/for-users/connecting-zeus/connect-umbrel#using-tailscale-network
* https://theroadtonode.com/en/lightning-extensions/lnd-connect

If you need any help or would like to ask me something, don't hesitate to contact me!

