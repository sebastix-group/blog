---
layout: blog
title: "Carage: een automotive community platform in 2012"
date: 2021-03-05T20:50:36.484Z
description: Ongeveer 9 jaar geleden heb ik samen met 402automotive een
  eenvoudig community platform opgezet waar autoliefhebbers een profiel konden
  aanmaken van hun auto.
categories: social network, automotive, platform
comments: "true"
---
Het Carage platform staat niet meer online. Het platform was een gezamenlijk initiatief met [402automotive](https://www.402automotive.com). Ergens in 2013 is het offline gegaan wegens de beëindiging van de samenwerking tussen mij en de backend provider Caret waarop ik het platform had gebouwd. Daarna heb ik het, jammer genoeg, nooit opnieuw opgebouwd met een andere (opensource PHP) backend framework. Hierbij enkele screenshots van het platform uit 2012:

![](554200_481463265209979_1448107915_n.jpg)

![](149450_411176362238670_1778352227_n.jpg)

![](478600_409817385707901_745501075_o.jpg)

In een [oude blogpost](https://sebastix.nl/blog/io/carage-social-network-applicatie-voor-autoliefhebbers/) van oktober 2012 heb ik al eens eerder geschreven over Carage. In deze post heb ik beschreven hoe het platform werkte in combinatie met Facebook Open Graph objects en actions. Daarmee was het mogelijk om een eigen Carage / autoprofiel met een tijdlijn te creëren op jouw Facebook profiel.

![](timeline-facebook.png)

![](google-chrome-5.png)

![](google-chrome.png)

Wegens tijdgebrek ben ik afgelopen jaren niet heel actief meer met mijn auto-hobby, maar ik zie wel mogelijkheden om dit idee op een soortgelijke manier uit rollen voor mijn andere hobby: fietsen! Persoonlijk zou ik het tof vinden om een profiel aan te maken van mijn fiets op bijvoorbeeld mijn Strava account of Komoot.