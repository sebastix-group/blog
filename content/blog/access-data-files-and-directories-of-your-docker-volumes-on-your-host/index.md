---
layout: blog
title: Access data, files and directories of your Docker volumes on your host
date: 2022-08-04T07:16:01.297Z
description: Are you looking where all the data with files are being stored on
  your machine outside the containers when using Docker volumes?
categories: docker
comments: "true"
---
Have a look at this directory where you will find all the volumes:

`sudo ls -la /var/lib/docker/volumes`

I was looking for the data directory of my Nextcloud instance which I'm running with a docker-compose file.\
I found all the data in this directory:

`/var/lib/docker/volumes/nextcloud_nextcloud/_data/data/sebastix/files`

Thanks to [this link](https://forums.docker.com/t/how-to-access-docker-volume-data-from-host-machine/88063/4) finding the answer I was looking for. Please note that you need root permissions to access these files.