---
layout: blog
title: Photodump and my highlights from Adopting Bitcoin Arnhem
date: 2024-05-30T13:13:32.76Z
description: "From May 23 till May 25 I was in the Bitcoin capital of The Netherlands: Arnhem. There was a full program around the conference day on Saturday. I and many other bitcoiners gathered to celebrate the 10th anniversary of #bitcoincity Arnhem."
categories: bitcoin Arnhem bitcoincity
comments: "true"
---
## Playing the Blockhunters board game
It was very honourable to play with one of the first prototypes of the boardgame [Blockhunters](https://blockhuntersgame.com/) developed by [Arnold Hubach](https://njump.me/nprofile1qy88wumn8ghj7mn0wvhxcmmv9uq3jamnwvaz7tmjv4kxz7fwwdhx7un59eek7cmfv9kz7qpqw98ems6ryhpv7zvmhwp5sv65p0pwrnvzw4lucn0ch776qan9ntds8dxyq9). 

![](blockhuntersarnhem.jpg)
![](IMG_0938.jpg)

## Nostr meetup: Nostrdam Arnhem
![](IMG_0942.jpg)

A nice small present from [Jurjen](https://njump.me/nprofile1qy88wumn8ghj7mn0wvhxcmmv9uq32amnwvaz7tmjv4kxz7fwv3sk6atn9e5k7tcqyrlmedcxnaa2t4kmz20t88na32tcj3ndy4dxx7avr6lcv9as2aqygt8ncz9) and meeting [Nathan Day](https://njump.me/nprofile1qyghwumn8ghj7mn0wd68ytnhd9hx2tcpr9mhxue69uhhyetvv9ujuumwdae8gtnnda3kjctv9uqzp384u7n44r8rdq74988lqcmggww998jjg0rtzfd6dpufrxy9djk8ygccp5) in real life from [btcmap.org](https://btcmap.org). A project that deserves some Nostr integration (which he agrees with!).

![](IMG_0941.jpg)

A gift from Bitpopart together with [Patrick](https://njump.me/nprofile1qyvhwumn8ghj7mn0wd68ytnnv43xzum5d9uzuer9wchsz9thwden5te0wfjkccte9ejxzmt4wvhxjme0qqs2a5vnhea6ph3f85furxq7fl34reztwz622lq8an4vu2l2rgjzz4gxsfeld) (who organised Adopting Bitcoin Arnhem), [Jeroen](https://njump.me/nprofile1qy28wumn8ghj7mn0wd68y6tp9eehqctrv5hszrnhwden5te0dehhxtnvdakz7qpq7plqkxhsv66g8quxxc9p5t9mxazzn20m426exqnl8lxnh5a4cdns72jzh7), [Mr. Bitpopart](https://njump.me/nprofile1qy2hwumn8ghj7un9d3shjtn90p5hgtnsw43z7qgjwaehxw309ahx7um5wghxcafwddjj7qpqgwa27rpgum8mr9d30msg8cv7kwj2lhav2nvmdwh3wqnsa5vnudxqun2jkc) in the middle and [Djuri](https://njump.me/nprofile1qy88wumn8ghj7mn0wvhxcmmv9uq3xamnwvaz7tmsw4e8qmr9wpskwtn9wvhsqg94zfaq3nenv938fqq2gwrcsx5lnrsyh8phz9hf9hjj2pycvdwyygukx5tt). I'm on the right ;-)

## Gravel at the Veluwe
On Friday morning I enjoyed a 70km gravel ride on the Veluwe to Radio Kootwijk and back. 
![](IMG_0943.jpeg)
![](IMG_0951.jpeg)
![](IMG_0955.jpeg)
![](IMG_0961.jpeg)
![](IMG_0962.jpeg)
![](IMG_0964.jpeg)
![](IMG_0966.jpeg)
## Bitdevs meetup
In the afternoon I attended the Bitdevs meetup. It was my first time and I expected a lot of technical stuff to be discussed. This wasn't the case, all conversations were led very professionally in an easy, understandable way.
Also, the [Silent payments](https://bitcoinops.org/en/topics/silent-payments/) (https://silentpayments.xyz/) feature was introduced to me. A very interesting feature integrated in Bitcoin recently which I would like to dive in later. 
## Meeting the Noderunners community 
On Thursday I got a nice pin from a Noderunner community member. The Noderunners are a community with 200+ bitcoin-only minded people. I already knew some of them, but every time I attend a Bitcoin event I always learn to know new people who are often also members of that community. During the Bitcoin Business meetup on Friday night, I also met a new Noderunner who was also camping at the same site as me. He was literally standing beside me with his tent!
![](IMG_1003.jpeg)
Noderunners on stage
![](noderunners.jpg)
## Conference day
You can check out the livestream recordings here:
- Mainstage: https://www.youtube.com/live/T0vJ4uEsCwc
- Arnhem stage: https://www.youtube.com/live/-GTw6k1IkC0 

These are the sessions I enjoyed the most:
- [Bitcoin & human rights, what 99% don't get](https://pretalx.com/adopting-bitcoin-arnhem-2024/talk/YBZ8BG/) with [Anita Posch](https://njump.me/nprofile1qy88wumn8ghj7mn0wvhxcmmv9uq32amnwvaz7tmjv4kxz7fwv3sk6atn9e5k7tcqypw2mqkgnrhxvqfhzx2966rl0k25naj95qgcge76ut67yax9nrt07fvw0pt) 
- [AML regulations and financial freedom panel discussion](https://pretalx.com/adopting-bitcoin-arnhem-2024/talk/QFQCDK/ ) with 
  - [Sjors Provoost](https://njump.me/nprofile1qy88wumn8ghj7mn0wvhxcmmv9uq3zamnwvaz7tmwdaehgu3wwa5kuef0qqsgdp0taan9xwxadyc79nxl8svanu895yr8eyv0ytnss8p9tru047qlqcp9l) (Bitcoin core developer)
  - Rebecca van Essen (relation manager Financial Intelligence Unit Netherlands)
  - [Lyudmyla Kozlovska](https://njump.me/nprofile1qy88wumn8ghj7mn0wvhxcmmv9uq3jamnwvaz7tmjv4kxz7fwwdhx7un59eek7cmfv9kz7qpq3ajk3hhvqys2ev4y68jwxywgs8fsdsuk4y5gkzs874jdyrccvf5q3t5wvx) (president of Open Dialogue Foundation)
  - Dorien Rookmaker (Member of the European Parliament)
  - Maud Bökkerink (financial and cryptocurrency expert & consultant)
- [Non-custodial and custodial solutions and their global impact](https://pretalx.com/adopting-bitcoin-arnhem-2024/talk/V8WSZD/) 
- [Fiat the system vs Bitcoin the tool](https://pretalx.com/adopting-bitcoin-arnhem-2024/talk/KBAU8L/) with [Rigel Walshe](https://njump.me/nprofile1qyvhwumn8ghj7un9d3shjtnndehhyapwwdhkx6tpdshsz9thwden5te0wfjkccte9ejxzmt4wvhxjme0qqsfhc93au47jc5hrtx32t9fswxqee9hrvg94fxmgq7kzpxc52ut49qzv6jmd)

I also got my book The Genesis Book signed by the author [Aaron van Wirdum](https://njump.me/nprofile1qy88wumn8ghj7mn0wvhxcmmv9uq3jamnwvaz7tmjv4kxz7fwwdhx7un59eek7cmfv9kz7qpqart8cs66ffvnqns5zs5qa9fwlctmusj5lj38j94lv0ulw0j54wjqmu990h).

![](IMG_0972.jpeg)
![](IMG_0977.jpeg)
![](IMG_0978.jpeg)
![](IMG_0980.jpeg)
![](IMG_0981.jpeg)
![](IMG_0982.jpeg)

**Resources**
- https://adoptingbitcoinarnhem.com/
- https://bitcoinfocus.nl/2024/05/28/353-bitcoinfeest-in-arnhem/
- https://piped.r4fo.com/watch?v=l3sBoztfLMI 
