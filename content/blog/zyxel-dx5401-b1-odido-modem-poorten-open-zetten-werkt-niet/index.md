---
layout: blog
title: Odido's Zyxel T54 DX5401-B1 poorten open zetten werkt niet 
date: 2023-12-18T13:18:04.144Z
description: Nadat in april 2023 glasvezel was aangelegd in de meterkast (Sprundel, gemeente Rucphen), kwam er pas na 8 maanden (!!!) licht binnen en dus een werkende internetverbinding. Deze verbinding (1gbit/s) is welliswaar tien keer zo snel ten opzichte van ons vorige abonnement, maar op dit moment ervaar loop ik vast op een beperking van de door Odido geleverde Zyxel modem. Het openzetten / doorschakelen van poorten (portforwaring) werkt niet. 
categories: Odido, glasvezel, zyxel modem
comments: "true"
---

Thuis maak ik gebruik van diverse apparaten / applicaties die via internet van buitenaf benaderbaar moeten zijn. Dit is mogelijk via verschillende poorten via je modem (NAT). Tegenwoordig heeft elke modem de optie om dit in te stellen. Bijvoorbeeld voor het spelen van bepaalde games is dit nodig of voor je huisbeveiligssysteem met IP camera's. 
Nadat ik aan de slag ging om alle configuratie over te nemen van mijn oude modem (inclusief statische IP adressen voor de apparaten), kwam ik er al snel achter dat de poorten die ik openzet dicht blijven. Het testen doe ik vanaf mijn kantooradres. 

## Portforwarding werkt niet op Odido's Zyxel T54 DX5401-B1 modem

Na diverse berichten van anderen te hebben gelezen is mijn conclusie dat Odido's versie van de firmware op deze modem de boosdoener is. De firmware zorgt ervoor dat bepaalde functies op de modem zijn uitgeschakeld. De originele firmware van Zyxel biedt namelijk meer opties.  
Het is vreemd dat je in de admin van de router wel de optie hebt om portforwards te configureren, maar dat dit vervolgens niet werkt. In mijn ogen is dit een fout / bug in de firmware, het werkt immers niet zoals je verwacht. Contact met de klantenservice levert ook niets op, want daar is de benodigde technische kennis bij de mensen niet aanwezig. 

## Poort 443 openzetten (WAN) voor extern beheer werkt wel

Bij de instellingen van extern beheer kun je ook een poort (bijvoorbeeld 443) instellen waarmee je de admin / het beheer van het modem kunt openen in je browser.
Daar kun je een vinkje zetten bij de checkbox WAN, zodat deze via internet bereikbaar is. Als ik vervolgens een test uitvoer, zie ik dat poort 443 wel openstaat.
Dit is echter geen oplossing, want je wilt niet dat iedereen het beheerspaneel van jouw router kan benaderen. 

## Alternatief

Gelukkig hebben we sinds 28 januari 2023 een vrije modemkeuze in Nederland. De Zyxel T54 modem is waardeloos in mijn geval, dus ik overweeg om een [Fritz!Box 5590 modem (XGS-PON)](https://nl.avm.de/producten/fritzbox/fritzbox-5590-fiber-xgs-pon/) aan te schaffen.

Nog een ander alternatief is om gebruik te maken van [Tailscale Funnel](https://tailscale.com/blog/introducing-tailscale-funnel). 

---

**Welke bronnen heb ik geraadpleegd?**

* Tweakers forum: [[Odido Glasvezel] Ervaringen & Discussie](https://gathering.tweakers.net/forum/list_messages/2206944/last) 
* [Zyxel T54 Port forwarding Poort 80/443](https://community.odido.nl/thuisnetwerk-566/zyxel-t54-port-forwarding-poort-80-443-352509?tid=352509&fid=566)
* [Port forwarding instructies Zyxel op het Odido forum](https://community.odido.nl/bekabeld-internet-492/port-forwarding-zyxel-t50-met-glasvezel-wil-echt-niet-werken-323803/index2.html?postid=1578994#post1578994)