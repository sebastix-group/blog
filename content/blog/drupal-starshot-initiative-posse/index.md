---
layout: blog
title: Drupal’s new Starshot initiative & POSSE
date: 2024-05-31T13:13:32.76Z
description: "At DrupalCon Portland 2024 Dries announced a big and ambitious initiative for Drupal: Starshot. The goal is to build a new default experience for less technical people to build websites with Drupal. This version will extend all core features provided by  Drupal combined with all the best community-contributed modules out there."
categories: 
comments: "true"
---
![](starshot.png)

With Drupal Starshot the aim is to solve some problems that Drupal has lacked for several years:
- Drupal is very hard to use without technical knowledge
- Drupal feels like a CMS from the early ‘10
- Contributing to existing code and functionality is complicated
- There is no clear path how to use Drupal with the best-contributed modules as many profiles or distributions are giving you bad experiences (Drupal wishes to be Lego, but every block has its own set of instructions)
- It takes much effort and time to adopt new technologies into Drupal

I’m pretty sure every Drupal agency has its version of Drupal Starshot. It also applies for me, as I worked out a template with my favourite contrib modules and my set of configurations. I’m updating this framework weekly with best practices from each Drupal project I maintain. So Drupal Starshot will not fix any urgent problem for me and the agencies. 

There are three great new features coming with Starshot. These should be already there in Drupal for a long time...(at least in my opinion). Any CMS competitor has it. Not having these features is one of the reasons why Drupal is behind in UX innovation and finding the audience who need to build pages in a fly.
1. Automated updates (Drupal core + contrib modules + all its dependencies) 
2. Easy to install modules in a project browser (I would like to call this the module browser) 
3. Recipes (easy to install and configure a set of features powered by contrib modules)

Drupal is still catching up with the status quo in the CMS landscape. The real strength of Drupal is being a content development framework (CMF) providing a lot of flexibility for managing your content structure. The adoption of the PHP Symfony framework was an excellent move with Drupal 8 in 2014. But that is already 10 years ago…

I think Starshot is still too much focused on all the technical details, on the how’s and what’s of this initiative. I’m missing a strong Why Drupal Startshot incentive for the non-tech people who would like to tinker with building websites. The landscape of website builders is very competitive. Startshot is Drupal’s attempt to set a new low-code website builder in this market. I'm not sure how much you can win in this market by just being another new low-code site-builder. 

If you would like to do a technical deep-dive, here’s a [preview](https://github.com/phenaproxima/starshot-prototype) of Starshot on Github. Or watch the video below.

https://youtu.be/l5Qz078Mqjg
## POSSE 
As someone who likes to tinker with new web technologies and mixing them up, I’m would like to advocate for some features in Starshot that would distinguish it from other website builders. 

I would like to propose some features which serve the purpose of Drupal of being an open web champion (see [Open Web Manifesto](https://www.drupal.org/association/open-web-manifesto)). Most of us know the existence of the open web is under a massive pressure of all those big platforms nowadays.
Inspired by Mike Misnack piece ‘[protocols, not platforms](https://knightcolumbia.org/content/protocols-not-platforms-a-technological-approach-to-free-speech)’ I would to advocate for POSSE features within every CMS. For this we need to integrate protocols which enable distributed content publishing. There are already some neat solutions build by [Mixpost](https://mixpost.app/), [Buffer](https://buffer.com/), [Openvibe](https://openvibe.social/) and [Nootti](https://nootti.com/) which are demonstrating this. Many other integrations in bigger platforms are in development ([Ghost]([https://activitypub.ghost.org/](https://activitypub.ghost.org/) ), [Flipboard](https://about.flipboard.com/inside-flipboard/flipboard-begins-to-federate/) and [WordPress](https://wordpress.com/support/enter-the-fediverse/)) as well. 
There are many contrib modules where you are required to set up a (permissioned) API connection with third parties like the big social media platform. All this is happening on the application and platform level. With the protocols mentioned below, this interoperability of data moves to the application (client) and protocol level without the need of a third party (platform). This means the connection can be made directly (permissionless) with the protocol.
### ActivityPub 
I'm not aware of any existing Drupal platform using ActivityPub as a protocol to cross-post content into the fediverse. 
I only know the [ActivityPub](https://www.drupal.org/project/activitypub) module should enable this so your content could be federated with the fediverse. This means the content on your website is fetched by other fediverse instances so people active on those could read your content. 
### ATProto (BlueSky) 
It's still very, very, very early for ATProto to work with as a developer. BlueSky has pivoted its attention on building a protocol to building a platform. It's worth reading [this long article](https://whtwnd.com/knksm5.final-techblog.com/entries/A%20Customer%20Journey%20of%20an%20atproto%20User) whats like to build with ATProto today. 
[Openvibe](https://openvibe.social/) and [Nootti](https://nootti.com/) have proven it's possible to cross-post content to the BlueSky network from an application.
### Nostr 
With this protocol I'm tinkering a lot. I started to build a contrib module in Feb '23 and caught up with Swentel who was doing the same while he also was building a handy Nostr PHP helper library. As he moved on and hadn't the time to maintain this library, I took over this responsibility. 
Also Dries (Drupal founder and tech lead) discovered the protocol pretty early and took the time to write something about it on his blog '[Nostr, love at first sight](https://dri.es/nostr-love-at-first-sight)'.
I already worked out some proof-of-concepts how Nostr enables POSSE within Drupal.
- [Nostr integration for CCHS.social](https://sebastix.nl/blog/nostr-integration-for-cchs-social-drupal-cms/)
- [Nostr POSSE with Drupal with long-form content (NIP-23)](https://habla.news/u/sebastian@sebastix.dev/nostr-posse-drupal-long-form-content-nip-23)

Nostr is not only a neat solution for POSSE, but it can be beneficial for Drupal in many other ways. [Read more How could Drupal adopt Nostr?](https://sebastix.nl/blog/how-could-drupal-adopt-nostr/)

With all the work done for now and all the work ahead of me, this raises the question 'How can I pledge to Drupal Starshot with this knowledge?'. I'm someone who likes to pioneer and build cutting-edge solutions with the newest technologies out there. My experience is that most Drupal developers are quite conservative when it comes to looking at and adopting new technologies. At this moment I'm focused on the further development of the nostr-php package (which can be used by any PHP-powered system) and it will take some months before I will pivot my attention to building out Drupal contribs. More info on this is [available here](https://habla.news/u/sebastian@sebastix.dev/82fcc2b4).
I haven't dived into how recipes work with Drupal, but when the time is there I can imagine it would make sense to create a Drupal recipe with all the Nostr contribs + configurations as ingredients. To be continued! I've got a lot of work to do 🫡.

*I've send a submission on https://www.drupal.org/starshot#pledge with a link to this blogpost. Let's wait what feedback comes back =)*

**Resources**
- [https://indieweb.org/POSSE](https://indieweb.org/POSSE) 
- [https://activitypub.ghost.org/](https://activitypub.ghost.org/) 
- [https://www.citationneeded.news/we-need-to-talk-about-digital-ownership/](https://www.citationneeded.news/we-need-to-talk-about-digital-ownership/) 
- [https://app.digitalpublicgoods.net/a/11319](https://app.digitalpublicgoods.net/a/11319) 
- [https://www.theverge.com/2023/4/20/23689570/activitypub-protocol-standard-social-network](https://www.theverge.com/2023/4/20/23689570/activitypub-protocol-standard-social-network)
- [https://www.hojtsy.hu/blog/2024-may-11/15-reasons-i-am-excited-about-drupals-new-starshot-initiative](https://www.hojtsy.hu/blog/2024-may-11/15-reasons-i-am-excited-about-drupals-new-starshot-initiative) 
- [https://mglaman.dev/blog/starshot-recipe-cook-ambitious-drupal-applications](https://mglaman.dev/blog/starshot-recipe-cook-ambitious-drupal-applications) 
- [https://www.drupaleasy.com/blogs/ultimike/2024/05/ruminations-drupal-starshot](https://www.drupaleasy.com/blogs/ultimike/2024/05/ruminations-drupal-starshot) 
- [https://overcast.fm/+BRvaND-uI](https://overcast.fm/+BRvaND-uI) 