---
layout: blog
title: Hoe (a)sociaal is BeReal?
date: 2022-08-25T13:23:29.736Z
description: Er is een nieuwe social media hype. Tenminste, als ik de vele
  nieuwsberichten mag geloven rondom de app BeReal. Deze app belooft dat je de
  ongefilterde realiteit ziet van jouw échte vrienden. Je kunt er namelijk
  alleen authentieke berichten posten.
categories: social media bereal bigtech
comments: "true"
---
BeReal legt zijn app als volgt uit:

> Your Friends for Real.
>
> Everyday at a different time, everyone is notified simultaneously to capture and share a Photo in 2 Minutes. A new and unique way to discover who your friends really are in their daily life.

[BeReal zit momenteel in een exponentiele groeifase met het aantal nieuwe gebruikers.](https://www.marketingfacts.nl/berichten/gebruik-social-app-bereal-stijgt-stevig-door-onder-jongeren/)

## **⚠️ Je hebt slechts 2 minuten om te handelen! FOMO!? ⚠️**

In het boek [Het Alles van Dave Eggers](https://www.debezigebij.nl/boek/het-alles/) komen er vele verschillende applicaties aan bod ([lees een review hier](https://www.seb247.com/books/every.html)). BeReal doet me denken aan een van die applicaties die de hoofdpersoon Delaney eraan herinnert om meerdere keren per dag een Popeye pose te posten naar haar volgers. Als ze dit niet doet, wordt ze geshamed. Hoe meer shames je hebt des te lager je sociale score wordt. BeReal herinnert je slechts een keer per dag om iets te posten.

![](images-3.fill.size_2000x422.v1657554038.jpg)

De unieke feature van BeReal speelt direct in op de angst om iets te missen als je niet direct handelt. BeReal veronderstelt dat je jouw echte vrienden zult leren kennen; alleen zij zullen iets delen wanneer het time-window van twee minuten van start gaat om je post te delen. Doe je niet mee? Dan zou je wel eens commentaar kunnen krijgen van je vrienden. [Is dit waar echte vriendschap om draait?](https://www.trouw.nl/cultuur-media/de-ironie-van-bereal-hoe-real-is-het-sociale-medium-van-het-moment-nou-echt~b02abf74/) Wat als je nu een keer vroeg naar bed gaat en BeReal pas om 22h54 komt met het dagelijke BeReal momentje?

## **Hoe privacyvriendelijk is de app volgens het** **Exodus privacy report?**

De app bevat vijf trackers (die precies volgen wat jij doet op de app) en vraagt om 38 permissies van jou en je telefoon. *Note: gecheckt op 18 agustus 2022, in de toekomst kan dit namelijk veranderen bij elke nieuwe update van de app.* Dit rapport laat zien dat BeReal het net zo goed (of slecht) doet als [Instagram](https://reports.exodus-privacy.eu.org/en/reports/com.instagram.android/latest/) op het gebied van privacy: die app bevat twee trackers en vraagt om 40 permissies. Het volledige rapport vind je [hier](https://reports.exodus-privacy.eu.org/en/reports/com.bereal.ft/latest/).

*Reminder voor mijzelf: over een half jaar op 1 maart 2023 checken welke veranderingen er zijn doorgevoerd betreffende de trackers en permissies.*

![](schermafbeelding-2022-09-01-om-08.44.43.png)

## **Hoe gaat BeReal geld verdienen?**

Op dit moment heeft BeReal nog geen business model waarmee ze geld verdienen. Middels investeringen van venture capitalists wordt er nu alleen geld uitgegeven. Uiteindelijk willen deze venture capitalists een rendement terugzien van hun investering, zeker als blijkt dat BeReal enkele tientallen miljoenen gebruikers heeft in verschillende landen. Zou BeReal dan bijvoorbeeld ook advertenties gaan verkopen om deze te tonen in de app? Of kiest men voor een freemium model waarmee je extra functies in de app krijgt als je een klein maandelijks bedrag betaalt? Het zijn slechts enkele opties waar je aan kunt denken.

Je wilt er dus eigenlijk achter komen wat de intenties zijn van BeReal. Daarvoor moeten we op zoek naar de visie van de oprichter en maker van de app. Dit is de fransman Alexis Barreyat. Na een uurtje research moet ik tot mijn verbazing concluderen dat er vrijwel niets te vinden is over hem als persoon. Niks over hoe hij als ondernemer in het leven staat. Het enige wat ik kan vinden is dat hij 2,5 jaar heeft gewerkt bij GoPro en dat hij al sinds 2020 werkt aan BeReal. In [deze LinkedIn post](https://www.linkedin.com/posts/alexisbarreyat_nous-sommes-super-contents-denfin-pouvoir-activity-6633332857970532352-M038/) van twee jaar geleden zegt Alexis dat hij klaar is met alle pulp op social media en daarom besloot om zijn eigen social te maken: 

> After being tired and annoyed with all the bullshit on social media, I decided to launch my own.

Ik had eerlijk gezegd wel verwacht een aantal interviews te kunnen vinden, maar helaas. Dit maakt me alleen maar meer wantrouwend over de intenties van Alexis met BeReal. Het is best ironisch dat je qua authenticiteit niets kunt vinden over de maker van de app, maar dat de app  met nadruk zichzelf profileert als voorvechter van échte authenticiteit. 

Op [Wikipedia](https://en.wikipedia.org/wiki/BeReal) lezen we inmiddels dat er flinke investeringen zijn gedaan in BeReal:

> In April 2022, BeReal received a $30 million funding round led by Andreessen Horowitz and Accel. In May 2022, BeReal secured $85 million in a funding round led by Yuri Milner's DST Global, increasing its valuation to about $600 million. 

Of BeReal *here to stay* is, zal mede afhangen van het business model waarmee ze zullen moeten overleven in het digitaal kapitalistisch systeem. 

Persoonlijk denk ik dat op termijn BeReal zal worden overgenomen door een grote tech partij zodra de exponentionele groei van aantal gebruikers én gebruik afneemt. Dan schat ik de waardering van de app op een paar miljard dollar. 

**Update 23-08: Instagram is al bezig met een copycat**\
Op [Engadget](https://www.engadget.com/instagram-bereal-candid-challenges-232117658.html) is te lezen dat Instagram werkt aan een functie die precies lijkt op die van BeReal.

**Update 29 augustus: SnapChat kopieert dubbele camera functie van BeReal**\
Te lezen op [Tweakers](https://tweakers.net/nieuws/200440/snapchat-functie-laat-gebruikers-snaps-maken-met-voor-en-achtercamera-tegelijk.html). Nieuw is dit niet, want deze functie zat al in de [Frontback](https://www.androidplanet.nl/apps/frontback-android/) app (2014). 

## **Asociaal**

Ik vind het concept en dus ook de app BeReal alles behalve sociaal. Sociaal betekent voor mij dat je aandacht voor elkaar hebt, in plaats van dat alles alleen om jouzelf draait of om apps. Stel je eens voor dat iedereen BeReal gebruikt en je zit heerlijk op een druk terras met jouw vrienden. Plots verschijnt de melding van BeReal dat je nu twee minuten de tijd hebt om een post te maken. Dit gebeurt uiteraard niet alleen bij jou, maar ook bij alle anderen op het terras. Een melding die als interruptie alle gesprekken afleidt naar de app. Je hebt even geen aandacht meer voor de ander, maar voor je telefoon. Is dat wat we sociaal en social media willen noemen?

*Funny thing*. Deze retweet van de appmaker Alexis toont precies wat ik bedoel:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">when the bereal notification comes through <a href="https://t.co/Zq40T3N70y">pic.twitter.com/Zq40T3N70y</a></p>&mdash; stan⛷ (@stxn_h) <a href="https://twitter.com/stxn_h/status/1511797193631539208?ref_src=twsrc%5Etfw">April 6, 2022</a></blockquote> 

*Andere bronnen*  

* [](https://mashable.com/article/bereal-app-authenticity)<https://mashable.com/article/bereal-app-authenticity>
* [](https://www.dazeddigital.com/science-tech/article/55959/1/bereal-app-social-media-dystopian-nightmare-gen-z-authentic-instagram)<https://www.dazeddigital.com/science-tech/article/55959/1/bereal-app-social-media-dystopian-nightmare-gen-z-authentic-instagram>
* <https://netwerkmediawijsheid.nl/bereal/>
* <https://www.livewall.nl/blogs/BeReal-inzetten-voor-marketing> 
* [https://www.businessinsider.com/review-bereal-photo-sharing-app-compared-to-instagram-snapchat-facebook-2022-4](<* https://www.businessinsider.com/review-bereal-photo-sharing-app-compared-to-instagram-snapchat-facebook-2022-4>)