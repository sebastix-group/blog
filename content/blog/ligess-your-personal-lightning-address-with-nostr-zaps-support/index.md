---
layout: blog
title: Receive nostr zaps with your lightning node with Ligess (self-hosted, personal lightning address)
date: 2023-03-23T12:00:00.266Z
description: From the first moment lightning zaps were introduced in Nostr (NIP-57) I was looking for a way how to receive these sats from zaps to my own lightning node. I discovered Ligess and stucked to that when I noticed Mutatrum was already building a solution for receiving zaps.
categories: nostr zaps ligess lightning bitcoin
comments: "true"
---

At this moment, most zaps over Nostr are received by custodial wallets with lightning addresses like Wallet of Satoshi and Alby ([source](https://stacker.news/items/154820)).

From a more self-sovereign point of view you would like to have the option to use a self-custodial wallet with a lightning address which accept zaps. 

## Lightning zaps with Ligess

[Mutatrum](https://github.com/mutatrum) did some nice work with integrating lightning zaps into Ligess, a self-hosted personal lightning address application. Ligess runs as a nodejs application. In my opinion this tool acts like a lightning extension / micro application. 

- The Ligess repository: https://github.com/dolu89/ligess

- The [merged pull request](https://github.com/Dolu89/ligess/pull/9) with the changes by Muratum for integrating NIP-57 Lightning Zaps

- NIP-57: [https://github.com/nostr-protocol/nips/blob/master/57.md](https://github.com/nostr-protocol/nips/blob/master/57.md)

## My setup with Ligess

I've used the documentation on https://theroadtonode.com for installing, configuring and maintaining the software for running my own lightning node. This setup is running for more than two years.

I've written a documentation article as a contribution for The Road to Node how you can install Ligess: [https://trtn.sebastix.dev/lightning-extensions/ligess](https://trtn.sebastix.dev/lightning-extensions/ligess).

In summary, these are the pieces running on my node device:

### Nginx reverse proxy configuration for my domain lnd.sebastix.com

```nginx
upstream ligess {
        server 127.0.0.1:4080;
    }

server {
    server_name lnd.sebastix.com;

    listen [::]:443 ssl http2;
    listen 443 ssl http2;
    ssl_certificate /etc/letsencrypt/live/lnd.sebastix.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/lnd.sebastix.com/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
    ssl_session_cache shared:ligess:1m;

    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Forwarded-Host $host;

    location /.well-known/lnurlp/ {
        if ($request_method = 'GET') {
            add_header 'Access-Control-Allow-Origin' '*';
        }
        proxy_pass http://ligess;
    }
    location /ln {
        proxy_pass http://ligess;           
    }
}
server {
    if ($host = lnd.sebastix.com) {
        return 301 https://$host$request_uri;
    } 
    server_name lnd.sebastix.com;     
    listen 80;
    listen [::]:80;
    return 404;
}
```

### Add Nostr private key as environment variable in the Ligess `.env` file

```env
LIGESS_NOSTR_ZAPPER_PRIVATE_KEY=<a_hex_encoded_private_key>
```
Pro tip from Mutatrum: you can use a seperate key-pair here just for signing the zap notes.  
Use [Rana](https://github.com/grunch/rana) or [noscl](https://github.com/fiatjaf/noscl#generate-a-key) if you would like to generate a key-pair on your machine.

### Systemd configuration for running Ligess as a system service

```systemd
[Unit]
Description=Ligess
Requires=lnd.service
After=lnd.service
[Service]
User=lnd
WorkingDirectory=/home/lnd/ligess
ExecStart=yarn dev
Restart=always
TimeoutSec=120
RestartSec=30
[Install]
WantedBy=multi-user.target
```

With this setup you can send me zaps over Nostr. The sats of those zaps will be send to my self-hosted lightning wallet connected to my lightning node. 

- My lightning address: [sebastian@lnd.sebastix.com](sebastian@lnd.sebastix.com)

- The lnurl pay endpoint provided by Ligess: https://lnd.sebastix.com/.well-known/lnurlp/sebastian 

## Temporary solution

The NIP-57 is probably temporary and will be replaced or appended with other solutions in the future. You can read a nice discussion why [here](https://github.com/nostr-protocol/nips/pull/224#issuecomment-1418302436).

**Please remember Nostr is in a very, very early phase of development and adoption.**

I expect things will change a lot when [BOLT12](https://bolt12.org/) is finished ([source](https://nitter.nl/LN_Capital/status/1627338727247814657)). The topic around lightning powered micropayments over Nostr will evolve fast.