---
title: "Concept ontwikkeling scripting public place"
date: "2008-09-23T21:45:49.000Z"
description: "Ik ben begonnen in de publieke ruimte door te kijken naar persoonlijke irritaties. Al snel had ik het beeld van een wink..."
categories: [internet]
comments: true
---

Ik ben begonnen in de publieke ruimte door te kijken naar persoonlijke irritaties. Al snel had ik het beeld van een winkelstraat in gedachte samen met nog een heleboel andere mensen. Iedereen kent het wel dat je iemand tegemoet loopt en een kant moet kiezen langs welke zijde je hem of haar passeert. Links of rechts? Het gebeurt nogal eens dat het toch voorkomt dat je elkaar rakelijks passeert of gewoon een fysieke botsing hebt. Is daar nou niks voor te verzinnen?

Natuurlijk wel. Mijn radicale oplossing is door gewoon de winkelstraat opnieuw te construeren met een regelgeving net zoals we die kennen op de snelweg. Oversteekplaatsen, wachtplaatsen, een weg langs de winkels waar men 1 kant op loopt en een grote weg in het midden voor het publiek welke niet direct in een winkel aan de kant hoeft te zijn. Schematisch snel geschetst zou er dat ongeveer zo uit moeten zien:

  
[![](../../../images/winkelstraat.jpg "Winkelstraat")](../../../images/winkelstraat.jpg)

  
Vanuit dit idee ben ik verder gaan denken; wat is hier nou eigenlijk de achterliggende gedachte ervan?

  
Waarom houden wij hier in Nederlands allemaal rechts aan en in een paar andere landen links?

  
Kan ik van ongeschreven regels in de publieke ruimte, geschreven regels maken?

  
Hoe zullen mensen zo'n gereguleerd systeem gebruiken?

  
  
\---  
 Eigenlijk is de publieke ruimte gewoon een gebruiksvoorwerp. Wanneer en waarom (om even aan te sluiten bij Rob's gedachtegang) zijn we in de publieke ruimte? Vaak alleen maar om ons te verplaatsen van punt A naar punt B. In de publieke ruimte zijn we maar tijdelijk. Op punt A en B zijn we vaker. Dit zijn voor ons ook vaak vertrouwde, veilige omgevingen. Dit in tegenstelling tot de publieke ruimte. Niet overal in de publieke ruimte is het zo veilig en vertrouwd om je daar doorheen te verplaatsen. Als we in de auto zitten, zitten we eigenlijk een stukje in een veilige, betrouwbare omgeving welke zich weer in de publieke ruimte bevindt. Een ruimte waar heel veel regels gelden en je ook weet waar je aan toe bent.  
 Hoe zit dat dan in drukke openbare ruimtes zonder auto? Daar zie ik zelf vaak nog best veel chaos. Een systeem met veel open gaten en waar soms geen duidelijke regels aanwezig zijn. Net zoals het met z'n allen winkelen/verplaatsen/staan in de winkelstraat. Of op het perron wachtend op de trein. Wachtende mensen zijn hierbij een interessant gegeven. Wachten is niet leuk, het kost tijd terwijl je het liefst datgene al zou waar je op aan het wachten bent. Welke regels gelden bij het wachten? Vaak moet je ook op je beurt wachten. Hoe eerder je ergens bent, hoe eerder je ook aan de beurt bent bij iets waar je op aan het wachten bent. Bij de pinautomaat is dit soms niet vanzelfsprekend wanneer er meer dan één pinautomaat is. Het zou wel eens kunnen dat voor elke pinautomaat mensen staan te wachten. Hoe irritant is het dat iemand in de andere rij minder lang heeft moeten wachten om te pinnen dan jij? Het zou logischer zijn dat er één wachtrij zou zijn voor alle automaten. Dit kan natuurlijk ook net zo goed gelden voor het ergens instappen (trein, bus, tram), afrekenen bij een automaat/kassa.

Goed, even resumerend naar mijn concept. Wat wil ik nu?

  
**Ik wil dat mensen zich zekerder, veiliger, vertrouwder voelen door nieuwe regels toe te voegen in de publieke ruimte (of al bestaande regels opnieuw te ontwerpen).**

  
Er moet in bepaalde publieke ruimtes een betere flow ontstaan van handelingen die mensen aan het verrichten zijn. De twee meest voorkomende (en veel meer zijn er ook niet) handelingen in de publieke ruimte zijn eigenlijk toch wel verplaatsen en wachten. Ik wil me gaan concentreren rondom nieuwe regels rondom deze handelingen en hoe zou ik er voor zorgen dat mensen zich ook daadwerkelijk zekerder en vertrouwder kunnen gaan voelen op die plek?
