---
title: "Childmap"
date: "2008-01-16T16:44:22.000Z"
description: "
Dit kwartaal hebben we ook les gehad in 'infographics' van onze studieleider Illustratie. Dat betekende dus dat we ook ..."
categories: [internet]
comments: true
---

![](http://www.interactieontwerpen.nl/_sebastian/previewImages/childmap.jpg)  
Dit kwartaal hebben we ook les gehad in '*infographics*' van onze studieleider Illustratie. Dat betekende dus dat we ook aan de slag moesten met papier en potlood. De hoofdopdracht deze periode was om een **kaart** te maken van je omgeving toen je **10 jaar** was.  
  
  
Voor mij is het ongeveer 11 jaar geleden dat ik nog een leeftijd had van 10 jaar. Ik wist nog goed wat ik op die leeftijd deed; buiten spelen! De basisschool was niets voor mij, veel te saai. Ik heb alle belangrijkste lokaties op de kaart gezet. Deze lokaties heb ik getekent en daarna verder bewerkt op de computer. Een nieuwe aanpak voor mij persoonlijk die ik nog niet eerder had toegepast. Hieronder kun je het resultaat bekijken.  
  
Zwart/wit, met enkele grafische aanpassingen  
[![](http://www.interactieontwerpen.nl/_sebastian/mapping/map_1.jpg)](http://www.interactieontwerpen.nl/_sebastian/mapping/map_1.jpg)  
  
In kleur, door de computer  
[![](http://www.interactieontwerpen.nl/_sebastian/mapping/map_2.jpg)](http://www.interactieontwerpen.nl/_sebastian/mapping/map_2.jpg)  
  
De laatste versie heb ik laten zien als beoordeling, maar ik ben duidelijk te ver gegaan in de vormen. De contrasten zijn verdwenen en het is ineens te druk geworden door de kleuren en belijning. Ik zou een vorm moeten vinden die tussen deze twee versies in zou moeten zitten.
