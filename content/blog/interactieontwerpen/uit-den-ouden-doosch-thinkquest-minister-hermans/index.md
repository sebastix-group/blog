---
title: "Uit den ouden doosch: ThinkQuest & Minister Hermans"
date: "2010-02-23T16:18:59.000Z"
description: "Vandaag kwam ik twee oude krantenartikelen uit BN/deStem tegen met mij te zien op de foto. Zie hieronder, ik heb ze maar..."
categories: [internet]
comments: true
---

Vandaag kwam ik twee oude krantenartikelen uit BN/deStem tegen met mij te zien op de foto. Zie hieronder, ik heb ze maar even snel gedigitaliseerd voordat de knipsels uit elkaar gaan vallen over tien jaar  
  
[![ThinkQuest derde prijs](http://farm5.static.flickr.com/4055/4381477327_9e578f2bfe.jpg)](http://www.flickr.com/photos/sebastix/4381477327/ "ThinkQuest derde prijs")  
  
[![Minister hHermans bij Sebastian Hagens](http://farm3.static.flickr.com/2515/4381477653_c4ee27af54.jpg)](http://www.flickr.com/photos/sebastix/4381477653/ "Minister hHermans bij Sebastian Hagens")
