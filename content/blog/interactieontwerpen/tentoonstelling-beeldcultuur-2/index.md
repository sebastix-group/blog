---
title: "Tentoonstelling beeldcultuur"
date: "2007-11-12T18:41:34.000Z"
description: "

In a group of four students we made an exhibition about the visual culture of these days. The basic premise was that i..."
categories: [internet]
comments: true
---

![tentoonstelling](http://www.interactieontwerpen.nl/_sebastian/previewImages/preview_beeldcultuur.jpg)  
  
In a group of four students we made an exhibition about the visual culture of these days. The basic premise was that images are everywhere around us and that they want something from us. We exaggerated this finding into a form whichs tells the same to the spectator.  
  
View the exhibition demo movie:  
<ins datetime="2007-11-12T16:43:28+00:00">http://interactieontwerpen.nl/\_sebastian/beeldCultuur/tentoonstelling.mov</ins> (70MB)
