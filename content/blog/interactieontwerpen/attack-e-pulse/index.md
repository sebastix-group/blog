---
title: "Attack @ E-Pulse"
date: "2009-10-19T19:39:36.000Z"
description: "Mijn interactieve installatie ATTACK staat momenteel in de expositie van het E-Pulse festival die van 15 t/m 25 oktober ..."
categories: [internet]
comments: true
---

Mijn interactieve installatie ATTACK staat momenteel in de expositie van het E-Pulse festival die van 15 t/m 25 oktober duurt en te bezoeken is in het Electron in Breda. Meer info: <http://www.e-pulsefestival.nl>  
  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="344" width="425"><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="src" value="http://www.youtube.com/v/N8zsv4cbCP8&hl=nl&fs=1&"></param><embed allowfullscreen="true" allowscriptaccess="always" height="344" src="http://www.youtube.com/v/N8zsv4cbCP8&hl=nl&fs=1&" type="application/x-shockwave-flash" width="425"></embed></object>  
  
  
  
 Sinds vorige versie heb ik weer een aantal upgrades toegevoegd:  
  
\- nieuwe computer  
\- nieuwe DV camera  
\- nieuw beamerscherm  
  
 Dit maakt de gehele beleving van de installatie weer een stuk intenser! Hier enkele foto's:  
  
[![IMG_0342](http://farm3.static.flickr.com/2788/4026812256_d27565cd1b_m.jpg)](http://www.flickr.com/photos/sebastix/4026812256/ "IMG_0342")[![IMG_0345](http://farm3.static.flickr.com/2422/4026813528_b4fa479b1b_m.jpg)](http://www.flickr.com/photos/sebastix/4026813528/ "IMG_0345")[![IMG_0344](http://farm4.static.flickr.com/3001/4026813192_f2b199c790_m.jpg)](http://www.flickr.com/photos/sebastix/4026813192/ "IMG_0344")[![IMG_0343](http://farm3.static.flickr.com/2426/4026060229_ecedf7b3d1_m.jpg)](http://www.flickr.com/photos/sebastix/4026060229/ "IMG_0343")
