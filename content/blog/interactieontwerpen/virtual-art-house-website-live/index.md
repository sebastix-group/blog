---
title: "Virtual Art-House website LIVE"
date: "2009-06-04T23:16:02.000Z"
description: "

http://www.virtualarthouse.nl

De website van mijn stageproject is eindelijk on-line!..."
categories: [internet]
comments: true
---

![](../../../images/vah-logo.jpg "Virtual Art-House")  
  
<http://www.virtualarthouse.nl>  
  
 De website van mijn stageproject is eindelijk on-line!
