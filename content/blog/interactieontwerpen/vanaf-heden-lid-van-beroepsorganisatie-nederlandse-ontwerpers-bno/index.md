---
title: "Vanaf heden lid van Beroepsorganisatie Nederlandse Ontwerpers (BNO)"
date: "2010-01-12T13:05:56.000Z"
description: "Vorig jaar heb ik me aangemeld bij de BNO (Beroepsorganisatie Nederlandse Ontwerpers) en ben vanaf deze maand ook opgeno..."
categories: [internet]
comments: true
---

Vorig jaar heb ik me aangemeld bij de BNO (Beroepsorganisatie Nederlandse Ontwerpers) en ben vanaf deze maand ook opgenomen binnen deze organisatie als lid.  
 Groot voordeel waar ik direct gebruik van maken is het verwijzen naar de algemene voorwaarden die door de BNO is opgesteld. Zie hier: [http://leden.bno.nl/nl/service/AVW\_2005\_BNO1.pdf](http://leden.bno.nl/nl/service/AVW_2005_BNO1.pdf)  
  
![](http://files.droplr.com/files/41343510/cAxe0.FirefoxSchermSnapz001.png "BNO")  
  
 Momenteel beschik ik nog over een studentlidmaatschap (nog met beperkte rechten), maar dit zal worden omgezet naar een starterslidmaatschap zodra ik ben afgestudeerd komend jaar. BNO ondersteunt verder alle ontwerpers uit verschillende disciplines op het gebied van financiën, juridische zaken, verzekeringen enzovoorts. Meer informatie kun je vinden op <http://www.bno.nl>.
