---
title: "Arduino"
date: "2008-01-30T21:56:25.000Z"
description: "
Een eerste kennismaking met de Arduino Diecimila, eerst kleine computer waar je kleine programma's naar kunt uploaden e..."
categories: [internet]
comments: true
---

![](http://www.interactieontwerpen.nl/_sebastian/previewImages/preOffscreen.jpg)  
Een eerste kennismaking met de Arduino Diecimila, eerst kleine computer waar je kleine programma's naar kunt uploaden en vervolgens analoge en digitale input kanalen kunt aansturen voor in- en output signalen (sensoren, led'jes, motoren etc).  
  
  
  
\[flash http://www.youtube.com/watch?v=1-OTlukA0cU\]  
\[flash http://www.youtube.com/watch?v=iyXoOWeXQe4\]
