---
title: "Een virtuele wereld die de controle overneemt"
date: "2009-04-17T17:38:27.000Z"
description: "Amazing 3D immersion technology from IDEO Labs on Vimeo...."
categories: [internet]
comments: true
---

<object height="281" width="500"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=4177769&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="281" src="http://vimeo.com/moogaloop.swf?clip_id=4177769&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1" type="application/x-shockwave-flash" width="500"></embed></object>  
[Amazing 3D immersion technology](http://vimeo.com/4177769) from [IDEO Labs](http://vimeo.com/user668273) on [Vimeo](http://vimeo.com).
