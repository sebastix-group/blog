---
title: "Audiomap StJoost academie"
date: "2007-11-12T19:45:56.000Z"
description: "

1 uur lang moest ik allerlei data verzamelen van de academie met mijn oren. Met deze data was het de bedoeling een 'ma..."
categories: [internet]
comments: true
---

![](http://interactieontwerpen.nl/_sebastian/previewImages/akademiemap.jpg)  
  
1 uur lang moest ik allerlei data verzamelen van de academie met mijn oren. Met deze data was het de bedoeling een 'map' te maken. Een van de belangrijkste kenmerken van het academie gebouw op het gebied van geluid is toch wel de **echo** die je bijna overal wel hoort. Per lokatie vind je een apart 'venster' op de map. Op die locaties heb ik een opname gedaan van 20 seconde en naderhand teruggeluisterd en het audiospoor bekeken. Dit spoor heb ik weer terug gezet in het venster. De rode balk achter het spoor is de hoeveelheid echo op die locatie. Hoe meer rood, hoe meer echo. Op de tweede map heb ik die echo ook proberen te laten zien door het venster een herhaling op elkaar te geven en wat diepte door transparantie van de vensters.  
  
  
Hieronder vind je eerst de eerste poging.  
[![](http://interactieontwerpen.nl/_sebastian/mapping/akademiemap/akademiemap1.jpg)  
  
Tweede versie van de map.  ](http://interactieontwerpen.nl/_sebastian/mapping/akademiemap/akademiemap1.jpg)  
[![](http://interactieontwerpen.nl/_sebastian/mapping/akademiemap/akademiemap2.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/akademiemap/akademiemap2.jpg)
