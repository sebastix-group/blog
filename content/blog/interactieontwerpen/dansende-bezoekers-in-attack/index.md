---
title: "Dansende bezoekers in ATTACK"
date: "2009-11-08T23:54:16.000Z"
description: "Het duurt nog een tijdje voordat ik een goed samenvattend filmpje heb die inzichtelijk maakt wat interactieve installati..."
categories: [internet]
comments: true
---

Het duurt nog een tijdje voordat ik een goed samenvattend filmpje heb die inzichtelijk maakt wat interactieve installatie doet. Toch wil ik jullie alvast niet van een leuk filmpje onthouden die ik tijdens afgelopen E-Pulse festival heb opgenomen.  
  
<object height="400" width="500"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=7330655&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="400" src="http://vimeo.com/moogaloop.swf?clip_id=7330655&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1" type="application/x-shockwave-flash" width="500"></embed></object>

[Dancing Duo @ ATTACK](http://vimeo.com/7330655) from [Sebastian Hagens](http://vimeo.com/sebastix) on [Vimeo](http://vimeo.com).
