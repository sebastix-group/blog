---
title: "Feedback scriptie"
date: "2010-03-16T14:57:39.000Z"
description: "Half februari (ongeveer twee weken te laat) heb ik mijn scriptie 'een digitale levenslijn' ingeleverd. Vandaag was het d..."
categories: [internet]
comments: true
---

Half februari (ongeveer twee weken te laat) heb ik mijn scriptie 'een digitale levenslijn' ingeleverd. Vandaag was het de dag voor de nodige feedback op dit document. Hieronder enkele citaten die reflecteren:

> Verder is de opbouw zeer degelijke en goed doordacht, met goed gekozen deelonderwerpen. Alle aspecten die een rol spelen komen aan bod: de scriptie lijkt compleet.  
>   
>  Heel interessant: vergelijking tussen omgang met historische artefacten: de context waarin beide ontstaan blijkt belangrijker dat het artefact zelf. Dit besef wordt doorgetrokken naar digitale artefacten.  
>   
>  De scriptie is een echte eye-opener. Levendig geschreven met heldere voorbeelden. Een goede eigen visie. Soms wordt de scriptie persoonlijk gemaakt. Dat bevordert het inlevingsvermogen.  
>   
>  Er is een goed besef van de sociale context waarin de problematiek speelt.  
>   
>  Ontdekkingen zijn een basis voor een web 3.0 toepassing! Ben benieuwd naar het vervolg in het afstudeerproject!

  
 Uiteraard zijn er ook verbeter punten. De scriptie heb ik nog niet publiekelijk gepubliceerd omdat ook ik nog niet geheel tevreden ben. Het geheel kan nog een stuk attractiever vorm worden gegeven en dat is iets waar ik niet goed in ben. Daarom ben ik op zoek naar hulp van personen die me hierbij kunnen helpen, het is ook de bedoeling dat ik deze -nog grotendeels te ontwerpen- stijl doortrek naar mijn afstudeerproject. Wie kan mij helpen?
