---
title: "Attack - eindpresentatie minor"
date: "2008-04-03T17:09:13.000Z"
description: "

De bezoeker wordt uitgedaagd in een tweede wereld met een projectie van de reële wereld. In deze tweede, virtuele w..."
categories: [internet]
comments: true
---

![](http://interactieontwerpen.nl/_sebastian/previewImages/minorAttack.jpg)  
  
 De bezoeker wordt uitgedaagd in een tweede wereld met een projectie van de reële wereld. In deze tweede, virtuele wereld kijkt de virtuele levensvorm waar zich de reële levensvorm bevindt en begint deze aan te vallen, irriteren. Deze virtuele levensvorm kan worden geassocieerd met een bijvoorbeeld zwerm wespen die zich voelt aangevallen binnen hun leefomgeving.  
  
  
  
 De tweede levensvorm waarmee de bezoeker mee geconfronteerd wordt is afgeleid van een bestaande levensvorm in de natuur. In de natuur bestaan er namelijk ook zwermen die samen dingen voor elkaar krijgen, waarin een individu zou falen. Een zwerm heeft vaak meer capaciteiten dan een levensvorm binnen de zwerm.  
 Het gedrag van mijn virtuele levensvorm lijkt er veel op die van een natuurlijke zwerm. Als we terugkijken naar de allereerste digitale zwerm dan gelden er namelijk drie regels binnen een zwerm; seperation, alignment en cohesion.  
 Seperation houdt in dat een partikel (onderdeeltje van de zwerm) zijn bewegingsrichting aan moet passen als het risico dreigt op een botsing. Alignment vertelt hoe het partikel zijn richting aan moet passen aan die van zijn buren.  
 De laatste ‘cohesion’ zorgt ervoor dat er geen partikel los raakt van de zwerm.  
  
 Bij mijn virtuele zwerm is het wel mogelijk dat er partikels losraken van de zwerm. Er is sprake van een bepaalde aantrekkingskracht tussen de partikels die juist daardoor een zwerm blijven. De snelheid waarmee de zwerm zich beweegt is afhankelijk van de hoeveelheid bezoeker (hoeveelheid verstoring in de neutrale leefomgeving van de zwerm). De zwerm leeft namelijk in rusttoestand in een volledig witte omgeving. De zwerm zal reageren op alles wat niet wit is in zijn omgeving. Zo ontstaat er een min of meer ongecontroleerd gedrag tussen de zwerm en bezoeker. Echter zal de zwerm zich moeten gaan opsplitsen tussen meerdere verstoringen in zijn leefomgeving als er meerdere bezoekers aanwezig zijn.  
  
*De installatie is een groot experiment om te zien hoe een digitale levensvorm zich kan gedragen op een reële levensvorm en andersom. Het belangrijkste is het onderzoek hoe bezoekers zich gedragen en hoe ze zich voelen bij het ervaren van de installatie.*   
  
**Demo video van installatie**  
<object height="344" width="425"><param name="movie" value="http://www.youtube.com/v/T3ziJ_N-kc4&hl=en&fs=1"></param><param name="allowFullScreen" value="true"></param><embed allowfullscreen="true" height="344" src="http://www.youtube.com/v/T3ziJ_N-kc4&hl=en&fs=1" type="application/x-shockwave-flash" width="425"></embed></object>  
  
![](http://interactieontwerpen.nl/_sebastian/previewImages/technischetekening.jpg)  
  
**Benodigdheden:**  
 3x witte schotten  
 1x PowerBook G4  
 1x beamer  
 1x lange VGA kabel  
 1x lange Firewire kabel voor DV-camera  
 1x haspel  
 1x DV camera  
 1x statief  
 1x DVI &gt; VGA verloopstekker  
 1x luidsprekers  
  
**Conclusie / persoonlijke evaluatie**  
  
 Als student Interactie Ontwerpen was deze minor ook een enorme uitdaging. Zelf ben ik me nog maar half bewust wat voor mogelijkheden er tegenwoordig tot je beschikking staan als het gaat om techniek en de interactie mogelijkheden tussen mens en machine. Ik keek daarom ook erg uit naar de minor om aan de slag te gaan met voor mij onbekende sensoren en eens OffScreen te gaan werken.  
 Toch is het niet helemaal van het scherm af gegaan in mijn geval. Ik ben vanaf het begin af aan al aan de slag gegaan met processing. Eerst heb ik gekeken hoe dit programma in elkaar zit en later heb ik de koppeling gemaakt naar externe sensoren met behulp van het Arduino bordje. De kracht van sensoren gekoppeld aan processing om deze te programmeren is sterk. Ik heb meer inzichten gekregen in verschillende parameters die je kunt afvangen bij je gebruiker. Dit hoeft namelijk lang niet altijd een input te zijn die via een scherm binnenkomt via de computer, maar kan net zo goed een externe sensor zijn gekoppeld aan je Arduino.  
 Verschillende ideeën zijn mijn hoofd gepasseerd en ik vond het moeilijk om echt sterk in een concept te gaan staan wat ik wilde gaan doen. Het meest fascinerende vond ik altijd nog de gewone camera en hoe je bepaalde dingen kunt afvangen in je beeld. Vanuit dit stukje techniek ben ik een concept gaan maken. Moeizaam ging dat wel.  
 Helemaal Offscreen ben ik niet gegaan in deze minor, want mijn uiteindelijke presentatie vind toch plaats met een beamer. Met de camera kan ik veel dingen tracken binnen het gezichtsveld en op deze dingen heb ik vervolgens een virtuele levensvorm losgelaten. Samen met ook geluid heb ik geprobeerd de toeschouwer een onaangenaam gevoel mee te geven dat deze wordt aangevallen in een virtuele ruimte. Ik probeer de kloof tussen de reële en virtuele wereld hiermee te verkleinen en bij de toeschouwer ervaring / herinnering op te wekken dat hij of zij aangevallen wordt en zich moet verdedigen.  
 Helemaal doen slagen in dit doel lukt niet, want men weet dat het een opgezette omgeving is en jouw fysieke aanwezigheid nog altijd in de reële wereld is en niet in de virtuele wereld. Toch doet het wel iets met je als je het ervaart.  
 De minor heeft me veel nieuwe inzichten gegeven in de mogelijkheden op het gebied van verschillende manieren interactie tussen mens en machine. Zo kan ik in de toekomst vanuit een breder perspectief gaan denken om een mogelijke interface te maken om bepaalde data tot de gebruiker te laten komen bijvoorbeeld. Dit hoeft namelijk lang niet altijd met een toetsenbord en muis
