---
title: "2033"
date: "2007-09-24T18:12:35.000Z"
description: "We kunnen niet zonder slaven. Toch proberen we onze eigen ideale slaaf te creeren. Maar waarom kunnen we dan nog steeds ..."
categories: [internet]
comments: true
---

We kunnen niet zonder slaven. Toch proberen we onze eigen ideale slaaf te creeren. Maar waarom kunnen we dan nog steeds niet zonder slaven leven?  
 We gaan omringd worden door robots. Robots die ons leven horen te gaan vereenvoudigen om zelf ingewikkeldere en moeilijkere taken uit te voeren. Hierdoor worden we intelligenter. Zoals techniek zo moeten werken; onze intelligentie bevorderen. Ons tot dingen in staat laten zien waar we onze fantasie werkelijkheid maken.  
  
  
**2033**  
 De gehele industrie bestaat uit robots. Alle automatiseringsprocessen worden uitgevoerd door de technische slaven die in dienst werken van grote bazen van grote bedrijven. De wereld draait nog steeds op het kapitalisme, consumeren is het belangrijkste in de maatschappij om de markt draaiende te houden. De techniek gaat hard vooruit, de mensen hollen er achteraan. Niet iedereen houdt dit bij, velen ouderen isoleren zich ver weg van het technische geweld om zich heen. Voor hen zijn er teveel beelden, knoppen en informatie om tot zich te nemen. De eisen liggen elke dag hoger om je een weg te banen tussen de technische creaties van de mens. Het is een grote chaos geworden. Alleen de meest intelligente mensen blijven overeind tussen al deze aandacht vragende input en output schermen, interfaces en apparaten.  
 In huishoudelijke kring proberen we onze taken steeds meer uit te besteden om te kunnen overleven in de wereld buiten ons huisje. We schaffen een robot aan die we binnen een paar dagen hebben geleerd wat deze moet doen binnen het huis. De aanrecht schoon houden, de planten water geven, de tuin harken, de vissen voeren, de bedden opmaken. Ongeveer alles wat een doorsnee huisvrouw of -man van 30 jaar terug moest doen. Een gezin is amper thuis, alleen in de avond en nacht om uit te rusten. Dan zit je er niet op te wachten dat er thuis nog tientallen klussen gedaan moeten worden. De robosapien doet dat voor ze, het slaafje van het gezin.  
 Dit is de nieuwe vorm van slavernij die wordt geschetst in deze wereld. Een slavernij zonder negatieve ondertoon. Een slaaf in je huis hebben is iets onmisbaars, zonder een robo-slaaf hoor je er niet bij. Dit beeld wordt gecreeerd door de media om je heen. Net zoals dat 30 jaar geleden ook al gebeurde. Zo manipulatief de mens was, is deze dat nog steeds ondanks dat de intelligentie toeneemt. De menselijke slavernij is niet meer, net zoals men dat dacht 30 jaar geleden. Maar toch ergens in kleine, onhumane ruimtes worden mensen geslagen om te werken. Te werken aan robot-bommen. Deze mensen ontvoeren mensen uit de Westere wereld, brainwashen ze waardoor ze weer kind worden en vervolgens aan het werk worden gezet. In kampen onder de grond. Grotten in woestijnen waar de robot-bommetjes zo groot als insecten worden gemaakt. Complete legers, klaar om in het Westen weer bloed en verderf te zaaien. Precies zoals het 30 jaar geleden ook gebeurde. Zelfmoordaanslagen om de Westerlingen proberen te verdrijven.  
  
 We kunnen niet zonder slaven...  
  
*- Artificiële Emoties (AE)  
\- Artificiële Intelligentie*  
  
*Aanvullend beeldmateriaal:*  
  
<http://www.activrobots.com/ROBOTS/index.html>  
<http://www.irobot.com>  
<http://www.mobilerobots.com/commercial.html>  
  
 Robo-human  
 \[flash http://www.youtube.com/watch?v=BCNTNKq-LOE\]  
  
 The world in 2057 (Discovery Channel)  
 \[flash http://video.google.it/videoplay?docid=-7582986795752940587&amp;q=2057+The+World\]  
  
 Morphogenesis: Shaping Swarms of Intelligent Robots  
 \[flash http://www.youtube.com/watch?v=-G66iL\_\_VdA\]  
  
 \[flash http://www.youtube.com/watch?v=GM2Bw2zPSqc\]  
 \[flash http://www.youtube.com/watch?v=mJbpqykvTBk\]  
  
 Leg-wheel Hybrid Walking Vehicle  
 \[flash http://www.youtube.com/watch?v=sYIzGjLNIyI\]  
  
 Documentaire die mooi aansluit bij mijn concept  
<http://thoughtware.tv/videos/show/623>  
  
 Human 2.0  
<http://thoughtware.tv/videos/show/93>
