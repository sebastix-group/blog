---
title: "Flash opdracht"
date: "2007-10-03T15:36:00.000Z"
description: "Opdracht voor Flash waarin we een object van boven een gevoel van zwaarte moesten meegeven. Je kunt het balletje oprapen..."
categories: [internet]
comments: true
---

Opdracht voor Flash waarin we een object van boven een gevoel van zwaarte moesten meegeven. Je kunt het balletje oprapen en laten vallen.  
  
\[flash http://interactieontwerpen.nl/\_sebastian/flash/drag\_functie.swf\]
