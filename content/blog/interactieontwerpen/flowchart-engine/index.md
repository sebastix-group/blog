---
title: "Flashgame 'Engine' - concept"
date: "2008-09-09T10:42:54.000Z"
description: "Voor de 9e periode 'PLAY' moeten we binnen korte tijd een flashgame concept bedenken en visualiseren en mogelijk ook mak..."
categories: [internet]
comments: true
---

Voor de 9e periode 'PLAY' moeten we binnen korte tijd een flashgame concept bedenken en visualiseren en mogelijk ook maken.  
 In de game bestuur je een motor (geen met twee wielen, maar een motorblok uit een auto) die het moet opnemen tegen andere motorblokken. Je maakt je tegenstanders stuk door er tegenaan de botsen. Zelf krijg je ook schade en deze dien je te herstellen door middel van upgrades die van je tegenstanders afvallen; bougies, zuigers, olie, bouten en moertjes.  
  
  
  
[![](/images/flowchart1.jpg)](/images/flowchart1.jpg)
