---
title: "Assessment & studieplan"
date: "2009-11-11T12:15:39.000Z"
description: "Vandaag heb ik mijn -als het goed is- laatste assessment om vervolgens mijn afstudeerperiode in te gaan op de academie S..."
categories: [internet]
comments: true
---

Vandaag heb ik mijn -*als het goed is*- laatste assessment om vervolgens mijn afstudeerperiode in te gaan op de academie St.Joost. Hiervoor heb ik een studieplan uitgeschreven die een goed beeld geeft van mij waar welke fase ik me bevindt als persoon/student/interactie ontwerper.

   
  
<span>Tijdens mijn stageperiode is voor mezelf duidelijk geworden dat ik op een eigen werkplek waar ik eigen baas ben het beste kan werken. Dit sluit ook volledig aan op mijn eigen toekomstvisie waar ik graag voor mezelf wil werken, in plaats van fulltime voor een baas. Ik weet hoe het is om voor een baas te werken en ben dan ook van mening dat een dergelijke werksituatie voor mij op een bepaald moment een beperking wordt voor mijn persoonlijke zelfontwikkeling en ook als ‘interactie ontwerper’ binnen in een nog te kaderen stroming.</span>

  
<span>Van deze bevindingen was ik me al langer bewust, ik was daarom altijd al wel zoekende naar een eigen werkplek. Een werkplek los van thuis. Die thuiswerkplek voldeed al een tijd niet meer aan mijn eisen, omdat ik vooral merkte dat ik hier veel te veel afleiding had en nooit de diepgang kon creëren in mijn opdrachten. Tijdens mijn stage heb ik er serieus werk van gemaakt om te kijken welke mogelijkheden er voor mij zijn als startende ondernemer. Naast mijn studie ben ik namelijk altijd al actief geweest als ZZP’er die opdrachten voor derden maakte. Ik durf te zeggen dat ik de capaciteiten heb om dit ook in de toekomst op de juiste manier verder op te bouwen en mezelf als Sebastix verder te profileren als een interactie ontwerper.</span>

  
  
<span>Met open vizier recht op mijn doel afgaan, dat is een lijfspreuk die perfect past bij mij als persoon, ondernemer en interactie ontwerper. Ik weet heel goed wat ik wil en heb nog eens teruggekeken in het verleden waar ik als 15-jarige puber al op het internet schreef dat ik later mijn brood wilde gaan verdienen met iets wat toen in mijn portfolio stond (heel divers en heel pril uiteraard). </span>

  
![](../../../images/Naamloos.png "Sebastix 15 jaar")

  
  
  
<span>Helaas werkt het fotootje van mezelf ernaast niet meer, anders had het erg mooi beeld geweest. Ik spreek daar al van digitale media, kunst en pixels terwijl ik nog helemaal niet wist welke mogelijkheden in de wereld (studie, technieken, bedrijven) waren hoe, wat waar ik dat precies moest gaan doen. </span>

  
<span> </span>

  
<span>Zoals ik al eerder zei, sinds mijn stage wist ik dat er iets moest veranderen in mijn omgeving om mezelf te blijven ontwikkelen. Dat heeft geresulteerd dat ik met een boel pijlen ben gaat schieten verschillende kanten op om iets voor mezelf te creëren waar ik in de juiste omgeving mijn passie kan blijven beleven.</span>

  
<span> </span>

  
<span>Uiteindelijk is het dicht in de omgeving van de kunstacademie gebleven namelijk Starterslift. Na een stevige cursus en pitch ben ik toegelaten en staan er een heleboel deuren klaar om te openen. Bewust open ik nog niet elke deur, omdat de prioriteiten nog niet bij fulltime voor mezelf werken liggen. Het belangrijkste wat ik nodig had was een eigen, inspirerende werkplek in een creatieve omgeving net zoals de kunstacademie dat ook is geweest en nog steeds is. </span>

  
![](../../../images/IMG_0406.JPG "werkplek Sebastix")

  
  
  
<span>Er ontstaat onbewust al een compleet nieuw netwerk om me heen en heb ik naast een eigen werkplek een coach toegewezen gekregen die in mijn ogen dezelfde capaciteiten (en meer!) heeft als de docenten op de academie. Persoonlijke ontwikkeling staat nog altijd bovenaan en dit zal ook wellicht voor de rest van je leven zo blijven. Deze nieuwe omgeving waar ik me nu in bevindt moet voor mij een instrument worden welke structuur in mijn opdrachten / passie gaat geven met een inkomen waarmee ik andere stappen moet gaan maken in het leven (huisje, boompje, beestje bla-die-bla). </span>

  
<span> </span>

  
  
<span>Dat is een stukje toekomstvisie op het moment van nu. Nu even terug naar de tijd die nog rest op de academie voor mij.  
 Komende maanden is het de bedoeling om een scriptie te schrijven en een afstudeerproject te realiseren. Hiermee moet ik laten zien aan de academie dat ik iets heb geleerd, waar ik sta in mijn vakgebied, hoe ik me profileer met een thema; de capaciteiten heb om ook buiten de academie op beide benen te kunnen staan. Het afstuderen moet een visitekaartje worden van mezelf als interactie ontwerper en me een richting heen duwen waar ik graag naar toe wil. Dat brengt me dus weer terug bij mij eerder geschreven woorden waar ik mee bezig ben buiten de academie om. Dat is dus het plan, de kunstacademie is niet het enige instrument meer die ik gebruik om naar mijn doelen toe te werken.</span>

<span> </span>

  
<span>Naast studeren ben ik dus ook altijd bezig met ondernemen en dit brengt wel een tweestrijd met zich mee. Want het gaat om focus, pas als je je ergens meer dan 100% op focust brengt het je verder in jezelf en je opdracht. Het is dus belangrijk dat ik heel goed mijn focusmomenten kan inplannen om deze diepgang na te streven. Een scriptie schrijven en een afstudeerwerk maken is namelijk niet het enige waar ik me bezig moet houden. Wanneer het merkbaar is dat ik te weinig focus op een opdracht, confronteer me hiermee. Vaak zit de focus dan ergens anders. Dit is wel belangrijk mocht het teveel tussen mij en mijn uiteindelijk bewijs van BDes komen te staan.</span>

  
<span> </span>

  
<span>Mijn doelstelling mbt afstuderen:</span>

  
  
  
- <span style="font-family: Symbol; color: gray;"><span><span style="font-style: normal; font-variant: normal; font-weight: normal; font-size: 7pt; line-height: normal; font-size-adjust: none; font-stretch: normal; font-family: "Times New Roman";"> </span></span></span><span>nieuwe hoogtepunt bereiken </span><span>*<span style="color: #7f7f7f;">  
   (ATTACK is nog altijd het hoogtepunt van mij van mijn studie)</span>*</span>
  
- <span>multidisciplinair project neerzetten (met anderen kom je verder)</span>
  
- <span>aansluitend op mijn scriptie (archiveren van websites/digitaal erfgoed)</span>
  
- <span>mezelf verder ontwikkelen als ‘interactie ontwerper’</span>
  
- <span>heldere profilering als interactie ontwerp</span>
  
- <span>meer kennis van de ‘interactie ontwerpen’ omgeving</span>
  
- <span>meer vormgeving</span>
  
- <span style="font-size: 12pt; font-family: Cambria;">betere eigen visie + praktijk op: digitale informatie dichter naar de realiteitsbeleving</span>
  

  
  
  
  
  
  
