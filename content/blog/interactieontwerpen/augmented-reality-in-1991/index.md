---
title: "Augmented Reality in 1991"
date: "2010-01-29T18:27:27.000Z"
description: "Na het lezen van 'MEMENTO: a digital physical scrapbook for memory sharing' (www.springerlink.com/index/K7K8048678MK2534..."
categories: [internet]
comments: true
---

Na het lezen van 'MEMENTO: a digital physical scrapbook for memory sharing' (<cite>www.springerlink.com/index/K7K8048678MK2534.pdf</cite>) voor mijn scriptie werd er verwezen naar een project genaamd Digital Desk van Xerox. Via Google kwam ik uit op het volgende filmpje uit 1991 waar al een hele mooie toepassing is te zien welke je tegenwoordig kunt plaatsen in de categorie Augmented Reality. Nu bijna 20 jaar later zouden we nog steeds warm worden van de mogelijkheden die hier worden gedemontstreerd:  
  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="587" width="587"><param name="id" value="VideoPlayback"></param><param name="src" value="http://video.google.com/googleplayer.swf?docid=5772530828816089246&hl=nl&fs=true"></param><embed height="587" id="VideoPlayback" src="http://video.google.com/googleplayer.swf?docid=5772530828816089246&hl=nl&fs=true" type="application/x-shockwave-flash" width="587"></embed></object>
