---
title: "Paper beeldcultuur"
date: "2007-10-10T21:41:08.000Z"
description: "Sebastian Hagens – Interactie Ontwerpen
Paper beeldcultuur


Image

Sinds er mensen op de aardbol rondlopen, bestaan e..."
categories: [internet]
comments: true
---

Sebastian Hagens – Interactie Ontwerpen  
Paper beeldcultuur  
  
  
**Image**  
  
Sinds er mensen op de aardbol rondlopen, bestaan er beelden. Sinds de homosapiens rondlopen, zijn er beelden gemaakt om iets te kunnen communiceren. Het begon primitief met stenen. Maar hoe intelligenter de mens werd, hoe meer middelen deze vond en creëerde om beelden te kunnen maken. Echter zien we in deze stijgende lijn dat de hoeveelheid beeld alleen maar is toegenomen in de geschiedenis en nooit is afgenomen. Waarom?  
  
  
De creativiteit van de mens kent geen grenzen, zo ook niet in het maken van beelden. Opnieuw en opnieuw maken we beelden, kopiëren we deze en verspreiden we deze. In de huidige informatiemaatschappij gaat dit sneller dan ooit en het lijkt erop dat het in de toekomst alleen nog maar gaat toenemen. De techniek neemt steeds grotere sprongen en stelt ons in staat om ook steeds grotere sprongen te maken met communiceren.  
Beheersen beelden onze leven? Nee. We hebben de beelden nodig om sneller iets te herkennen en zo efficiënter iets te kunnen concluderen. Als alles om je heen geen beeldnaam zou hebben, wist je ook niet waar je moest zoeken als je iets aan het zoeken was. Beelden vereenvoudigen ons leven. Beelden vertellen meer dan een tekst, beelden werken op bepaalde niveaus zelfs universeel buiten de grammatica van een taal om.  
Toch zie ik om me heen veel mensen die niet meer voor zichzelf kunnen denken en zich teveel laten beïnvloeden door beelden. Hiermee bedoel ik voornamelijk de reclame beelden. In de reclame wereld wordt de laatste jaren steeds slimmer gebruik gemaakt van hoe beeld kan werken. De individualisering neemt toe en ieder individu probeert zich toch met een bepaald imago te identificeren. Het imago wordt gemaakt met hoofdzakelijk beelden in diverse media. Iedereen probeert toch op zijn eigen manier uniek te zijn, want dit wordt immers van je gevraagd door de andere mensen om je heen. Sommige grote merken verkopen producten zelfs met een slogan die letterlijk zegt dat je je eigen leven moet leiden en ontwerpen. Goed voorbeeld hiervan is IKEA: ‘design your own life’. Deze uitspraak koppelen ze dan wel aan meubelen die iedereen kan kopen, in plaats van jij alleen. En het werkt nog ook; mensen kopen massaal spullen bij de IKEA.  
Er wordt teveel gewogen aan de kwaliteit van het imago wat wordt uitgebeeld in de media. Het gaat allang niet meer om de kwaliteit van het product, maar om het imago van het product. Elk jaar is er wel een nieuwe trend op het gebied van gadgets, gecreëerd in een hype met beelden zonder het product ooit zelf een te hebben getest. Met alleen hoogwaardige kwaliteitsproducten verkoop je tegenwoordig niks meer, het imago moet minstens net zo goed zijn of zelfs beter.  
Juist door dit soort fenomenen over beeldgebruik wordt men wel steeds slimmer. Over een tientallen jaren zal de jeugd heus niet zo manipuleerbaar zijn als de jeugd van nu. Een mooie uitspraak die me is bijgebleven over de jeugd is, ‘de toekomstige jeugd is een Einstein-generatie’. Een generatie die niet meer voor de gek te houden is, een generatie die geen dilemma’s accepteert en een generatie die de marktwerking begrijpt. Toch zie ik het binnen mijn generatie niet gebeuren dat we door de reclame heen kunnen prikken en bewust worden dat we voor de gek worden gehouden. Binnen de generatie waar ik in leef, wordt er nog teveel naar elkaar gekeken hoe men zich identificeert met imago’s. Veel vrienden van me buiten school om denken in deze hokjes. Ik confronteer ze bewust niet met dit gegeven. Als ik het wel doe, dan denk ik dat ik een heleboel onbegrip zal opwekken. Sommige zullen mijn punt wel snappen, maar dan toch verder leven en denken in het hokje waar ze zich in bevinden. Zoals ik al zei; ik zie het niet gebeuren dat de huidige generatie de imago(beeld)cultuur van reclame zal doorbreken. Zelf leef ik ook binnen een hokje, juist om niet buiten het hokje te vallen en te worden weggestopt in een ander hokje door de ogen van mijn vrienden. Maar mijn vrienden blijven mijn vrienden, ondanks deze gedachtegang. Het is niet negatief bedoeld, enkel als kritiek en om mijn standpunt duidelijk te maken.  
  
De hoeveelheid beelden zal komende tiental jaren alleen maar toenemen. Echter vind ik het nog maar moeilijk in te vullen wat deze beelden allemaal gaan communiceren. Onze manipulatie-krachten groeien sterk en we hebben de macht straks werkelijk elk beeld wat in onze gedachte zit, te kunnen maken. We worden met de jaren intelligenter door de techniek en daarom denk ik dat we zeker door beelden heen kunnen prikken en ons bestaan nog altijd kunnen terug herleiden naar de echte werkelijkheid (zien, horen, voelen, proeven, ruiken en anticiperen) in plaats van te gaan geloven in beelden die een perfecte wereld laten zien en ons proberen er te naar laten streven. Wanneer dit gaat gebeuren? Zodra een toonaangevende groep mensen zich bewust wordt dat men er niet beter, gelukkiger, mooier etc van wordt.
