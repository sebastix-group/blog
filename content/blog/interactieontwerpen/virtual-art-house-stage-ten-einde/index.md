---
title: "Virtual Art-House stage ten einde"
date: "2009-09-02T20:08:16.000Z"
description: "Al ongeveer een maand geleden is mijn stageperiode afgelopen. Direct na de afronding ben ik even twee weken Europa in ge..."
categories: [internet]
comments: true
---

Al ongeveer een maand geleden is mijn stageperiode afgelopen. Direct na de afronding ben ik even twee weken Europa in geweest met vrienden om even het systeem hier te ontduiken. Heerlijk!  
 Echter is nu ook weer de tijd aangebroken om de draad weer op te pakken; studie.  
  
 Goed, eerst zal ik nog even de laatste screenshots die ik heb van het Virtual Art-House even hier neerzetten met de mededeling dat dit voorlopig even het laatste beeldmateriaal is ervan. Komende maand volgt er een opening samen met de fysieke opstelling. Foto's volgen dan!  
  
[![Screenshot_9](http://farm3.static.flickr.com/2522/3882124850_66cff1efdc_t.jpg)](http://www.flickr.com/photos/sebastix/3882124850/ "Screenshot_9")[![Screenshot_8](http://farm4.static.flickr.com/3444/3882124614_6e2274e681_t.jpg)](http://www.flickr.com/photos/sebastix/3882124614/ "Screenshot_8")[![Screenshot_7](http://farm4.static.flickr.com/3595/3882124276_037b8c826b_t.jpg)](http://www.flickr.com/photos/sebastix/3882124276/ "Screenshot_7")[![Screenshot_6](http://farm3.static.flickr.com/2592/3881325809_c50760bc00_t.jpg)](http://www.flickr.com/photos/sebastix/3881325809/ "Screenshot_6")[![Screenshot_5](http://farm3.static.flickr.com/2631/3881325333_fcbe812b9e_t.jpg)](http://www.flickr.com/photos/sebastix/3881325333/ "Screenshot_5")[](http://www.flickr.com/photos/sebastix/3881325011/ "Screenshot_4")  
  
[![Screenshot_4](http://farm3.static.flickr.com/2655/3881325011_c32714fa3f_t.jpg)](http://www.flickr.com/photos/sebastix/3881325011/ "Screenshot_4")[![Screenshot_3](http://farm4.static.flickr.com/3485/3882122654_b8e265065a_t.jpg)](http://www.flickr.com/photos/sebastix/3882122654/ "Screenshot_3")[![Screenshot_2](http://farm3.static.flickr.com/2588/3881324545_a01f378d99_t.jpg)](http://www.flickr.com/photos/sebastix/3881324545/ "Screenshot_2")[![Screenshot_1](http://farm4.static.flickr.com/3438/3882122174_d287975745_t.jpg)](http://www.flickr.com/photos/sebastix/3882122174/ "Screenshot_1")  
  
 Dit waren de laatste foto's tijdens de afronding en voor de presentatie van het Virtual Art-House:  
  
[![ready for afronding stage](http://farm4.static.flickr.com/3465/3774696686_17353bf886_t.jpg)](http://www.flickr.com/photos/sebastix/3774696686/ "ready for afronding stage")[![virtual arthouse is gedocumenteerd!](http://farm4.static.flickr.com/3487/3773048448_5e104675cb_t.jpg)](http://www.flickr.com/photos/sebastix/3773048448/ "virtual arthouse is gedocumenteerd!")
