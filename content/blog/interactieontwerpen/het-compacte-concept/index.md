---
title: "Het 'compacte' concept"
date: "2009-04-14T12:21:41.000Z"
description: "

versie1.0 (2 april 2009)
Wat bedoelen we met het compacte concept?
Het compacte concept is een grof uitgekleed con..."
categories: [internet]
comments: true
---

![](../../../images/vah-logo.jpg "Virtual Art-House")  
  
 versie1.0 (2 april 2009)  
***Wat bedoelen we met het compacte concept?**  
 Het compacte concept is een grof uitgekleed concept van het algemene concept rondom het Virtual Art-House (VAH). Het is de bedoeling dat met dit compacte concept de technische realisatie kan starten en dat deze precies moet communiceren naar Cinto toe wat er precies ontwikkeld kan worden. Dit concept zal daarom ook veel meer gedetailleerde, concreter informatie bevatten dan het algemene concept.*  
  
**Inhoud compacte concept**  
 • 3D grid  
 • De ruimte  
 • De kunstwerken  
 • Navigatie  
 • De functionaliteiten  
  
**3D grid**  
 De virtuele ruimte zal bestaan uit een 3D wereld (zie de ruimte) die ingedeeld wordt door een grid. In dit grid bevinden zich lijnen die bepaalde X,Y,Z coördinaten representeert. Op bepaalde coördinaten snijden deze lijnen zich; de snijpunten. Deze snijpunten worden de punten waar de kunstenaar hun digitale kunstwerken (zie de kunstwerken) kunnen plaatsen. Welke schaal er gehanteerd moet worden tussen de coördinaten is afhankelijk van hoe de beleving gaat aanvoelen als je je in deze ruimte bevindt.  
**De ruimte**  
 We definiëren de ruimte in het VAH als een gebied rondom objecten. Objecten ontsluiten ruimte, waardoor je begrenzingen in de ruimte krijgt. In principe ontsluiten we met het 3D grid al ook verschillende ruimtes; dit zijn begrenzingen. Deze zijn niet zichtbaar (wel voor de technische kant) voor de bezoekers van de ruimte.In deze versie hebben we nog geen beperking toegevoegd aan de ruimte.

![](../../../images/vah/definitie-ruimte.jpg "Definitie ruimte")

  
  
**De kunstwerken**  
 In de ruimte komen de kunstwerken te staan van de kunstenaars. Deze werken zijn digitaal en worden geplaatst op de snijpunten van de lijnen in het 3D grid.  
 De mogelijke kunstwerken kunnen bestaan uit de volgende digitale bestanden:  
 • afbeeldingen (JPG, PNG, TIFF)  
 • audio (.mp3, .wav, .aif)  
 • video’s (.mp4, .mov, .wmv)

**Navigatie**  
 We kunnen ons vrij bewegen door de ruimte als een soort zwevende persoon door het Virtual Art-House en de werken die zich hierin bevinden. Je kunt je vooruit, achteruit bewegen en je zicht naar links, rechts, onder of boven veranderen.  
 Het voor- en achteruit bewegen geschiedt middels het toetsenbord:  
 W – vooruit  
 S – achteruit  
 A – zijwaarts naar links  
 D – zijwaarts naar rechts  
 Je zicht veranderen geschiedt middels de muis. Het midden is je focus van je zicht en deze is gekoppeld aan je muis. Waar je de muis heen beweegt, beweegt ook het middenpunt van je zicht. In deze versie is er aan dit punt nog een soort ‘cursor’ toegevoegd om duidelijk te maken hoe dit werkt.

**De functionaliteiten**  
 • inladen van diverse digitale bestanden in een simpele functie  
 • digitale bestand een coördinaat meegeven  
 • digitaal bestand plaatsen in het grid met dit coördinaat  
 • de manier van navigatie uitgelegd onder het kopje navigatie

**DEMO video's**  
[Bekijk hier demo video #1 met grid](<../../../images/vah/04. VAH Filled Grid.mov>)  
[Bekijk hier demo video #2 zonder grid](<../../../images/vah/05. VAH Filled no Grid.mov>)
