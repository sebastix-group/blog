---
title: "Digitaal archief"
date: "2008-04-28T13:17:49.000Z"
description: "Ik wil een digitaal archief maken van verschillende ‘web2.0’ API’s van bestaande digitale archieven. YouTube lijkt..."
categories: [internet]
comments: true
---

Ik wil een digitaal archief maken van verschillende ‘web2.0’ API’s van bestaande digitale archieven. YouTube lijkt me heel mooi om mee te beginnen om daar een interface voor te ontwikkelen waar je door de gehele YouTube database kunt zoeken en er ook gerelateerde dingen verschijnen. Het zou ook mogelijk zijn om andere platformen erbij te betrekken zoals Digg, Google, Delicious, Technorati, eBay en Flickr.  
  
  
  
**\*1\***  
 \[ **Eerste stap** \] is om te kijken wat er mogelijk is met YouTube en wat ik daarmee ga doen. Wat wordt mijn concept wat ik wil gaan doen met deze database.  
 Bij YouTube filmpjes horen ook thumbnails welke weer beelden zijn. Aan deze filmpjes zijn gekoppeld:  
\- tags  
\- category  
\- beschrijving  
\- datum  
\- maker  
\- rating  
\- views  
\- video responses  
\- related videos  
\- comments  
  
 Ik ben van mening dat in de toekomst de bestaande data op het web steeds betekenisvoller worden. Data staat in een bepaalde context en heeft een bepaalde inhoudelijke waarde. Deze gegevens zijn slecht te vinden, bijvoorbeeld Google gaat grotendeels uit van statistieken en niet op de kwaliteit van de inhoud van data in een bepaalde context.  
**\*/1\***  
  
 Door verschillende bestaande platformen die data generen (zoeken) aan elkaar te koppelen wil ik een nieuw platform maken die deze platformen dichter bij elkaar brengt. Ik wil de data die wordt verzameld meer betekenis geven en in een duidelijkere context plaatsen als men het resultaat te zien krijgt na een zoek handeling.  
  
 API’s:  
<http://www.programmableweb.com/apis>  
  
 \[ youtube \]  
[http://code.google.com/apis/youtube/developers\_guide\_protocol.html](http://code.google.com/apis/youtube/developers_guide_protocol.html)  
  
 \[ google \]  
<http://code.google.com/apis/maps/index.html>  
<http://code.google.com/apis/ajaxsearch/>  
  
 \[ digg \]  
<http://apidoc.digg.com/>  
  
 \[ flickr\]  
<http://www.flickr.com/services/api/>  
  
 \[ technorati \]  
<http://www.technorati.com/developers/api/>  
<http://kailashnadh.name/ducksoup/>  
  
 \[ delicious \]  
[http://www.google.nl/search?q=delicious+api&amp;ie=utf-8&amp;oe=utf-8&amp;aq=t&amp;rls=org.mozilla:nl:official&amp;client=firefox-a](http://www.google.nl/search?q=delicious+api&ie=utf-8&oe=utf-8&aq=t&rls=org.mozilla:nl:official&client=firefox-a)  
  
 \[ ebay \]  
<http://developer.ebay.com/>  
  
 Soortgelijke platformen naast mijn concept:  
<http://www.piclens.com/demo/> (afbeeldingen)  
<http://ajaxian.com/archives/tagbulb-tag-search-simplified>
