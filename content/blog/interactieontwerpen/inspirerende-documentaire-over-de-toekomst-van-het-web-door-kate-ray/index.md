---
title: "Inspirerende documentaire over de toekomst van het web (door Kate Ray)"
date: "2010-05-09T10:21:33.000Z"
description: "

Web 3.0 from Kate Ray on Vimeo.

Korte documentaire over het semantische web 'web3.0' door Kate Ray met sleutelfig..."
categories: [internet]
comments: true
---

<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="464" width="618"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="src" value="http://vimeo.com/moogaloop.swf?clip_id=11529540&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=&fullscreen=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="464" src="http://vimeo.com/moogaloop.swf?clip_id=11529540&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=&fullscreen=1" type="application/x-shockwave-flash" width="618"></embed></object>  
  
[Web 3.0](http://vimeo.com/11529540) from [Kate Ray](http://vimeo.com/kateray) on [Vimeo](http://vimeo.com).  
  
 Korte documentaire over het semantische web 'web3.0' door Kate Ray met sleutelfiguren aan het woord zoals Nova Spivack, Clay Shirky, Tim Berners-Lee. Meer info: http://kateray.net/film/
