---
title: "Hyves backup"
date: "2010-09-15T15:35:23.000Z"
description: "Update januari 2011: HVSbackup - applicatie die veilig en snel jouw Hyves download
Bovenstaande applicatie heb ik in de..."
categories: [internet]
comments: true
---

**Update januari 2011:** [**HVSbackup - applicatie die veilig en snel jouw Hyves download**  ](http://www.hvsbackup.nl "Hyves backup")Bovenstaande applicatie heb ik in december 2010 geïntroduceerd. Met HVSbackup ben je in staat om heel simpel in 1 actie al jouw vrienden, foto's, krabbels, wiewatwaars en blogs te downloaden van jouw Hyves naar jouw computer. [Klik hier om naar de website van HVSbackup te gaan.](http://www.hvsbackup.nl "HVSbackup")  
  
 Een backup van jouw Hyves maken? Dat gaat niet...niet gemakkelijk in ieder geval.  
 Waarom zou ik dat willen? Je digitale artefacten (zoals je krabbels, berichten, foto's, video, wiewatwaar's) raak je vrij gemakkelijk kwijt door vergeten. Herinneren we ons cu2 nog? Velen van ons hadden daar een profiel, maar dat is nu niet meer. Maar kun jij je nog herinneren wat erop stond? Waarschijnlijk heel vaag, maar zou je het leuk vinden om het terug te zien? Waarschijnlijk wel.  
  
 Wat nu als hetzelfde gebeurt met Hyves? Over 10 jaar zul je je nog steeds herinneren dat je een Hyves profiel had en wat je daarmee deed. Echter laat je ook een heleboel informatie achter bij Hyves elke keer dat je actief bent. Hoeveel foto's deel jij via Hyves? Hoeveel krabbels heb jij al gehad en verzonden? Het zijn allemaal leuke artefacten die een persoonlijke waarde hebben. Die waarde zal niet verdwijnen en zal zeker over 10-15 jaar nog steeds aanwezig zijn wanneer je wilt terugblikken naar de tijd van nu.  
 Is Hyves er dan niet meer? Wellicht moeten we dat aan Hyves vragen, want het antwoord is hier niet gegeven.  
  
 Daarom zou een backup van onze Hyves wel handig zijn, het moet ook niets extra's kosten en geen tijd in beslag nemen om deze te maken. En daarom ook Root:\\, mijn uitgewerkte demo met een sterke ideologie dat we onze digitale artefacten niet moeten weggeven maar juist bij onszelf moeten houden. We zullen deze dus eerst terug moeten halen bij oa Hyves, zij hebben onze digitale sporen.
