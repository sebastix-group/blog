---
title: "MSN tagCloud MAPPING project"
date: "2008-01-16T18:01:42.000Z"
description: "
De beoordelingen zijn achter de rug en ik heb mijn eindwerk on-line gezet. Voor de complete uitleg van het project kun..."
categories: [internet]
comments: true
---

![](http://www.interactieontwerpen.nl/_sebastian/previewImages/msntagCloud.jpg)  
 De beoordelingen zijn achter de rug en ik heb mijn eindwerk on-line gezet. Voor de complete uitleg van het project kun je rechts bovenaan op de pagina klikken op [MSN tagCloud MAPPING project](http://interactieontwerpen.sebastix.nl/msn-tagcloud-mapping-project/) maar je kunt ook gelijk de demo's bekijken:  
  
[Bekijk de statische MSN tagClouds](http://www.interactieontwerpen.nl/_sebastian/mapping/main/last/statisch/msn/demo_statisch.php)  
[Bekijk de geanimeerde MSN tagClouds](http://www.interactieontwerpen.nl/_sebastian/mapping/main/last/dynamisch/msn/demo_dynamisch.php)
