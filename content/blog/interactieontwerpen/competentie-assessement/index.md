---
title: "COMPETENTIE ASSESSEMENT"
date: "2008-11-08T16:10:15.000Z"
description: "Hoe verloopt mijn studieproces tot nog toe?



Het volledige tweede studiejaar voelde ik me als een vis in het water..."
categories: [internet]
comments: true
---

*Hoe verloopt mijn studieproces tot nog toe?*  
  
  
  
 Het volledige tweede studiejaar voelde ik me als een vis in het water en verliep mijn proces goed. De achtste periode is voor mij de periode geweest waar ik het meeste uit mezelf heb gehaald vergeleken met andere periodes. Na het uitkomen van de zomervakantie ben ik teveel in mijn eigen werkzaamheden blijven hangen waardoor mijn studieproces stagneerde. Ik besteedde te weinig tijd aan mijn conceptontwikkeling om tot een positief resultaat te komen. Na de herfstvakantie heb ik toen besloten om voor een herkansing te gaan en mijn minor project beter uit te voeren zodat deze op Playgrounds kon staan. Ik heb hier absoluut geen spijt van gehad, want deze ervaring is tot nu toe ook de meest leerzame en inspirerende geweest tot nu toe. Het gevoel waar ik vorig jaar het schooljaar mee afsloot, is weer helemaal terug. En daar doe ik het voor.  
  
*Waar sta ik nu, wat heb ik ontdekt?*  
  
 Mijn fascinatie ligt ergens bij de virtuele wereld. Deze wereld wordt steeds dominanter en er vinden steeds meer verschuivingen hierin plaats, ook naar de reële wereld toe. Dit heeft op allerlei gebieden gevolgen en biedt nieuwe mogelijkheden die ik graag ontdekt. Ik sta ergens tussen deze twee wereld in en wil graag de grenzen ervan opzoeken op verschillende manieren.  
  
*Waar wil ik heen, wat wil ik meer ontwikkelen?*  
  
 Aansluitend op mijn vorig antwoord zou ik graag specifieker willen ontdekken waar precies mijn fascinaties liggen binnen die twee werelden en waar mijn sterke kanten hierin ook liggen. Conceptontwikkeling blijft een zwakte en ik moet hierin dus meer tijd steken om een bepaald idee compleet te kunnen maken op het gebied van doel, concept en uitwerking.  
  
*Wat moet ik doen om dit te bereiken?*  
  
 Ik moet blijven onderzoeken, meer buiten de muren komen van school en meer tijd steken in het ontwikkelen van een eigen visie en concept. Ik moet beter leren organiseren en kunnen plannen met de tijd die ik nog heb op school.  
  
*Wat zijn mijn sterke/zwakke punten?*  
  
 Een absoluut zwaktepunt van mij is dat ik slecht prioriteiten kan stellen. Dit komt ook voort uit het feit dat ik buiten school om heel veel andere dingen kan doen die ook vaak financieel iets opleveren, maar ook voldoening geven omdat ik het al kan. Het is echter niet zo uitdagend dan als wat ik op school doe en kies ik dus ook soms te snel voor een makkelijke weg dan de moeilijke weg. Ik moet meer weigeren om me gewoon wel volledig in te zetten op school. Tot nu toe heb ik mezelf nog geen enkele keer voor de volle 200% ergens voor ingezet denk ik. Ergens ben ik me er van bewust dat ik meer uit mezelf kan halen.  
 Een sterk punt is dat ik een hele brede kennis heb van mogelijkheden zeker in het gebied van de virtuele wereld. Communicatief weet ik daar dingen goed te doorgronden hoe ze werken.  
  
*Welke competenties beheers ik? (-- | - | +/- | + | ++)*  
  
 Vermogen tot kritische reflectie +  
 Vermogen tot groei en vernieuwing +/-  
 Omgevingsgerichtheid –  
 Organiserend vermogen ++  
 Communicatief vermogen +  
 Vermogen tot samenwerken +  
  
*Welke competenties heb ik onvoldoende ontwikkeld?*  
  
 Ik moet meer tijd steken in mijn conceptontwikkeling en proberen hier het maximale eruit proberen te halen, zodat ik van mezelf weet waar hier mijn grens ligt.  
  
*Herkansingsafspraken; hoe zijn deze geregeld?*  
  
 Ik krijg nog te horen in welke vorm ik moet herkansen voor periode 9.  
  
*Reflecteer ik regelmatig in mijn ontwikkelingsportfolio? Weblog?*  
  
 Ik probeer dit zoveel mogelijk te doen voor elk vak op mijn weblog en houdt hier ook zoveel mogelijk mijn proces bij. In vergelijking met mijn studiegenoten, ben ik hier heel actief in.  
  
*Hoe stel ik mij op de hoogte van ontwikkelingen in mijn beroepsgebied?*  
  
 Internet is mijn grootste bron voor informatie en ontwikkelingen. Maar veel dingen krijg ik ook mee van mijn docenten en studiegenoten. Het zou een goede zaak zijn om vaker in groepsverband uitstapjes te regelen naar buiten toe en eventueel speciale middagen lekker veel dingen te bekijken en te bespreken (deden we vaak met Sarah).  
  
 ---  
  
 Vorig assement heb ik voor mezelf enkele pijl gericht op de volgende competenties:  
 - presenteren  
 - vormgeving  
 - conceptontwikkeling  
 Bij alle drie heb ik progressie geboekt, vooral het presenteren gaat me een stuk beter af merk ik. Vormgeving is iets waar ik meer aandacht aan moet besteden in de details. Conceptontwikkeling blijft mijn grootste zwakte.
