---
title: "Touchscreen mediaplayer voor expositie Woord&Wapen Grote Kerk Breda"
date: "2010-01-05T18:37:09.000Z"
description: " 

Afgelopen twee maanden heb ik hard gewerkt aan een zeer bijzonder en interessant project.



Tot mijn verbazing..."
categories: [internet]
comments: true
---

[![Alive!](http://farm3.static.flickr.com/2777/4225576524_4afcd8d7ed_m.jpg)](http://www.flickr.com/photos/sebastix/4225576524/ "Alive!")[ ![Bijna klaar!](http://farm3.static.flickr.com/2802/4224365537_e54b06e3df_m.jpg)](http://www.flickr.com/photos/sebastix/4224365537/ "Bijna klaar!")  
  
 Afgelopen twee maanden heb ik hard gewerkt aan een zeer bijzonder en interessant project.  
  
  
  
 Tot mijn verbazing heb ik afgelopen tijd eigenlijk niets gepost hier op mijn weblog over een zeer interessant project welke ik afgelopen twee maanden heb gerealiseerd. Namelijk een stukje software voor een touchscreen met de Grote Kerk van Breda als opdrachtgever. Geen kleine klant dus en het was ook bepaald ook geen kleine opdracht. Momenteel is de Grote Kerk bezig om een expositie vorm te geven die vanaf 15 januari te bezoeken is. Het is de grootste expositie tot nu in de Kerk, dus ik ben heel benieuwd.  
 Ik kopieer even de tekst van de website van de expositie <http://www.woordenwapen.nl>:  
> ## *Nieuwe expositie in de Grote Kerk: te zien vanaf 15 januari 2010:*
> 
>   
> # Expositie Woord en Wapen, in dienst van de Nassaus
> 
>   
> ## *"De spectaculaire wedergeboorte van een kerkinterieur"*
> 
>   
>  **In de lucht: 277 gereconstrueerde wapenborden.  
>  Op de grond: 220 digitaal ontsloten grafmonumenten.  
>  In expositie: 170 unieke museumstukken én het verhaal over 22 geestelijke en militaire dienaren van de Oranje-Nassaus.  
>  Een bijzondere expositie die u moet zien !**   
>   
>  In 2010 bestaat de Grote of Onze Lieve Vrouwe Kerk van Breda 600 jaar. Dat was voor de beheerstichting de reden om opdracht te geven tot het organiseren van een expositie over de periode 1637 – 1813  
>   
>  Expositiesamensteller Walter van de Garde creëerde na een voorbereiding van twee jaar een mega-presentatie die het verhaal vertelt van de Grote Kerk van Breda in de protestantse constellatie van die tijd. Van de Garde tekende ook voor de eerdere succesvolle exposities “Pronk en Praal” met 150.000 bezoekers en “Praal en Devotie”, die 100.000 bezoekers trok.

  
> In de prachtige lichte wandelkerk, die de Grote Kerk vandaag de dag nog steeds is, wordt het interieur van de 17e en de 18e eeuw herschapen. Met de mensen, die in die periode invloedrijk waren. Niet alleen in de kerk, maar ook op diverse andere terreinen van het geestelijke, politieke, maatschappelijke en militaire leven van die tijd. Met als verbindende factor hun relatie met de leden van ons koninklijk huis: de Oranje-Nassaus.  
>   
> ![](http://www.grotekerkbreda.nl/pix/flyer_woordwapen_vsl.jpg "Woord en Wapen")

  
 In totaal zijn er bijna 1700 afbeeldingen ontsloten middels een Adobe AIR applicatie welke draait op twee touchscreens in de kerk van iets meer dan 230 grafzerken die in de gehele kerk liggen.  
 Het was een hele uitdaging om een goed werkende applicatie te ontwikkelen binnen anderhalve maand tijd. Het scherm zelf is al behoorlijk verouderd waar ik mee moest werken; deze waren al bijna 10 jaar oud. Tegenwoordig zijn we allemaal verwend met multi-touch toestellen zoals iPhones, maar dat ging voor deze touchscreen niet op. Dus de interactie vormen zijn behoorlijk beperkt gebleven in simpele aanraakhandelingen 'kliks' of iets aan blijven raken 'ingedrukt houden'. Toch ben ik tevreden met het resultaat, ik zal altijd blijven zeggen dat het beter kon. Echter spelen tijd en budget ook altijd een belangrijke rol met dit soort projecten.  
  
 Een aantal belangrijke dingen heb ik wel geleerd nu na de eerste keer iets te hebben gerealiseerd voor een touchscreen mediaplayer.  
  
- Software en hardware moeten op elkaar afgesteld zijn om tot een zo perfect mogelijk resultaat te komen. In dit geval was het de hardware die de grootste beperking was.
  
- Bij een soortgelijk project in de toekomst zal ik me hard maken om ook de hardware te leveren waardoor dus software en hardware zo goed mogelijk op elkaar zullen aansluiten.
  
- De Grote Kerk van Breda heeft een gigantisch archief van nationaal-historische waarde van honderden jaren oud.
  
- Dropbox was wederom een fantastische toevoeging om samen te werken aan dit project.
  

  
 Bij deze wil ik dan ook [Het Licht](http://www.hetisaan.net/) (Victor Freriks) -technische ondersteuning- en [Amanda Butterworth](http://www.amandabutterworth.com/) -grafisch ontwerp- bedanken.  
  
 14 januari is de opening van de expositie waar Prins Willem Alexander als eerste gast van de tentoonstelling gaat openen en beleven! Binnenkort meer verslag in tekst en beeld over het project!  
  
 Hieronder nog een aantal foto's die ik tijdens de realisatie heb genomen:  
  
[![Begint ook al een aardig smoeltje te krijgen nu!](http://farm3.static.flickr.com/2542/4176239775_0c7bbd399f.jpg)](http://www.flickr.com/photos/sebastix/4176239775/ "Begint ook al een aardig smoeltje te krijgen nu!")  
  
[![Solving my problems!](http://farm3.static.flickr.com/2604/4186933243_573c763507.jpg)](http://www.flickr.com/photos/sebastix/4186933243/ "Solving my problems!")  
  
[![Werkplek vandaag lekker warm in de kamer deze zondag](http://farm3.static.flickr.com/2733/4199130631_0471e9ac8b.jpg)](http://www.flickr.com/photos/sebastix/4199130631/ "Werkplek vandaag lekker warm in de kamer deze zondag")  
  
[![bf06d7575bdb320a317879b62077baf4_full](http://farm3.static.flickr.com/2705/4225734544_7b9f0f3cd1.jpg)](http://www.flickr.com/photos/sebastix/4225734544/ "bf06d7575bdb320a317879b62077baf4_full")
