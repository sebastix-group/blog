---
title: "MSN chats mapping - concept en experiment"
date: "2007-12-03T15:40:34.000Z"
description: "
De tijd is rijp dat we ons gaan richten op onze belangrijkste hoofdopdracht deze periode. De bedoeling is dat we met e..."
categories: [internet]
comments: true
---

![](http://interactieontwerpen.nl/_sebastian/previewImages/mapping1.jpg)  
 De tijd is rijp dat we ons gaan richten op onze belangrijkste hoofdopdracht deze periode. De bedoeling is dat we met een gegeven data-set een mapping maken die een vernieuwende visie creëert op de gekozen data.  
 Mijn keuze is gevallen op mijn **chat activiteiten** met het programma Adium (chat service MSN). Adium registreert je conversaties in XML bestanden (*chatlogs*). Het is vrij gemakkelijk om deze data te laden, uit te lezen en vervolgens in kaart te brengen.  
  
 Het uitlezen gebeurt middels actionscript in Flash. Ik heb hiervoor al enkele testjes gedaan.  
 Hieronder vind je een schematische weergave het proces:  
  
[![msn mapping](http://interactieontwerpen.nl/_sebastian/mapping/main/flowchartmapping.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/main/flowchartmapping.jpg)  
[![msn mapping](http://interactieontwerpen.nl/_sebastian/mapping/main/schetsmapping1.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/main/schetsmapping1.jpg)  
  
 Echter worden deze visualisaties geladen uit een door mij aangepaste chatlog, want ik zit momenteel nog met wat technische problemen waardoor het niet in één keer mogelijk was om vanuit een bestaande chatlog te werken. Hieronder vind je mijn eerste twee experimenten met de techniek en vorm.  
  
[![msn mapping](http://interactieontwerpen.nl/_sebastian/mapping/main/test1.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/main/test1.jpg) [![msn mapping](http://interactieontwerpen.nl/_sebastian/mapping/main/test2.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/main/test2.jpg)  
  
 De vorm is nog niet zo spannend. De visualisaties missen nog een context waardoor het een (nieuwe) betekenis moet gaan krijgen. In ieder geval moet er gestreefd worden om een **nieuw gezichtspunt** te creëren. Deze visualisatie zit ook nog maar op één niveau. Het laat nu nog maar één conversatie zien, terwijl ik er meer dan 3600 heb onderverdeeld onder ongeveer 200 contacten. Het liefst maak ik deze lagen ook zichtbaar en kan men er doorheen navigeren...  
*Eerst moeten nog de technische problemen worden opgelost om vervolgens meer onderzoek te gaan doen naar verschillende contextvormen om mijn dataset zo interessant mogelijk te maken.*  
  
 -----  
  
 Via de website <http://www.tagcrowd.com> heb ik enkele tagclouds gemaakt van chatgesprekken. Een **tagcloud** laat zien welke woorden het meest worden gebruikt in een stuk tekst. Hierdoor kun je al snel in 1 oogopslag zien wat het belangrijkste gesprekonderwerp was. Hoe groter en dieper van kleur een woord; hoe vaker het woord is gebruikt in het gesprek. Zo zie je hieronder 2 tagclouds van 2 gesprekken. Het eerste gesprek ging over een probleem dat ik had met mijn auto en het tweede gesprek is met een goede vriendin van mij.  
  
[![msn mapping](http://interactieontwerpen.nl/_sebastian/mapping/main/visual_tagcloud1_0.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/main/visual_tagcloud1_0.jpg)  
[![msn mapping](http://interactieontwerpen.nl/_sebastian/mapping/main/visual_tagcloud2.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/main/visual_tagcloud2.jpg)  
  
 Hieronder heb ik een testje gedaan met kleur. De kleur van een tagcloud wil ik koppelen aan de intensiteit van een gesprek. De intensiteit wordt bepaald aan de hand van hoe snel er gereageerd wordt op elkaars reacties.  
  
[![msn mapping](http://interactieontwerpen.nl/_sebastian/mapping/main/visual_tagcloud1_1.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/main/visual_tagcloud1_1.jpg)  
  
**Onderzoeksbronnen:**  
  
<http://web.media.mit.edu/~francis/projects/cheiro/>  
 Hier wordt een poging gedaan om in een chat meer emotie te geven aan de teksten. Een statische tekst heeft normaal gesproken nooit zoveel emotie als bijvoorbeeld een verbaal gesprek. In chatprogramma's wordt daarom het meest gewerkt met smilies om emotie uit te drukken. In dit project proberen ze het met een andere typografie met animaties. Dit is de demo waar je eerst een tekst typt die je naderhand moet uitdrukken met je muis: <http://web.media.mit.edu/~francis/cheiro/>  
  
[http://infosthetics.com/archives/2006/09/crystalchat\_chat\_history\_diagram.html](http://infosthetics.com/archives/2006/09/crystalchat_chat_history_diagram.html)  
 Een chat geschiedenis visualisatie  
  
[http://www.nataliarojas.com/p55/msn\_history/](http://www.nataliarojas.com/p55/msn_history/)  
 Dit is een voorbeeld van een dezelfde mapping concept die ik wil gaan maken. Een heel cool project die werkt en is gemaakt met Processing. Echter krijg ik bij het zien van deze mapping niet echt het idee dat het me iets nieuws vertelt wat je normaal ziet. Het is meer een leuk technisch dingetje.
