---
title: "Werkplek Virtual Art-House"
date: "2009-04-21T18:43:34.000Z"
description: "Vandaag was het eindelijk zover dat we onze eigen werkplek hebben in Zinc te Roosendaal. Gelegen aan de Molenstraat is d..."
categories: [internet]
comments: true
---

Vandaag was het eindelijk zover dat we onze eigen werkplek hebben in Zinc te Roosendaal. Gelegen aan de Molenstraat is dit een nieuw initiatief gericht op studenten en starters. Er komt ook een zogenaamd starterscentrum (een initiatief dat door de economische crisis meer focus en voorrang krijgt in de ontwikkeling), dus dat is al boeiend genoeg om voor mij straks eens te informeren qua mogelijkheden in de verdere toekomst. Goed, we hebben nu tot aan eind juli een eigen werkplek op een zeer nette locatie. Wij zijn er zeer content mee! Hier de foto's:  
  
![](../../../images/vah/kantoor/IMG_0150.JPG "VAH werkruimte")  
  
  
  
![](../../../images/vah/kantoor/IMG_0151.JPG "VAH werkruimte")  
  
![](../../../images/vah/kantoor/IMG_0152.JPG "VAH werkruimte")  
  
![](../../../images/vah/kantoor/IMG_0153.JPG "VAH werkruimte")  
  
![](../../../images/vah/kantoor/IMG_0154.JPG "VAH werkruimte")  
  
![](../../../images/vah/kantoor/IMG_0155.JPG "VAH werkruimte")  
  
![](../../../images/vah/kantoor/IMG_0156.JPG "VAH werkruimte")  
  
![](../../../images/vah/kantoor/IMG_0157.JPG "VAH werkruimte")
