---
title: "Toegelaten tot Starterslift"
date: "2009-10-10T17:08:11.000Z"
description: "

Met bijzonder veel trots wil ik melden dat ik ben toegelaten tot de Starterslift. Starterslift is een stichting jong..."
categories: [internet]
comments: true
---

![](http://blushuis.nl/sites/all/themes/firehose/logo.png "Blushuis")  
  
 Met bijzonder veel trots wil ik melden dat ik ben toegelaten tot de Starterslift. Starterslift is een stichting jonge ondernemers helpt bij het op de markt brengen van producten, diensten en concepten.  
  
 Dit jaar richt de Starterslift zich ook op de creatieve sector en juist ik ben nu een van meerdere creatieve starters die heel veel middelen en ondersteuning krijgt.  
  
 Wat betekent dat voor mij concreet?  
 Voor mij is de belangrijkste prioriteit dat ik met mijn werk het huis uitga waar ik leef, ik wil mijn werkzaamheden extern op een eigen plek kunnen verrichten. Dit betekent dat ik per direct een studio kan huren via Starterslift in het Blushuis in Breda. Het Blushuis is een creatieve hotspot met nog ongeveer 30 andere creatieve ondernemers waar alle nodige faciliteit aanwezig zijn om te kunnen werken.  
 Verder krijg ik een coach die me constant zal ondersteunen bij het ondernemen.  
  
 Op zeer korte termijn zal ik dus mijn eigen studio gaan inrichten en flink wat nieuwe mensen gaan ontmoeten! Dit is absoluut een hele grote stap voor mij en ik heb er ontzettend veel zin in om daar aan de slag te gaan. Binnenkort meer met de nodige beeldmaterialen!
