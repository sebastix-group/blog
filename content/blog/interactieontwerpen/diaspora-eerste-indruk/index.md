---
title: "Diaspora; eerste indruk"
date: "2010-09-16T13:34:39.000Z"
description: "In de tijd dat ik bezig was met het afronden van mijn scriptie 'Een digitale levenslijn' en opzetten van mijn eindexamen..."
categories: [internet]
comments: true
---

In de tijd dat ik bezig was met het afronden van mijn scriptie 'Een digitale levenslijn' en opzetten van mijn eindexamenwerk root:\\ was er in die tijd een heleboel te doen rondom de privacy van Facebook. Tussen die berichten door verscheen een groep jonge programmeurs met een idee 'een privacy bewust social network waar jouw informatie gedecentraliseerd is'. Diaspora, een bijzonder sterk idee met veel potentie. Daarom lukte ze het met de juiste timing in de juiste context veel geld bij elkaar te verzamelen via crowdfunding platform [kickstarter.com](http://www.kickstarter.com/projects/196017994/diaspora-the-personally-controlled-do-it-all-distr).  
  
 Zelf ben ik dan al snel verbaasd hoe snel een dergelijke idee kan worden opgeblazen tot een enorme lucht; Diaspora krijgt aandacht en ze krijgen toegang tot middelen waar ze mee aan de slag kunnen. En hier ben ik dan ook beetje jaloers...want ik wil dit ook...maar dan met mijn idee en uitwerking die root:\\ heet.  
  
 De afgelopen maanden hebben de vier heren van Diapora veel gepresenteerd, partners gevonden en een eerste alpha van hun platform ontwikkeld. Op [trydiaspora.com](http://www.trydiaspora.com) kun je testen hoe hun allereerste alpha versie werkt van Diaspora (mocht je ook willen testen, je kunt me toevoegen met *sebastix@trydiaspora.com*). Eigenlijk is het inderdaad een zoveelste social network waarop je je kunt registreren (we zijn social network moe). En dat vind ik jammer, want de belofte die Diaspora biedt is dat mijn informatie wel centraal staat (waar?) en ik zelf de controle heb welke stukjes informatie ik deel met welke vrienden. Echter komt dit aan de voorkant (nog) niet terug. Maar het is ook te vroeg om daar over te oordelen; er is immers nog geen groot netwerk tussen mij en anderen. Aan de andere kant kun je jezelf wel afvragen of deze onderscheidende waardes juist nu ook nodig zijn (je moet consistent zijn in wat je doet en brengt ben ik van mening) om de lijn van succes door te zetten.  
  
 Al met al ben ik vooral enorm enthousiast over de hype die Diaspora teweeg brengt in social media landschap. In mijn ogen is het een enorme luchtbel die ze de middelen heeft gegeven om prachtige stappen te maken. Vandaag is het eerste resultaat daarvan terug te zien; echter ben ik niet onder de indruk. Tussen mijn regels door zul je voelen wat ik nu zou voorspellen wat met Diaspora gaat gebeuren; toch rond ik af door te zeggen dat ik uitkijk naar dat wat er nog komen gaat van Diaspora. In oktober is het de bedoeling dat er over drie weken een public Diaspora moet komen waar we kunnen gaan netwerken. Die periode is kort, maar de heren van Diaspora zijn ambitieus (zie hun roadmap &amp; wishlist op <http://github.com/diaspora/diaspora/wiki/Roadmap>) dus ik sta absoluut achter ze en blijf ze op de voet volgen. Immers mag ik ook niet vergeten wat de kracht is van een opensource platform, ze kunnen gebruik maken van de community die nu achter ze staat waarmee Diaspora heel erg snel mee kan ontwikkelen aan een open, privacy bewust, gedecentraliseerd social network.  
  
 Meer informatie:

  
- <http://www.joindiaspora.com>
  
- <http://twitter.com/joindiaspora>
  
- <http://github.com/diaspora/diaspora>
  
- <http://techcrunch.com/2010/09/15/diaspora-revealed/>
  
- <http://www.diaspora-news.net/>
  
