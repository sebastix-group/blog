---
title: "ATTACK (interactive installation)"
date: "2008-04-03T17:19:04.000Z"
description: "



Tentoonstellingen:
- E-Pulse Festival, Electron in Breda oktober 2009
- INTERFERENCE, Electron in Breda 23 jan..."
categories: [internet]
comments: true
---

![](/images/minorAttack.jpg)  
  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="509" width="634"><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="src" value="http://www.youtube.com/v/JDtKOHPIvNE&hl=nl_NL&fs=1&color1=0xe1600f&color2=0xfebd01"></param><embed allowfullscreen="true" allowscriptaccess="always" height="509" src="http://www.youtube.com/v/JDtKOHPIvNE&hl=nl_NL&fs=1&color1=0xe1600f&color2=0xfebd01" type="application/x-shockwave-flash" width="634"></embed></object>  
  
 Tentoonstellingen: **- E-Pulse Festival, Electron in Breda oktober 2009  
 - INTERFERENCE, Electron in Breda 23 januari t/m 29 maart 2009** **- Playgrounds Audiovisual Arts Festival, De Nieuwe Vorst in Tilburg 30 &amp; 31 oktober 2008**  
  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="375" width="500"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="src" value="http://vimeo.com/moogaloop.swf?clip_id=2124550&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="375" src="http://vimeo.com/moogaloop.swf?clip_id=2124550&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1" type="application/x-shockwave-flash" width="500"></embed></object>  
[Demo interactive installation Attack](http://vimeo.com/2124550) from [Sebastian Hagens](http://vimeo.com/sebastix) on [Vimeo](http://vimeo.com).  
  
**INTERFERENCE in Electron te Breda 23 januari t/m 29 maart 2009**  
  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="377" width="500"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="src" value="http://vimeo.com/moogaloop.swf?clip_id=2945416&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="377" src="http://vimeo.com/moogaloop.swf?clip_id=2945416&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1" type="application/x-shockwave-flash" width="500"></embed></object>  
[ATTACK at Interference](http://vimeo.com/2945416) from [Sebastian Hagens](http://vimeo.com/sebastix) on [Vimeo](http://vimeo.com).  
  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="377" width="500"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="src" value="http://vimeo.com/moogaloop.swf?clip_id=2945455&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="377" src="http://vimeo.com/moogaloop.swf?clip_id=2945455&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1" type="application/x-shockwave-flash" width="500"></embed></object>  
[ATTACK at Interference](http://vimeo.com/2945455) from [Sebastian Hagens](http://vimeo.com/sebastix) on [Vimeo](http://vimeo.com).  
  
 ATTACK heeft op een geïmproviseerde wijze ten toon gesteld gestaan tijdens INTERFERENCE 'licht&amp;geluid' tentoonstelling. Omdat de tentoonstelling maar liefst meer dan twee maanden duurde, was het een hele uitdaging om een werkende installatie op te leveren die gedurende die tijd ook bleef werken. Helaas is dit niet helemaal gelukt, want de computer waarop de installatie draaide liet het vaak afweten (camera beeld liep vast) en het beeld liep ook niet optimaal snel met de zwerm. Een krachtigere computer is hiervoor nodig.  
 Zelf heb ik nog wel wat gesleuteld aan het geluid, deze beweegt nu mee met de zwerm. Dat wil zeggen; als de zwerm zich aan de rechterkant bevindt, dan is het geluid ook aan deze kant te vinden. Ik heb ook geprobeerd het geluid op enkele manieren interactiever te maken (oorzaak-gevolg relatie tussen gebruiker-zwerm meer proberen te vertalen in verandering van geluid) maar dit liep in de tijd dat ik het moest realiseren op niets uit (in ieder geval niet op iets wat 2 maanden zou werken).  
 Ondanks deze tegenslagen heeft de installatie het wel overleefd en laten zien dat er meer in zit. Ik ga er ook zeker mee door en ik hoop in oktober een nieuwere versie van ATTACK te presenteren. Er is namelijk al aan mij gevraagd of ik in die maand de installatie weer wil opstellen tijdens een ander festival. Nog enkele foto's van ATTACK in het Electron:  
  
![](../../../images/PICT0052.JPG)  
![](../../../images/PICT0054.JPG)  
  
![](../../../images/PICT0062.JPG)  
  
**Playgrounds Audiovisual Arts Festival in Tilburg 30 &amp; 31 oktober 2008**  
  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="375" width="500"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="src" value="http://vimeo.com/moogaloop.swf?clip_id=2154953&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="375" src="http://vimeo.com/moogaloop.swf?clip_id=2154953&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1" type="application/x-shockwave-flash" width="500"></embed></object>  
[Visitors Playgrounds interactive installation Attack](http://vimeo.com/2154953) from [Sebastian Hagens](http://vimeo.com/sebastix) on [Vimeo](http://vimeo.com).  
  
 Attack - mijn interactieve installatie heeft tijdens het Playgrounds audiovisual arts festival twee dagen lang tentoongesteld gestaan aan het publiek. Zelf heb ik flink wat tijd gestoken in een goede afwerking van de installatie zodat deze stand-alone kon draaien tijdens het festival.  
 Met een gordijndoek heb ik een semi-transparant doek gemaakt waarop de beamer kon projecten. Om dit doek heb ik een frame gemaakt met wat loodgietersmaterialen. Onder het doek heb ik een plaat geplaatst om er een geheel van te maken. De camera stond hierachter ook opgesteld. Hieronder enkele foto's van de opstelling:  
  
![](../../../images/IMG_0042.JPG "attack1")  
[![](../../../images/IMG_0046.JPG "attack1")](../../../images/IMG_0046.JPG)  
[![](../../../images/IMG_0048.JPG "attack1")](../../../images/IMG_0048.JPG)  
[![](../../../images/IMG_0049.JPG "attack1")](../../../images/IMG_0049.JPG)  
[![](../../../images/IMG_0050.JPG "attack1")](../../../images/IMG_0050.JPG)  
[![](../../../images/IMG_0051.JPG "attack1")](../../../images/IMG_0051.JPG)  
[![](../../../images/IMG_0052.JPG "attack1")](../../../images/IMG_0052.JPG)  
  
**3D visualisaties (april 2009)**  
 Ooit wil ik mijn installatie als een standalone installatie uitvoeren die op elke willekeurige plek kan worden geplaats. Hieronder vind je een impressie hoe er dat mogelijk uit komt te zien.  
![](../../../images/attack/maquette3D-3.png "attack 3D")  
![](../../../images/attack/maquette3D-7.png "attack 3D")  
![](../../../images/attack/maquette3D-9.png "attack 3D")  
![](../../../images/attack/maquette3D-8.png "attack 3D")  
[Klik hier voor meer visualisaties](http://interactieontwerpen.sebastix.nl/3d-impressie-attack-installatie/)  
  
 -----  
  
**CONCEPT**  
 De bezoeker wordt uitgedaagd in een tweede wereld met een projectie van de reële wereld. In deze tweede, virtuele wereld kijkt de virtuele levensvorm waar zich de reële levensvorm bevindt en begint deze aan te vallen, irriteren. Deze virtuele levensvorm kan worden geassocieerd met een bijvoorbeeld zwerm wespen die zich voelt aangevallen binnen hun leefomgeving.  
  
 De tweede levensvorm waarmee de bezoeker mee geconfronteerd wordt is afgeleid van een bestaande levensvorm in de natuur. In de natuur bestaan er namelijk ook zwermen die samen dingen voor elkaar krijgen, waarin een individu zou falen. Een zwerm heeft vaak meer capaciteiten dan een levensvorm binnen de zwerm.  
 Het gedrag van mijn virtuele levensvorm lijkt er veel op die van een natuurlijke zwerm. Als we terugkijken naar de allereerste digitale zwerm dan gelden er namelijk drie regels binnen een zwerm; seperation, alignment en cohesion.  
 Seperation houdt in dat een partikel (onderdeeltje van de zwerm) zijn bewegingsrichting aan moet passen als het risico dreigt op een botsing. Alignment vertelt hoe het partikel zijn richting aan moet passen aan die van zijn buren.  
 De laatste ‘cohesion’ zorgt ervoor dat er geen partikel los raakt van de zwerm.  
  
 Bij mijn virtuele zwerm is het wel mogelijk dat er partikels losraken van de zwerm. Er is sprake van een bepaalde aantrekkingskracht tussen de partikels die juist daardoor een zwerm blijven. De snelheid waarmee de zwerm zich beweegt is afhankelijk van de hoeveelheid bezoeker (hoeveelheid verstoring in de neutrale leefomgeving van de zwerm). De zwerm leeft namelijk in rusttoestand in een volledig witte omgeving. De zwerm zal reageren op alles wat niet wit is in zijn omgeving. Zo ontstaat er een min of meer ongecontroleerd gedrag tussen de zwerm en bezoeker. Echter zal de zwerm zich moeten gaan opsplitsen tussen meerdere verstoringen in zijn leefomgeving als er meerdere bezoekers aanwezig zijn.  
  
***DOEL**  
 De installatie is een groot experiment om te zien hoe een digitale levensvorm zich kan gedragen op een reële levensvorm en andersom. Het belangrijkste is het onderzoek hoe bezoekers zich gedragen en hoe ze zich voelen bij het ervaren van de installatie.*   
  
<object height="375" width="500"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=2124550&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="375" src="http://vimeo.com/moogaloop.swf?clip_id=2124550&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1" type="application/x-shockwave-flash" width="500"></embed></object>[Demo interactive installation Attack](http://vimeo.com/2124550) from [Sebastian Hagens](http://vimeo.com/sebastix) on [Vimeo](http://vimeo.com).

  
  
**Benodigdheden:**  
 3x witte schotten  
 1x PowerBook G4  
 1x beamer  
 1x lange VGA kabel  
 1x lange Firewire kabel voor DV-camera  
 1x haspel  
 1x DV camera  
 1x statief  
 1x DVI &gt; VGA verloopstekker  
 1x luidsprekers  
  
**Conclusie / persoonlijke evaluatie (januari 2008)**   
  
 Als student Interactie Ontwerpen was deze minor ook een enorme uitdaging. Zelf ben ik me nog maar half bewust wat voor mogelijkheden er tegenwoordig tot je beschikking staan als het gaat om techniek en de interactie mogelijkheden tussen mens en machine. Ik keek daarom ook erg uit naar de minor om aan de slag te gaan met voor mij onbekende sensoren en eens OffScreen te gaan werken.  
 Toch is het niet helemaal van het scherm af gegaan in mijn geval. Ik ben vanaf het begin af aan al aan de slag gegaan met processing. Eerst heb ik gekeken hoe dit programma in elkaar zit en later heb ik de koppeling gemaakt naar externe sensoren met behulp van het Arduino bordje. De kracht van sensoren gekoppeld aan processing om deze te programmeren is sterk. Ik heb meer inzichten gekregen in verschillende parameters die je kunt afvangen bij je gebruiker. Dit hoeft namelijk lang niet altijd een input te zijn die via een scherm binnenkomt via de computer, maar kan net zo goed een externe sensor zijn gekoppeld aan je Arduino.  
 Verschillende ideeën zijn mijn hoofd gepasseerd en ik vond het moeilijk om echt sterk in een concept te gaan staan wat ik wilde gaan doen. Het meest fascinerende vond ik altijd nog de gewone camera en hoe je bepaalde dingen kunt afvangen in je beeld. Vanuit dit stukje techniek ben ik een concept gaan maken. Moeizaam ging dat wel.  
 Helemaal Offscreen ben ik niet gegaan in deze minor, want mijn uiteindelijke presentatie vind toch plaats met een beamer. Met de camera kan ik veel dingen tracken binnen het gezichtsveld en op deze dingen heb ik vervolgens een virtuele levensvorm losgelaten. Samen met ook geluid heb ik geprobeerd de toeschouwer een onaangenaam gevoel mee te geven dat deze wordt aangevallen in een virtuele ruimte. Ik probeer de kloof tussen de reële en virtuele wereld hiermee te verkleinen en bij de toeschouwer ervaring / herinnering op te wekken dat hij of zij aangevallen wordt en zich moet verdedigen.  
 Helemaal doen slagen in dit doel lukt niet, want men weet dat het een opgezette omgeving is en jouw fysieke aanwezigheid nog altijd in de reële wereld is en niet in de virtuele wereld. Toch doet het wel iets met je als je het ervaart.  
 De minor heeft me veel nieuwe inzichten gegeven in de mogelijkheden op het gebied van verschillende manieren interactie tussen mens en machine. Zo kan ik in de toekomst vanuit een breder perspectief gaan denken om een mogelijke interface te maken om bepaalde data tot de gebruiker te laten komen bijvoorbeeld. Dit hoeft namelijk lang niet altijd met een toetsenbord en muis
