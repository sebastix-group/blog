---
title: "SIARR - visualisaties"
date: "2007-10-01T11:30:28.000Z"
description: "Visualisaties van de werkwijze van SIARR











Wat nog uit te werken?
- Gebruiksaanwijzing van SIARR
..."
categories: [internet]
comments: true
---

Visualisaties van de werkwijze van SIARR  
![werking flowchart](http://www.interactieontwerpen.nl/_sebastian/siarr/flowchart_werking.jpg)  
  
  
![](http://www.interactieontwerpen.nl/_sebastian/siarr/1_meetfase_0.jpg)  
![](http://www.interactieontwerpen.nl/_sebastian/siarr/2_visualfase_0.jpg)  
![](http://www.interactieontwerpen.nl/_sebastian/siarr/3_visualfase_1.jpg)  
![](http://www.interactieontwerpen.nl/_sebastian/siarr/4_analysefase_0.jpg)  
![](http://www.interactieontwerpen.nl/_sebastian/siarr/5_analysefase_1.jpg)  
![](http://www.interactieontwerpen.nl/_sebastian/siarr/6_analysefase_2.jpg)  
![](http://www.interactieontwerpen.nl/_sebastian/siarr/7_analysefase_3.jpg)  
  
 Wat nog uit te werken?  
\- Gebruiksaanwijzing van SIARR  
\- Toegepast op 3 mogelijke scenario's; gevisualiseerd  
  
 De scenario's vereenvoudigen. De geleerde handelingen blijven simpel in een omgeving.  
  
 Wat nog meer gevolgen heeft de aanwezigheid van SIARR? Zoals de Wii:  
<http://www.wiihaveaproblem.com/>  
  
 PFtrack; programma waar video omgezet kan worden in een 3D omgeving met tracking punten  
<http://www.thepixelfarm.co.uk/products/products.aspx?PID=3>  
  
<http://nl.wikipedia.org/wiki/Autopoiese>  
<http://www.vrealities.com/P5.html>  
<http://www.vrealities.com/i-visionhrv.html>  
<http://www.vrealities.com/hmd.html>  
<http://www.vrealities.com/headtrackers.html>
