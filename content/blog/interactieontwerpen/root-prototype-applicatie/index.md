---
title: "root:\ prototype applicatie (video)"
date: "2010-09-08T17:04:56.000Z"
description: "

Dit is een screencast van de door mij ontwikkelde root:\ desktop applicatie (http://www.myroot.me). Het is een proto..."
categories: [internet]
comments: true
---

<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="391" width="625"><param name="id" value="scPlayer"></param><param name="quality" value="high"></param><param name="bgcolor" value="#FFFFFF"></param><param name="flashVars" value="thumb=http://content.screencast.com/users/Sebastix/folders/Jing/media/fbe4bc78-a454-426a-8f6c-e1a8145905e1/FirstFrame.jpg&containerwidth=1920&containerheight=1200&content=http://content.screencast.com/users/Sebastix/folders/Jing/media/fbe4bc78-a454-426a-8f6c-e1a8145905e1/00000005.swf&blurover=false"></param><param name="allowFullScreen" value="true"></param><param name="scale" value="showall"></param><param name="allowScriptAccess" value="always"></param><param name="base" value="http://content.screencast.com/users/Sebastix/folders/Jing/media/fbe4bc78-a454-426a-8f6c-e1a8145905e1/"></param><param name="src" value="http://content.screencast.com/users/Sebastix/folders/Jing/media/fbe4bc78-a454-426a-8f6c-e1a8145905e1/jingswfplayer.swf"></param><embed allowfullscreen="true" allowscriptaccess="always" base="http://content.screencast.com/users/Sebastix/folders/Jing/media/fbe4bc78-a454-426a-8f6c-e1a8145905e1/" bgcolor="#FFFFFF" flashvars="thumb=http://content.screencast.com/users/Sebastix/folders/Jing/media/fbe4bc78-a454-426a-8f6c-e1a8145905e1/FirstFrame.jpg&containerwidth=1920&containerheight=1200&content=http://content.screencast.com/users/Sebastix/folders/Jing/media/fbe4bc78-a454-426a-8f6c-e1a8145905e1/00000005.swf&blurover=false" height="391" id="scPlayer" quality="high" scale="showall" src="http://content.screencast.com/users/Sebastix/folders/Jing/media/fbe4bc78-a454-426a-8f6c-e1a8145905e1/jingswfplayer.swf" type="application/x-shockwave-flash" width="625"></embed></object>  
  
 Dit is een screencast van de door mij ontwikkelde root:\\ desktop applicatie (<http://www.myroot.me>). Het is een prototype / proof-of-concept (met nog teveel bugs en te weinig functionaliteit), dus helaas nog niet beschikbaar voor iedereen om gebruik van te maken.
