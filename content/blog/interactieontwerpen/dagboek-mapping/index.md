---
title: "Dagboek mapping"
date: "2007-11-25T20:40:22.000Z"
description: "
Met een GPS ontvanger heb ik één dag bij me gehouden en de route opgeslagen die ik die dag heb afgelegd. Verder wat t..."
categories: [internet]
comments: true
---

![](http://www.interactieontwerpen.nl/_sebastian/previewImages/dagboek.jpg)  
Met een GPS ontvanger heb ik één dag bij me gehouden en de route opgeslagen die ik die dag heb afgelegd. Verder wat tekst toegevoegd wat me is bijgebleven die dag met wat foto's van dingen en/of lokaties die het meest relevant waren.  
  
  
  
[![dagboek mapping](http://interactieontwerpen.nl/_sebastian/mapping/dagboek/finaldagboek.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/dagboek/finaldagboek.jpg "dagboek")
