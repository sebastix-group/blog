---
title: "Digitaal beeld - onderzoek"
date: "2007-09-21T18:07:17.000Z"
description: "Tags:
honda, civic, integra, type-r, d16, b16, b18, d14, k20, f20, vtec, ek9, eg6, ee9, mentalism, crx, turbo, circuit, ..."
categories: [internet]
comments: true
---

*Tags:  
honda, civic, integra, type-r, d16, b16, b18, d14, k20, f20, vtec, ek9, eg6, ee9, mentalism, crx, turbo, circuit, sprint, 1/8, 1/4, mugen, spoon, VTi, JDM*  
  
  
http://www.youtube.com/watch?v=f9xqVTVMF98 (type-r battle circuit)  
http://www.youtube.com/watch?v=fBS-wKwkiAU (Honda impossible dream)  
http://www.youtube.com/watch?v=GuyaVcqTgic (Honda Choir Civic)  
http://www.youtube.com/watch?v=m\_86RuYXoJA (Chrash test)  
http://www.youtube.com/watch?v=yDbX8I202VU (Japanse verslaggever)  
http://www.youtube.com/watch?v=5pQDHrulux4 (Honda Acura NSX promo)  
http://www.youtube.com/watch?v=LpiASVaLo0U  
http://www.youtube.com/watch?v=uFKqVsWR4S4 (diesel motor promo)  
http://www.youtube.com/watch?v=zYZXacgvyMI (Mugen Dominator)  
http://www.youtube.com/watch?v=XnqZlhce\_fk (Civic Type RR)  
http://www.youtube.com/watch?v=Rl8T9KFgjWc (Spoon EK9 - japans)  
http://www.youtube.com/watch?v=MiA5a3UGLkw (EK9)  
http://www.youtube.com/watch?v=BUWA3Y5iSsM (EK9 sound)  
http://www.youtube.com/watch?v=5ngR8qLCTZg (Japanse streetrace part1)  
http://www.youtube.com/watch?v=b6TgUtSdgwo (Japanse streetrace)  
http://www.youtube.com/watch?v=sM4nhxA5hQs (Japanse drifting)  
http://www.youtube.com/watch?v=Uf2QoY7IXkE (Japanse illegal streetracing)  
http://www.youtube.com/watch?v=Pyf7tFojvkM (streetracing Japan)  
http://www.youtube.com/watch?v=ImGTIo4hCUU (streetrace compilatie)  
http://www.youtube.com/watch?v=mpkEwvx-d6U (build-up foto's)  
http://www.youtube.com/watch?v=sW2ZvLCQo84 (Honda mentalism)  
http://www.youtube.com/watch?v=SUsDi8Aqjes (Honda Atom)  
http://www.youtube.com/watch?v=8ysc7KYzjBQ (grasmaaier racing)  
http://www.youtube.com/watch?v=42qg34u8H8Q (grasmaaier racing 2)  
http://www.youtube.com/watch?v=VuozotZ61v0 (grasmaaiers)  
http://www.youtube.com/watch?v=UGY92JFMT7E (grasmaaiers)  
http://www.youtube.com/watch?v=eWieNovUBiQ (wooo die sedan gaat vet hard!)  
http://www.youtube.com/watch?v=ezh07W-jplk  
http://www.youtube.com/watch?v=lNplFsbK1Pc (NSX &amp; S2000)  
http://www.youtube.com/watch?v=SKeL6gHStko (k20 eg dyno)  
http://www.youtube.com/watch?v=lA1alTWnXO4 (Seattle)  
http://www.youtube.com/watch?v=UEzSQRJURyk (k20 marco)  
http://www.youtube.com/watch?v=EjNS951-ojs (EK vs EK mountains night)  
http://www.youtube.com/watch?v=zUKcjK6XuMM (initial D style close down)  
http://www.youtube.com/watch?v=Pda\_nzfrYY0 (japans battle)  
http://www.youtube.com/watch?v=Vu\_pyi8j0N0 (JDM super battle)  
http://www.youtube.com/watch?v=\_0FfRkQrnMU (gekke japanners D1 drift)  
http://www.youtube.com/watch?v=-scqkOQB4iY (japanners &amp; honda's)  
峠スペシャルバトル Japanse / Chinese filmpjes:  
http://www.youtube.com/watch?v=IQT8jqTrqcU  
http://www.youtube.com/watch?v=oM\_cKiRkP48 (vette oldschool)  
http://www.youtube.com/watch?v=QTj\_nLEHCeQ  
http://www.youtube.com/watch?v=0DsV0Td\_CLM  
http://www.youtube.com/watch?v=1ASzCZl2yKM  
http://www.youtube.com/watch?v=Ukg8wDjZDMU  
http://www.youtube.com/watch?v=lBoq3JqFhjY  
http://www.youtube.com/watch?v=kwzc8lmU1lQ (mafketels)  
http://www.youtube.com/watch?v=zf14LilntxI (trailer)  
http://www.youtube.com/watch?v=WxH7rZMF76c  
http://www.youtube.com/watch?v=ekytfwgtxAU (jaja, driften met je fiets)  
http://www.youtube.com/watch?v=PLxUYjAeEKQ (another oldskool JDM)  
http://www.youtube.com/watch?v=f4YqlzhXA-4
