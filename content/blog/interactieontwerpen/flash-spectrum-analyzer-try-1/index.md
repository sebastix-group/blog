---
title: "Flash spectrum analyzer - try #1"
date: "2007-11-20T21:36:01.000Z"
description: "Klik op de link hieronder om de spectrum analyzer te openen:
http://interactieontwerpen.nl/_sebastian/mapping/flash/try..."
categories: [internet]
comments: true
---

Klik op de link hieronder om de spectrum analyzer te openen:  
[http://interactieontwerpen.nl/\_sebastian/mapping/flash/try1.swf](http://interactieontwerpen.nl/_sebastian/mapping/flash/try1.swf)  
  
 Eerste experiment resultaat om met een stuk code een ***data-set*** <span style="font-size: xx-small;">(een audiobestand)</span> om te zetten naar een visueel beeld <span style="font-size: xx-small;">("map")</span> met actionscript 3.0. Het audio bestand is één van de opnames die ik heb gemaakt voor mijn eerder gemaakte audiomap van de academie. De vorm die je nu ziet bij dit experiment vertelt nog helemaal niets over het geluid, lokatie of iets dergelijks. De context ontbreekt. Het is de bedoeling om dit straks wel naar een visualisatie te vertalen die meer vertelt dan alleen "oh, een *cool* plaatje!"
