---
title: "Scriptie: digitale levenslijn idee"
date: "2009-11-24T11:52:55.000Z"
description: "Ik wil een digitale levenslijn kunnen laten zien van mij over een flink aantal jaren. Deze digitale levenslijn maakt inz..."
categories: [internet]
comments: true
---

Ik wil een digitale levenslijn kunnen laten zien van mij over een flink aantal jaren. Deze digitale levenslijn maakt inzichtelijk wat mijn interesses waren op bepaalde momenten, waar ik actief was en welke data ik achterliet  
 Waarom? Het is belangrijk om mijn belangrijkste momenten op elk moment terug te kunnen zien, net zoals het ook altijd leuk is om je foto plakboek va 10 jaar terug te bekijken.  
  
  
  
 Vorige week heb ik te weinig gedaan aan mijn scriptie, maar het blijft wel actief binnen mijn hoofd. Uit de gemaakte mogelijke hoofdstukindeling en onderzoeksvraag keuze / stelling kwam voor mij al snel naar voren dat ‘Archiveren’ van websites nog steeds veel te breed is. Er zijn namelijk echt teveel websites te vinden die onder die noemer vallen! Met dit probleem bleef ik dus in mijn hoofd zitten, want het moet specifieker worden in mijn te kiezen richting van websites. Wat doe ik dan al snel? Naar mezelf kijken en vanuit daar vertrekken, wat zou ik interessant vinden wat per direct gearchiveerd moet worden om straks over 20 jaar terug te zien. Goeie vraag! Het deed me denken aan een soort van digitale levenslijn die ik dan voor mezelf zou willen uitzetten. Een verzameling van mijn digitale activiteiten (welke automatisch data generen) vanaf het moment dat ik actief ben in de digitale wereld tot aan nu. Ik kan me heel goed voorstellen dat daar iedereen wel interesse in heeft, om een soort beeld (archief) te krijgen van jouw digitale levenslijn die veel antwoorden kan geven op:

  
- Op welke virtuele locaties ben ik actief?
  
- Wat doe ik daar?
  
- Wanneer was ik daar?
  
- Wat heb ik allemaal gezegd?
  
- Welke beelden laat ik achter?
  
- Wat was mijn doel wat ik daar deed?
  
- Wat zijn de belangrijkste momenten geweest voor mij?
  

  
 Het sluit ergens aan bij de vorming van een digitale identiteit (vertrokken vanuit je digitaal verleden), net zoals we deze hebben in onze fysieke omgeving. Ik wil ook gelijk even een bruggetje maken naar de fysieke wereld met deze vraagstelling. Zolang we hier rondlopen, doet iedereen wel iets voor zichzelf waar hij bepaalde fysieke data opslaat (fotoboeken, film dv-bandjes etc). Vaak blijven de meest indrukwekkende momenten heel vers in je eigen geheugen hangen als herinneringen (vaak weet je ook dat je zo'n moment tegemoet gaat, zoals een weekend weg naar Barcelona). Zulke intense momenten zul je niet vaak meemaken nu in de digitale wereld, omdat de beleving nog niet zo sterk is dat al je mogelijke zintuigen worden geprikkeld (als al je zintuigen heel intens worden geprikkeld op een bepaald moment, zal het moment beter worden opgeslagen). Je zintuigen zijn dus een soort criteria voor je fysieke momenten die mogelijk worden gearchiveerd of juist niet. Als we terug kijken naar het archiveren van jouw digitale momenten, moet er ook een criteria worden gehanteerd. Het gaat uiteindelijk niet om de kwantiteit van deze data, maar de kwaliteit. Welke is echt relevant geweest naar jou persoonlijk toe, heeft indruk gemaakt? Net zoals er tussen jou als persoon en een belevingsmoment je zintuigen zitten (als criteria), moet er dat tussen jou als persoon en een digitale beleving ook criteria zitten. Het is heel goed mogelijk dat deze criteria digitaal technisch van aard moet zijn, net zoals je zintuigen dat zijn als een soort natuurlijke, biologisch instrument waar je geen controle op hebt.  
  
 Na het lezen van een verslag van een lezing van Lev Manovich van het Virtueel Platform (<http://www.virtueelplatform.nl/print/2610>) herkende ik ook daar de behoefte naar een eigen persoonlijk ‘spoor’ welke wordt achtergelaten door elk individu die virtueel actief is. Hij verteld daar ook dat bewegingen van miljarden mensen kunnen worden gevisualiseerd aan de hand van een representatie van dit digitale spoor in het jaar 2030.  
 Manovich stelt dat een cultuuranalyse (door hem gemaakt) in de toekomst steeds meer zich zal richten op het herkennen van patronen in datastromen. Hieruit volgen dan visualisaties die deze nieuwe patronen inzichtelijk maken en ons een nieuwe kijk op zo’n bepaalde datastroom te geven.  
 Manovich verwijst ook naar Lee Byron's visualisatie van zijn persoonlijke luistergedrag (<http://www.leebyron.com/what/lastfm/>) waar een visualisatie te zien is over een horizontale lijn waar hij allemaal naar heeft geluisterd op Last.fm:  
![](http://www.leebyron.com/what/lastfm/poster.jpg "last.fm")  
 Kleuren zijn hier gekoppeld aan artiesten. Je kunt uit deze visualisatie verschillende conclusies trekken in één oogopslag. Tenminste, als de artiesten grafisch beter waren gekoppeld aan de kleuren…  
 Onder andere zou deze doelstelling ook mee moeten worden genomen in mijn onderwerp die Manovich hier stelt.  
  
**Nog een reden; waarom een digitale levenslijn?**  
 Door de komst van steeds meer sociale media! Door deze sociale media produceren we digitale informatie die van oorsprong ontstaat op het internet. Deze data is digitaal, niet analoog! Het is dus lastig om nu een fysieke bron aan te wijzen, deze is er namelijk gewoonweg niet. Het ontstaan van dit soort informatie is digitaal oorspronkelijk van aard. Voor fysieke, analoge data hebben we al structuren om te archiveren, maar voor digitale data een stuk minder.  
 Enkele voorbeelden van digitaal oorspronkelijk data zijn alle social media platforms (Twitter, Facebook, Flickr, YouTube en nog vele andere), weblogs, proccesing.org &amp; openframeworks projecten, iPhone applicaties, computer games, internet fora. De hoeveelheid aan digitaal oorspronkelijk data is ondertussen al vele malen groter dan de analoog oorspronkelijk data. En tussen alle genoemde voorbeelden kan ik me bijna niet voorstellen dat de huidige jongeren generatie en de generaties die volgen, nergens een virtuele positie hebben ingenomen en daar actief zijn.
