---
title: "wat is het hier stil"
date: "2011-06-03T09:44:39.000Z"
description: "Inderdaad, het is hier stil. Mijn meest recente post gaat bijna al zes maanden terug. De stilte heeft een positieve r..."
categories: [internet]
comments: true
---

Inderdaad, het is hier stil. Mijn meest recente post gaat bijna al zes maanden terug.  
  
 De stilte heeft een positieve reden; ik heb het onzettend druk met diverse projecten en sinds januari zijn er een aantal zaken in een stroomversnelling gekomen. Daar waar ik zes maanden terug nog bezig was met een WWIK, moet ik deze nu al gaan stopzetten.  
 Het zet mij ook aan het denken wat ik nu precies met deze blog ga doen, deze blog is mijn persoonlijke uitlaatklep als ontwerper/ondernemer. Mijn Twitter is op dit vlak veel handiger en actueler waar ik diverse zaken deel die me bezighouden. De materie rondom persoonlijke data en online privacy blijven me ook ontzettend boeien, hierover schrijf ik meer op <http://www.myroot.me>. Daarnaast ben ik ook druk bezig om rootapp te gaan introduceren als applicatie die in het verlengde ligt van de root-gedachtegoed. Genoeg in de pijplijn met een heleboel andere projecten.  
  
 Verwacht komende tijd hier geen updates van mijn kant. Toch weten wat me bezighoudt? Volg me op Twitter en dan ben je zeker als eerste op de hoogte.
