---
title: "Huis van de toekomst - onderzoek media links"
date: "2007-09-21T18:08:54.000Z"
description: "Eerste onderzoeksbeelden bij opdracht 'Huis van de toekomst'.


http://www.hethuisvanmorgen.nl/ (KPN)

[flash http:..."
categories: [internet]
comments: true
---

Eerste onderzoeksbeelden bij opdracht 'Huis van de toekomst'.  
  
  
<http://www.hethuisvanmorgen.nl/> (KPN)  
  
 \[flash http://www.youtube.com/watch?v=zEax1mJhJQ0\]  
 (Warcraft 3 interface)  
  
 \[flash http://www.youtube.com/watch?v=lP2qCDCBv2w\]  
 (HTC hands on)  
  
 \[flash http://www.youtube.com/watch?v=7mVuy9iQqRM\]  
 (house of future impress)  
  
 \[flash http://www.youtube.com/watch?v=l2oMmCyiJZA\]  
 (multi touch table)  
  
 \[flash http://www.youtube.com/watch?v=d-htzy0xZpo\]  
 (Asimo)  
  
 De schakelaar:  
<http://nl.wikipedia.org/wiki/Schakelaar>  
<http://tweakers.net/nieuws/47508/Japanners-ontwikkelen-nieuw-type-optische-schakelaar.html>
