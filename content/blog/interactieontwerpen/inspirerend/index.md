---
title: "Inspirerend..."
date: "2009-04-16T13:16:58.000Z"
description: "World Builder from Bruce Branit on Vimeo.

 ..."
categories: [internet]
comments: true
---

<object height="282" width="500"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=3365942&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="282" src="http://vimeo.com/moogaloop.swf?clip_id=3365942&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1" type="application/x-shockwave-flash" width="500"></embed></object>  
[World Builder](http://vimeo.com/3365942) from [Bruce Branit](http://vimeo.com/user1349603) on [Vimeo](http://vimeo.com).  
  
<object height="326" width="446"><param name="movie" value="http://video.ted.com/assets/player/swf/EmbedPlayer.swf"></param><param name="allowFullScreen" value="true"></param><param name="wmode" value="transparent"></param><param name="bgColor" value="#ffffff"></param><param name="flashvars" value="vu=http://video.ted.com/talks/embed/JoAnnKuchera-Morin_2009-embed_high.flv&su=http://images.ted.com/images/ted/tedindex/embed-posters/JoAnnKuchera-Morin-2009.embed_thumbnail.jpg&vw=432&vh=240&ap=0&ti=516"></param><embed allowfullscreen="true" bgcolor="#ffffff" flashvars="vu=http://video.ted.com/talks/embed/JoAnnKuchera-Morin_2009-embed_high.flv&su=http://images.ted.com/images/ted/tedindex/embed-posters/JoAnnKuchera-Morin-2009.embed_thumbnail.jpg&vw=432&vh=240&ap=0&ti=516" height="326" pluginspace="http://www.macromedia.com/go/getflashplayer" src="http://video.ted.com/assets/player/swf/EmbedPlayer.swf" type="application/x-shockwave-flash" width="446" wmode="transparent"></embed></object>
