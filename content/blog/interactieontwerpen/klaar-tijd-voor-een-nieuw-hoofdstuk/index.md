---
title: "Klaar, tijd voor een nieuw hoofdstuk"
date: "2010-08-03T13:06:04.000Z"
description: "Mijn hoofd zit voller dan ooit met ideeën. Sinds zondag ben ik terug van een tweeweekse vakantie waarin we met z’n tw..."
categories: [internet]
comments: true
---

M[![](http://files.droplr.com.s3.amazonaws.com/files/41343510/1thYbR.Schermafbeelding%202010-08-03%20om%2010.53.40.png "Frankrijk trip 2010")](http://drp.ly/1thYbR)ijn hoofd zit voller dan ooit met ideeën. Sinds zondag ben ik terug van een tweeweekse vakantie waarin we met z’n tweeën (mijn vriendin en ik) meer dan 3000km door Frankrijk hebben afgelegd. Voordat ik op vakantie ging ben ik ook -eindelijk-geslaagd op de kunstacademie AKV|St.Joost aan de opleiding Interactie Ontwerpen. Mijn studietijd zit er (voorlopig) op. Ik keek er echt naar uit om eindelijk verlost te zijn van ‘verplichtingen’ opgelegd door een ander die niet altijd evenredig aansloten op mijn eigen overtuigingen, onderbuikgevoelens en externe factoren waar ik rechtstreeks mee in verbinding stond. Dit gevoel groeide in de laatste half jaar ‘afstuderen’ omdat er een fantastische deur werd geopend in november vorig jaar die het avontuur van zelfstandig ondernemer liet en nog steeds laat zien. Desalniettemin ben ik de tijd die ik heb mogen doorbrengen op de academie zeer dankbaar, want door alles wat de academie mij heeft geleerd ben ik dichter tot mijzelf gekomen (wie ik ben, wat ik wil en wat ik kan).  
  
  
  
**Root:\\**[![](http://root.sebastix.nl/images/root-logo-01.png "root")](http://www.myroot.me)  
 De afgelopen maanden in mijn afstudeerperiode heb ik alles op non-actief gezet wat mij bezighield als niet-student zijnde, maar als hobbyist/ondernemer zijnde. Dat was nodig, want ik moest nog flink wat werk verzetten om uiteindelijk af te kunnen studeren met een scriptie en eigen project. Met mijn eindexamenwerk zijn er veel verbindingen (interacties) tot stand gekomen die Root beter hebben gemaakt dan wanneer ik dit alleen had uitgewerkt. Hiermee bedoel ik de samenwerkingsverbanden (oa [Amanda Butterworth](http://www.amandabutterworth.com), [Stylism](http://www.stylism.nl)) die ik ben aangegaan en alle andere input die ik heb gehad van mensen van buiten de academie. Deze input die van buiten naar binnen werkte heeft Root een professioneel aanzicht gegeven. Samen met de overtuiging waar ik samen met en in dit project voor sta, geloof ik daarom ook in een toekomst voor Root. Deze toekomst is afhankelijk van mijn inzet en motivatie. Voor hoe lang? Jaren en misschien wel voor de rest van mijn leven. Ik zie namelijk een enorm potentieel (kansen) voor Root waar ik me onvoorwaardelijk voor wil inzetten. Waar alles nu staat is een beginpunt van een groter proces waarin ik verder wil. Maar dit zal ik niet alleen kunnen. De nodige contacten zijn al gelegd om Root tot een entiteit te maken en nieuwe contacten worden nog steeds opgedaan. Binnenkort meer hierover, want succesvol worden en blijven is een langer termijn verhaal en draait om mijn persoonlijke overtuiging zonder concessies te doen.  
  
**Anderen**  
 Met Root zal ik nog geen heerlijk belegde boterham kunnen eten, dus het blijft voor mij belangrijk om me bezig te houden met anderen en wat zij willen. Dit is mijn eigen netwerk waarin ik een rol van betekenis kan spelen door mijn eigen kennis over te brengen en toe te passen waarin zowel ik als de ander beter van wordt. Op de agenda staan een aantal leuke websites die ik mag maken en wie weet komen er nog vele andere interessante bezigheden naar mij toe.  
 In deze rol zal ik me dus blijven profileren als Sebastix – Interactie Ontwerper.  
  
**Inspiratie**[![](http://www.epo.be/covers/maven/9789490574031.jpg "De  ultieme kudde")](http://www.selexyz.nl/product/9789490574031/mark-earls/de-ultieme-kudde/)  
 Mijn ideeën worden veelal gevoed door bronnen. Deze bronnen zijn verschillend van aard maar ik merk dat ik me steeds meer open stel voor boeken waarin interessante perspectieven worden geschetst gericht op veelal onderwerpen rondom marketing. Het boek waardoor ik me momenteel laat inspireren is ‘De ultieme kudde’ van Mark Earls. In het begin van dit boek schetst Mark onze natuurlijke aard door de vergelijking te trekken met de aap. Bijzonder interessante benadering hierin is dat wij van nature sociale dieren zijn en dat de hele Westerse cultuur met haar toenemende illusie van individualisering hier eigenlijks haaks op staat. De kern van dit punt is dat wij dagelijks bezig zijn met menselijke interacties en dat alles wat wij scheppen (cultuur) op deze basis is gebouwd. Toch zijn er maar weinig begrijpbare modellen voor deze interacties (er zijn veel te veel marketingstrategieën die maar half werken) en zijn er nog minder bedrijven en/of personen die vanuit dit inzicht werken. Dit gegeven verbaast mij ook dat eigenlijk de term ‘Interactie Ontwerper’ relatief onbekend is of eigenlijk nooit bestaan heeft, terwijl we juist leven in een omgeving waarin interacties de basis zijn van ons bestaan vanuit zowel een natuurlijk als een cultureel perspectief.  
  
 Verdere onderwerpen waar ik binnenkort dieper op in zal proberen te gaan in de context van een eigen project en/of passie:

  
- onze sociale sporen
  
- overvloed en schaarste (verleden – nu – toekomst)
  
- de relevantie van een context
  
- authentiek gedrag
  

  
 Maar nu wordt het eerst tijd om weer flink wat werk te verrichten nu ik alles even van me af heb geschreven wat er zoal speelde in mijn hoofd.
