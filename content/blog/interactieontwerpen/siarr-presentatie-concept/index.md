---
title: "SIARR - presentatie concept"
date: "2007-10-09T20:47:06.000Z"
description: "De gebruiksaanwijzing van SIARR legt uit hoe de SIARR werkt en functioneert binnen het huis. Leer de SIARR simpele, rout..."
categories: [internet]
comments: true
---

De gebruiksaanwijzing van SIARR legt uit hoe de SIARR werkt en functioneert binnen het huis. Leer de SIARR simpele, routineklusjes doen zodat de bewoners meer kunnen rusten en zich opnieuw kunnen opladen voor de volgende dag werken.  
  
 Ik heb hiervoor een gebruiksaanwijzing voor de SIARR in elkaar gezet, zodat het voor mensen die niet weten wat mijn gehele concept is beter duidelijk wordt wat deze is. De gebruiksaanwijzing is in zo'n vorm gegoten zodat deze eigenlijk al verkocht zou kunnen worden.  
  
  
[Bekijk Gebruiksaanwijzing SIARR](http://interactieontwerpen.nl/sebastian/files/2007/10/gebruiksaanwijzingsiarr.pdf "Gebruiksaanwijzing SIARR")  
  
 Een reclamespotje kan wellicht het beste vertellen wat mijn uitgangspunt is.  
**Wat is mijn uitgangspunt voor dit spotje?**  
*Men heeft een SIARR nodig in elk huis, anders verliest men zichzelf in de wereld buiten het huis.*  
  
**Concepten reclamespotje**  
 Vorm: (interactief) filmpje door de ogen van de robot  
  
 1 - we zien een vrouw stofzuigen  
 2 - vrouw is moe en wilt rusten  
 3 - we kijken door de ogen van de vrouw of robot (is nog onduidelijk voor de kijker)  
 4 - de wereld krijgt een virtuele wereld erbij (digitale informatie)  
 5 - we zien de vrouw op de bank genieten van de rust  
 6 – opnieuw het beeld van stofzuigen met de digitale informatie (augmented reality view)  
 7 – concept visualisatie van SIARR verschijnt met de tekst: “de wereld wordt een stuk aangenamer door uw SIARR (concept)” - ik moet hier nog een betere pakkende slogan voor verzinnen  
  
[Bekijk mijn reclame spotje concept](http://interactieontwerpen.nl/sebastian/files/2007/10/spotjesiarr.swf "Reclame spotje concept")  
  
 Samen heb ik dus nu een gebruiksaanwijzing en een reclame spotje voor de SIARR. Echter kom ik visualiserend achter dat het hele systeem ontzettend veel valkuilen heeft. Teveel valkuilen. Hele simpele handelingen zou SIARR wel kunnen doen, maar omdat het huis eigenlijk elke dag anders is; is dit een stuk ingewikkelder dan het lijkt. Uit de ene handeling komt vaak een andere handeling voort; er vormt zich een groot netwerk van handelingen door de SIARR die elkaar gaan overlappen en uiteindelijk onverwachte conflicten gaat opleveren.  
  
 Ik heb nu 1 kant uitgewerkt; de kant vanuit de positieve ontwerper en met de gedachte dat de techniek ons echt gaat helpen en ons de rust in het huis gaat bieden en creeeren. Nu ga ik verder met de andere kant, de flop. De SIARR wordt 1 grote flop in de maatschappij. Hoe ga ik dat visualiseren? Met nieuwsberichten, speciale websiteberichten, mogelijke eigen scenario's in mijn eigen huis etc etc. Dat is wat me te doen staat deze week.
