---
title: "Kamers van vlees"
date: "2007-12-04T18:00:52.000Z"
description: "
Wat doet vlees met ons? Hoe reageren wij erop? Welke emoties kan vlees bij ons losmaken? Het is aan de bezoeker om die ..."
categories: [internet]
comments: true
---

![](http://interactieontwerpen.nl/_sebastian/previewImages/vlees2.jpg)  
Wat doet vlees met ons? Hoe reageren wij erop? Welke emoties kan vlees bij ons losmaken? Het is aan de bezoeker om die vragen voor zichzelf te beantwoorden.  
  
In mijn [vorige blog](http://interactieontwerpen.nl/sebastian/2007/11/20/beeld-en-techniek-interactief-tentoonstelling-vlees/) over dit concept vertelde ik dat ik de bezoekers willen leren spelen met vlees. Dit uitgangspunt is enigzins veranderd. Ik ben er nu op uit om mensen in een bepaalde situatie te dwingen waar ze worden aangewezen op één van hun zintuigen. Veel handelingen voert men onbewust uit. Net zoals het eten van vlees, maar als je het zicht wegneemt bij een persoon bij het eten van vlees vraagt die zichzelf gelijk af of het wel zo is of diegene wel vlees eet!?  
  
Hieronder heb ik een poging gedaan om het onderwerp vlees te categoriseren met een schema en daaronder in beeld.  
[![schema](http://interactieontwerpen.nl/_sebastian/mapping/beeldentechniek/schemaVlees.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/beeldentechniek/schemaVlees.jpg "schema")  
[![](http://interactieontwerpen.nl/_sebastian/mapping/beeldentechniek/categorien.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/beeldentechniek/categorien.jpg)  
  
**ZIEN**  
De bezoeker komt een kamer binnen met alleen maar vleesbeelden om zich heen. Het hele spectrum van het beeld met het onderwerp vlees komt op de bezoeker af. Het is de bedoeling dat de bezoeker uitgedaagd wordt om dichter naar de beelden te komen om ze beter te bekijken. De ruimte zal niet heel groot worden; 3 bij 5 meter.  
[![](http://interactieontwerpen.nl/_sebastian/mapping/beeldentechniek/zienkamerbehang.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/beeldentechniek/zienkamerbehang.jpg) [![](http://interactieontwerpen.nl/_sebastian/mapping/beeldentechniek/kamerZien1.jpg)](http://interactieontwerpen.nl/_sebastian/mapping/beeldentechniek/kamerZien1.jpg)  
  
**HOREN**  
Bij vlees zijn er ook verschillende geluiden denkbaar. Denk aan levend vlees (knorrende varkens), het doodmaken van vlees (krijsen), bereiden van vlees (het sudderen in de pan), vlees dat zichzelf verkoopt (reclame). In deze ruimte is verder dan ook niets te zien en is de vorm neutraal. Een klein kamertje van 2 bij 2 meter met enkel een speaker.  
  
**PROEVEN**  
De bezoeker gaat deze ruimte binnen zonder zicht. Hij/zij wordt geblinddoekt begeleid naar een aantal plaatsen om vervolgens verschillend vlees op te eten en te proeven. Het hoeft hier persé geen echt vlees te staan, maar je kunt ook iets eten wat lijkt op dat je vlees eet.  
  
**RUIKEN &amp; VOELEN**  
Deze twee zintuigen worden gecombineerd in een donkere ruimte waar je niets kunt zien. De geur van vlees (wisselend van rottend vlees tot een heerlijke maaltijd) overheerst en je dient je weg te vinden door te voelen. Deze ervaring zal enigszins benauwend zijn, omdat het vermogen van zicht is weggenomen door de zwarte "leegt" die voor je lijkt te zijn.
