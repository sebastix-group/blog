---
title: "Beeld en techniek - interactief concept &#8216;vlees&#8217;"
date: "2007-11-20T21:58:13.000Z"
description: "Opdracht:
Maak een demo van een interactief concept met een thema/onderwerp. Mijn uitgangspunt dient op een non-lineare ..."
categories: [internet]
comments: true
---

<u>Opdracht:</u>  
Maak een demo van een interactief concept met een thema/onderwerp. Mijn uitgangspunt dient op een **non-lineare manier** te worden gelezen door de ontvanger. Hoe er van punt A naar B wordt gegaan, staat niet van te voren vast. Deze weg is variabel tot een zekere mate en dient zelf door de ontvanger (onbewust) te worden gecreëerd.  
  
Onderwerp: **VLEES**  
  
  
Vlees is een begrip dat mensen gelijk doet denken aan een stuk materie wat je kunt opeten. Echter is vlees meer dan alleen dat. Elk levend organisme heeft wel een zekere vorm van vlees. Ik beperk me meer op het meest herkenbare vlees. Vlees als een samenstelling van spierenweefsel. Wat is nu de inhoud van vlees? Het is wat we dagelijks zien, een natuurlijke bekende vorm. Eigenlijk is vlees een verzamelwoord geworden van eten in onze samenleving. Kun je vlees altijd eten? Volgens mij wel. Of je er ziek van wordt is een 2e niet relevante vraag. Vlees is een heel ruim begrip voor een materiaal wat levende organismen (zoogdieren) met zich mee dragen. Ik als mens, draag vanaf mijn geboorte ook altijd vlees mee. Vlees kan zich herstellen, vlees kan verdwijnen.  
*<font size="1">Ander vlees: vruchtvlees</font>*  
  
<u>**Ik wil mijn toeschouwer laten spelen met vlees**</u>  
  
<u>Vlees in categorieën:</u>  
11. vlees als lusobject(meest estisch beeld ervan)
  
12. rauw vlees, maar bewerkt als product
  
13. vlees als spier (slachterij)
  
14. vlees als levend organisme
  
15. dood vlees
  
  
  
<u>Presentatie</u>  
Belangrijk in de demo die gemaakt wordt, is dat de <font size="3">gebruiker een handeling doet en deze ook begrijpt</font>. Het verband oorzaak – gevolg moet duidelijk zijn. Het liefst moeten alle **zintuigen** geprikkeld worden om zo dicht mogelijk bij de echte ervaring te komen die wordt gesimuleerd.  
Binnen een interactieve tentoonstelling dien ik als ontwerper verschillende <font size="3">doe-activiteiten te ontwerpen</font>.  
Wat zijn de doe-activiteiten met betrekking tot vlees?  
26. vangen
  
27. bereiden
  
28. consumeren
  
Ik wil mijn bezoekers laten spelen met het onderwerp vlees en ‘zelf’ laten ontdekken wat vlees nu eigenlijk echt is. ‘Zelf’ de betekenis laten ontdekken van het woord vlees in de ruimste zin van het woord.  
  
<u>Concept</u>  
<font color="#666666"><del datetime="2007-11-20T20:00:41+00:00">1:  
Ik ga de gevonden beelden naast elkaar zetten geselecteerd op de vorminhoud. Als ik deze recht naast elkaar ga zetten, ontstaat er een nieuw beeld met als hoofdonderwerp wat één aparte afbeeldingen ook heeft.</del>  
  
<del datetime="2007-11-20T20:15:34+00:00">2  
Elke bezoekers moet bij het begin van de tentoonstelling een keuze maken uit een beeld. Hiermee bepaalt hij/zij zijn eerste persoonlijke voorkeur voor een bepaald beeld binnen het onderwerp vlees.</del></font>  
  
<del datetime="2007-11-29T15:19:11+00:00">3  
Iedere bezoeker krijgt aan het begin van de tentoonstelling een stuk vlees mee. Met dit stuk vlees zal hij of zij gedurende de tentoonstelling enkele handelingen moeten verrichten.</del>  
  
4  
Samen met de beelden krijgt de bezoeker te maken met verschillende zintuigelijke ervaringen.
