---
title: "Eerste stap concept"
date: "2008-01-28T20:51:44.000Z"
description: "Het zevende kwartaal is een minor periode. Het is de bedoeling dat je een minor kiest buiten je eigen vakstudie, maar om..."
categories: [internet]
comments: true
---

Het zevende kwartaal is een minor periode. Het is de bedoeling dat je een minor kiest buiten je eigen vakstudie, maar omdat het voor ons interactie ontwerpers ook allemaal nieuw is, hebben we onze eigen minor gekozen. Deze minor gaan we van het scherm af een interactieve installatie maken, technieken onderzoeken, ermee experimenteren en nieuwe sofware leren. Het begin is gemaakt met een of meerdere concepten.  
  
  
**Mijn persoonlijke interesses:**  
 *- Augmented Reality*  
Begin dit schooljaar kwam ik in aanraking met het begrip augmented reality. Een gemengde wereld van de reële (zintuiglijke) wereld en een virtuele wereld (techniek). Ik zie het als een mogelijke extra dimensie die steeds belangrijker gaat worden in de toekomst. Wellicht een zesde zintuig, de techniek die ons meer informatie kan verschaffen en wij niet kunnen waarnemen met onze zintuigen (horen, voelen, proeven, zien, ruiken).  
  
*- Zintuigelijke belevingen (triggers, prikkelingen, emotie, ervaring)*  
Ik ben van mening dat een installatie vaak een bepaalde ervaring wilt oproepen bij de toeschouwer. Dit lukt het beste als je de toeschouwer op alle zintuiglijke vlakken kunt raken met je installatie. De combinatie van horen, zien, voelen, proeven en ruiken moeten in de juiste context worden toegepast om de toeschouwer zoveel mogelijk te prikkelen.  
 **Concepten:**  
  
*- Spel 1vs1 ‘control’*  
Een interactieve installatie werkt op input van een of meerdere toeschouwers of een ander gecontroleerd medium. Er is sprake van controle en een logische reactie na de actie (handeling). In een spel is het de bedoeling om de andere oncontroleerbaar te maken en de controle bij jou zelf te houden. De ander doet precies hetzelfde en gaat de strijd aan met jou acties. Je bestrijdt andermans acties met je eigen acties en daarom worden jouw acties reacties op acties en reacties van de ander.  
  
*- Reality meets your own virtuality*  
.1 - Een augmented reality wereld waar jij zelf met behulp van de techniek een extra dimensie kan toevoegen. Aantekeningen maken in je directe omgeving. Bijvoorbeeld een prikbord die in de realistische wereld leeg is, maar met jou eigen virtuele toevoeging persoonlijke boodschappen heeft. De realistische wereld een gedeelde wereld met andere mensen, de toegevoegde virtuele wereld is een persoonlijke en alleen voor jou toegankelijk.  
.2 - De virtuele wereld kan ook gekoppeld zijn aan de techniek die jou iets kan vertellen op dat moment en op die locatie. Zo kun jij niet de temperatuur in waardes voelen, een sensor wel. Idem voor de luchtvochtigheid etc.  
 *- Discotheek (lichten) ‘playground’*  
Ik ben zelf een fanatieke stapper in het weekend en discotheken zijn voor mij de beste locaties om even los te gaan uit de normale wereld. Persoonlijk zou ik het heel erg leuk vinden om iets toe te voegen in een discotheek aan het decor wat niet door de VJ achter de knopjes bepaald worden, maar door het publiek. Het meeste licht reageert op het geluid of op de knoppen van de VJ. Dit kun je misschien ook terugkoppelen aan het publiek. Het publiek is een massa en niet 1 persoon. Mensen bewegen en het meest directe contact tussen een bezoeker en de discotheek is de vloer. Met de vloer wordt meestal het minste gedaan dus ik zou het wel een uitdaging vinden om iets extra’s toe te voegen aan de vloer en een publiek die daar invloed op heeft.  
Tevens heeft iedereen wel een interface tot zijn beschikking in een discotheek; de mobiele telefoon. Het is wellicht mogelijk dat je met je mobiele telefoon een eigen VJ wordt in een discotheek.  
  
**Techniek:**  
  
Motion traction  
VR bril  
Wii controller ?  
Videocamera’s  
Sensoren  
Mobiele telefoon  
Arduino  
...  
  
**Interessante installaties/projecten:**  
  
<http://corpora.ycam.jp/en/outline.html>  
Sensoren, augmented reality  
Een extra realiteit (virtueel) wordt toegevoegd aan de realistische lokatie met behulp van allerlei sensoren die verschillende waardes meten zoals temperatuur, lichtintensiteit etc  
  
<http://www.electricmoons.com/>  
Witte ballonnen die een 3D grid vormen en die kan worden veranderd. Het is een indrukwekkend beeld door de vorm en grootte. Het is een prachtig schouwspel met een bijbehorende muziekstuk waar je heerlijk bij kunt relaxen en absorberen. Waar ligt de interactiviteit?  
  
<http://www.nextwall.de/content/projekt/taggedinmotion.php>  
Virtuele graffiti in realistische ruimtes. Met een VR bril en wat hulpmiddelen kun jij in de ruimte graffiti gaan maken.  
  
<http://youtube.com/watch?v=y6izXII54Qc>
