---
title: "Project Pipeline online"
date: "2009-04-12T14:22:54.000Z"
description: "
Het het tiende kwartaal van de studie Interactie Ontwerpen was het thema 'webdesign' en hebben we een groepsopdracht g..."
categories: [internet]
comments: true
---

![](/images/projectPipeline.jpg "Project Pipeline")  
 Het het tiende kwartaal van de studie Interactie Ontwerpen was het thema 'webdesign' en hebben we een groepsopdracht gehad. Een hele toegepaste, concrete opdracht die behoorlijk vernieuwend, innovatief en grensverleggend was. Dit wel bekeken vanuit het kunstacademie perspectief; een behoorlijke nieuwe weg die we hebben genomen met dit project. Er is namelijk een stuk software ontworpen en ontwikkeld; PROJECT PIPELINE.  
  
 DEMO VIDEO:  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="314" width="500"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="src" value="http://vimeo.com/moogaloop.swf?clip_id=4130226&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="314" src="http://vimeo.com/moogaloop.swf?clip_id=4130226&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1" type="application/x-shockwave-flash" width="500"></embed></object>  
[Project Pipeline application demo](http://vimeo.com/4130226) from [Sebastian Hagens](http://vimeo.com/sebastix) on [Vimeo](http://vimeo.com).  
  
[Klik hier om meer te lezen](http://interactieontwerpen.sebastix.nl/project-pipeline/)
