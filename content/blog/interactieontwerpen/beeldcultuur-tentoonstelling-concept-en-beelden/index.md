---
title: "Beeldcultuur - tentoonstelling concept en beelden"
date: "2007-09-27T12:11:12.000Z"
description: "Met onze tentoonstelling willen met héél veel beelden, zo weinig mogelijk vertellen.


Wat voor beelden hebben wij all..."
categories: [internet]
comments: true
---

Met onze tentoonstelling willen met héél veel beelden, zo weinig mogelijk vertellen.  
  
  
**Wat voor beelden hebben wij allemaal om ons heen?**  
1.0 statische beelden  
2.0 dynamische beelden (bewegend beeld)  
  
**\_1.0 Wat voor statische beelden hebben wij allemaal om ons heen?**  
-.1 foto's  
-.2 tekeningen  
-.3 reclame  
-.4 tekst  
  
**\_\_1.0.1 Welke genres foto's hebben we?**  
-.1 nieuws fotografie  
-.2 documentaire fotografie  
-.3 reclame fotografie  
-.4 kunst fotografie  
-.5 amateur fotografie  
-.6 snapshots / close-ups  
  
**\_\_1.0.2 Welke genres tekeningen (ook digitaal) hebben we?**  
-.1 schilderijen  
-.2 airbrush  
-.3 grafiek  
-.4 zeefdruk  
-.5 drukwerk  
-.6 logo's  
-.7 iconen  
-.8 infographics  
  
**\_\_1.0.3 Wat voor reclame uitingen kennen we in stilstaand beeld?**  
-.1 posters  
-.2 flyers  
-.3 advertenties  
  
**\_\_1.0.4 Wat voor teksten hebben we?**  
-.1 krant  
-.2 grafisch  
  
  
**\_2.0 Wat voor dynamische beelden hebben wij allemaal om ons heen?**  
-.1 film  
-.2 videoclip  
-.3 nieuws  
-.4 animaties  
-.5 reclame  
  
**\_\_2.0.1 Wat voor films hebben we?**  
-.1 comedy  
-.2 actie  
-.3 horror  
-.4 erotiek  
-.5 porno  
-.6 detective  
-.7 romantiek  
-.8 sience-fiction  
-.9 drama  
  
**\_\_2.0.2 Wat voor video's hebben we?**  
  
**\_\_2.0.3 Wat voor nieuws hebben we?**  
  
**\_\_2.0.4 Wat voor animaties hebben we?**  
-.1 2D animaties  
-.2 3D animaties  
-.3 tekening animaties  
  
**\_\_2.0.5 Wat voor reclame hebben we?**  
-.1  
  
*Onze doelgroep: **jong-volwassenen***  
  
Komende week gaan wij beelden verzamelen. De uitwerking hierboven is een opstartpunt om te vertrekken bij het verzamelen. Het gaat er niet om wat de inhoud van het beeld is. De essentie is dat we veel beeld verzamelen, minder belangrijk is wat elk beeld te vertellen heeft.
