---
title: "Virtual Art-House (stage)"
date: "2009-03-06T21:50:52.000Z"
description: "
Op deze pagina zijn alle werkzaamheden en ontwikkelen terug te vinden van mijn stageperiode bij het Virtual Art-House ..."
categories: [internet]
comments: true
---

![](../../../images/vah-logo.jpg "logo")  
 Op deze pagina zijn alle werkzaamheden en ontwikkelen terug te vinden van mijn stageperiode bij het Virtual Art-House project. Deze zijn onderverdeeld in blog-posts waarin alles is terug te vinden. Ik geef op deze pagina slechts een kleine aankondiging en een link naar de desbetreffende post met meer informatie.  
  
**Wat is het Virtual Art-House?**  
 Virtual Arthouse heeft tot doel om studenten en scholieren van het middelbaar onderwijs kennis te laten maken met de bijzondere expositiemogelijkheden van moderne kunst en daarbij ook de kunstwerken zelf een andere rol toe te bedelen dan zij doorgaans verwachten. Het project is gericht op een interactief en op jongeren gerichte presentatie. Deze zal via scholen circuleren gedurende een periode van vijf jaar waarbij niet alleen de interesse voor kunst en de presentatie zelf moet ontstaan maar ook vastgehouden moet worden door de dynamiek van het project.  
  
**Wat is het beoogd resultaat?**  
 Het Virtual Arthouse is een project met een looptijd van vijf jaar. Gedurende die vijf jaar zal de fysieke tentoonstellingsruimte met daarbij de virtuele ruimte geplaatst worden bij diverse Brabantse scholen. Behalve het doel om de interesse op te wekken bij studenten en leerlingen van het middelbaar onderwijs voor kunst en haar presentatievorm is het project er ook op gericht om hen zelf te laten participeren.  
 Zij kunnen als groep hun bijdrage leveren aan het virtuele landschap en daarin behalve ruimte te creeëren voor de kunstwerken ook zelf hun bijdrage leveren door naargelang hun interesse en opleiding bijvoorbeeld tentoonstellingsaffiches of anderszins relevante items in deze wereld achter te laten. Aan het eind van de vijf jaar is er een wereld die niet alleen naar de locatie en opleiding/school refereert maar ook een tastbaar tijdsbeeld achterlaat.  
  
**Wat is mijn functie? Wat wordt er van mij verwacht?**  
 Het is de bedoeling dat ik als interactie ontwerper bovenstaande visie en het beoogde resultaat in een realistisch uit te voeren concept. Ik doe dit niet alleen, ik doe dit samen met Yoeran Muster die Communication &amp; Multimedia Design studeert (richting interface &amp; interaction design). Het concept wordt door ons samen met de stichting en Cinto (de uitvoerende partij) ontworpen, waar daar het beheer en eindverantwoordelijkheid bij ons komt te liggen als interactie ontwerpers. Gedurende de periode zullen wij stap voor stap, concept naast realisatie, testend en wel een Virtual Art-House ontwikkelen. We worden hierin ook begeleidt door kunstenaar Simon Kentgens (<http://www.simonkentgens.com/>).  
  
**Wie is mijn werkgever?**  
[![](/images/cbkwb.jpg "Centrum voor Beeldende Kunst en Vormgeving West-Brabant")](http://www.cbk-westbrabant.nl/)  
 Stichting voor Beeldende kunst en Vormgeving West-Brabant  
  
**Met wie wordt er samengewerkt?**  
[![](/images/zoomvliet.gif "ROC Zoomvliet College")](http://www.rocwb.nl/)[![](/images/cinto.jpg "Cinto")](http://www.cinto.nl)  
> ***Competenties***  
> <span style="font-size: x-small;">Flexibiliteit  
>  Communicatieve vaardigheden ontwikkelen  
>  Werken in een teamverband  
>  Eigen plaats in een team verwerven (eigen expertise goed inschatten)  
>  Structuren zien en maken in het werkproces  
>  Vooruitzien (werk snel oppakken en aan de hand van het tijdspad in kunnen vullen)  
>  Werk kunnen inplannen  
>  Creatieve vaardigheden  
>  Overdraagbaar maken van idee en taken</span>

  
**De informatieve website**  
 Voor het project moet zo snel mogelijk een informatieve website worden opgezet met alle nodige informatie. Het ontwerp voor deze website is door ons gemaakt en de technische realisatie van de website gebeurd door Cinto.[  
 Klik hier voor het beeldmateriaal en meer informatie](http://interactieontwerpen.sebastix.nl/informatieve-website-virtual-art-house/)[  
 Klik hier om de website te bezoeken!](http://interactieontwerpen.sebastix.nl/virtual-art-house-website-live/)  
  
**De mogelijke technieken**  
 Vooraf was duidelijk dat het virtuele gedeelte moest worden ontwikkeld in een website omgeving. Natuurlijk kunnen we het begrip website heel ruim opvatten, maar vertrekkend vanuit het concept sloten deze drie technische termen het beste aan. Allen zijn technieken die worden ingezet als componenten binnen een website. Het uiteindelijke resultaat zal echter niet aanvoelen als een website, maar meer als een applicatie. Klik hieronder voor meer informatie en onderzoeksresultaten per techniek.  
[HTML5 canvas &amp; javascript (excanvas) ](http://interactieontwerpen.sebastix.nl/html5-canvas-javascript/)  
[Flash (Papervision3D)](http://interactieontwerpen.sebastix.nl/flash-in-3d-papervision3d/)  
[Microsoft .NET Silverlight (3D, WPF, Blend)](http://interactieontwerpen.sebastix.nl/net-silverlight-3d-mogelijkheden-onderzoek/)  
  
**Het compacte concept voor de technische realisatie**  
[Klik hier om deze te bekijken (versie april 2008)](http://interactieontwerpen.sebastix.nl/het-compacte-concept/)  
  
**Visualisaties van het Virtual Art-House tijdens het proces** [Resultaten testles 29 juni](http://interactieontwerpen.sebastix.nl/resultaten-van-de-testles-29-juni-virtual-art-house/)  
[Update 17 juli](http://interactieontwerpen.sebastix.nl/update-virtual-art-house/)  
[Laatste visualisaties](http://interactieontwerpen.sebastix.nl/virtual-art-house-stage-ten-einde/)
