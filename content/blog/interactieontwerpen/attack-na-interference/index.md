---
title: "ATTACK na INTERFERENCE"
date: "2009-04-13T17:25:38.000Z"
description: "
ATTACK heeft op een geïmproviseerde wijze ten toon gesteld gestaan tijdens het INTERFERENCE ‘licht&amp;geluid’ te..."
categories: [internet]
comments: true
---

![](/images/minorAttack.jpg)  
 ATTACK heeft op een geïmproviseerde wijze ten toon gesteld gestaan tijdens het INTERFERENCE ‘licht&amp;geluid’ tentoonstelling in Electron te Breda. Omdat de tentoonstelling maar liefst meer dan twee maanden duurde, was het een hele uitdaging om een werkende installatie op te leveren die gedurende die tijd ook bleef werken.  
  
  
  
 Helaas is dit niet helemaal gelukt, want de computer waarop de installatie draaide liet het vaak afweten (camera beeld liep vast) en het beeld liep ook niet optimaal snel met de zwerm. Een krachtigere computer is hiervoor nodig.  
 Zelf heb ik nog wel wat gesleuteld aan het geluid, deze beweegt nu mee met de zwerm. Dat wil zeggen; als de zwerm zich aan de rechterkant bevindt, dan is het geluid ook aan deze kant te vinden. Ik heb ook geprobeerd het geluid op enkele manieren interactiever te maken (oorzaak-gevolg relatie tussen gebruiker-zwerm meer proberen te vertalen in verandering van geluid) maar dit liep in de tijd dat ik het moest realiseren op niets uit (in ieder geval niet op iets wat 2 maanden zou werken).  
 Ondanks deze tegenslagen heeft de installatie het wel overleefd en laten zien dat er meer in zit. Ik ga er ook zeker mee door en ik hoop in oktober een nieuwere versie van ATTACK te presenteren. Er is namelijk al aan mij gevraagd of ik in die maand de installatie weer wil opstellen tijdens een ander festival. Nog enkele video's en foto's van ATTACK in het Electron:  
  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="377" width="500"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="src" value="http://vimeo.com/moogaloop.swf?clip_id=2945416&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="377" src="http://vimeo.com/moogaloop.swf?clip_id=2945416&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1" type="application/x-shockwave-flash" width="500"></embed></object>  
[ATTACK at Interference](http://vimeo.com/2945416) from [Sebastian Hagens](http://vimeo.com/sebastix) on [Vimeo](http://vimeo.com).  
  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="377" width="500"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="src" value="http://vimeo.com/moogaloop.swf?clip_id=2945455&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="377" src="http://vimeo.com/moogaloop.swf?clip_id=2945455&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1" type="application/x-shockwave-flash" width="500"></embed></object>  
[ATTACK at Interference](http://vimeo.com/2945455) from [Sebastian Hagens](http://vimeo.com/sebastix) on [Vimeo](http://vimeo.com).  
  
  
![](../../../images/IMG_0088.JPG)  
![](../../../images/PICT0052.JPG)  
![](../../../images/PICT0054.JPG)  
![](../../../images/PICT0059.JPG)  
![](../../../images/PICT0062.JPG)  
![](../../../images/PICT0064.JPG)
