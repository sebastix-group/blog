---
title: "Slaven van de toekomst"
date: "2007-09-21T18:12:40.000Z"
description: "Tags
Robocop, terminator, humanoid, robosapien, iRobot, cyborg, tronics, Asimo, Androide

Robots zijn de slaven van d..."
categories: [internet]
comments: true
---

<span><span>*Tags*</span>  
*<span>Robocop, terminator, humanoid, robosapien, iRobot, cyborg, tronics, Asimo, Androide</span>*</span>  
  
 Robots zijn de slaven van de toekomst. Een slaaf is eigendom van een persoon. Zij vallen onder het eigendomsrecht. In Nederland zijn slaven altijd goed behandeld, er werd in het recht vastgesteld welke verplichtingen de eigenaar had tegenover zijn slaaf.  
  
 In de 17e eeuw zijn veel Europese landen kolonies gaan stichten in Afrika, Zuid-Amerika en een deel van de Caribische eilanden. De kolonisten zetten de slaven aan het werk op grote plantages voor suiker, tabak etc. Via grote schepen werden deze goederen vervoerd naar Nederland. Ook nieuw gekochte slaven werden vervoerd met deze schepen. Deze handel vond vooral plaats op het eiland CuraÇao. Nederland had in de 17e eeuw een sterke vloot en was ook een van de welvarendste landen ter wereld. Dit was grotendeels te danken aan de handel in Europa met de specerijen die ze van de andere kant van de wereld haalden. De Gouden Eeuw wordt in elk Nederlands geschiedenis uitgebreid behandeld.  
 Vanaf het begin van de slavernij hebben de kolonisten altijd te maken gehad met opstanden. Aan het einde van 18e eeuw volgde er een grote slavenopstand. De slaven eiste hun vrijheid. Deze opstand werd niet gewonnen door de slaven, maar het gevolg was wel dat er een beschermende slavenwetgeving werd opgesteld. Nederland heeft een aandeel van 5% gehad van de totale slavenhandel op de wereld.  
 Waarom is de slavernij afgeschaft? De opstand groeide, ook van het eigen volk van christelijke groeperingen. Veel militairen in de koloniën stierven aan onbekende ziektes. Ook steeg de ontwikkeling van landbouwmachines die ervoor zorgde dat slaven werden vervangen door machines. Toch heeft het tot 1981 dat wereldwijd slavernij is afgeschaft, op papier.  
  
 Het woord slaaf heeft een negatieve ondertoon, maar eigenlijk gebeurt er vandaag de dag nog steeds hetzelfde. We halen we werkers van elders. We halen deze mensen hier om op het land en in fabrieken te komen werken. Polen, Oost-Duitsers, Russen, Bulgaren etc. Blijkbaar hebben we deze mensen nodig, die voor ons werk verrichten wat we zelf niet willen verrichten. Voelen wij ons Nederlanders te trots om dat werk te doen? Hebben wij ooit wel eens op het land gewerkt? Nederlanders voelen zich goed, zo goed als de adel. De adel behoort geen vieze handen te krijgen. We maken misbruik van het feit dat er mensen op deze aardbol wonen die tegen ons opkijken en er veel voor over hebben om naar onze mooie wereld te komen. In ruil daarvoor moeten ze wel komen werken. Maar na een verloop van tijd gaan deze mensen zich ook omhoog werken en steeds beter voelen tussen onze trotsheid. Opeens gaan ze zich niet meer minderwaardig voelen en steeds meer eisen stellen. Conflicten volgen, allemaal gebaseerd op het feit dat iedereen gelijk is en gelijke rechten hebben. Die regel hebben we immers zelf bepaald op papier. Helaas werkt het in de praktijk nooit zo. Er is altijd een bepaalde hiërarchie tussen personen, waar ook ter wereld in wat voor omgeving dan ook. Conflicten zijn niet te vermijden. De oplossing vinden we altijd op onze eigen manier. Praten, zwijgen, intimideren, geweld etc. Is het niet een actueel gegeven dat wij Nederlanders moeite hebben met allochtonen ? Het zal straks ook precies zo verlopen met alle werkkracht uit Oost-Europa. Wat volgt er daarna? Welk land is daarna aan de beurt om haar bevolking naar elders te laten gaan om te werken en uiteindelijk daar wortel te schieten.  
  
 Wat zou het toch een stuk eenvoudiger zijn als we die werkkracht simpelweg kunnen vervangen door robots. Jawel, machines die alle menselijke handelingen kunnen verrichten. Deze machines zijn immers door onszelf gecreëerd en als het goed is zullen ze ons in elk situatie gehoorzamen. Nederland wil vooruit. Het is altijd gelukt over de ruggen van werkkrachten uit andere omgevingen. Ooit zijn deze ruggen verbrijzeld en op. Dan stort de hele Westerse wereld toch in, want deze is gebouwd op het kapitalisme.  
  
 De techniek vervangt steeds meer in ons leven. Leidt vervangen altijd tot beter? Techniek werkt vereenvoudigend. Worden we hier nu dommer of slimmer van? Ik denk dat we slimmer worden. Om tot een bepaalde techniek te komen moet je eerst een heel denk- en doenproces uitvoeren; logica zien, verbanden leggen, conclusies trekken. Dat vereist veel tijd. We komen op een steeds hoger niveau, daarom lijkt het dat we steeds dommer worden voor de simpele handelingen. Maar dat is niet zo, oefening baart kunst. Hoe vaker je iets doet, hoe beter het gaat. Het doen maakt ons slimmer, vaker denken over een bepaald onderwerp geeft je meer inzichten. Meer technologie leidt tot beter, tot slimmere mensen. Onze intellegentie stijgt, dit proces is zelfs een toenemende stijging. De mens wordt zelfs zo slim om straks net zo slimme mensen (of domme robots) te maken zoals in de oertijd. Het zal nog jaren duren dat we met onze technische kennis de eerste robosapiens hebben gemaakt. Een robot die kan overleven in elke omgeving. Daarmee is het begin van een nieuw ras. Vanuit het Westers, kapitalistisch denken zullen deze robosapiens vooral functioneel zijn. We houden ze letterlijk kunstmatig dom om ze voor ons te laten werken. Het zijn onze toekomstige slaven.  
  
<span>-----  
*“Robotica is de intelligente connectie tussen de gedachte en het uitvoeren daarvan.” (Michael. Brady)”*</span>  
  
*Robota = werken (Tsjechisch)  
 Robot = slaaf  
 "Karel Capek " in zijn toneelstuk "Rossum's Universal Robots."*  
 -----  
  
<span>Uitgangspunt</span>  
 Slaven zijn onmisbaar in onze maatschappij  
  
**<span>Concept</span>**  
 In 2033 hebben we weer de beschikking over slaven. In elke stad kun je tegen een aantrekkelijk bedrag een slaaf kopen. Geen menselijke slaaf, maar een robot. Een robosapien! Je dient hem zelf allerlei dingen aan te leren in een bepaalde context waar deze slaaf voor jou gaat werken. Laat je slaaf de dingen doen waar jij een hekel aan hebt. Er blijft meer tijd over voor belangrijkere dingen zoals je werk, hobby’s en sport.  
 Alleen op deze manier kun jij je als persoon door blijven ontwikkelen, moeilijkere vraagstukken oplossen en de complexiteit begrijpen van hogere problemen. Je bent het niet waard om eenvoudige klussen uit te voeren. Jij wordt steeds slimmer, alleen met een slaaf voor je eenvoudige, simpele handelingen ben je in staat om ook echt slimmer te blijven worden.  
  
**<span>Technieken</span>**  
 Robotica  
 Cyborgs  
 Automatisering  
 Contourherkenning  
 Grijpers  
 Artificiële Intelligentie (AI)  
 Automatische piloot  
 Bots  
 Amputaties in de medische wereld (door technische onderdelen)  
  
**<span>Links</span>**  
[http://www.honda-robots.com/index\_ns.html](http://www.honda-robots.com/index_ns.html)  
<http://nl.wikipedia.org/wiki/Robot>  
<http://www.neoweb.nl/infopages/robotica.html>  
<http://www.planet.nl/planet/show/id=69402/contentid=825777/sc=b61575>  
<http://noorderlicht.vpro.nl/artikelen/8805378/>  
  
**Videos**  
 \[flash http://www.youtube.com/v/GM2Bw2zPSqc\]  
 \[flash http://www.youtube.com/v/mJbpqykvTBk\]  
 \[flash http://www.youtube.com/v/Q3C5sc8b3xM\]  
 \[flash http://www.youtube.com/v/CyIHzCsbA\_w\]  
 \[flash http://www.youtube.com/v/e26Gnc3E0yg\]  
 \[flash http://www.youtube.com/v/7yo2sG6j7J4\]  
 \[flash http://www.youtube.com/v/R9IQlhAVvbk\]  
 \[flash http://www.youtube.com/v/weKJjknf748\]  
 \[flash http://www.youtube.com/v/V7FVjATcqvc\]  
  
[Presentatie concept week 38](http://interactieontwerpen.sebastix.nl/slaven-van-de-toekomst/flash-actionscript-menu/ "Presentatie concept week 38")
