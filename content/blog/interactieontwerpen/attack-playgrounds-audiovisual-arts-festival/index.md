---
title: "Attack - Playgrounds audiovisual arts festival"
date: "2008-11-04T23:50:58.000Z"
description: "ATTACK installatie
Playgrounds Audiovisual Arts Festival in Tilburg 30 & 31 oktober 2008

Visitors Playgrounds intera..."
categories: [internet]
comments: true
---

**ATTACK installatie**  
*Playgrounds Audiovisual Arts Festival in Tilburg 30 &amp; 31 oktober 2008*  
  
<object height="375" width="500"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=2154953&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="375" src="http://vimeo.com/moogaloop.swf?clip_id=2154953&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=00ADEF&fullscreen=1" type="application/x-shockwave-flash" width="500"></embed></object>  
[Visitors Playgrounds interactive installation Attack](http://vimeo.com/2154953) from [Sebastian Hagens](http://vimeo.com/sebastix) on [Vimeo](http://vimeo.com).  
  
[Lees meer](http://interactieontwerpen.sebastix.nl/attack-project-minor-offscreen/)
