---
title: "HTML5 canvas & meer"
date: "2010-08-30T13:14:52.000Z"
description: "Op 11 maart 2009 deelde ik een bericht via mijn weblog waarin ik een heleboel HTML5 + canvas voorbeelden aanhaalde. Dit ..."
categories: [internet]
comments: true
---

Op 11 maart 2009 [deelde ik een bericht](http://interactieontwerpen.sebastix.nl/html5-canvas-javascript/) via mijn weblog waarin ik een heleboel HTML5 + canvas voorbeelden aanhaalde. Dit stuk onderzoek had ik gedaan na aanleiding van mijn stage opdracht Virtual Art-House. Ik was op zoek naar een stukje techniek waarmee we een virtuele ruimte konden ontwikkelen waarin 'kunstwerken' van scholieren werden gepresenteerd.  
  
 Wat is HTML5 canvas?

> In [HTML5](../tag/html5/ "HTML5") is een canvas element (HTML code: &lt;canvas&gt;) opgenomen die het mogelijk maakt bitmap afbeeldingen dynamisch te renderen met behulp van [javascript](../tag/javascript/ "javascript").  
>  Een canvas is een gebied waarin je een graphic kunt tekenen. Met behulp van javascript kun je bijvoorbeeld heel eenvoudig een lijntje tekenen van punt A naar punt B (met beide een x,y coördinaat).

  
 Anderhalf jaar geleden was HTML5 en haar nieuwe mogelijkheden relatief onbekend vergeleken met nu. Toch is dat bericht over HTML5 + canvas + javascript één van mijn best bekeken pagina's op mijn weblog:  
  
[![](http://files.droplr.com.s3.amazonaws.com/files/41343510/1D5B27.Firefox.png "HTML5 canvas analytics")](http://files.droplr.com.s3.amazonaws.com/files/41343510/1D5B27.Firefox.png)*Statistieken [pagina HTML5-canvas-javascript](http://interactieontwerpen.sebastix.nl/html5-canvas-javascript/) vanaf 11-04-2009 t/m nu (29-08-2010).*  
  
  
  
 In de periode mei 2010 zien we een duidelijke stijging. Dit zijn bezoekers via Google die op mijn weblog terecht zijn gekomen zoekend met de termen 'html5 canvas'. Deze stijging kan ik simpel verklaren door het feit dat er in de periode ineens een ware HTML5 hype is ontstaan. Een hype die vooral door Apple in gang is gezet met de introductie van de iPad.  
  
 Om even iets dieper in te gaan op wat HTML5 precies betekent en in welke context je deze nieuwe webstandaard moet plaatsen raad ik je aan deze video te bekijken van Jeremy Keith (via [@askibinski](http://twitter.com/askibinski)):  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="374" width="663"><param name="id" value="flashObj"></param><param name="bgcolor" value="#FFFFFF"></param><param name="flashVars" value="videoId=596569258001&playerID=524210192001&playerKey=AQ%2E%2E,AAAAdgSazbk%2E,3qPqKG4m0GxWuPGtFVUW03BCAQMIFPtq&domain=embed&dynamicStreaming=true"></param><param name="base" value="http://admin.brightcove.com"></param><param name="seamlesstabbing" value="false"></param><param name="allowFullScreen" value="true"></param><param name="swLiveConnect" value="true"></param><param name="allowScriptAccess" value="always"></param><param name="src" value="http://c.brightcove.com/services/viewer/federated_f9?isVid=1&isUI=1"></param><embed allowfullscreen="true" allowscriptaccess="always" base="http://admin.brightcove.com" bgcolor="#FFFFFF" flashvars="videoId=596569258001&playerID=524210192001&playerKey=AQ%2E%2E,AAAAdgSazbk%2E,3qPqKG4m0GxWuPGtFVUW03BCAQMIFPtq&domain=embed&dynamicStreaming=true" height="374" id="flashObj" seamlesstabbing="false" src="http://c.brightcove.com/services/viewer/federated_f9?isVid=1&isUI=1" swliveconnect="true" type="application/x-shockwave-flash" width="663"></embed></object>  
  
 Conclusie van de video over HTML5: het is de standaard voor zowel web ontwikkelaars als browser ontwikkelaars waarin alle huidige standaarden uit het verleden in zijn opgenomen. Dus wat je al kent van HTML, kun je nog steeds blijven gebruiken. Je hoeft dus niets nieuws te leren. HTML is vooral een 'mindshift' waar we doorheen moeten.  
  
 Echter biedt HTML5 wel nieuwe elementen waarmee je eenvoudig een meer logische website kunt ontwerpen en ontwikkelen. Meer over die elementen kun je onder andere hier vinden:  
 HTML5 tag elementen (w3schools) - [http://www.w3schools.com/html5/html5\_reference.asp](http://www.w3schools.com/html5/html5_reference.asp)  
 Verschillen tussen HTML5 en HTML4 - <http://www.w3.org/TR/html5-diff/>  
 Zoek [hier verder op Google](http://www.google.nl/search?source=ig&hl=nl&rlz=&=&q=html5+tutorial&aq=4&aqi=g10&aql=&oq=html5+&gs_rfai=) om meer informatie op te halen hierover.  
  
 Belangrijk om te beseffen is dat HTML5 een opzichzelfstaande standaard is waaraan een heleboel andere stukjes techniek kunnen worden gekoppeld. Zoals CSS en Javascript. De velen prachtige voorbeelden die op het internet staan, zijn niet alleen met HTML5 gemaakt. Dat kan namelijk niet. Juist in de combinatie met CSS3 en Javascript krijg je resultaten die vernieuwend zijnen een potentieel gezicht laten zie hoe het web er straks meer en meer uit gaat zien.  
  
 Hier komt ook het &lt;canvas&gt; element om de hoek krijgen die met HTML5 wordt geïntroduceerd. Waarom vind ik dit element zo spannend?  
 Jaren terug experimenteerde ik veel met Flash. Met actionscript was het mogelijk om bepaalde objecten op de stage aan te maken en deze bijvoorbeeld te animeren. Aan een MovieClip object kon je ontzettend veel eigenschappen hangen en aanpassen over een tijdlijn. Dat was gewoon ontzettend leuk; met stukjes code een interactief object maken met bijvoorbeeld mooie animaties. Met actionscript3.0 in Flash werd de Sprite object geïntroduceerd waarmee dit ook kon alleen dan binnen een object georiënteerde omgeving zonder tijdlijn. Verder heb ik twee jaar geleden ook kennisgemaakt met Processing waarin je wederom met codes helemaal los kan gaan met verschillende objecten. En juist dit spannende wat kon in Flash en Processing, kan nu ook met een standaard element in HTML: het canvas element. Helaas heb ik zelf nog geen spannende voorbeelden gemaakt die de potentie van dit element laten zien. Daarom kom ik nu met een frisse, nieuwe lijst met HTML5 canvas voorbeelden:  
  
 Tetris - <http://aduros.emufarmers.com/easel/>  
 Textures - <http://www.benjoffe.com/code/demos/canvascape/textures>  
 Casey Reas experiment - <http://reas.com/twitch/>  
 HTML5 grafieken - <http://www.rgraph.net/>  
 Cloud - <http://www.professorcloud.com/mainsite/canvas-nebula.htm>  
 Parallax - [http://webdev.stephband.info/parallax\_demos.html](http://webdev.stephband.info/parallax_demos.html)  
 Lightning - <http://mapstest.kosmosnimki.ru/api/examples/canvas/lightning.html>  
 3D Tiler - <http://labs.hyperandroid.com/html5-tiler-3d>  
<http://grenlibre.fr/demo/same/>  
 Clock - <http://www.perisic.com/canvasclock/>  
 Magnetic - <http://hakim.se/experiments/html5/magnetic/02/>  
 Color picker - <http://www.benjoffe.com/code/tools/colours/>  
 K3D examples - [http://www.kevs3d.co.uk/dev/canvask3d/k3d\_test.html](http://www.kevs3d.co.uk/dev/canvask3d/k3d_test.html)  
 Many Lines - <http://open.adaptedstudio.com/html5/many-lines/>  
 Physics Box2D - <http://box2d-js.sourceforge.net/index2.html>  
 Physics Sketch - <http://physicsketch.appspot.com/>[  
 Klik hier nog voor meer (oudere) voorbeelden.](http://interactieontwerpen.sebastix.nl/html5-canvas-javascript/)  
  
 Meer uitleg over HTML5 canvas:  
 HTML5: het canvas element - <http://www.netsetters.nl/2010/05/html5-het-canvas-element/>  
 En toen was er canvas - <http://blog.finalist.com/2010/06/28/en-toen-was-er-canvas/>  
  
 En bekijk ook absoluut de Processing.js javascipt bibliotheek: <http://www.processingjs.org>
