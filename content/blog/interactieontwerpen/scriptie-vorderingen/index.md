---
title: "Scriptie vorderingen"
date: "2009-11-09T14:05:03.000Z"
description: "Er moet komende weken even heel hard gewerkt worden door mij aan mijn scriptie! Half december moet de eerste leesversie ..."
categories: [internet]
comments: true
---

Er moet komende weken even heel hard gewerkt worden door mij aan mijn scriptie! Half december moet de eerste leesversie van mijn scriptie af zijn.  
 Een probleem houdt me al enige tijd bezig dat ik mijn allereerste gemaakte website nergens meer kan terugvinden die online stond op http://www.stuffgames.com. Ik kan me nog heel goed herinneren dat ik hier een review had geplaatst over Command &amp; Conquer Red Alert en hier veel reacties op kreeg (wat er allemaal verkeerd aan die review was). Toen was ik een jaar of 11-12 geloof ik. Tot mijn allergrootste spijt heb ik dus helemaal niets meer van dit materiaal!  
  
 Dit breng me dus bij het onderwerp van mijn scriptie; hoe bewaren en beschermen we onze websites?  
 Mijn gegeven voorbeeld is wel erg persoonlijk natuurlijk, in mijn scriptie is het de bedoeling dit een stuk breder en meer maatschappelijk te brengen.  
  
 Op dit moment wordt er al aardig gearchiveerd voor de toekomst door het welbekende 'Internet Archive' (<http://www.archive.org/about/about.php>).  
 Ik ben nog altijd hard op zoek naar de discourse die aansluit bij mijn vraagstelling; iemand tips en adviezen?
