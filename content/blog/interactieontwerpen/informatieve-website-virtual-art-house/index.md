---
title: " Informatieve website Virtual Art-House"
date: "2009-03-25T23:02:08.000Z"
description: "

Voor het project Virtual Art-House (VAH) dient ook een informatieve website te worden gelanceerd met daarop een teks..."
categories: [internet]
comments: true
---

![](../../../images/vah-logo.jpg "Virtual Art-House")  
  
 Voor het project Virtual Art-House (VAH) dient ook een informatieve website te worden gelanceerd met daarop een tekstuele en visuele uitleg over het project. Bezoekers van de website moeten zich kunnen inlezen over het project, een visuele indruk krijgen en in een later stadium ook een soort demo kunnen bezichtigen. Doel van de website is informeren.  
 Wij kregen opdracht om deze website samen met Cinto zo snel mogelijk online te krijgen en alle nodige voorbereidingen voor Cinto daarvoor uit te voeren.  
  
  
  
 Voor het VAH is al eerder een publicatie gemaakt in drukvorm door het [grafisch bureau HetDok](http://www.hetdok.info). Aangezien ik (en mede collega Yoeran) geen grafisch ontwerper van oorsprong zijn, zijn we vertrokken vanuit het materiaal welke HetDok al had gemaakt. Naast deze bronnen heb ik zelf wel een LOGO gemaakt voor het VAH-project, dit ook omdat ik sterk van mening ben dat het project een logo nodig heeft naar buiten toe.  
 Goed, dit is een visuele impressie van het grafisch werk van HetDok:  
  
![](../../../images/hetdok-impressie.jpg "HetDok")  
  
 Vertrekkend uit deze grafische stijl hebben we de volgende visuele uitwerking van de website gemaakt:  
  
![](../../../images/visual-website1.jpg "visual1")  
  
![](../../../images/visual-website2.jpg "visual2")  
  
 Uiteindelijk is het dit ontwerp geworden welke momenteel wordt omgezet in een werkende website door Cinto:  
  
![](../../../images/visual-website3-170309-1.jpg "visual3-1")  
  
![](../../../images/visual-website3-170309-2.jpg "visual3-2")  
  
 De website zelf is dus nog niet online, maar zodra deze dat wel is zal ik een post plaatsen met daarin het eindresultaat.
