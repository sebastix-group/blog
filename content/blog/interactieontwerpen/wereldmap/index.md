---
title: "Wereldmap"
date: "2007-11-12T19:00:08.000Z"
description: "

Dit is een interactieve wereldkaart met daarop enkele steden. Door met je muis over de rode bolletjes te gaan, versc..."
categories: [internet]
comments: true
---

![](http://www.interactieontwerpen.nl/_sebastian/previewImages/wereldmap.jpg)  
  
 Dit is een interactieve wereldkaart met daarop enkele steden. Door met je muis over de rode bolletjes te gaan, verschijnt de plaatsnaam van de stad. De steden worden extern ingeladen uit een XML bestand. Vervolgens worden er met een berekening de plaats bepaald in het bestand. We hebben gebruik gemaakt van de *Equirectangular projection*, omdat deze een verhouding hanteert van 2:1.  
  
  
 Bekijk de kaart:  
[http://interactieontwerpen.nl/\_sebastian/mapping/wereldmap/wereldmap.swf](http://interactieontwerpen.nl/_sebastian/mapping/wereldmap/wereldmap.swf)
