---
title: "ZINC in het nieuws (waar ik dus nu zit)"
date: "2009-04-24T10:57:24.000Z"
description: "Ik werd net gewezen op dit filmpje over de opening van Zinc in Roosendaal, de plek waar ik momenteel werk voor stage. In..."
categories: [internet]
comments: true
---

Ik werd net gewezen op dit filmpje over de opening van Zinc in Roosendaal, de plek waar ik momenteel werk voor stage. In de toekomst (deze zit er nog niet fysiek) komt hier ook het starterscentrum Roosendaal. Veel informatie erover kan ik niet vinden op het internet, maar het lijkt een initiatief dat gesteund wordt door de overheid en juist in deze tijd extra aandacht krijgt bij het opstarten.  
  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="344" width="425"><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="src" value="http://www.youtube.com/v/XcRAT_GMkSY&hl=nl&fs=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="344" src="http://www.youtube.com/v/XcRAT_GMkSY&hl=nl&fs=1" type="application/x-shockwave-flash" width="425"></embed></object>
