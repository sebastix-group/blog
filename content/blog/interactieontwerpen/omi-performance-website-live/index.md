---
title: "OMI-Performance website LIVE"
date: "2009-07-24T09:19:26.000Z"
description: "

http://www.omi-performance.com

Eindelijk...eindelijk...eindelijk heb ik vandaag een stukje levenswerk (gezien het..."
categories: [internet]
comments: true
---

[![](http://dev.omi-performance.com/newsletter/website-screenshot-0709.jpg "screenshot website")](http://www.omi-performance.com)  
  
<http://www.omi-performance.com>  
  
 Eindelijk...eindelijk...eindelijk heb ik vandaag een stukje levenswerk (gezien het aantal uren!) online gezet; de nieuwe website voor OMI-Performance. OMI-Performance is meer voor mij geworden dan alleen een merk, imago, bedrijf en producten. Het is iets geworden wat ik met me meedraag na ongeveer een jaar denkwerk, ontwikkeling en veel experimenteren.  
  
  
  
 OMI-Performance is uniek! Het eerste Europese merk dat aftermarket performance onderdelen gaat aanbieden die ze zelf heeft ontwikkeld en nu dus op de markt brengt. Dat gaat natuurlijk niet zomaar, daarom heb ik afgelopen maanden vele tientallen uren werk verzet samen met Martin en Bob. De 2 mannen achter het hele concept en uitvoering.  
  
 Het is meer dan alleen de website welke ik heb opgeleverd tot nu toe, maar de gehele identiteit en uitstraling van het merk en producten hangen aan elkaar samen. Dit dient natuurlijk allemaal op de juiste lijn met elkaar in balans te zijn en ik hoop dat dit voor een flinke boost gaat zorgen voor OMI komende jaren. De website is online, maar daarmee begint het eigenlijk allemaal pas. Slechts een handjevol producten kan er worden besteld via de website, ik ben benieuwd hoeveel er dit zullen zijn over een jaar of twee.  
  
[Klik hier om de website te bekijken](http://www.omi-performance.com)
