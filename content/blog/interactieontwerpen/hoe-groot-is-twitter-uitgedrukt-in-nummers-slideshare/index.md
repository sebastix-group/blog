---
title: "Hoe groot is Twitter uitgedrukt in cijfers (slideshare)?"
date: "2010-09-13T11:07:03.000Z"
description: "
View more presentations from Raffi Krikorian.
Bron: Twitter bericht van @davidvanleeuwen
..."
categories: [internet]
comments: true
---

<div id="__ss_5181240" style="width: 425px;"><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="522" id="__sse5181240" width="625"><param name="allowFullScreen" value="true"></param><param name="allowScriptAccess" value="always"></param><param name="src" value="http://static.slidesharecdn.com/swf/ssplayer2.swf?doc=by-the-numbers-100911160711-phpapp01&stripped_title=twitter-by-the-numbers"></param><param name="name" value="__sse5181240"></param><param name="allowfullscreen" value="true"></param><embed allowfullscreen="true" allowscriptaccess="always" height="522" id="__sse5181240" name="__sse5181240" src="http://static.slidesharecdn.com/swf/ssplayer2.swf?doc=by-the-numbers-100911160711-phpapp01&stripped_title=twitter-by-the-numbers" type="application/x-shockwave-flash" width="625"></embed></object>  
<div style="padding: 5px 0 12px;">View more [presentations](http://www.slideshare.net/) from [Raffi Krikorian](http://www.slideshare.net/raffikrikorian).</div>  
<div style="padding: 5px 0 12px;">Bron: [Twitter bericht](https://twitter.com/davidvanleeuwen/status/24319235151) van [@davidvanleeuwen](https://twitter.com/davidvanleeuwen)</div>  
</div>
