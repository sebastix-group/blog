---
title: "Microsoft .NET (Silverlight) 3D mogelijkheden onderzoek"
date: "2009-03-23T22:35:13.000Z"
description: "Omdat er binnen het stageproject wordt samengewerkt met Cinto (de uitvoerende partij) is er ook een onderzoek geweest bi..."
categories: [internet]
comments: true
---

Omdat er binnen het stageproject wordt samengewerkt met Cinto (de uitvoerende partij) is er ook een onderzoek geweest binnen een Microsoft .NET omgeving. Cinto is een Certified Microsoft Partner welke veel websites, applicaties en dergelijke ontwikkeld in een .NET omgeving. Silverlight is het stukje techniek waarmee Microsoft Adobe's Flash concurreert. Silverlight componenten in websites kom je een stuk minder tegen dan Flash, maar dat wil niet zeggen dat je er niet hetzelfde (en wellicht wel meer!) dan Flash. Hieronder staan enkele links naar inspiratiebronnen, uitleg over Silverlight en dergelijke die meer inzichten geven in Silverlight en een .NET omgeving.  
  
  
  
[http://download.cnet.com/-NET-3D-Visualization-Tool-Package/3000-10250\_4-10872853.html](http://download.cnet.com/-NET-3D-Visualization-Tool-Package/3000-10250_4-10872853.html)  
  
<http://drawlogic.com/2007/07/15/3d-textured-silverlight/>  
  
<http://silverlight.net/forums/t/1860.aspx> - forum draad met meerdere verwijzingen  
  
<http://www.shinedraw.com/3d-illusion/flash-vs-silverlight-3d-image-space/> - 3D vergelijking tussen Flash en Silverlight  
  
<http://metalinkltd.com/?p=114> - WPF 3D engine (door Microsoft)  
  
<http://bubblemark.com/> - WPF in .net  
<http://bubblemark.com/3d/wpfe.htm>  
  
 WPF 3D engine uitleg:  
<http://dotnetslackers.com/articles/silverlight/SilverlightFirstStepsAnalogClock.aspx>  
  
<http://www.codeplex.com/aXelerateSL3D> - andere 3D engine  
  
<http://www.markdawson.org/kit3d/> - Kit3D engine  
  
<http://www.markdawson.org/kit3d/demos/photos/default.html> - buggy 3D grid images  
  
<http://www.simple-talk.com/dotnet/.net-framework/building-a-simple-3d-engine-with-silverlight/> - tutorial om een 3D omgeving te maken  
  
<http://www.shinedraw.com/3d-illusion/flash-and-silverlight-3d-image-rotation/>  
  
<http://www.codeproject.com/KB/silverlight/CubeProject.aspx>  
  
<http://193.138.157.233/graduate-awards/deelnemers-en-opdrachten/172-3d-silverlight> - afstudeerproject met 3D in Silverlight  
  
<http://stackoverflow.com/questions/251718?sort=newest> - discussie over WPF (xbap)  
  
 Een voorbeeld van drie vlakken waarop de zwaartekracht werkt, je kan ballen en blokken laten vallen:  
<http://www.chriscavanagh.com/Chris/CJC.Bullet/BulletDemo1.xbap>  
  
 Een voorbeeld van een kubus opgebouwd uit verschillende vierkante vlakken, de kubus kan draaien en inzoomen kan via de mousewheel:  
<http://windowsclient.net/direct/cubeapp/CubeApp.xbap>  
  
 Een 3D kubus waarin balletjes rondstuiteren, met de muis kun je om de kubus heen draaien:  
<http://bubblemark.com/3d/silverlight1.1.htm>  
  
 Wat Flash kan, kan Silverlight op:  
<http://scorbs.com/workapps/photobook/PhotoBook.xbap>  
  
 RubixCube in Silverlight:  
<http://live.visitmix.com/MIXtify/TenKDisplay.aspx?SubmissionID=0096>  
  
\---  
 Omgeving:  
 .NET Framework 3.0/3.5 runtime  
  
 Software:  
 Visual Studio 2008  
 Expression Blend  
 WPF/E plugin  
  
 Hoe werkt Silverlight?  
<http://www.silverlight-training-guide.com/>  
\---  
**WPF**  
 .NET 3.0 introduced the Windows Presentation Foundation (WPF) API. WPF is a supercharged UI framework  
 for building desktop applications. WPF integrates graphics, animations, a rich data binding layer, video / audio  
 services and print-ready document functionality into a single unified programming model.  
  
**XAML**  
 XAML is an XML-based grammar which can be used to describe a tree of .NET objects (UI-based or otherwise). The XAML file is typically compiled into a WPF application as a binary resource, named ‘BAML’ (the Binary Application Markup Language). At runtime, BAML is used to hydrate declared objects into memory.  
  
**XBAP**  
 XAML Browser Application (XAML is eXtensible Application Markup Language - an XML type markup to store the UI).  
 XBAPs are \*not\* ASP.NET applications and have no native web-awareness. The browser is simply used as a host for the lightweight WPF window (technically termed a ‘WPF-Page’. The benefit however, is that XBAPs provide a much richer UI than traditional HTML/script code. As well, an XBAP can be programmed to invoke remote web services to communicate with ‘home base’.
