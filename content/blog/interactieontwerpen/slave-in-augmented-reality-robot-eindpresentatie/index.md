---
title: "Slave in Augmented Reality Robot eindpresentatie"
date: "2007-11-12T18:38:10.000Z"
description: "

The household is in the future, perhaps entirely controlled by robots. This project visualizes a possible approach t..."
categories: [internet]
comments: true
---

![Slave in Augmented Reality](http://www.interactieontwerpen.nl/_sebastian/previewImages/preview_siarr.jpg)  
  
 The household is in the future, perhaps entirely controlled by robots. This project visualizes a possible approach to this point of view. However, this vision seems not so feasible as the most people usually assume.  
  
 \[ link: [SIARR Wiki pagina](http://interactieontwerpen.nl/reader/index.php/SIARR) by [Sebastian Hagens](http://interactieontwerpen.nl/sebastian) \]
