---
title: "Sleep Cycle, is dit mijn slaapgedrag?"
date: "2010-01-25T10:30:23.000Z"
description: "Sinds eind vorig jaar heb ik me voorgenomen om me een bepaald luxe te gunnen; natuurlijk wakker worden zonder techniek. ..."
categories: [internet]
comments: true
---

Sinds eind vorig jaar heb ik me voorgenomen om me een bepaald luxe te gunnen; natuurlijk wakker worden zonder techniek. Geen irritante snooze elke ochtend op een vaste tijd. Nee, wakker worden wanneer jij er klaar voor bent. Ik hoef niet te noemen dat dit een stuk gemoedelijker wakker worden is dan wanneer je abrupt wakker wordt gepiept door een alarm. Er zijn ondertussen ook genoeg technische hulpmiddelen die je proberen natuurlijker wakker te laten worden; van steeds harder gaande alarmen tot aan de Philips wake-up light.  
 Het helemaal natuurlijk wakker worden elke ochtend is echter mij ook niet gegund, zonder je te 'verslapen' en eigenlijk dus je dag te laat beginnen op locaties waar je geacht wordt om toch op tijd aanwezig te zijn. Ook voor mij begint een normale dag met werkzaamheden zo rond een uur of negen, dat is voor mij al vroeg genoeg. Ik ben een avond/nachtmens, geen ochtendmens.  
  
 Maar ik heb iets fantastisch gevonden voor op mijn iPhone! Sleep cycle!  
  
![](../../../images/sleepcycle/sleepgraph1819.jpg "Sleep cycle")  
  
  
  
 Ik gebruik deze applicatie bijna nu elke nacht sinds twee weken tijd. En de applicatie beloofd mijn slaapgedrag te lezen en vervolgens steeds slimmer te worden wanneer deze mij kan wakker maken door een ingestelde tijd door mij. Deze tijd staat ingesteld op 07:45, maar dat wil niet zeggen dat mijn alarm dan ook daadwerkelijk op dat tijdstip afgaat. Nee, mijn alarm gaat ook vaak genoeg eerder af...maar nooit later natuurlijk.  
 De applicatie wekt jou op het moment wanneer deze ziet dat jij je in een lichte slaap bevindt, wakker worden in een lichte slaap betekent minder vermoeiend wakker worden en voel je je de rest van de dag een stuk fitter! En het werkt, zo ervaar ik het tenminste. Hieronder staat alle grafieken van mijn slaapgedrag van vele nachten.  
  
![](../../../images/sleepcycle/sleepgraph0304.jpg "Sleep cycle")  
![](../../../images/sleepcycle/sleepgraph0405.jpg "Sleep cycle")  
![](../../../images/sleepcycle/sleepgraph0506.jpg "Sleep cycle")  
![](../../../images/sleepcycle/sleepgraph0607.jpg "Sleep cycle")  
![](../../../images/sleepcycle/sleepgraph0708.jpg "Sleep cycle")  
![](../../../images/sleepcycle/sleepgraph0809.jpg "Sleep cycle")  
![](../../../images/sleepcycle/sleepgraph1011.jpg "Sleep cycle")  
![](../../../images/sleepcycle/sleepgraph1112.jpg "Sleep cycle")  
![](../../../images/sleepcycle/sleepgraph1213.jpg "Sleep cycle")  
![](../../../images/sleepcycle/sleepgraph1314.jpg "Sleep cycle")  
![](../../../images/sleepcycle/sleepgraph1415.jpg "Sleep cycle")  
![](../../../images/sleepcycle/sleepgraph1718.jpg "Sleep cycle")  
![](../../../images/sleepcycle/sleepgraph1819.jpg "Sleep cycle")  
  
 De applicatie weet nu dat ik gemiddeld zo'n 7:08 slaap per nacht en herkent ook dit patroon. Het idee is dus dat hij met deze data slimmer wordt en weet wanneer ik op het juiste moment het beste wakker wordt. En dat gaat steeds beter, ik sta elke ochtend toch weer iets fitter op. En toch slaap ik hetzelfde aantal uren per nacht.  
 Daarnaast heeft deze applicatie ook hele fijne geluiden / muziekjes die héél zoet zijn, maar heerlijk zijn om door gewekt te worden. Dat geluid vond ik vanaf het begin al een blijver.  
  
 Verder is het natuurlijk niet de perfecte applicatie om mee wakker te worden, maar deze applicatie wint het wel van mijn traditioneel alarm op mijn nachtkastje. Het slaapgedrag wordt verder geanalyseerd aan de hand van de accelerometer in de iPhone die bewegingen registreert. Je dient je iPhone dan ook ergens op je bed te leggen. In mijn geval leg ik deze onder mijn dekbedovertek (dat is Brabants?) in de hoek naast mijn kussen. Deze input meet niet 100% je echte slaapactiviteiten; deze vinden plaats in onze hersenen. Maar de vertaling vanuit je hersenen naar bewegingen en vervolgens naar de iPhone zijn wel interessant. Het is wachten tot matrasfabrikanten ook met zulke innovatieve toepassingen komen in samenwerking met alarmklok fabrikanten.  
  
 Meer informatie over deze applicatie:  
<http://www.iphoneclub.nl/53022/review-sleep-cycle-slaappatroon-vastleggen-en-gewekt-worden/>  
[http://sync.nl/uitgerust-wakker-met-iphone-app-sleep-cycle/?from\_rss=true](http://sync.nl/uitgerust-wakker-met-iphone-app-sleep-cycle/?from_rss=true)  
<http://www.dutchcowgirls.nl/gadgets/3104>
