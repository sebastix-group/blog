---
title: "Social media backup tools"
date: "2010-01-19T10:45:33.000Z"
description: "Na aanleiding van mijn scriptie ben ik al een tijdje aan het zoeken naar handige tools om persoonlijke informatie op ver..."
categories: [internet]
comments: true
---

Na aanleiding van mijn scriptie ben ik al een tijdje aan het zoeken naar handige tools om persoonlijke informatie op verschillende 'web2.0' websites te archiveren. Waarom? De gedachte dat ik niet direct in het bezit ben van al die informatie -die van mij is- zet mij aan het denken. Want ik wil eigenlijk over een jaar of 25 nog steeds beschikken over deze informatie om deze dan delen met mijn kinderen (ben dan bijna 50, dusja) en andere warme relaties.  
 Met de gedachte dat we over 25 jaar nog steeds over deze informatie beschikking neem ik niet zo 1-2-3 voor lief. Ik kan namelijk wel een aantal persoonlijke voorbeelden uit verleden halen waar ik informatie online had staan die van mij afkwamen; echter ben ik deze volledig kwijt (zowel online als offline).  
  
 Goed, welke tools zijn er tegenwoordig om die informatie toch naar jezelf toe te trekken en te archiveren?  
  
  
  
 Niet bijzonder veel! Of ik moet nog verder zoeken...maar hieronder enkele bevindingen van mij:  
  
[Printyourtwitter.com](http://www.printyourtwitter.com)  
 Een hele simpele tool waarmee je je tweets van jezelf en gerelateerd kunt binnenhalen en vervolgens worden opgeslagen in een PDF bestand. Helaas lukte de eerste poging al niet om van al mijn tweets een PDF te maken (maximaal 3200). Dus ik probeerde het met maximaal 1000 tweets, met succes. Ik kreeg 715 tweets van mezelf terug in een PDF bestand. De tweets krijg je, alvorens je een PDF maakt in een HTML bestand dus die heb ik ook bewaard; HTML kan ik eenvoudiger uitlezen dan een PDF bestand.  
  
[Backupify](http://www.backupify.com/)  
 Deze website / dienst is interessant. Je kunt hier namelijk meerdere platformen koppelen voor backups. En meerdere platformen zijn in ontwikkeling om te kunnen archiveren. Het koppelen van je bestaande accounts op die websites gaat relatief makkelijk; bij de een moet je wat meer doen dan de ander.  
 De backups die worden gecreëerd door Backupify is echter ook niet altijd even makkelijk om op te slaan.

  
- ![Flickr](https://secure.backupify.com/images/flickr.gif) Flickr  
   Belangrijk bij Flickr backups is dat je een titel moet toevoegen aan elke foto die je upload via deze dienst, anders kun je deze niet terughalen via Backupify. Backupify slaat elke foto op haar eigen server. Waarom?! Dit wil ik niet...ik wil de foto zelf kunnen downloaden in 1 keer, want nu staat de foto alsnog bij een derde partij en is dus niet in mijn beheer. Een optie om alles in 1 keer te downloaden is er niet...helaas.
  

  
  
- ![Twitter](https://secure.backupify.com/images/twitter.gif) Twitter  
   XML bestanden van je updates, mentions, direct messages, je volgers en meer. Deze XML bestanden zijn niet moeilijk en altijd zeer toegankelijk te maken in verschillende omgevingen.
  

  
  
- ![Delicious](https://secure.backupify.com/images/delicious.gif) Delicious  
   Yes! 1 XML bestand met daarin al mijn bookmarks! Dit is wat ik zoek, handig om uit te lezen!
  
- ![Zoho](https://secure.backupify.com/images/zoho.gif) Zoho  
   Lijkt niet te werken...
  
- ![Google Docs](https://secure.backupify.com/images/gdocs.gif) Google Docs  
   Ik heb hier enkele bestanden staan. Deze worden gekopieerd naar de server van Backupify zodat je ze vanaf daar kunt downloaden. Dus nee, de bestanden zijn niet in mijn beheer.
  
- ![Photobucket](https://secure.backupify.com/images/photobucket.gif) Photobucket  
   Maak ik geen gebruik van.
  
- ![Wordpress](https://secure.backupify.com/images/wordpress.gif) Wordpress  
   Hiervoor moet je een Wordpress plugin installeren op je eigen weblog alvorens Backupify hier backups van kan maken. Aangezien mijn Wordpress wel min of meer in eigen beheer is, vind ik het backuppen van mijn eigen weblog niet noodzakelijk. Via RSS kan ik namelijk ook al wel de nodige informatie verzamelen en het geheel staat gehost op een eigen server.
  
- ![Basecamp](https://secure.backupify.com/images/basecamp.gif) Basecamp  
   Maak ik geen gebruik van.
  
- ![Gmail](https://secure.backupify.com/images/gmail.gif) Gmail  
   Lijkt niet te werken...
  
- ![Facebook](https://secure.backupify.com/images/facebook.gif) Facebook  
   Van elke foto die op je Facebook staat wordt een backup gemaakt naar Backupify, net zoals wordt gebeurt met je foto's op Flickr. Verder is er ook 1 algemene XML bestand waarin je profielinformatie staat, jouw links, jouw status updates en meer. Het lijkt erop dat Backupify niet alles opslaat wat in je eigen profiel is ingesloten. Bij nader inzien bij het bekijken van dit XML bestand lijkt de data niet echt netjes 'nested' met een duidelijke structuur. Twijfelgeval of ik wel iets met die backups kan.
  
- ![FriendFeed](https://secure.backupify.com/images/friendfeed.gif) FriendFeed  
   Maak ik geen gebruik van.
  
- ![Blogger](https://secure.backupify.com/images/blogger.gif) Blogger  
   Maak ik geen gebruik van.
  
- ![Hotmail](https://secure.backupify.com/images/hotmail.gif) Hotmail  
   Maak ik geen gebruik van.
  

  
 Tot 31 januari is Backupify gratis, daarna kost het geld. Veel backup tools kosten geld kwam ik achter in mijn zoektocht. Het zijn geen hoge bedragen, maar mensen zullen niet snel betalen als ze de toegevoegde waarde niet zien van hun backups (en deze ligt ver in de toekomst).  
  
[BackupMy.net](http://backupmy.net/)  
  
 Deze website biedt ook de mogelijkheid om je emails, tweets, weblog en foto's te backuppen. Dit kost wel een paar dollars per jaar. Zo te zien kun je wel gratis -tijdelijk- je tweets backuppen. Uiteraard gevolgd met een tweet die je dan verspreid \*spam?\*, maar dat is niet zo erg. Het valt me wel op dat ik tot twee keer toe Twitter toegang moet verlenen aan BackupMy. Na het aanmaken van een account heb ik wel de beschikking over de backup van mijn tweets in welgeteld drie bestandsformaten; HTML, JSON en XML. Verhip, dat is wel een pluspuntje!  
  
[SocialSafe](http://socialsafe.net/)  
 Een tool waarmee je je Facebook account mee kunt archiveren / backuppen. Ik vond een mooie video op hun website, dus waarom deze niet hier plaatsen? Scheelt mij weer typwerk en jouw leeswerk.  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="488" width="650"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="src" value="http://vimeo.com/moogaloop.swf?clip_id=6989487&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=&fullscreen=1"></param><embed allowfullscreen="true" allowscriptaccess="always" height="488" src="http://vimeo.com/moogaloop.swf?clip_id=6989487&server=vimeo.com&show_title=1&show_byline=1&show_portrait=0&color=&fullscreen=1" type="application/x-shockwave-flash" width="650"></embed></object>  
  
 Dit is een Adobe AIR applicatie. Het mooie -wat die andere tools niet doen- van deze applicatie is dat je ook al daadwerkelijk je backups inzichtelijk worden gemaakt. Je kunt namelijk al zien wat je precies hebt verzameld en deze kun je vergelijken met andere eerdere backups. Dit voegt al iets toe; je kunt vergelijken en veranderen waarnemen. Een vraag die me wel bijblijft; waar worden de backups opgeslagen? In de FAQ op de website vind ik het antwoord. Een goed antwoord; lokaal op je eigen computer. Een duim voor deze applicatie! Jammer dat het alleen voor Facebook is...voor nu. Daarnaast kost het nu ook eigenlijk niets, nog geen € 2,50. Ik ga eens overwegen om deze applicatie te downloaden en te gebruiken voor mijn eigen Facebook. Een hele mooie unieke toevoeging vind ik namelijk het onderling vergelijken van de backups en de daaruit komende resultaten (het inzichtelijk maken van veranderingen).  
  
[TweetBackup.com](http://tweetbackup.com/)  
 Wederom een dienst waarmee je al je tweets kunt verzamelen en daarna kunt opslaan als tekst, CSV, RSS of HTML. Ook een aanrader, mede om deze eenvoudig in gebruik is.  
  
  
*Andere backup tools:*  
  
[http://brandmanagement20.com/services/backup-and-protect/  
 http://www.tweets2mail.com/](http://brandmanagement20.com/services/backup-and-protect/)  
<http://www.almostsavvy.com/2009/04/12/back-it-up-own-your-linkedin-information/>  
  
**Tot slot**  
 Hoe zit het verder met de Nederlandse social media? Hyves? En waarom zie ik heel weinig voorbij komen rondom backuppen van je gegevens op LinkedIn (LinkedIn biedt wel een eigen export functie)? Gat...gaatje...in de markt? Een backup maken van je Hyves is momenteel niet mogelijk, geen enkele partij die een dienst aanbiedt waarmee dit kan. Volgens mij houdt Hyves dit ook bewust tegen.  
  
 Verder wil ik nog een kanttekening maken bij alle diensten die hun backups toch op hun eigen server plaatsen, of in ieder geval ergens waardoor de informatie nog steeds niet in jouw beheer valt. Offline en online loopt steeds meer in elkaar over, maar als bepaalde informatie echt op een schijf staat die ik fysiek bevat kan ik pas gerust stellen dat ik een backup heb.  
  
 Hmmm...genoeg te bespreken ook in mijn scriptie hierover. Snel maar weer eens verder daarmee!  
  
 Verder hoor ik graag jouw reactie op deze materie, dat kan op deze pagina; de reactie functionaliteit heb ik sinds kort weer geactiveerd.  
  
*Ander leesvoer en gebruikte bronnen:*  
[http://www.readwriteweb.com/archives/cloud\_storage\_for\_social\_media\_twitter\_backup.php](http://brandmanagement20.com/services/backup-and-protect/)  
[http://www.appscout.com/2009/06/lifestream\_backup\_keeps\_your\_o.php](http://www.appscout.com/2009/06/lifestream_backup_keeps_your_o.php)  
[http://askbobrankin.com/social\_networking\_backup.html](http://askbobrankin.com/social_networking_backup.html)  
<http://www.archief20.org/group/archiveren20/forum/topics/twitter-archiveren>
