---
title: "Data; waarom?"
date: "2010-03-31T10:20:40.000Z"
description: "Voor langere tijd houdt het me al bezig wat we toch allemaal moeten met al die digitale informatie genaamd data die voor..."
categories: [internet]
comments: true
---

Voor langere tijd houdt het me al bezig wat we toch allemaal moeten met al die digitale informatie genaamd data die voor het oprapen ligt voor mensen zoals ik. Ik doe namelijk iets met data en wat ik ermee doe is alles zeggend in datgene wat ik wil vertellen en hoe anderen mij zien of gaan zien. Dat laat onderstaand filmpje van IBM ook zien, een typisch 'feel good' filmpje maar ik geloof wel in de kern die het filmpje verteld.  
  
 Update 12-02-2011; helaas is de video verwijderd op YouTube door de gebruiker (IBM)  
  
 Daarop aansluitend durf ik ook te stellen en me achter de uitspraak van Clay Shirky te zetten; **It's not information overload. It's but filter failure**. Juist door me te laten inspireren met deze gedachtegoed, zie ik nog een heleboel mogelijkheden in de toekomst met data en kan ik eigenlijk niets anders stellen dat alles nog echt in kinderschoenen staat. Hoe denk jij daarover?  
  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="513" width="640"><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="src" value="http://www.youtube.com/v/LabqeJEOQyI&hl=nl_NL&fs=1&"></param><param name="allowfullscreen" value="true"></param><embed allowfullscreen="true" allowscriptaccess="always" height="513" src="http://www.youtube.com/v/LabqeJEOQyI&hl=nl_NL&fs=1&" type="application/x-shockwave-flash" width="640"></embed></object>  
  
 Deze materie houdt me al langer bezig, zeker na het lezen van diverse artikelen op Frankwatching.com van oa Sander Duivestijn die als een soort trigger hebben gewerkt aan aantal maanden terug ter voorbereiding van mijn scriptie;  
  
 Data is de nieuwe olie - <http://www.frankwatching.com/archive/2009/11/02/data-is-de-nieuwe-olie/>  
  
 Stop met zoeken - <http://www.frankwatching.com/archive/2009/11/27/stop-met-zoeken/>  
  
 Het derde oog - <http://www.frankwatching.com/archive/2009/11/29/het-derde-oog/>
