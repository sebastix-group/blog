---
title: "HTML5 canvas & javascript"
date: "2009-03-11T16:54:20.000Z"
description: "Bekijk hier een meer recent bericht (30 augustus 2010) over HTML5 en canvas op mijn weblog: http://interactieontwerpen.s..."
categories: [internet]
comments: true
---

Bekijk hier een meer recent bericht (30 augustus 2010) over HTML5 en canvas op mijn weblog: <http://interactieontwerpen.sebastix.nl/html5-canvas-meer-voorbeelden/>  
  
**HTML Canvas**  
[http://en.wikipedia.org/wiki/Canvas\_(HTML\_element)](http://en.wikipedia.org/wiki/Canvas_(HTML_element))  
  
 In HTML5 is een canvas element opgenomen die het mogelijk maakt bitmap afbeeldingen dynamisch te renderen met behulp van javascript.  
 Een canvas is een gebied waarin je een graphic kunt tekenen. Met behulp van javascript kun je bijvoorbeeld heel eenvoudig een lijntje tekenen van punt A naar punt B (met beide een x,y coördinaat).  
  
  
  
 Canvas element op W3C / WHATWG.org:  
<http://www.whatwg.org/specs/web-apps/current-work/#the-canvas-element>  
  
 Een goede tutorial waarin de basis wordt uitgelegd:  
[https://developer.mozilla.org/En/Canvas\_tutorial](https://developer.mozilla.org/En/Canvas_tutorial)  
 Tutorial door Apple:  
<http://developer.apple.com/documentation/AppleApplications/Conceptual/SafariJSProgTopics/Tasks/Canvas.html>  
  
 Het is ook mogelijk om in 3D te tekenen in een canvas element. Hier zijn al enkele bestaande API’s voor waarvan sommige met OpenGL werken.  
<http://www.c3dl.org/>  
  
 Artikel met uitleg hoe canvas in 3D werkt:  
<http://www.devx.com/webdev/Article/38983>  
  
 Tutorial hoe Wolfenstein3D te maken:  
<http://dev.opera.com/articles/view/creating-pseudo-3d-games-with-html-5-can-1/>  
  
**Canvas API’s:**  
  
 Google excanvas  
<http://groups.google.com/group/google-excanvas>  
<http://excanvas.sourceforge.net/>  
  
 Voorbeelden:  
 • [http://iterationsix.com/particle\_fountain/particle\_fountain.html](http://iterationsix.com/particle_fountain/particle_fountain.html)  
 Interactieve particle dropper met de muis  
  
 • <http://www.benjoffe.com/code/demos/canvascape/>  
 3D shooter  
  
 • [ http://www.gradius-js.com/ ](<http://www.gradius-js.com/ >)  
 2D shooter  
  
 • [https://developer.mozilla.org/en/A\_Basic\_RayCaster](https://developer.mozilla.org/en/A_Basic_RayCaster)  
 3D doolhof  
  
 • <http://andrewwooldridge.com/canvas/canvasgame001/canvasgame002.html>  
 2D game  
  
 • <http://www.blobsallad.se/>  
 Erg leuke interactieve blob  
  
 • <http://arapehlivanian.com/wp-content../../../images/2007/02/canvas.html>  
 Vliegend door de ruimte  
  
 • [http://skuld.bmsc.washington.edu/~merritt/gnuplot/canvas\_demos/](http://skuld.bmsc.washington.edu/~merritt/gnuplot/canvas_demos/)  
 Canvas driver gnuplot based  
  
 • <http://wiioperasdk.com/indexfps.php>  
 3D doolhof met een Wii controls?  
  
 • <http://tapper-ware.net/canvas3d/>  
 3D canvas waar je doorheen kunt navigeren  
  
 • <http://ernestdelgado.com/public-tests/canvas-gpsmap/>  
 Navigatiemap in canvas  
  
 • <http://www.nihilogic.dk/labs/mariokart/>  
 Mariokart!  
  
 • <http://www.xs4all.nl/~peterned/3d/>  
 3D canvas met Flickr foto’s  
  
<http://gyu.que.jp/jscloth/>  
 3D canvas voorbeelden  
  
 Bekijk hier een meer recent bericht (30 augustus 2010) over HTML5 en canvas op mijn weblog: <http://interactieontwerpen.sebastix.nl/html5-canvas-meer-voorbeelden/>
