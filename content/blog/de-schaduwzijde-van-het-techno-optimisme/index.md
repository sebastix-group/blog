---
layout: blog
title: De schaduwzijde van het techno-optimisme
date: 2021-01-19T10:52:32.939Z
description: "Wat is de schaduwzijde van het techno-optimisme? "
categories: Online privacy, data kapitalisme
comments: "true"
---

<audio controls style="width:100%">
    <source src="/blog/audio/futureshockhansschnitzlereindmix.mp3" type="audio/mpeg">
</audio>

> Na jaren van dataroof, bespieding en misleiding kent Silicon Valley ons door en door. Ooit werd de technologische vooruitgang bejubeld, maar nu zien we steeds meer de schaduwzijde van deze ontwikkeling. Tegenlicht-redacteur Tom Reijner sprak op de Europese Dag van de Privacy met filosoof en publicist Hans Schnitzler, die al vroeg zag hoe het de verkeerde kant opging.

<https://www.nporadio1.nl/podcasts/future-shock/1561-future-shock-de-schaduwzijde-van-het-techno-optimisme-43>