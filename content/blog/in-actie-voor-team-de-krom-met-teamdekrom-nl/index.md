---
layout: blog
title: In actie voor Team De Krom met teamdekrom.nl
date: 2022-01-26T08:36:42.277Z
description: "Eind juni ga ik vijf dagen fietsen in Italië voor Giro di KiKa
  over de mooiste cols in de Dolomieten en rondom Bormio. Dit doe ik niet
  alleen, maar met een heel team: Team De Krom. We hebben als doel zoveel
  mogelijk geld ophalen voor stichting KiKa. Hier zijn we al volop mee bezig met
  verschillende initiatieven. Afgelopen weekend stond onze tweede trainingsrit
  op de agenda over verschillende onverharde paden. "
categories: teamdekrom, giro di kika, drupal, commerce, mollie
comments: "true"
---
Een paar weken terug heb ik voorgesteld om dit te organiseren in navolging van onze eerste trainingsrit. Deze eerste rit hebben we in groepsverband verreden met ongeveer 60 deelnemers. Vanwege de verschillende covid-beperkingen was dat ditmaal niet mogelijk. Mijn oplossing hiervoor was om een GPS-route uit te werken zodat iedere deelnemer deze individueel kan rijden met fietsnavigatie. Alle inschrijvingen en betalingen wilde ik hiervoor digitaal verwerken via een website. Ik ben dan ook aan de slag gegaan met het uitzetten van de routes en het realiseren van de website. 

![https://teamdekrom.nl](schermafbeelding-2022-01-26-om-15.20.13.png "https://teamdekrom.nl")

De website: <https://teamdekrom.nl>

### De routes uitzetten

Hoe heb ik de routes uitgewerkt? Een belangrijk aandachtspunt van de route was dat deze toegankelijk moest zijn voor een brede groep deelnemers. Daarmee bedoel ik dat er geen te moeilijke passages in de route zitten. Ik heb mijn kennis van de lokale wegen, de Strava heatmap en de Komoot routebouwer gebruikt om de eerste conceptroutes uit te werken. Vervolgens ben ik meerdere malen met de fiets deze routes gaan verkennen. Na elke verkenning heb ik de route hier en daar aangepast. In totaal heb ik drie verkenningsritjes gedaan om uiteindelijk tot een definitieve 45km en 75km route te komen.

![](schermafbeelding-2022-01-26-om-15.31.59.png)

![](schermafbeelding-2022-01-26-om-15.33.25.png)

### De website

Naast het uitzetten van deze routes ging ik dus aan de slag met het uitwerken van de website op <https://teamdekrom.nl>. Welke technische onderdelen heb ik verwerkt in deze site?

* [Drupal 9](https://www.drupal.org/) als basis
* [Drupal Commerce](https://www.drupal.org/project/commerce) voor een complete webshop functionaliteit
* [Mollie](https://www.drupal.org/project/mollie) voor het afhandelen van alle betalingen
* [Symfony Mailer](https://www.drupal.org/project/symfony_mailer) voor het versturen van mails
* [Matomo](https://www.drupal.org/project/matomo) om statistieken bij te houden
* [DaisyUI](https://daisyui.com/) als CSS framework met herbruikbare componenten

### De resultaten

We hebben de website met daarop de informatie over de trainingsrit slechts een week van tevoren gedeeld via onze socials. Ons team bestaat uit meer dan 15 personen, dus we bereikten al snel veel mensen.

* Via de website zijn er 50 betalingen verwerkt
* In totaal hebben zich 103 deelnemers via de site aangemeld
* Op de website hadden we 490 unieke bezoekers in een week tijd

Op de dag zelf hebben mensen ook nog iets extra's gedoneerd.

In totaal hebben we met deze trainingsrit **€ 1235,-** opgehaald.\
Het was waanzinnig succes en een echte teamprestatie!

![](5pf9hn_kyeann6ephviwomni8_lh1nqodya8hyq85tg-2048x1127.jpg)

### **Meer informatie vind je op onze Giro di KiKa pagina waar je ook je steentje kunt bijdragen met een donatie:**

### **<https://www.girodikika.nl/team-de-krom>**