---
layout: blog
title: Animating a conic-gradient on a button border with CSS
date: 2021-06-02T12:18:43.632Z
description: Working on my CSS/SASS skills and my new website. Have a look to a
  small piece of CSS code of an animated conic-gradient on a button border.
categories: CSS, SASS, SCSS
comments: "true"
---
<iframe height="500" style="width: 100%;" scrolling="no" title="Animated conic-gradient border on button" src="https://codepen.io/Sebastix/embed/gOmvmWK?height=265&theme-id=light&default-tab=css,result" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href='https://codepen.io/Sebastix/pen/gOmvmWK'>Animated conic-gradient border on button</a> by Sebastian Hagens
  (<a href='https://codepen.io/Sebastix'>@Sebastix</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>