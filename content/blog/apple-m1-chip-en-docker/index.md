---
title: Apple’s M1 chip en Docker
date: 2020-12-02T11:13:13.000Z
description: De revolutionaire M1 computer chip van Apple voor desktop
  computers. Maar werkt de Docker software al?
categories:
  - apple
  - macbook
  - m1
  - docker
comments: true
---
Het is lang geleden dat ik Apple iets revolutionairs heb zien brengen op de markt.
Eerlijk gezegd ben ik al jaren niet meer onder de indruk van de innovatieprestaties van ze. Tot vorige maand. Apple heeft een eigen ARM chip ontwikkeld én deze nu ook op de markt gebracht in een aantal desktop computers. De Mac Mini, Macbook Air en Macbook Pro 13inch. De prestaties van deze chip [zijn indrukwekkend](https://tweakers.net/nieuws/174706/reviews-tonen-dat-macs-met-apple-m1-soc-veel-sneller-zijn-dan-intel-varianten.html). 

## Docker

Het was voor mij een trigger om te kijken of deze nieuwe chip mij over de streep trekt om mijn huidige Macbook Pro’s (midden 2014 en begin 2015) te vervangen. Tijdens de ontwikkeling van applicaties en websites merk ik geregeld dat ik het maximale vraag qua hardware resources. Met name het werkgeheugen van 16GB is daarin een beperkende factor. 
Als developer werk ik met PHPStorm en Docker. Aangezien Docker virtualisatiesoftware is voor het opzetten van containers waarin ik apps of websites ontwikkel, was ik benieuwd of deze software al geschikt was voor de ARM architectuur van Apple’s M1 chip. Helaas kwam ik al snel [dit bericht](https://www.docker.com/blog/apple-silicon-m1-chips-and-docker/) tegen dat dit nog niet mogelijk is. En dat geldt dus ook voor andere virtualisatiesoftware zoals VirtualBox, VMWare en Parallels.   

## Geduld

Na nog wat meer linkjes ([hier](https://medium.com/better-programming/macbook-m1-breaks-docker-development-14566ab6fa2e) en [hier](https://finestructure.co/blog/2020/11/27/running-docker-on-apple-silicon-m1)) te hebben gevonden heb ik de conclusie getrokken dat ik geduld moet hebben. 
Gelukkig zitten de ontwikkelaars van Docker niet stil.\
https://appleinsider.com/articles/20/11/29/early-docker-build-demonstrates-incoming-apple-silicon-support

https://twitter.com/morlhon/status/1332609373051478016

Volg [dit issue](https://github.com/docker/roadmap/issues/142) op Github om de voortgang in de gaten te houden rondom Docker’s compatibiliteit voor de M1 chip van Apple.

*Hoe lang het gaat duren voordat we aan de slag kunnen met Docker?*\
Daar durf ik weinig over te zeggen, maar ik hoop dat het kan binnen 3 tot 6 maanden.