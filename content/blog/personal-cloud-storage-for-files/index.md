---
layout: blog
title: Personal cloud storage for files
date: 2021-11-08T15:13:44.361Z
description: Previous years I used STACK as a personal cloud storage solution.
  Before I started using STACK, I had a hardware device called Transporter Sync
  with a 2TB harddrive attached to it. The Transporter Sync hard- and software
  gave me own cloud based storage solution with a native expierence in my
  operating system (MacOS). Just like Dropbox , Google Drive or iCloud.
categories: personal storage, backup
comments: "true"
---
In a couple of years, both solutions stopped working. Transporter Sync discontinued their product after their company was aquired by Nexan. STACK stopped their free storage solution because they are expierencing backup issues due legacy hardware configurations.

Over more than a year I'm running a 24/7 computer connected to the internet and I should be able to setup my own hosted storage solution. Also a virtual private server (VPS) can be used, but that's an expensive solution if you need a couple of terabytes of storage. 

What do I expect from this solution?

1. The most important is that the solution will be independent from a third party as much as possible. The data should be stored decentralized and ownership should be secured with a personal private key configuration. 
2. The solution can be used on multiple devices and operating systems. 
3. File usage should be native integrated in my operation system.

In my search for a solution, check out the options I've looked into.

### NextCloud

[https://nextcloud.com/](https://nextcloud.com/)

NextCloud is much more than a data storage solution. It can be used for all different types of data such as your contacts, calendar, chats, email, passwords etc.

Docker setup: https://github.com/nextcloud/docker & [https://blog.ssdnodes.com/blog/installing-nextcloud-docker/](https://blog.ssdnodes.com/blog/installing-nextcloud-docker/) 

### Seafile

[https://www.seafile.com/en/home/](https://www.seafile.com/en/home/) 

File sync system and collaboration features on files. It has full OS integration, just like Google Drive and Dropbox. 

Docker setup: [https://manual.seafile.com/docker/deploy_seafile_with_docker/](https://manual.seafile.com/docker/deploy_seafile_with_docker/) & [https://itsfoss.com/deploy-seafile-server-docker/](https://itsfoss.com/deploy-seafile-server-docker/)

### Syncthing

[https://syncthing.net/](https://syncthing.net/) 

Peer-to-peer file synchronisation software. Use any device as a node to keep files in sync.

Docker setup: [https://docs.linuxserver.io/images/docker-syncthing](https://docs.linuxserver.io/images/docker-syncthing), https://github.com/joeybaker/docker-syncthing,  

Native MacOS integration: https://github.com/syncthing/syncthing-macos 

### SIA and SiaSync - blockchain based decentralized storage

[https://sia.tech/](https://sia.tech/) and [https://github.com/tbenz9/siasync](https://github.com/tbenz9/siasync) 

You need to run a full node which will take a lot of harddrive space on your computer because you have to download and sync the SIA blockchain.

### Brig

[https://brig.readthedocs.io/en/master/](https://brig.readthedocs.io/en/master/) 

A decentralized storage application with version control. Technical skills are required to install this application.

### 0box from 0chain - blockchain based decentalized storage

[https://0chain.net/0Box.html](https://0chain.net/0Box.html)

Only available for mobile devices. Also this tool is using an own token (ZCN) on a blockchain network.

### Fission Drive

[https://drive.fission.codes/](https://drive.fission.codes/) 

Not able to sign in. I've made two different accounts to test this tool, but I'm stuck on the login. 

### Slate

[https://slate.host](https://slate.host/)

All uploaded data is public and storing data private is not available yet.

### Space

[https://space.storage/](https://space.storage/) 

[https://blog.space.storage/posts/how-is-space-different-from-cloud-storage](https://blog.space.storage/posts/how-is-space-different-from-cloud-storage)

Not availble, this project is in development.

# My choice

In my opinion, NextCloud, Seafile and Syncthing are the most mature solutions. I haven't installed one of those yet, but when I've found some time I will start with a NextCloud installation running with Docker.

A blockchain based storage solution is something to consider in the future, because of the fully decentralized character of storing your data. I hope a hybrid model will evolve here: using a self hosted storage solution with a backup feature with a blockchain storage provider like SIA or Filecoin.