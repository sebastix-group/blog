---
layout: blog
title: Empower webapps with the BeforeInstallPromptEvent feature
date: 2024-04-12T16:54:53.1Z
description: As a fullstack Drupal developer I’m already working more than two years on an ambitious project for Health-Thing. The project offers a platform where knowlegde is shared between young healthcare professionals about their wellbeing. With this platform the goal is to retain them in this healthcare space as there is a massive fallout in this sector.
categories: pwa, webapp, openweb, apple, chrome, android, safari
comments: "true"
---

## Progressive Web App (PWA)

For the best experience it’s strongly advised to use the platform as a progressive web application (PWA) on your device (smartphone and desktop). This means you can ‘install’ the website as a webapp. If you’re curious what is possible with a PWA, please check out https://whatpwacando.today/. In this blog I’ll point out the BeforeInstallPromptEvent which is very handy to inform the visitor the install the website as a webapp.

## BeforeInstallPromptEvent object and API

Docs: https://developer.mozilla.org/en-US/docs/Web/API/BeforeInstallPromptEvent

> A PWA can provide its own in-page UI for the user to open the install prompt, instead of relying on the UI provided by the browser by default. This enables a PWA to provide some context and a reason for the user to install the PWA, and can help make the install user flow easier to discover.

With the BeforeInstallPromptEvent we can trigger a browser function to bring up a notification  which asks to install the website as a webapp.

This is a JavaScript snippet which applies to a `<button id="install" hidden>Install</button>` element in your HTML document.

```jsx
let installPrompt = null;
const installButton = document.querySelector("#install");

window.addEventListener("beforeinstallprompt", (event) => {
  event.preventDefault();
  installPrompt = event;
  installButton.removeAttribute("hidden");
});

installButton.addEventListener("click", async () => {
  if (!installPrompt) {
    return;
  }
  const result = await installPrompt.prompt();
  console.log(`Install prompt was: ${result.outcome}`);
  installPrompt = null;
  installButton.setAttribute("hidden", "");
});
```

On desktop it will looks like this:

![](https://shares.sebastix.dev/dR6rNjKI.png#4abb877ca50c2335a32aec5ac00f5abf61df1c330d17c07e060e13b52e772ded)

On your mobile device it looks like this:

![](https://shares.sebastix.dev/DJbPWhG3.png#d00dfd7ad64049c90cf687f059bf1f2fff913af8333b9ce5ca4281b68cda7043)

## Browser support

Please check [caniuse.com](https://caniuse.com/?search=beforeinstallprompt) to find out on which device + browser this neat feature works.

![](https://shares.sebastix.dev/XdWBXUja.png#049d02f3efbbdc038723b4e9c3bd590404777df0897b071542da164968d9a5f7)

As you can see in the screenshot above, it’s not available in all modern browsers (yet).

- No support in the Safari and Firefox browser
- No support on iOS devices in any browser

## Workaround for iOS devices

What you can do is build a custom solution for iOS devices, because iOS supports installing a webapp on your device. But Apple is lacking to make this feature easy and friendly to use.

This is my current custom solution integrated with the Drupal PWA contrib module.

![](https://shares.sebastix.dev/rpTnBnlI.png)

In the screenshot above you can see the blue button on the bottom on an Android device (Google browser). On the right you can see a banner at the bottom on an iOS device (Safari browser) instructions on how to install the webapp. In the video below you can see it in full action.

<video controls>
    <source src="/blog/video/Screen-Recording-2024-04-12-16-44-37.mp4" type="video/mp4"></source>
</video>

https://shares.sebastix.dev/eerRly7C.mp4 

## Submit a webapp to the App Store

While Apple announced their plans to remove the PWA features, I anticipated this in looking for a solution how we could publish the Health-Thing webapp as a native application to the App Store. It took me less than an hour to come up with the solution below.

![](https://shares.sebastix.dev/9IgelyHF.png)

**Resources**

- https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Guides/Making_PWAs_installable
- https://developer.apple.com/support/dma-and-apps-in-the-eu#ios-app-eu
- https://mattcool.tech/posts/escaping-the-app-store-with-progressive-web-apps
- https://stackoverflow.com/questions/55302527/does-ios-support-the-beforeinstallpromp-event
- https://bugs.webkit.org/show_bug.cgi?id=193959