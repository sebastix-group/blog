---
layout: blog
title: Automatisch elke week Bitcoin of andere crypto kopen
date: 2021-03-15T19:54:19.266Z
description: Sparen in Bitcoin of een andere crypto asset doe je door periodiek
  en geautomatiseerd euro's om te wisselen op een exchange.
categories: cryptocurrency, dca, bitcoin
comments: "true"
---
Sinds ik me vorige zomer ben gaan verdiepen in de wereld van cryptocurrencies, leerde ik al snel de investeringsstrategie [dollar cost averaging](https://en.wikipedia.org/wiki/Dollar_cost_averaging) kennen (periodiek voor een vast bedrag een aandeel kopen). Daar zijn vast al vele apps of andere tools voor dacht ik toen! Helaas bleek het tegendeel waar, dat aanbod was namelijk beperkt. Gelukkig was er wel 1 tool die precies in mijn straatje pastte: [Bitcoin-DCA](https://github.com/jorijn/bitcoin-dca) gerealiseerd door [Jorijn](https://www.jorijn.com). *In sommige kringen wordt hij ook wel de magiër genoemd.* Het programma is geschreven in PHP met het Symfony framework. Aangezien de code *opensource* is, kun je elke regel inzien. Je kunt zelf ook aanpassingen maken in een eigen versie van de tool (dit noemen we dan een *fork*). Eind vorig jaar heb ik een fork gemaakt, want ik wilde naast Bitcoin ook een andere crypto assets kopen. Nadat ik een begin had gemaakt, was Jorijn zelfs zo vrij om mijn aanpassingen te [beoordelen](https://github.com/Sebastix/crypto-dca/pull/1). Dat was ontzettend leerzaam en het geeft een stimulans om de code nog beter te maken. Sinds deze week heb ik mijn *fork* in gebruik genomen om geautomatiseerd te kopen.

De software van Jorijn voor Bitcoin dollar costs averaging vind je op <https://github.com/Jorijn/bitcoin-dca>. De documentatie over hoe je de software installeert en gebruikt staat op <https://bitcoin-dca.readthedocs.io/en/latest/>. De fork die ik heb gemaakt voor het kopen van andere crypto assets is te vinden op <https://github.com/Sebastix/crypto-dca>.

Wat heb je nodig?

* Een bankrekingsnummer
* Een account op een crypto Exchange, zoals [Kraken](https://www.kraken.com) of [BL3P](https://bl3p.eu/)
* Een computer die 24/7 online is om periodiek te kunnen kopen
* Docker dient geïnstalleerd te zijn op die computer
* Een API key van Kraken die je configureert in de software
* Technische kennis..

Hoe werkt het?

1. Elke week wordt er een gepland betaling uitgevoerd via Instant SEPA van mijn bank (Bunq) naar mijn account op Kraken. Binnen enkele minuten is mijn saldo op Kraken verhoogd.
2. Het script draait nu elke maandagnacht om een vast bedrag aan Bitcoin te kopen via mijn Kraken account. Dit kun je zelf helemaal naar eigen wens instellen met *cronjobs* (hiermee stel je in welk programma er op een bepaald tijdstip wordt uitgevoerd).

![](photo_2021-03-15-21.06.49.jpeg)

Zo simpel werkt het.