---
title: Beste vijf podcasts van 4e kwartaal 2022
date: 2023-01-09T20:13:13.000Z
description: Ik heb besloten om het format van mijn vijf favoriete podcastshows te wijzigen naar mijn vijf favoriete podcastafleveringen die ik heb beluisterd afgelopen kwartaal.
categories: podcasts
comments: true
---

## [Lichaam in de woestijn (aflevering 1 - podcastserie Altijd Aan)](https://overcast.fm/+-afrmT9tU)

> Met een schermtijd van gemiddeld 7 uur per dag dompelt student Ylke van der Zwet zich regelmatig volledig onder in het digitale. Ze vergeet wel eens te eten, omdat ze de hongergevoelens niet registreert, ontdekte ze tijdens de digitale detox. 
> 
> Wat gebeurt er als we door digitalisering ons lichaam vergeten? Welke gevolgen heeft de smartphone voor onze mentale gezondheid? We leggen de bevindingen van de studenten voor aan filosoof en docent aan de Bildung Academie Hans Schnitzler en psycholoog Thijs Launspach.



## [Karen Hamaker-Zondag over cyclisch denken, chaostheorie en technocratie vs de vrije mens](https://overcast.fm/+3T54evYzc)

https://youtu.be/ht-7a319T_k

> In deze aflevering is Karen Hamaker-Zondag te gast. Karen is een internationaal bekende spreekster op het gebied van astrologie en Jung-iaanse psychologie. In deze video gaat ze dieper in op verschillende cyclische theorieën zoals de Kondratieffgolfen en de theorie van Ray Dalio. Op basis van deze cycli concludeert ze dat de aankomende jaren erg onrustig zullen worden. Ook bespreken we de chaostheorie en het Economic Confidence Model van Martin Armstrong. Tot slot gaan we dieper in op wat technocratie is, de rol van het World Economic Forum en de regionale initiatieven die instaand om het tegengewicht in te bieden.



## [Waarom Mastodon Twitter niet gaat vervangen - met Maarten den Braber](https://overcast.fm/+IriRZu-Gw)

> Maarten den Braber is ondernemer en strategisch adviseur digitale gezondheid voor bestuurders en bedrijven en  eerder te horen in [S07E10](https://www.metnerdsomtafel.nl/podcast/s07e10-quantified-self-maarten-den-braber.html). Dit keer zit hij aan tafel als oprichter en beheerder van de Nederlandse Mastodon-instance [mastodon.nl](https://mastodon.nl/).
> 
> Als je de afgelopen weken het nieuws een béétje hebt gevolgd dan weet je dat er veel te doen is geweest over de overname van Twitter door Elon Musk. Het heeft Twitter zowel bezoekers-records als de grootste exodus van gebruikers tot nu toe opgeleverd. Veel gebruikers die hun heil elders zoeken doen dat op Mastodon, een open source en decentraal ‘federated’ sociaal netwerk.
> 
> We spreken Maarten zowel over de techniek van Mastodon, het draaien van een instance en de lokale cultuur, als hoe het als gebruiker is om te wennen aan een overwegend nieuw sociaal netwerk met een eigen manier van denken.



## [De toekomstige dans tussen mens en machine](https://overcast.fm/+rRoThQdq4)

> We zitten momenteel in een fase waarin technologie en de mens via implantaten met elkaar versmelten. Het is een gevolg van het feit dat technologie en media de afgelopen decennia steeds intiemer zijn geworden en ze langzaam naar de mens zijn toe gekropen om er vervolgens in te op te gaan. Alleen, verdwijnt technologie in de mens of de mens in de technologie? Deze vraag staat centraal in deze SCHOKVAST, live geregistreerd tijdens de Dutch Media Week.



## [Arno Wellens en Bert Slagter: De weg naar de CBDC-hel is geplaveid met goede bedoelingen](https://overcast.fm/+3T56WF7b0)

https://youtu.be/vDJDDrJyQWY

> In deze aflevering zijn financieel journalist Arno Wellens en economisch analist Bert Slagter te gast. Arno staat bekend om zijn uitgebreide kennis over de economie en de achterliggende vormgeving van Central Bank Digital Currencies. Bert Slagter bestudeert complexe systemen, risico en onzekerheid. 
> In deze video gaan Arno en Bert dieper in op wat een Central Bank Digital Currency is en wat de voor- en nadelen hiervan zijn. Ook bespreken ze de richting die de Europese Commissie en de ECB lijken op te gaan met de digitale euro en toetsen ze dit aan hun ideaalbeeld. Tot slot hebben ze het over de Europese Digitale Identiteit en hoe dit gekoppeld kan worden aan de digitale euro.