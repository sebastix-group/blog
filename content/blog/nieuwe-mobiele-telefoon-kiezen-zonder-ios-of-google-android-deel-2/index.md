---
layout: blog
title: Nieuwe mobiele telefoon kiezen zonder iOS of Google Android - deel 2
date: 2021-10-23T19:58:14.293Z
description: In mijn vorige blog in deze serie schreef ik waarom ik een mobiele
  telefoon wil zonder iOS of Google. In deze blog wil ik een aantal mobiele
  telefoons uitlichten die hiervoor geschikt zijn.
categories: google, apple, privacy
comments: "true"
---
De keuze is reuze. Door verschillende combinatie mogelijkheden van toestellen en besturingssystemen wordt het al snel een complex overzicht qua mogelijkheden. Bekijk deze video maar eens:

https://youtu.be/4wcDkSl1wJ4

![](schermafbeelding-2021-10-06-om-15.57.43.png)

[Link naar bovenstaande tabel](https://sebastix.notion.site/5dec6498158242589efefa6d653dcf1f?v=43325346213d4e4d89d9ce37c054bd06) 

![](schermafbeelding-2021-10-06-om-15.58.02.png)

[Link naar bovenstaande tabel](https://sebastix.notion.site/9d69764cb1c24cdb92bbba0ee2c3b64f?v=263fa5df827f4b2f85c8ce1dcb4ab83f)

## Volla Phone

<https://volla.online/en/index.html>

Dit toestel is in de basis een Gigaset GS290, maar dan met een andere coating met het Volla logo. Het toestel kan worden geleverd met Volla OS of Ubuntu Touch. Ook SailfishOS wordt ondersteund als besturingssysteem. De Volla Phone lijkt me in de basis een prima telefoon. Groot pluspunt is de grote accucapaciteit van 4700mAh. Je hebt zelfs de keuze om de telefoon met een robuuste case te bestellen. Dan krijg je een waterdichte telefoon (IP68), extra sterk scherm en een grotere accu van 6200mAh.

Volla OS: 

https://youtu.be/HaSzGMXqPc0

Ubuntu Touch: 

https://youtu.be/neG2Z21epLI

Prijs: € 359,-

## Google Pixel

Het klinkt misschien ironisch, maar de toestellen van Google zijn prima geschikt om te gebruiken voor verschillende alternatieve besturingssystemen. De meest populaire zijn GrapheneOS en LineageOS. Beide zijn een zogenaamde Android ROM, dat betekent dat het operating system is afgesplitst van het open source Android systeem. 

Naast deze twee systemen, kun je er ook Ubuntu Touch installeren net zoals op de Volla Phone.

Google Pixel 3A prijs: € 300,-\
Google Pixel 5 prijs: € 762,-

## OnePlus

Qua specificaties is de OnePlus misschien wel het beste toestel dat je kunt kiezen om een alternatief systeem op te zetten. De keuze uit een recent toestel is echter beperkt, want veel alternatieve besturingsystemen ondersteunen de nieuwste toestellen niet. De OnePlus 6T is het meest gangbare model om hiervoor te gebruiken.

OnePlus 6T prijs: € 475,- (op Marktplaats ongeveer € 250)

## PinePhone

Dit toestel voor de echte nerds. Dit toestel is ontworpen met het idee dat je zelf onderdelen kunt vervangen als bijvoorbeeld je camera of scherm stuk is. Alle vervangingsonderdelen kun je hier vinden: https://pine64.com/product-category/smartphone-spare-parts/. Qua specificaties is de PinePhone absoluut niet spannend, de hardware is eigenlijk best wel verouderd. Daar is de prijs ook naar, voor 200 euro kun je geen gemiddelde telefoon verwachten. Waar de PinePhone in uitblinkt, is de technische veelzijdigheid. De PinePhone biedt namelijk de meeste keuze voor welk besturingssysteem je wilt installeren. 

Na het schrijven van dit concept, heeft de Pine communuty [een nieuwe PinePhone Pro aangekondigd](https://www.pine64.org/2021/10/15/october-update-introducing-the-pinephone-pro/). Qua hardware is deze telefoon vele malen beter met onder andere een snelle processor, meer werkgeheugen en een betere camera.

De PinePhone maakt onderdeel uit van een hele reeks aan andere apparaten. Ookwel de Pine64 familie genoemd. Andere apparaten zijn bijvoorbeeld:

* Laptop
* Smartwatch
* Tablet
* Webcam
* Mini computers

https://youtu.be/lFELJ3E_-G4

[Bekijk hier de product pagina](https://pine64.com/product-category/pinephone/?v=0446c16e2e66) 

Meer specificaties: <https://wiki.pine64.org/wiki/PinePhone>

Prijs: € 200,-

## Fairphone

Dit toestel is van Nederlandse makelij ([Dave Hakkens is de uitvinder](https://davehakkens.nl/)) en bij uitstek de telefoon voor de duurzame enthousiasteling. Met de Fairphone sluit je de meest groene mobile deal met de minst grote impact op het mileu. Fairphone doet jou de belofte dat je minimaal vijf jaar vooruit kunt met deze telefoon.

## Je telefoon gebruiken als desktop computer = mobile / os convergence

> Mobile is the tendency for mobile devices to become more similar to fixed-location devices with time. It is a long term technology trend that includes convergence of networks, user interfaces and services. 

Deze techniek bestond al in 2011, maar is nooit doorgebroken tot de massa. Nu lijkt deze techniek weer een nieuwe implus te krijgen met Ubuntu Touch en ook PureOS. Echter is de keuze zeer beperkt uit een toestel die je kunt gebruiken als personal computer. Een mobiele telefoon gebruiken als personal computer is pionierswerk waar je zelf veel zal moeten uitzoeken hoe alles werkend te krijgen. Als het eenmaal werkt, zie ik wel veel praktische voordelen. Wie weet weten we over een jaar of tien niet beter en doen we echt alles op 1 apparaat.

Ubuntu Touch met de Volla Phone:

https://www.youtube.com/watch?v=neG2Z21epLI&t=448s

PinePhone (heeft HDMI ondersteuning):

https://youtu.be/i9bQFzBsj1A

PureOS:

https://youtu.be/Mv1CF3qFavw

[Klik hier voor meer info](https://puri.sm/posts/converging-on-convergence-pureos-is-convergent-welcome-to-the-future/) 

## Mijn keuzes

#### 1. PinePhone

Goedkoop en de beste basis om mee te gaan experimenteren. Op deze telefoon kun je vrijwel elk besturingssysteem installeren. Waarschijnlijk kan ik deze telefoon (nog) niet dagelijks als vervanger voor mijn iPhone SE gebruiken. Dat komt omdat de technische specificaties nog niet goed genoeg zijn.  Afhankelijk van welk besturingssysteem ik zal gebruiken, zal ik nog steeds bepaalde applicaties nodig hebben die niet op de PinePhone zullen functioneren. Denk bijvoorbeeld aan bepaalde 2FA (two-factor authentication) apps of mobiel bankieren met Bunq.

![](schermafbeelding-2021-10-06-om-15.54.12.png)

Persoonlijke berichten als deze zijn goud! Een spontane aanbieding om vrijblijvend iemand anders zijn PinePhone te gebruiken om mee te experimenteren. 

#### 2. Oneplus 6T

Het toestel met de beste prijs/kwaliteit verhouding. Mijn vriendin is al jaren Oneplus gebruiker vanaf de One (op mijn advies aangezien ze geen Apple telefoon wilde) en ik ben altijd onder de indruk geweest van deze toestellen. De camera is indrukwekkend en ze is altijd in staat om vier tot zes jaar met OnePlus telefoon te doen. Dit is een toestel waar je geen spijt van krijgt. Via Marktplaats zijn er genoeg te vinden tussen de 150 euro en 250 euro.

#### 3. Volla Phone X

De Volla Phone variant met een extra robuuste body, grotere accu en een sterkere scherm. Deze telefoon moet tegen een stootje kunnen. Vooral handig als je een actief leven leidt.

![](schermafbeelding-2021-10-06-om-15.51.32.png)

Deze reactie ontving ik in een Telegram groep (UBports NEDERLANDS) van Koen Vervloesem. Voor mij was dat een bekende naam, aangezien ik hem ook volg op Twitter betreft online privacy & tech. 

In het volgend deel van deze blog serie zal ik mijn eerste bevindingen delen over de PinePhone. Als er iemand in de buurt nog een OnePlus 6T heeft die ik kan overnemen, hoor ik dat graag. Dan kan ik ook dit toestel gebruiken om mee te gaan experimenteren. 

\---

Hierbij enkele nuttige bronnen die ik heb geraadpleegd:

* [Tweakers pricewatch](https://tweakers.net/pricewatch/)
* [Volla Phone: Android privacy phone without Google](https://cstan.io/?p=12726&lang=en)
* [New, rugged Volla Phone X runs Android and Ubuntu](https://tuxphones.com/volla-phone-x-ubuntu-linux-phone/)
* [Chris Titus Tech](https://www.youtube.com/channel/UCg6gPGh8HU2U01vaFCAsvmQ)
* [OS convergence and the rise of the mobile office
](https://www.techrepublic.com/article/os-convergence-and-the-rise-of-the-mobile-office/)
* [Exploring the PinePhone with the multi-distro demo image](https://koen.vervloesem.eu/blog/exploring-the-pinephone-with-the-multi-distro-demo-image/)
* [The Sad Saga of Purism and the Librem 5 : Part 1](https://jaylittle.com/post/view/2019/10/the-sad-saga-of-purism-and-the-librem-5-part-1) (dank Mathijs voor het deze van deze info mbt leveringsproblemen van de Librem 5 telefoon van Purism)