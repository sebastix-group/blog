---
layout: blog
title: De camper als mobiele werkplek
date: 2021-01-29T21:32:39.144Z
description: Alleen maar thuiswerken is mentaal een uitdaging. Het is goed om te
  wisselen van omgeving voor je mentale welzijn. Al enige tijd had ik het idee
  om onze camper te gebruiken als kantoor.
categories: Camper, mobiele werkplek, mobiel kantoor
comments: "true"
---
Dit bericht schrijf ik nu in onze knusse Volkswagen T4 buscamper. Mijn vriendin, mijn dochtertje en onze heerlijke boxspring zijn nog geen 5 kilometer hiervandaan. Toch kruip ik straks in het hefdak om te slapen op een slechts 5 centimeter dun matras. Alleen ben ik hier niet. Bij het oprijden van het terrein telde ik ongeveer nog 10 andere campers.\
Ik sta nu op [Camperplaats de Rucphense Weide](https://www.rucphenseweide.nl/). Geopend in najaar 2020 door een enthousiast stel die naast het terrein woont. Morgenochtend komt de bakker en voor het avondeten heb ik het mezelf ook makkelijk gemaakt: een diner van het lokale restaurant wordt gebracht. De camperaar wordt hier volledig ontzorgd! 

![](img_3319.jpg)

![](img_3323.jpg)

Je zult je misschien afvragen wat ik hier nu doe. Om heel eerlijk te zijn, is dat om een blok van mijn been af te schudden. Al meer dan 2 jaar ben ik aan een (te) ambitieus project begonnen voor een goede vriend van me, namelijk het ontwikkelen van een online marktplaats website waar ondernemers hun producten kunnen verkopen in een eigen shop. Het concept laat zich het beste vergelijken met [Etsy](https://www.etsy.com/). De website bouw ik met [Drupal](https://www.drupal.org) samen met de [Commerce module](https://drupalcommerce.org/). De website bevat momenteel meer dan 400 producttypes met elk allemaal verschillende variaties. In het systeem zitten op dit moment nog een aantal grote complexe uitdagingen (problemen. Daar heb ik tijd (en rust) voor nodig om deze op te oplossen. Dat is de reden waarom ik mezelf heb teruggetrokken dit weekend in deze kleine ruimte met mijn laptop.

Ik hoop dat deze setting een bijdrage kan leveren bij het vinden van alle oplossingen.\
Oja, mocht je interesse hebben...onze camper kun je huren! Bekijk [hier](https://www.camptoo.nl/camper/18214/Volkswagen-T4-buscamper-Westfalia) onze Camptoo advertentie maar eens.

![](img_3322.jpg)

![](img_3325.jpg)