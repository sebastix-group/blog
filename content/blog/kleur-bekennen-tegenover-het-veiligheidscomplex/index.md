---
layout: blog
title: Kleur bekennen in het veiligheidscomplex
date: 2022-01-03T22:16:19.331Z
description: Terug in de tijd met deze Tegenlicht aflevering uit het seizoen
  2013/2014. In die periode zorgde het narratief rondom immigranten ervoor dat
  er meer surveillance middelen werden ingezet. Middelen die we vandaag de dag
  nog steeds zien.
categories: digitale identiteit
comments: "true"
---
Deze aflevering bestaat al ruim zeven jaar, maar is nog steeds actueel. Je zou het immigranten narratief kunnen vervangen met die van covid. Toegangsbewijzen met groene vinkjes is namelijk niets iets van alleen het afgelopen jaar. 

# [Klik hier om de aflevering te bekijken](https://www.vpro.nl/programmas/tegenlicht/kijk/afleveringen/2013-2014/veiligheidscomplex.html)

![Kleur bekennen](schermafbeelding-2022-01-04-om-16.56.54.png)

Let vooral op de woorden van [Ben Hayes](https://www.tni.org/en/bio/ben-hayes). Zijn woorden intrigreren me en geven me een interessante kijk op hoe het geld stroomt in deze miljardenbusiness.