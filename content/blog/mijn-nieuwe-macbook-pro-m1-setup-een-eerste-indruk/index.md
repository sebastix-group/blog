---
layout: blog
title: Mijn nieuwe Macbook Pro M1 setup - een eerste indruk
date: 2021-07-18T19:19:25.114Z
description: Na zes jaar heb ik afscheid genomen van mijn 13" Macbook Pro 2015
  model (Intel i7 processor, 128GB SSD harde schijf en 8GB werkgeheugen). Sinds
  een paar maanden werk ik op een 13" Macbook Pro M1 met een 1TB SSD harde
  schijf en 16GB werkgeheugen. Ben je benieuwd naar hoe ik het werken met deze
  nieuwe Macbook Pro ervaar?
categories: Apple, Macbook Pro
comments: "true"
---
Vorig jaar december schreef ik over de indrukwekkende prestaties van Apple's nieuwe chip, de M1: <https://sebastix.nl/blog/apple-m1-chip-en-docker/>. Wat mij tot nu toe altijd weerhield om deze Macbook te kopen, was dat [Docker](https://www.docker.com/) (virtualisatiesoftware waarmee ik ontwikkelomgevingen opzet) nog niet functioneerde met deze nieuwe chip. Mijn verwachting was toen dat het drie tot zes maanden zou duren voordat Docker de M1 chip zou ondersteunen met een stabiele release. Op 15 april [maakten ze bekend](https://www.docker.com/blog/released-docker-desktop-for-mac-apple-silicon/) dat de software werkt met de M1 chip. Dat was voor mij het signaal om een nieuwe 13" Macbook Pro te bestellen.

**Slechts twee USB-C aansluitingen**

Deze Macbook Pro bevat slechts twee USB-C poorten, wat betekent dat je er niet aan ontkomt om een hub te gebruiken met verschillende andere poorten zoals HDMI, USB, UTP of DisplayPort. Tenminste, als je net zoals ik graag werkt met verschillende randapparatuur. Zelf heb ik gekozen voor een hub van Belkin: de Thunderbolt 3 dock pro. Deze kost wel wat, maar dan heb je ook wat.

![](9749bc4d-3e58-4f93-8f3b-87ff1b4b3716.jpg)

![](d942d3cb-07b7-4f81-8eac-eaf9a89bd44e.jpg)

Door slechts een Thunderbolt 3 / USB-C kabel op mijn Macbook Pro aan te sluiten, kan ik het volgende koppelen via de Belkin hub:

* 21:9 ultrawide monitor met twee USB poorten via DisplayPort
* Toetsenbord (zit aangesloten op USB poort van de monitor)
* Webcam (zit aangesloten op USB poort van de monitor)
* Externe harde schijf via USB
* Labelwriter via USB
* UTP netwerkkabel 

Het opladen van de Macbook Pro gebeurt ook via deze kabel van de hub. \
Het enige wat niet werkt, is het geluid via de externe monitor.

Met deze setup heb ik momenteel nog de volgende poorten vrij om te gebruiken:

* 1 Thunderbolt 3 / USB-C poort op de Macbook Pro

  Op de hub:
* 1 USB-C poort aan de voorkant
* 1 USB poort aan de voorkant
* 2 USB poorten aan de achterkant
* 1 Tunderbolt 3 / USB-C poort aan de achterkant

**16GB werkgeheugen**

Het werkgeheugen is verdubbeld ten op zichte van mijn vorige laptop. Toch zou ik liever 32GB tot mijn beschikking hebben. Al enkele keren kwam het namelijk voor dat al het geheugen actief in gebruik was. Op zulke momenten wordt het systeem wel een stuk trager door het swappen van data naar de harde schijf. Een Macbook Pro M1 met 32GB is er helaas nog niet. De verwachting was dat deze op afgelopen Apple's Developers conference aangekondigd zou worden, maar dat is helaas niet gebeurd. Mocht er straks toch een Macbook Pro met 32GB of meer werkgeheugen uitkomen, dan ga ik wel serieus overwegen om te upgraden en mijn huidige Macbook Pro M1 dan weer van de hand te doen.

**Software development**

Ten op zichte van mijn vorige Macbook Pro is het echt een genot om op deze machine te werken. Zeker in combinatie met Docker werkt alles vele malen sneller en zie ik vaak direct software wijzigingen terug op mijn scherm. Bijvoorbeeld het opnieuw laden van een pagina van een Drupal website (zonder caching plus allerlei development tools) gaat nu vaak binnen 1-2 seconden. Op mijn vorige Macbook Pro duurde dit minstens 4 tot 5 keer langer.\
Dankzij mijn nieuwe laptop ben ik in staat om veel sneller software te ontwikkelen. Nu ik amper meer hoef te wachten tot wijzigingen zichtbaar zijn geworden, ben ik meer gefocust omdat ik niet meer de kans heb om even snel iets anders te doen terwijl ik moet wachten. Hierdoor zit ik veel sneller in een lekkere workflow en krijg ik meer dingen gedaan. 1-0 voor efficientie.