---
layout: blog
title: BeReal is privacy-vriendelijker geworden
date: 2023-03-03T12:00:00.266Z
description: Vandaag had ik een reminder staan om het Excodus privacy rapport te checken van BeReal. Deze reminder had ik ingepland na aanleiding van mijn eerdere geschreven blog 'Hoe (a)sociaal is BeReal'. 
categories: social media, bereal, privacy
comments: "true"
---
Mijn blog [Hoe (a)sociaal is BeReal?](/hoe-a-sociaal-is-bereal/) schreef ik eind augustus 2022 en daarin stond de volgende paragraaf:

> De app bevat vijf trackers (die precies volgen wat jij doet op de app) en vraagt om 38 permissies van jou en je telefoon. *Note: gecheckt op 18 agustus 2022, in de toekomst kan dit namelijk veranderen bij elke nieuwe update van de app.* Dit rapport laat zien dat BeReal het net zo goed (of slecht) doet als [Instagram](https://reports.exodus-privacy.eu.org/en/reports/com.instagram.android/latest/) op het gebied van privacy: die app bevat twee trackers en vraagt om 40 permissies. Het volledige rapport vind je [hier](https://reports.exodus-privacy.eu.org/en/reports/com.bereal.ft/latest/).
> *Reminder voor mijzelf: over een half jaar op 1 maart 2023 checken welke veranderingen er zijn doorgevoerd betreffende de trackers en permissies.*

Als je nu het rapport opent, zie je dat BeReal nog maar 2 trackers heeft en om 18 permissies vraagt van jou en je telefoon.
[Hier](https://reports.exodus-privacy.eu.org/en/reports/search/com.bereal.ft/) vind je de geschiedenis van alle verschillende versies. De update van versie 0.35.7 naar 0.54.1 laat een grote verandering zien in het aantal trackers en permissies:

![](sn2023-03-01.png)

Mijn conclusie is dus positief: BeReal is een stuk privacy-vriendelijker geworden op basis van het [Exodus rapport](https://reports.exodus-privacy.eu.org/en/reports/com.bereal.ft/latest/). Dat neemt echter niet weg dat ik BeReal nog steeds een asociale media app vind.
Want [hoe (a)sociaal is BeReal](/hoe-a-sociaal-is-bereal/)?