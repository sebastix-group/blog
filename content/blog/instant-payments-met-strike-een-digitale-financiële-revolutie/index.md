---
layout: blog
title: Instant payments met Strike, een digitale financiële revolutie?
date: 2021-01-08T20:16:18.912Z
description: "Instant betalingen doen in elke gewenste valuta voor zowel zender
  als ontvanger. Zonder tussenpartij. Zonder kosten (bijna). Dat kan met Strike.
  "
categories: Bitcoin, Lightning
comments: "true"
---
https://youtu.be/Rt2C3CsLi7k

Dit is revolutionair. Dit is een killer applicatie. Dit maakt ons leven een stuk eenvoudiger. Dit geeft je financiële vrijheid. [Meer info hier](https://global.strike.me/?kid=1EJV0B) om je aan te melden voor de beta versie van de applicatie. To be continued.