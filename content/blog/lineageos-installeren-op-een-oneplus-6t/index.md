---
layout: blog
title: LineageOS installeren op een Oneplus 6T
date: 2022-07-28T14:49:03.888Z
description: Tijdens mijn zoektocht naar een Apple en Google vrije telefoon heb
  ik in december vorig jaar Ubuntu Touch op een OnePlus 6T geïnstalleerd. Dit
  besturingssysteem is voor mij echter nog niet geschikt om dagelijks te
  gebruiken. Daarom heb ik voor een alternatief gekozen. In deze blog laat ik
  zien hoe ik LineageOS heb geïnstalleerd.
categories: degoogle oneplus6t lineageos
comments: "true"
---
Deze documentatie heb ik als basis gebruikt om LineageOS te installeren: <https://wiki.lineageos.org/devices/fajita/>

# De bootloader ontgrendelen

In mijn eerdere blogpost [Ubuntu Touch installeren op een OnePlus 6T via MacOS](https://www.notion.so/Ubuntu-Touch-installeren-op-een-OnePlus-6T-via-MacOS-d390bca009ec423c867c4a88c3bbeb31) kun je lezen hoe je de bootloader op je telefoon kunt uitschakelen.

# LineageOS installeren

Benodigdheden

* Bootloader moet ontgrendeld zijn op je telefoon
* Op je computer staan de commandline tools **Fastboot** en **ADB** geïnstalleerd waarmee je kunt communiceren met je telefoon via een USB kabel (<https://technastic.com/adb-fastboot-command-mac-terminal/>)

De stappen

1. Start het toestel op in fastboot-modus door de **Volume Up + Volume Down + Power knoppen** in te drukken als het toestel uit staat.
2. Na het uitvoeren van het commando `fastboot flash boot <recovery_filename>.img` startte het toestel niet meer op bij mij. De image werd geflashed naar slot `boot_b`. Pas toen ik deze image naar slot `boot_a` heb geflashed, lukte het om het toestel in recovery modus op te starten. Slot a actief zetten en daarna de image flashen naar het toestel en opnieuw opstarten:

   1. `fastboot set_active a`
   2. `fastboot flash boot <recovery_filename>.img`

   ![](img_6598.jpg)

   Het toestel is nu opgestart met de recovery image van LineageOS.
3. Ik wil met een zo schoon mogelijk toestel LineageOS installeren dus ik heb eerst alle data verwijderd: **Factory reset > Format data / factory reset > Format Data**

   > Now tap **Factory Reset**, then **Format data / factory reset** and continue with the formatting process. This will remove encryption and delete all files stored in the internal storage, as well as format your cache partition (if you have one).
4. Terug in het hoofdmenu kies ik nu voor **Apply Update > Apply from ADB**. Op mijn Macbook kan ik nu de ZIP file ([hier te downloaden](https://download.lineageos.org/fajita)) met LineageOS sideloaden op het toestel:

   `./adb sideload images/lineage-19.1-20220721-nightly-fajita-signed.zip`

   ![](img_6599.jpg)

   Het commando in mijn terminal op mijn Macbook Pro:

   ![](schermafbeelding-2022-07-27-om-09.29.13.png)
5. Terug naar het hoofdmenu kies je nu **Reboot system now** en het toestel start nu op met LineageOS!

# Apps installeren

Met een schone LineageOS installatie heb je een ontzettend lege telefoon zonder app store of Google diensten. Er zit zelfs geen email app op (na wat research las ik dat deze is verwijderd in de 18.1 release). Dit zijn alle apps tot je beschikking:

![](img_6608.jpg)

Wil je net zoals ik weten of je telefoon nu als daily driver te gebruiken is? Dan zal je toch enkele apps moeten installeren. Hoe doen we dat? Er zit namelijk geen appstore applicatie op de telefoon geïnstalleerd. In de handleiding om LineageOS te installeren kwam ook deze stap voorbij:

> *(Optionally)* If you want to install any add-ons, click `Advanced`, then `Reboot to Recovery`, then when your device reboots, click `Apply Update`, then `Apply from ADB`, then `adb sideload filename.zip` those packages in sequence.

met daarbij de volgende notitie:

> **NOTE:**  If you want the Google Apps add-on on your device, you must follow this step **before**  booting into LineageOS for the first time!

De Google Apps add-on heb je nodig om via de Google Play Store Android applicaties te installeren. Echter zou je kunnen stellen dat je dan geen deGoogled telefoon meer hebt. Meer informatie over Open GApps vind je hier: [](https://wiki.lineageos.org/gapps)<https://wiki.lineageos.org/gapps> en [](https://opengapps.org/)<https://opengapps.org/>.

## F-Droid

Er is gelukkig een alternatief, namelijk F-Droid. Wat je namelijk wel kunt, zijn applicaties verpakt in APK bestanden installeren op je telefoon. F-Droid is zo'n APK bestand wat je als applicatie kunt installeren.

Op [](https://f-droid.org/)<https://f-droid.org/> vind je de instructies om F-Droid te installeren. Klik op Download F-droid om de APK te downloaden. Deze vind je vervolgens terug in Files > Downloads. De standaardinstellingen van je browser voorkomen dat je apps kunt installeren van onbekende bronnen. Open de instellingen van je browser om toe te staan dat je F-Droid als applicatie wilt installeren. Als dit is gelukt, beschik je nu over de F-Droid app store als applicatie:

![](img_6609.jpg)

Zodra je F-Droid hebt geïnstalleerd, is mijn tip om de Aurora store downloaden en installeren. De Aurora Store geeft je namelijk dezelfde look&feel als de Google Play Store en is daarom veel fijner in gebruik. 

Zoek in de F-Droid app naar **Aurora Store om deze te installeren. Als dit is gelukt, heb je nu twee appstore applicaties op je telefoon: F-Droid en Aurora.**

![](img_6610.jpg)

Terug naar mijn punt met betrekking tot een email applicatie: om mijn mails te lezen, heb ik een email app nodig. Welke is dan de beste? Na wat zoeken op internet ([](https://www.fossmint.com/best-open-source-email-clients-for-android/)<https://www.fossmint.com/best-open-source-email-clients-for-android/>) kwam ik al snel tot de conclusie dat FairMail en Protonmail (deze gebruik al langere tijd naast Gmail) de beste apps zijn. FairMail heb ik geïnstalleerd via F-Droid en Protonmail via Aurora Store. 

Via de Aurora Store lijkt het erop dat je daar elke app kunt vinden & installeren die je ook in de Google Play Store staan. Wat er niet werkt, zijn de push berichten die via Google Play services verstuurd worden. Via Aurora heb ik ook mijn password manager tool Bitwarden, Tailscale en favoriete auto social media app Proudwheels gevonden en geïnstalleerd 🕺🏼 🚘 🏁.

Mijn andere blogs over mijn zoektocht naar een telefoon zonder Apple of Google: 

* [Aan de slag met Ubuntu Touch op een OnePlus 6T](https://www.sebastix.nl/blog/aan-de-slag-met-ubuntu-touch-op-een-oneplus-6t/)
* [Ubuntu Touch installeren op een OnePlus 6T via MacOS](https://www.sebastix.nl/blog/ubuntu-touch-installeren-op-een-oneplus-6t-via-macos/)
* [Deel 2: nieuwe mobiele telefoon kiezen zonder iOS of Google Android](https://www.sebastix.nl/blog/nieuwe-mobiele-telefoon-kiezen-zonder-ios-of-google-android-deel-2/)
* [Deel 1: Nieuwe mobiele telefoon kiezen zonder iOS of Google Android](https://www.sebastix.nl/blog/nieuwe-mobiele-telefoon-kiezen-zonder-ios-of-android-deel-1/)