---
title: "2find web search api concept"
date: "2008-06-09T20:15:23.000Z"
description: "2find is een concept. Het is een concept welke verschillende openbare API's met elkaar moet verbinden op één platform. Op dat platform dient de gebruiker zich te registreren en zijn profiel te personaliseren met interesses. Op 2find is het dan mogelijk om te zoeken (net zoals in Google). Het platform doorzoekt alle externe databases via de API's van alle grote platformen op het internet en koppelt deze aan elkaar."
categories: [internet]
comments: true
---

Wel wordt alle data opnieuw ingedeeld in de volgende categorieën:
- websites
- documents
- images
- videos
- products
- people
- location
- community

## Waarom deze data aan elkaar koppelen?  
**Voorbeeld** je buurman is gek op Japan en reizen. Als deze op 2find gaat zoeken naar 'Japan' krijgt hij bij websites een heleboel links terug met informatieve informatie over Japan. Bij documents zal je buurman reisverslagen vinden en informatie documenten over het land zelf. Bij products zal hij weer dingen te zien krijgen welke je nodig hebt op reis en mogelijk ook spullen die te maken hebben met Japan.  
Als ik ga zoeken naar Japan krijg ik ongeveer dezelfde resultaten terug, echter zal ik bij video's, products etc ook auto-gerelateerde resultaten terug krijgen. Waarom? Omdat 2find weet dat ik ook een grote interesse heb in Japanse automerken. Hieronder staan enkele beeldimpressies hoe het platform eruit zou kunnen gaan zien.

![](find_honda_b16_build.jpg)

Hieronder vind je het database model concept voor de werking van 2find:

![](technischontwerp.jpg)
![](datamodel.jpg)

Hieronder staat de schematische concept uitwerking van 2find:

![](schema_cat_links.jpg)
![](schema.jpg)
