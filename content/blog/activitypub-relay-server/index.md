---
layout: blog
title: ActivityPub relay server
date: 2022-12-20T14:08:31.328Z
description: Al enkele weken had ik op mijn lijstje staan om een ActivityPub relay server op te zetten als technisch experiment.
categories: activitypub relay server fediverse mastodon peertube
comments: "true"
---

ActivityPub relays servers zijn er voor server-to-server communicatie waar data onderling wordt uitgewisseld. In dit geval gaat het vooral om het uitwisselen van Mastodon berichten van leden. Voor kleinere Mastodon instances kan het heel erg nuttig zijn om meerdere relay servers toe te voegen om de interactie te bevorderen met leden op andere instances. Als je een actieve instance toevoegt die een relay server heeft, dan worden de berichten en leden van die instance zichtbaar (via de zoekfunctie bijvoorbeeld) voor jouw leden. Als je daarnaast wilt dat jouw instance beter gevonden wordt en meer interactie tussen leden krijgt, dan kun je zelf een relay server opzetten die toegevoegd kan worden door admins van andere instances.



Mijn (experimentele) relay server is actief op [https://relay.sebastix.dev](https://relay.sebastix.dev). 

De code die ik gebruik voor de relay server: [asonix/relay](https://git.asonix.dog/asonix/relay) (thx [Jorijn](http://toot.community/@jorijn) voor de tip)

Dit is de `docker-compose.yml` die ik hiervoor nu gebruik:

```docker
version: '3.9'

services:
  relay:
    image: asonix/relay:0.3
    hostname: relay
    ports:
      - "8090:8080"
    restart: always
    volumes:
      - /var/www/relay.sebastix.dev/:/mnt/
    environment:
      - HOSTNAME=relay.sebastix.dev
      - ADDR=0.0.0.0
      - API_TOKEN=32475_YOURTOKEN_121
      - HTTPS=true
      - SLED_PATH=/mnt/sled/db-0.34
      - LOCAL_DOMAINS=127.0.0.1:3000
      - LOCAL_BLURB=Contact <a href="https://mastodon.sebastix.dev/@sebastix">@sebastix</a> for more info
      - FOOTER_BLURB=Contact <a href="https://mastodon.sebastix.dev/@sebastix">@sebastix</a> for more info
```

Deze setup is aan verandering onderhevig aangezien dit voor mij nieuw is en continue aan het tweaken ben. 
Bij de variabel `LOCAL_DOMAIN` staat de URL waar Mastodon draait. In mijn geval is dat een middels docker container op poort 3000. De relay server draait op poort 8090 en via een nginx reverse proxy configuratie heb ik deze gekoppeld aan het domein relay.sebastix.dev.



Wil je https://relay.sebastix.dev toevoegen als relay server? Dan moet ik jouw instance domein eerst op de allowlist plaatsen. Zie mijn contactgegevens op de site of hieronder om mij een verzoek te sturen. 



Als beheerder van de Mastodon instance [nwb.social](https://nwb.social) van Nieuw West-Brabant ben ik op zoek naar Nederlandstalige activitypub relay servers. Heb jij zo'n relay server? Stuur me dan een bericht op [@sebastian@nwb.social](https://nwb.social/@sebastian) of per mail sebastian@nieuwwestbrabant.nl en ik voeg jouw relay server graag toe.


