---
layout: blog
title: Mijn allereerste website ging over edelstenen
date: 2021-05-17T20:09:32.648Z
description: "Ken je ze nog? CD's waar je vroeger muziek of documenten op
  brandde? Ik kwam de map tegen waarin ik deze allemaal bewaard heb. Zo ook een
  CD waarop geschreven staat: 'homepages brugklas'. Ik wist direct dat hier een
  kopie op staat van de allereerste website die ik heb gemaakt. "
categories: Website, HTML
comments: "true"
---
Voor zover ik weet, heeft deze website nooit live gestaan om de simpele reden dat ik toen nog niet wist hoe ik een website online moest zetten of een domeinnaam registreerde. Thuis had ik alleen internet van Zonnet via een 56k modem. Dat internet was beperkt tot  .nl domeinnamen en surfen deed ik voornamelijk via startpagina.nl en later via de zoekmachine Lycos.

In de brugklas vertelde onze tekenleraar [Sjaak Jansen](https://www.sjaakjansen.nl/) dat we een cursus konden volgen in homepages bouwen. "Iets doen met computers" klonk mij als muziek in de oren. Dit was precies de reden waarom ik had gekozen voor de middelbare school KSE: ze waren vooruitstrevend in ICT onderwijs. Tijdens de cursus leerden we HTML schrijven in het programma kladblok en hoe we deze teksten als .html bestanden moesten opslaan. Vervolgens gebruikten we de browser Internet Explorer om deze .html bestanden te openen zodat de geschreven HTML werd weergegeven als webpagina. 

![](img_3689.jpg)

Aan het einde van de cursus kregen we de opdracht om een homepage over een educatief onderwerp te maken. Wij kozen voor edelstenen omdat ik die op dat moment verzamelde. Lekker nerdy ;-) Thuis had ik een stapel tijdschriften 'Schatten van de aarde' liggen, die we gebruikten als bron voor de afbeeldingen en teksten op de website. De afbeeldingen werden nog oldskool ingescand en de tekst typten we over: de meest efficinte manier waar we destijds over beschikten :-).

De website over edelstenen die ik samen met Rob maakte heb ik nu alsnog online gezet:

<https://edelstenen.sebastix.dev>

Sindsdien heb ik toch best wel wat bijgeleerd over het maken van websites ;-).

![](schermafbeelding-2021-05-15-om-23.32.20.png)