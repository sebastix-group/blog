---
layout: blog
title: Bitcoin als consensus netwerk
date: 2020-12-27T15:11:29.565Z
description: Bitcoin als consensus netwerk. Wat betekent dat? Op sociaal,
  politiek, economisch en digitaal vlak? Wouter Constant neemt je mee in de
  'Bitcoin rabbit hole'.
categories: Internet, Bitcoin
comments: "true"
---
Wouter Constant legt uit wat Bitcoin is. Dat het meer is dan alleen een peer-to-peer betaalmiddel. Opgepikt in de [Satoshi Radio Telegram groep](https://t.me/satoshiradio) waar Wouter zijn uitgesproken scherpe visie ook altijd klaar heeft staan.

#### Wat behelst Bitcoin vanuit een sociaal oogpunt?

https://www.youtube.com/watch?v=GveI8ac9Nfs

#### De economie in Bitcoin en Bitcoin in de economie

https://www.youtube.com/watch?v=r9ZFsUH6Duk

#### Bitcoin, techniek en consensus

https://www.youtube.com/watch?v=KExeX2XdWpY