---
layout: blog
title: Stoppen met Whatsapp
date: 2021-01-07T07:22:21.884Z
description: Al sinds de overname van Whatsapp door Facebook, stop ik liever met
  het gebruiken van de app. Echter de behoefte aan toegang tot je sociale
  contacten was nog altijd te groot. Tot nu.
categories: Whatsapp, Facebook
comments: "true"
---
Jarenlang was het geluid dat Whatsapp nooit gegevens zou delen met Facebook. Tot 8 februari 2021.

<https://arstechnica.com/tech-policy/2021/01/whatsapp-users-must-share-their-data-with-facebook-or-stop-using-the-app/>

Als je de nieuwe algemene voorwaarden leest van Whatsapp, dan ontkom je er nu niet meer aan dat Whatsapp jouw gegevens gaat delen met Facebook. Ik heb zelf niet eens een Facebook profiel meer, deze heb ik vorig jaar verwijderd. Echter twijfel ik er niet aan dat Facebook wel een profiel van mij heeft. Een profiel die veel groter is dan ik me voor kan stellen. Daar zitten ongetwijfeld veel gegevens bij die verzameld zijn via Whatsapp. Nu legitimeren ze dit met nieuwe algemene voorwaarden.

Ik ben er klaar mee. Ik licht mijn familie en vrienden in dat ik stop met Whatsapp. Ongetwijfeld ga ik veel gesprekken missen, zeker in deze periode waar communicatie digital-first is (we sturen elkaar eerder een tekstbericht dan dat we een belletje doen). Wat ik wil, is dat ik iets te kiezen heb. Ik kies voor privacy, dan heb ik de keuze om wel of niet iets te delen of verbergen. Whatsapp ontneemt mij die keuze.

Er zijn alternatieven zoals Signal of Telegram. Deze keuzevrijheid hebben we en wat er nodig is, is een netwerkeffect in het gebruik van deze alternatieven. Dat effect creer je door bij jezelf te beginnen. Installeer zo'n app. Vertel het aan de mensen om je heen dat je ook daar digitaal bereikbaar bent. Het is net zo eenvoudig in gebruik als Whatsapp en het voorziet in precies dezelfde behoefte. Gewoon doen, dan heb je altijd een backup klaar staan om te blijven communiceren met anderen.

**UPDATE: data van EU burgers wordt niet gedeeld met Facebook.**

Ik blijf dus nog even appen met Whatsapp voor nu...