---
layout: blog
title: Exploring subscriptions and (mobile push) notifications with DANSE Drupal module
date: 2023-08-08T09:46:22.810Z
description: In this blog I'm discovering the possibilities of the Drupal module DANSE for building a user based subscription model for notifications and a notifications center.
categories: drupal danse subscriptions notifications mobile push webpush
comments: false
---

## My goal

- Send a notification email to all members on [CCHS.social](https://cchs.social) when a new link (a node) is posted

I'm aware this could be a very simple solution by writing some code which sends an email in the [`hook_entity_insert()`](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21entity.api.php/function/hook_entity_insert/10) or [`hook_entity_create()`]([hook_entity_create | entity.api.php | Drupal 10.1 | Drupal API](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21entity.api.php/function/hook_entity_create/10)) function. But I've a bigger challenge. For a client project I need to send different types of notifications so I need a more generic solution. That's where the [module DANSE](https://www.drupal.org/project/danse) could be a good fit. 

In this blog I'm exploring and testing the possibilities of DANSE in the most simple way. 

### Step by step installation and configuration

#### 1. Install and enable DANSE

Install and enable the module with Composer and Drush:  
`composer require drupal/danse`  
`drush en danse`

There are also some submodules which can be enabled for different event types:

* config
* content
* form
* generic
* log
* user
* webhook

![Screen-Shot-2023-08-07-10-25-01.58.png](Screen-Shot-2023-08-07-10-25-01.58.png)

#### 2. Configure events

An event object is created is saved as a content entity - [see Event entity class](https://git.drupalcode.org/project/danse/-/blob/2.2.x/src/Entity/Event.php). In the database you can find the data in the table `danse_event`.

You can create your own event listener as a plugin in a custom module (see this [example](https://git.drupalcode.org/project/danse_content_moderation)) or configure this in the UI of your entities (content types, taxonomies, block types, comments types, users etc).  
In my case I enabled the submodule DANSE content for configuring events on content entities. With this module enabled, you now can view a new vertical tab on `admin/structure/types/manage/[your_content_type]`(this is also the available now on other entity types like taxonomies, block or comment types):

![Screen-Shot-2023-08-07-11-08-51.11.png](Screen-Shot-2023-08-07-11-08-51.11.png)

Here you can tick the checkboxes of events which you would like to be available to subscribe to. In my case I ticked the 'publish' checkbox and some new fields show up you can configure:

![Screen-Shot-2023-08-07-11-15-50.28.png](Screen-Shot-2023-08-07-11-15-50.28.png)

With this configuration, every member/user with the role CCHS member can subscribe to any new published node of content type 'instagram profiel post' (which is just a link) on the user profile page:

![Screen-Shot-2023-08-07-11-18-45.40.png](Screen-Shot-2023-08-07-11-18-45.40.png)

#### 3. Subscriptions settings

Ssubscriptions are saved as user data in the database table `users_data`.

DANSE provides two new tabs on the user profile page called 'subscriptions' and  'notifications' (you can disable this on the DANSE settings page `/admin/config/system/danse`). Here you can subscribe to all configured events as explained in the step above. 

As an admin / website default setting I would like to send a notification to every member on the website, so I selected the role which should receive this:

![Screen-Shot-2023-08-07-11-21-39.83.png](Screen-Shot-2023-08-07-11-21-39.83.png)

#### 4. Handling & delivering notifications

Based on an event and a subscription, a notification object and notification action object are created and saved as a content entities. See [notification entity class](https://git.drupalcode.org/project/danse/-/blob/2.2.x/src/Entity/Notification.php) and [notification action entity class](https://git.drupalcode.org/project/danse/-/blob/2.2.x/src/Entity/NotificationAction.php).
In the database you can find the data in the tables `danse_notification` and `danse_notification_action`.

DANSE does not provide a way to deliver notifications out-of-the-box to users. You need to configure a way of sending notifications by yourself. I followed DANSE' advice using the [Push Framework]([Push Framework | Drupal.org](https://www.drupal.org/project/push_framework)) module (build by the same maintainers).

Install and enable the module with Composer and Drush:  
`composer require drupal/push_framework`  
`drush en push_framework`

![Screen-Shot-2023-08-08-10-02-47.45.png](Screen-Shot-2023-08-08-10-02-47.45.png)

Settings of the Push Framework can be found at `admin/config/system/push_framework`. In the screenshot above I already enabled the Push Framework Email module.

#### E-mail

Remember my goal is to send a role based notification by email when a new link is posted.  For this, we also need to install and enable the [Push Framework Email](https://www.drupal.org/project/pf_email).  
`composer require drupal/pf_email`  
`drush en pf_email`

After these steps, I expected a new queue was created (Push Framework) but there was none when I checked `/admin/config/system/queues`. After some digging into the code, I noticed things are getting somewhat complex/fuzzy for me now. There are several cron tasks which need to be run, so I just ran `drush cron` and the queue was now created with job items.

The first times I was testing this setup no emails were sent. The next day I started debugging, the emails were sent... I think I missed something in the cron tasks logic. If I'm correct, there are three cron tasks responsible for handling all events and notifications.

![Screen-Shot-2023-08-08-10-33-11.96.png](Screen-Shot-2023-08-08-10-33-11.96.png)

#### Mobile (web) push notifications with OneSignal

For my client project the wish is to send mobile notifications. The project already works as a progressive web application with the [Progressive Web App module](https://www.drupal.org/project/pwa). This [issue]([Create pwa_webpush submodule to manage push notifications [#3070130] | Drupal.org](https://www.drupal.org/project/pwa/issues/3070130)) needs work to make (web) push notifications work in that module, so I'm curious how it would work with enabling the [Push Framework OneSignal](https://www.drupal.org/project/pf_onesignal) module.  
The service OneSignal provides a way to send push notification with Web Push: https://onesignal.com/webpush. With their free tier you can send 1 notification to max 10.000 recipients. 

_I will dig into this later._ 

OneSignal documentation: https://documentation.onesignal.com/docs/web-push-quickstart & https://documentation.onesignal.com/docs/drupal

## A notifications block / widget

This is a block which you can use as a base for a user notifications center. The block is provided by the view 'DANSE User Notifications (Notification)' (view: `admin/structure/views/view/danse_user_notifications/edit/block_1`). This block show a number of unread notifications to the logged in user and the unread notifications.

I already started implementing this block in my client project, notice the right notification icon with indicator I worked out:
![Notification center icon with indicator](Screen-Shot-2023-08-09-12-00-48.53.png)


## Notes / tips / debugging

* When testing or debugging several configurations, make sure you run your cronjob after an event. Run the specific DANSE cron with ` drush cron:run danse_cron` when you're using the [Ultimate Cron](https://www.drupal.org/project/ultimate_cron) module.
* Open `/admin/reports/danse` to view all logs around events and notification.
* In the Manage Display tab of a content type you can add the 'DANSE subscription' field to allow users to subscribe to specific content. I haven't fully tested this, but it should render a dropbutton widget.
* Most cron tasks have a 15-minute interval to be run. My advice is to set the following cron tasks to a lower interval (you can do this with the Ultimate Cron module enabled at `admin/config/system/cron/jobs`) to make sure the tasks are always processed when the cron is ran:
  * Drupal Audit Notification Subscription Event    
  * Push Framework
  * Advanced Queue

Also check the interval of your cronjob to check how often your cron is called (my advice is to set it to a couple of minutes when a lot of events happen on your site).

### Future goals / wish list for the feature

- Send a daily/weekly/monthly digest email with the newest content to members, like [Message Digest](https://www.drupal.org/project/message_digest)
- Send notification when someone comments on your posted link
- Send notification when someone rates your profile
- Send notification when someone replies to your comment (integration with subcomments)
- Add individual subscription option for:
  - new activity on a link (such as a new comment)

Other useful resources I found when I wrote this blog:
* https://www.drupal.org/project/danse/issues/3194666
* https://www.hashbangcode.com/article/drupal-10-creating-notification-system-using-message-and-eca-modules
