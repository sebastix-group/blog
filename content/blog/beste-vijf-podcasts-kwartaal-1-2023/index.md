---
title: Beste vijf podcasts van 1e kwartaal 2023
date: 2023-04-12T20:13:13.000Z
description: Dit zijn de vijf beste podcasts die ik afgelopen kwartaal heb geluisterd.
categories: podcasts
comments: true
---

## [De multitaskende klootzak](https://overcast.fm/+-afoRA5jA)

## [AI responsibility in a hyped-up world](https://overcast.fm/+_j3VMWpRg)

## [Meer verdienen door minder te doen](https://overcast.fm/+JIU4dQI7k/)

## [Leaving the cloud](https://overcast.fm/+JptjHCw9M)

## [Craig Silverman and Google’s ad business of “porn, piracy, and fraud”](https://overcast.fm/+KXY8HzGzc)

### Bonus

## [Susan Linn - author of “Who’s Raising the Kids?”](https://overcast.fm/+KXY8Banqg)