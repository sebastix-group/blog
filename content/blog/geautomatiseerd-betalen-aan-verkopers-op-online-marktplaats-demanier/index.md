---
layout: blog
title: Geautomatiseerd betalen aan verkopers op online marktplaats demanier.nl
date: 2021-03-27T20:08:32.117Z
description: Automatisch betalingen uitvoeren met variabele bedragen is niet
  eenvoudig. Voor het platform demanier.nl, een online marktplaats, heb ik
  daarom een maatwerk oplossing in PHP uitgewerkt met behulp van de API van de
  online bank Bunq.
categories: PHP, Bunq, API
comments: "true"
---
Op online marktplaats [demanier.nl](https://www.demanier.nl) (nog in ontwikkeling bij het schrijven van deze blog) worden er duurzame producten verkocht door verschillende ondernemers. Elke ondernemer heeft er een eigen webshop. Daar bepalen zij zelf voor welke prijs zij hun product op het platform aanbieden. Demanier.nl rekent een bepaald percentage aan commissie plus transactiekosten. De rest van het bedrag is voor de ondernemer. Voor de consument lopen alle webshopbetalingen via payment service provider Mollie. Bij elk verkocht product wordt de ontvangen betaling opgesplitst in drie delen:

1. Transactiekosten van € 0,65
2. 4% commissie voor demanier.nl
3. Het deel voor de verkoper

Het laatste deel wordt periodiek uitbetaald aan de verkoper. Dit kun je laten doen door een payment service provider zoals Mollie, maar voor DeManier heb ik zelf de programmatuur uitgewerkt. De voordelen?

* Goedkoper: je betaalt minder kosten per transactie
* Volledig geautomatiseerd
* Volledige vrijheid in hoe je het proces uitwerkt

Al enkele jaren ben ik klant bij de online bank Bunq. De voornaamste reden hiervoor is dat zij een API (application programmable interface) hebben waarmee je de vrijheid hebt om betalingen te automatiseren, ook met variabele bedragen. Deze API gebruik ik in de programmatuur voor demanier.nl om per verkoper te berekenen hoeveel hij uitbetaald dient te krijgen. Hieronder zie je de PHP code snippet (in combinatie met Drupal en de [Bunq PHP library](https://packagist.org/packages/bunq/sdk_php)) waarin dit wordt uitgevoerd:

```php
    \Drupal::logger('demanier_split_payments')->notice(sprintf('Balance before processing payments: %d', $monetaryAccountBank->getBalance()->getValue()));
    // Process the split payments with the Bunq API
    foreach ($split_payments['sellers'] as $seller) {
      $account = $seller['account'];
      foreach ($seller['split_payment'] as $order_id => $payment) {
        $amount = new Amount(round($payment['amount']->getNumber(), 2), 'EUR');
        $type = ($bunqConfig->get('environment') === 'production') ? 'IBAN' : 'EMAIL';
        $counterpartyAlias = new Pointer(
          $type,
          $account['iban'],
          $account['name']
        );

        /** @var Payment $split_payment */
        $split_payment_id = Payment::create(
          $amount,
          $counterpartyAlias,
          $payment['description'],
          $monetaryAccountBank->getId()
        )->getValue();
        /** @var Payment $split_payment */
        $split_payment = Payment::get($split_payment_id, $monetaryAccountBank->getId())->getValue();
        \Drupal::logger('demanier_split_payments')->notice(sprintf('Updated balance after payment: EURO %d', $split_payment->getBalanceAfterMutation()->getValue()));
        \Drupal::logger('demanier_split_payments')->notice(
          sprintf('Split payment verwerkt (id %d): %s euro verstuurt naar %s tnv %s',
            $split_payment->getId(),
            round($payment['amount']->getNumber(), 2),
            $split_payment->getCounterpartyAlias()->getIban(),
            $split_payment->getCounterpartyAlias()->getDisplayName())
        );
      }
    }
```

Dit systeem neemt dus veel werk uit handen: grote bestanden met incasso batches zijn niet meer nodig! Bunq maakt met zijn API automatisch uitbetalen breed beschikbaar. Dit moet natuurlijk wel eenmalig goed geprogrammeerd worden ;-)