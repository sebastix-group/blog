---
layout: blog
title: Terug naar eenvoudige statistieken met Matomo, bye bye Google Analytics
date: 2021-08-02T07:14:03.951Z
description: "Al enkele jaren stoor ik me aan de rapportages van Google
  Analytics. Eenvoudige statistieken over je eigen website worden mijns inziens
  veel te ingewikkeld weergegeven. In de beginjaren van Google Analytics vond ik
  alles nog overzichtelijk, wat het leuk maakte om iets te doen met deze
  statistieken. Naarmate de complexiteit van deze analytics tool toenam,
  verdween ook het plezier in het volgen van de statistieken. In de tussentijd
  is er ook een compleet nieuw vakgebied ontstaan: online marketing. Dat
  vakgebied heb ik links laten liggen, een bewuste keuze."
categories: Matomo, Google Analytics
comments: "true"
---
Toen ik bij d-Media werkte (2014 - 2017), maakte ik kennis met Piwik: een alternatieve tool voor Google Analytics. Met de komst van allerlei cookie- en GDPR-wetgeving nam de adoptie van Piwik ook een vlucht. In 2018 heeft Piwik haar naam gewijzigd naar Matomo. Als je gebruik maakt van Matomo, hoef je je bezoeker geen cookies van een derde partij te laten accepteren als je de software host op je webserver. Een cookiebalk plaatsen op je website is dan vaak niet nodig.

## Voordelen Matomo tov Google Analytics

* Keuze om het zelf te hosten. Zoals een Wordpress of Drupal website, kun je Matomo als ‘website’ installeren op je eigen webserver.
* Je bent 100% eigenaar van alle data.\
  Alle data wordt opgeslagen in een database op je eigen server.
* Het is open source software.\
  Als je de software regelmatig bijwerkt naar de nieuwste versie, geniet je van de beste bescherming tegen virussen of hacks dankzij een grote community die continue verbeteringen aanbrengt in de programmatuur.
* Het is een eenvoudig design dus je hebt snel inzicht in de statistieken van je website. De rapportages zijn gemakkelijk te gebruiken.

## Eenvoudige statistieken, grip op inzichten

Over het laatste voordeel wil ik nog iets meer kwijt. Als niet online-marketeer wil ik snel en eenvoudig inzicht in bepaalde statistieken kunnen opzoeken. Statistiek hoeft niet moeilijk te zijn: het is een basisvaardigheid die je als leerling op de middelbare school hebt meegekregen.

Voor mijn eigen websites heb ik een aantal vragen die ik middels statistiek altijd kan beantwoorden:

* Wat zijn de best bezochte pagina’s op mijn website afgelopen periode (bijv. afgelopen maand)?
* Waarom zijn dit de beste bezochte pagina’s?
* Welke trends zijn er zichtbaar van pagina’s die minder of meer bezocht worden t.o.v. vorige maand?
* Waar komen de bezoekers vandaan? 
* Heeft het delen van links (bijv. deze blog) naar mijn websites via diverse digitale kanalen (email, social media) effect? Ofwel, hoe groot is mijn netwerkeffect?

![Matomo statistieken eigen website](schermafbeelding-2021-08-02-om-14.23.28.png)

Sinds ik gebruik maak van Matomo, is het weer een plezier om dagelijks een keer in de statistieken rond te kijken. Hoe vaker je kijkt, hoe meer nieuwe inzichten het je oplevert. Het eenvoudig verkrijgen van nieuwe statistische inzichten ben ik enkele jaren geleden kwijtgeraakt met Google Analytics, maar [Matomo](https://matomo.org/) heeft me dit weer teruggegeven.