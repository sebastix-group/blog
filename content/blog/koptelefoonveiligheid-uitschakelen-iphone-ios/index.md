---
title: "Koptelefoonveiligheid uitschakelen op je iPhone"
date: "2020-12-11T21:00:13.000Z"
description: "Sinds iOS 14.2 heeft Apple besloten om automatisch de volume van je koptelefoon te verlagen als deze volgens hen te hoog staat. Irritant in combinatie met mijn Aftershokz! Lees hier hoe ik het heb uitgeschakeld."
categories: [sebastix, weblog, internet]
comments: true
---

Sinds iOS 14.2 heeft Apple besloten, vanwege EU / WHO richtlijnen, om automatisch de volume van je koptelefoon te verlagen als deze volgens hen te hoog staat. 
Heel irritant, zeker in mijn geval als ik gebruik maak van mijn [Aftershokz](https://aftershokz.com/products/air) die niet in je oorschelp zitten maar middels 'bone conduction' het geluid doorgeven.

Deze [instructies](https://discussions.apple.com/thread/252019254?answerId=253852969022#253852969022) hebben deels me geholpen om de functie "uit te schakelen" op mijn iPhone. 

Ga naar 'Instellingen > Geluid > Koptelefoonveiligheid' en zet daar de optie 'Verminder harde geluiden' aan. Een schuifbalk verschijnt en zet deze maximaal op 100 decibel.
![test](IMG_3154.PNG)