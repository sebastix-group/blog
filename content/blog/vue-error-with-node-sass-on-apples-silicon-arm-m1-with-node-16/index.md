---
layout: blog
title: Vue error with node-sass on Apple's Silicon ARM M1 with Node version 15 and 16
date: 2021-06-04T06:19:30.290Z
description: "My Vue project (vue 2.x) gave me the following error on my new M1
  powered Macbook Pro:  Node Sass does not yet support your current environment:
  OS X 64-bit with Unsupported runtime (93)."
categories: node, sass, vuejs
comments: "true"
---
On my machine I installed Node version 16 and when I tried to run my Vue project, the following error occured:

```
 ERROR  Failed to compile with 1 error                                                                                                                      08:31:43

 error  in ./src/App.vue?vue&type=style&index=0&lang=scss&

Syntax Error: Error: Node Sass does not yet support your current environment: OS X 64-bit with Unsupported runtime (93)
For more information on which environments are supported please see:
https://github.com/sass/node-sass/releases/tag/v4.14.1


 @ ./node_modules/vue-style-loader??ref--8-oneOf-1-0!./node_modules/css-loader/dist/cjs.js??ref--8-oneOf-1-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-oneOf-1-2!./node_modules/sass-loader/dist/cjs.js??ref--8-oneOf-1-3!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/App.vue?vue&type=style&index=0&lang=scss& 4:14-416 15:3-20:5 16:22-424
 @ ./src/App.vue?vue&type=style&index=0&lang=scss&
 @ ./src/App.vue
 @ ./src/main.js
 @ multi (webpack)-dev-server/client?http://192.168.178.227:8081&sockPath=/sockjs-node (webpack)/hot/dev-server.js ./src/main.js
```

With Node version 15 I got quite the same error:

```
 ERROR  Failed to compile with 1 error                                                                                                                      08:45:34

 error  in ./src/App.vue?vue&type=style&index=0&lang=scss&

Syntax Error: Error: Node Sass does not yet support your current environment: OS X 64-bit with Unsupported runtime (88)
For more information on which environments are supported please see:
https://github.com/sass/node-sass/releases/tag/v4.14.1


 @ ./node_modules/vue-style-loader??ref--8-oneOf-1-0!./node_modules/css-loader/dist/cjs.js??ref--8-oneOf-1-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-oneOf-1-2!./node_modules/sass-loader/dist/cjs.js??ref--8-oneOf-1-3!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/App.vue?vue&type=style&index=0&lang=scss& 4:14-416 15:3-20:5 16:22-424
 @ ./src/App.vue?vue&type=style&index=0&lang=scss&
 @ ./src/App.vue
 @ ./src/main.js
 @ multi (webpack)-dev-server/client?http://192.168.178.227:8081&sockPath=/sockjs-node (webpack)/hot/dev-server.js ./src/main.js
```

Solution is quite simple: **use node version 14**.

I've done this with `n`, a Node's version manager application. See [this answer on Stackoverflow](https://stackoverflow.com/a/50287454).

Check which node version you're using

```
$ node -v 
v16.3.0
```

Install n

```
$ npm install -g n
```

Get list of available Node versions you can install

```
$ n ls-remote --all
16.3.0
16.2.0
..
15.14.0
15.13.0
..
14.17.0
14.16.1
..
```

Install Node version 14

```
$ sudo n install 14
```

List all local installed Node versions

```
$ n ls
node/14.17.0
node/16.3.0
```

Just run the command `sudo n` on your terminal to get an interactive shell to manage your Node version.