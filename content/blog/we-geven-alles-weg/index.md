---
layout: blog
title: We geven alles weg
date: "2011-10-01T08:40:26.000Z"
description: 
categories: online privacy, online identiteit, Bits of Freedom, social media, Big Brother, Facebook, Google, Hyves, LinkedIn, Twitter, personal graph
comments: "true"
---

Persoonlijke informatie is van jou en niet van Facebook, Google, Hyves of andere social media. Toch geven we dit alles al jarenlang weg aan hen, zonder eigenlijk goed te beseffen wat de mogelijke indirecte gevolgen kunnen zijn in de toekomst. Mijn inziens zijn er ook nog steeds genoeg personen die niet volledig doorhebben dat ze in deze constructie gratis gebruik kunnen maken van dergelijke social media. Onder velen heerst er een onverschillige houding tegenover het feit dat we massaal informatie over onszelf weggeven. ‘Ik heb toch niets te verbergen’ is een veelgehoord argument van de generaties Y en Z. De documentaire ‘Wat nou privacy’ van de VPRO brengt dat goed in beeld. Toch concludeert een rapport van het ECP dat bijna 70% van alle Nederlanders er problemen mee heeft dat bedrijven hun gegevens bewaren of registreren. Privacy is dan ook een containerbegrip geworden en iedereen zou voor zichzelf een betekenis moeten definiëren. In mijn beleving is privacy onlosmakelijk verbonden aan inzicht en controle.

## Veilig

Ik kan nu kiezen om diverse ‘what if’ scenario’s te schetsen over allerlei problemen, maar dit geeft in mijn ogen de huidige problematiek niet concreet weer. De problemen
nemen toe en zorgen voor een steeds actueler wordende discussie
rondom online privacy. De heren van Bits of Freedom kunnen dat als
geen ander bevestigen. Wie er met een kritische blik op gaat letten,
zal zien dat er dagelijks nieuws te vinden is over deze problemen en de bijbehorende discussie. Georganiseerde hacks en datalekken waardoor gegevens van tienduizenden personen op straat komen te liggen, werknemers die spullen delen die los staan van hun werk maar toch ontslag krijgen van werkgevers, foto’s op Facebook die door bedrijven worden gebruikt in hun mediacampagnes, bedrijven die ‘geanonimiseerde data’ verkopen aan de overheid: allemaal voorbeelden van manieren waarop onze online data ongewild gebruikt wordt door anderen. Zie ook hoe in een versneld tempo het Zwartboek datalekken wordt bijgewerkt.

## Doolhof

Heeft iemand van ons enig idee hoeveel persoonlijke data er rondzwerft, in welke databases van derden we allemaal staan of in wiens adresboek we voorkomen? Het is niet vanzelfsprekend dat wij recht hebben op deze inzagen, dat laten deze dossiers van Rejo Zenger duidelijk zien. Bovendien kun je je afvragen hoe moeilijk het zou zijn om jezelf te verwijderen uit een dergelijke database. Probeer hier maar eens na te gaan hoe complex dit geheel is. Of gebruik
deze visualisatie tool van ‘The Wall Street
Journal’ om te zien hoe de grootste
websites jouw surfgedrag archiveren en op
welke manier. Zou het echt zo zijn dat alle
Nederlanders in een database zitten van
een Engels marketingbureau? Vraag je jezelf
ook wel eens af hoeveel profielen je hebt
op het internet? Mijn Lastpass geeft op dit
moment aan dat ik 243 inloggegevens heb
voor diverse websites. Dat zou betekenen
dat ik, Sebastian Hagens, misschien wel
tientallen keer besta. Potentiële oplossingen zoals OpenID zijn afgeschreven, maar toch is er een social sign-in trend zichtbaar waarmee je jouw identiteit ontleent aan een populair social media profiel. Als je dit

gebruikt, zul je zien dat je vervolgens vaak nog meer gegevens moet invullen en dan zijn we dus weer terug bij af.

## Product you

Terug naar social media en het internet. De kracht van social media speelt in op onze sociaal behoeftes. We zijn van oudsher kuddedieren en zullen dit altijd blijven. Dat houdt in dat we behoefte hebben aan sociaal contact en welzijn (op de uitzonderingen daargelaten). We houden van aandacht en via social media is het eenvoudiger dan ooit om dit te krijgen. Via deze weg is het voor het eerst in de geschiedenis mogelijk om dit gedrag massaal vast te leggen! Daar waar onze overheden dit al tientallen jaren pogen te doen in de fysieke openbare ruimtes (Big Brother is watching us), wordt dit dankzij het internet onbewust al vanzelf vastgelegd. De grote internetbedrijven die dit principe snel doorhadden, zijn er in zeer korte tijd behoorlijk welvarend door geworden. Controle over en kennis van inzichten in dit gedrag zijn dus zeer waardevol als je de waarderingen van grote internetjongens bekijkt, zoals Facebook en LinkedIn.
We geven alles weg en zijn de regie kwijt op al onze digitale eigendommen. In ruil hiervoor kunnen we gratis gebruikmaken van diensten en producten van derden (laat het duidelijk zijn; ik ben fanatiek Google gebruiker omdat hun producten eenvoudig doen wat ze beloven). Wij zijn het product en de prijs die ervoor wordt betaalt voelen we niet direct. Gezien de slimheid van alle bedrijven die op alle manieren zoveel mogelijk van je willen weten kan er op den duur een behoorlijke ‘privacybom’ ontploffen. Zodra we deze kater voelen, zullen zaken pas echt radicaal gaan veranderen.
Het is relatief eenvoudig om persoonlijke datasets te kopen van diverse partijen en ze daarna in een eigen systeem samen te voegen om op die manier zo rijk mogelijk gevulde profielen te genereren. Waar ligt de grens of je dit wel of niet kunt herleiden naar een persoon? Wie houdt hier (internationaal) toezicht op? Op de CBP hoeven we niet te rekenen. De wet- en regelgeving op dit gebied loopt zeker 15 jaar achter, dus bedrijven hebben vrij spel om te doen wat ze willen met onze persoonlijke data.
Persoonlijke data centraliseert zich steeds meer op diverse locaties, waarvan Facebook de grootste is. Een wereld waar persoonlijke data op individueel niveau worden gecentraliseerd is het tegenovergestelde. Echter hebben we nog niet de beschikking over eenvoudig te adopteren hulpmiddelen om hier een begin mee te maken. Als elk individu op die manier inzicht krijgt in en controle krijgt over zijn gegevens, betekent dat automatisch dat diverse online privacy problemen worden opgelost en er tevens een nieuwe generatie aan toepassingen kan ontstaan. De eindgebruiker controleert en heeft inzicht. Vanuit dat inzicht kan hij invloed uitoefenen die past bij de behoefte, wens of het belang die hij deelt.
In een volgend artikel wil ik jullie meenemen in de wereld waarin het normaal is om met een systeem met andermans informatie geld te verdienen. Vele bedrijven doen dit en verdienen hier veel geld mee. Maar het is wel van belang voor iedere gebruiker om dit systeem te doorbreken. Het belang dat je eigenaar blijft over jouw data en hiermee waarde voor jezelf kunt creëren. In het derde en laatste artikel zal ik dit nader toelichten aan de hand van een personal graph en in een nieuw digitaal landschap.