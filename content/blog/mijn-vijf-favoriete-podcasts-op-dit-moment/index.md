---
layout: blog
title: Mijn vijf favoriete podcasts op dit moment
date: 2022-09-28T06:31:52.772Z
description: "Wat zijn op dit moment mijn vijf favoriete podcasts die ik
  wekelijks luister? "
categories: podcast
comments: "true"
---
## [S﻿atoshi Radio](https://satoshiradio.nl/)

[](https://satoshiradio.nl/)B﻿art Mol, Bert en Peter Slagter duiken wekelijks in de rabbithole van Bitcoin, cryptocurrency en andere financiele bewegingen in de wereld. De wekelijkse marktupdate geeft je een haarscherpe analyse wat er speelt in de macro-economie.

## [S﻿elfhosted](https://selfhosted.show/)

T﻿wee nerds die wekelijks de nieuwste opensource tools bespreken die je zelf kunt installeren.

## [E﻿indbazen](https://eindbazen.nl/podcasts/)

U﻿iteenlopende gasten over diverse ontwerpen schuiven aan bij deze podcast. De vragen die gesteld worden, dwingen deze gasten om de diepte in te gaan over zijn of haar expertise. Dit kan gaan over economie, technologie, psychologie, filosofie, sport, etc. Centraal staan jouw persoonlijke groei en welzijn. 

## [T﻿echtonic](https://techtonic.fm/)

M﻿ark Hurst heeft een zeer uitgesproken mening over BigTech en nodigt wekelijks interessante gasten uit om hierover te praten.

## [D﻿e Rode Lantaarn](https://dagennacht.nl/serie/de-rode-lantaarn/)

[](https://dagennacht.nl/serie/de-rode-lantaarn/)A﻿ls wielerliefhebber is dit de podcast om altijd op de hoogte te blijven wat er speelt in deze sportwereld.