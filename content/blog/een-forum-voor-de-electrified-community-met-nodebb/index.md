---
layout: blog
title: " Een forum voor de Electrified Online community"
date: 2021-05-24T07:44:00.000Z
description: "Sinds kort bestaat er een nieuw online platform voor liefhebbers
  van elektrische auto's: Electrified Online. Voor deze community heb ik een
  forum gerealiseerd met het opensource NodeBB softwarepakket."
categories: nodebb forum
comments: "true"
---
Tot het jaar 2012 heb ik veel gewerkt met [phpBB](https://www.phpbb.com) voor het opzetten van online fora (bijvoorbeeld voor de Civic Club Holland, nu [Honda Community Nederland](http://www.hondacommunity.nl/forum/)). Na deze periode liep het gebruik van fora fors terug door de opkomst van social media. Veel mensen voerden vervolgens de discussies op Facebook. 

## Het doel

Nu het gebruik van social media, met name Facebook, terugloopt, groeit de behoefte van mensen om kennis op een andere manier uit te wisselen. Het doel van [Electrified Online](https://www.electrified.online/) is om liefhebbers van elektrische auto's samen te brengen, zowel fysiek op evenementen als digitaal op internet. Electrified Online is actief op diverse social media, maar deze voorzien niet in de mogelijkheid om autoliefhebbers direct met elkaar in contact te brengen. Daarom is er voor gekozen om een forum op te zetten. Op die manier hoopt mijn klant daar de autofans met elkaar in contact te brengen.

Het forum is te bezoeken op <https://forum.electrified.online>.

## NodeBB

Naast NodeBB heb ik in mijn vooronderzoek ook andere forum software bekeken:

* [Discourse](https://www.discourse.org/)
* [Forem](https://github.com/forem/forem)
* [myBB](https://mybb.com/)
* [Flarum](https://flarum.org/)
* [phpBB](https://www.phpbb.com/)
* [vBulletin](https://www.vbulletin.com/)

Mijn advies was om te kiezen voor Discourse of NodeBB. Beide software worden actief doorontwikkeld en er is voldoende documentatie te vinden over hoe je deze moet installeren en onderhouden. Mijn klant maakte de keuze voor NodeBB, omdat de visuals van andere NodeBB fora, hem wel aanspraken. 

## Tech stack

De gebruikte software in de opbouw van backend naar frontend. 

1. [CentOS](https://www.centos.org/centos-linux/) 7 met [Nginx](https://nginx.org/en/) webserver
2. [Node.js](https://nodejs.org/en/) via [NVM](https://github.com/nvm-sh/nvm)
3. [Redis](https://redis.io/) database
4. [NodeBB software](https://github.com/NodeBB/NodeBB)
5. [Supervisord](http://supervisord.org/)

## Maatwerk thema

Alle NodeBB thema's zijn gebouwd op basis van het [Bootstrap 3 framework](https://getbootstrap.com/). Een van de vier standaard nodeBB thema's heb ik gebruikt als basis voor een maatwerk thema voor Electrified Online: het [Vanilla thema](https://github.com/NodeBB/nodebb-theme-vanilla). In het thema heb ik de header met het menu en de footer overgenomen van de reeds bestaande website. Daarnaast heb ik het ontwerp van de categorien aangepast door foto's toe te voegen en de titel hierop te centreren. 

![Electrified Online NodeBB forum](screenshot.png "Electrified Online NodeBB forum")

De code van het thema staat op mijn Github en kan dus door iedereen gebruikt worden: <https://github.com/Sebastix/nodebb-theme-electrified/>. 

## Technische uitdagingen en oplossingen

De hele technische scope van dit project was nieuw voor mij, aangezien mijn ervaring met Node.js applicaties summier was. Daarnaast heb ik de keuze gemaakt om een testomgeving vanaf nul op te bouwen. Dit had ik nog niet eerder gedaan met CentOS in combinatie met Nginx, Redis, Node.js en andere software. 

### Continues deployment met Gitlab

Voor het doorvoeren van wijzingen en updates op het forum heb ik ervoor gekozen om dit zoveel mogelijk te automatiseren via [Gitlab](https://about.gitlab.com/). In dit deployment proces heb ik ingesteld dat er altijd eerst een backup wordt gemaakt van het forum.

`mkdir -p /home/electrified/forum.electrified.online/backups/"$(date +"%Y%m%d")"`

`cp /var/lib/redis/dump.rdb /home/electrified/forum.electrified.online/backups/"$(date +"%Y%m%d")"/dump.rdb`

`cp -r /home/electrified/forum.electrified.online/public/uploads/home/electrified/forum.electrified.online/backups/"$(date +"%Y%m%d")"/`

Daarna worden de `package.json` en `package-lock.json` gekopieerd naar het project en worden deze geinstalleerd met `npm ci`.

Als laatste stap wordt het upgrade commando in nodeBB uitgevoerd en wordt het forum herstart met Supervisord.

`/home/electrified/forum.electrified.online/nodebb upgrade`

`supervisorctl restart forum.electrified.online` 

### Development omgeving met Docker

Het opzetten van een development omgeving op mijn Macbook Pro met Docker was een lastige puzzel. In mijn zoektocht naar een logische opzet, vond ik verschillende oplossingen. Elke oplossing had wel een minpunt, dus heb ik ervoor gekozen om een eigen Docker setup uit te werken op basis van verschillende opzetten en Docker Compose. Hieronder vind je de code snippets van deze setup uit het project.

`docker-compose.yml`

```yaml
# https://github.com/rez0n/docker-nodebb
version: '3.5'
services:
  nodebb:
    container_name: nodebb
    build:
      context: .
      dockerfile: Dockerfile
    restart: unless-stopped
    environment:
      URL: "http://localhost"
      DATABASE: "redis"
      DB_NAME: "0"
      DB_HOST: "redis"
      DB_PORT: "6379"
    volumes:
      - ./data/nodebb:/data
    networks:
      - nodebb
    ports:
      - "4567:4567"

  redis:
    container_name: redis
    image: redis
    restart: unless-stopped
    volumes:
      - ./data/redis:/data
    networks:
      - nodebb

networks:
  nodebb:
    driver: bridge
```

`Dockerfile`

```dockerfile
FROM node:lts

RUN mkdir -p /usr/src/app
RUN mkdir -p /data
WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY install/package.json package.json

RUN npm install \
    && npm cache clean --force

# Install grunt-cli
RUN npm install -g grunt-cli

COPY . /usr/src/app

VOLUME /data
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]

ENV NODE_ENV=development \
    daemon=false \
    silent=false

EXPOSE 4567
```

`entrypoint.sh`

```shell
#!/bin/bash
set -e

if ! [ -e /data/config.json ]; then

    if [ -n "$DATABASE" ]; then

        if [ "$DATABASE" == "redis" ]; then
            url=$URL database=$DATABASE redis__database=$DB_NAME redis__password=$DB_PASSWORD redis__host=$DB_HOST redis__port=$DB_PORT node app --setup --series
        elif [ "$DATABASE" == "mongo" ]; then
            url=$URL database=$DATABASE mongo__username=$DB_USER mongo__password=$DB_PASSWORD mongo__host=$DB_HOST mongo__port=$DB_PORT node app --setup --series
        fi

    else
        echo "Database setting is invalid"
    fi

    cp config.json /data/config.json \
    && ln -s /data/config.json /usr/src/app/config.json
else
    # Link config file (in cases when you recreated container)
    if [ ! -e /usr/src/app/config.json ]; then
        ln -s /data/config.json /usr/src/app/config.json
    fi

fi

if [ ! -e /data/uploads ]; then
    mv /usr/src/app/public/uploads /data/uploads \
    && ln -s /data/uploads /usr/src/app/public/uploads
else
    rm -rf /usr/src/app/public/uploads \
    && ln -s /data/uploads /usr/src/app/public/uploads
fi

if [ ! -e /data/package.json ]; then
    mv /usr/src/app/package.json /data/package.json \
    && ln -s /data/package.json /usr/src/app/package.json
else
    rm /usr/src/app/package.json \
    && ln -s /data/package.json /usr/src/app/package.json
fi

# Custom theme nodebb-theme-electrified
if [ ! -e /data/nodebb-theme-electrified ]; then
    cp -r /data/nodebb-theme-electrified /usr/src/app/node_modules/nodebb-theme-electrified \
    && ln -s /data/nodebb-theme-electrified /usr/src/app/node_modules/nodebb-theme-electrified
else
    rm -rf /usr/src/app/node_modules/nodebb-theme-electrified \
    && ln -s /data/nodebb-theme-electrified /usr/src/app/node_modules/nodebb-theme-electrified
fi

# Custom templates
if [ ! -e /data/templates ]; then
    cp -r /data/templates /usr/src/app/templates \
    && ln -s /data/templates /usr/src/app/templates
else
    rm -rf /usr/src/app/templates \
    && ln -s /data/templates /usr/src/app/templates
fi

if [ -f config.json ]; then
   /usr/src/app/nodebb build --series
#  # Update NodeBB
#  /usr/src/app/nodebb upgrade -mips
   /usr/src/app/nodebb dev
fi

exec "$@"
```

## Het resultaat

Het maken van het forum was een leerzaam proces, omdat ik veel nieuwe technische zaken heb aangeraakt. Het resultaat staat online op <https://forum.electrified.online>.