---
layout: blog
title: Custom Drupal theme met het Vanilla CSS framework
date: 2021-06-11T11:43:35.610Z
description: Al enkele jaren maak ik veelvuldig gebruik van het Bootstrap
  framework als frontend bibliotheek voor typografie, formulier, knoppen en
  andere interface elememten. Een paar weken terug maakte ik kennis met het
  Vanilla CSS framework van het Canonical web team. Dat framework sprak me
  positief aan, omdat het er goed uitzag en technisch summier is opgezet (CSS
  only).
categories: Vanilla CSS, Drupal, Bootstrap, NPM, Gulp
comments: "true"
---
Net als Bootstrap bevat het [Vanilla CSS framework](https://vanillaframework.io/) veel verschillende interface elementen. Vanilla is een CSS only framework, dat wil zeggen dat er geen Javascript bij zit. In Vanilla is wel alles voorbereid om interactieve elementen te maken, zoals een dropdown menu. Je zult dan wel zelf de Javascript hiervoor moeten schrijven.

Nu ik actief in de weer ben met Vanilla, wordt ik uitgedaagd om actiever aan mijn Javascript skills te werken. Daarnaast probeer ik nu altijd eerst een oplossing in CSS uit te werken, bijvoorbeeld als je iets wilt animeren. Vanilla CSS gebruik ik nu in twee type projecten: Vue.js en Drupal.

### Drupal

Voor Drupal heb ik hiervoor een custom thema voor opgezet. De benodigdheden voor Vanilla zijn eenvoudig te gebruiken in het thema dankzij npm.

Om Vanilla toe te voegen, voer je dit commando uit in je theme map

```
npm install vanilla-framework
```

 Alle bestanden worden opgeslagen in de \`node_modules\` map. Al deze bestanden kun je includen door de volgende regels toe te voegen bovenaan je root SASS bestand

```
@import 'node_modules/vanilla-framework/scss/vanilla';
@include vanilla;
```

Meer informatie en andere installatie methods: <https://vanillaframework.io/docs>

### Gulp

Voor het compileren van SASS naar CSS maak ik gebruik van Gulp. Zie [deze instructies](https://vanillaframework.io/docs) in de Vanilla documentatie. Mijn gulpfile.js in de theme map ziet er als volgt uit:

```
'use strict';

var gulp = require('gulp'),
  sass = require('gulp-sass');

gulp.task('build-css', function () {
  return gulp.src('./scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('css'));
});

gulp.task('watch', function () {
  gulp.watch('./scss/*.scss', gulp.series('build-css'));
});
```

Hierbij een screenshot eenvoudige website die ik heb opgezet met Drupal in combinatie met Vanilla.

![RTC de Smid](screenshot-2021-06-12-at-21-45-25-volledig-wielerprogramma-rtc-de-smid-rtc-de-smid.png)