---
layout: blog
title: Aan de slag met Nostr
date: 2023-01-27T21:00:00.000Z
description: Afgelopen weken heb ik veel gehoord en gelezen over Nostr. De reden waarom ik hier enthousiast over ben, is omdat er twee interesses samenkomen in één wereld. Dat is de Bitcoin community (de mensen!) én (decentrale) social media. Om mijn enthousiasme te ventileren, heb ik deze blog geschreven waarin ik mijn eerste indrukken deel over dit social network protocol. 
categories: nostr bitcoin lightning network social media
comments: "true"
---

[Nostr](http://nostr.net) is een afkorting die staat voor **N**otes and **O**ther **S**tuff **T**ransmitted by **R**elays en is een open protocol om decentrale, censuurvrije (sociale) netwerken te vormen.

## **Statistieken**

Als we naar de cijfers kijken, zien we dat de adoptie van Nostr een flinke toename kent in de afgelopen twee maanden. Het aantal mensen dat gebruik maakt van het protocol is gestegen van een paar duizend naar meer dan 400.000.

![https://nostr.band/stats.html](Screen-Shot-2023-01-27-09-36-37.45.png)

*Bron: [Nostr Stats](https://nostr.band/stats.html)*

Dit zijn twee in het oog springende datums:

* 20 december: Jack Dorsey twittert over Damus (een Nostr app die lijkt op Twitter) - [link](https://nitter.namazso.eu/jack/status/1605248689962123264#m)
* 23 januari: Edward Snowden introduceert zichzelf op Nostr - [link](https://snort.social/e/note1qmdvmgxv0wpstkc7lq23kysnz77qkxkcum0969e83xjdtgs3ujlsmcmdqr)



-- 400.000 is natuurlijk nog bijzonder weinig, zeker als je kijkt naar de groei van Mastodon in diezelfde periode. Daar is het aantal gestegen van 300.000 naar meer dan 9 miljoen leden ([bron](https://bitcoinhackers.org/@mastodonusercount)). 
  

> 💡 Mastodon? Zie mijn blog [Liever een fediverse dan een metaverse](https://sebastix.nl/blog/liever-een-fediverse-dan-een-metaverse/)

## **Mijn Nostr profiel verifiëren**

Je kunt jouw Nostr profiel verifiëren als je een eigen domeinnaam hebt. Zie de uitleg in [NIP-05](https://github.com/nostr-protocol/nips/blob/master/05.md) hoe je dit doet middels een [Internet Identifier](https://datatracker.ietf.org/doc/html/rfc5322#section-3.4.1).  

Mijn pubkey heb ik omgezet naar een hex value met [damus key converter](https://damus.io/key/).   
Dit is mijn pubkey: `npub1qe3e5wrvnsgpggtkytxteaqfprz0rgxr8c3l34kk3a9t7e2l3acslezefe` en dit is de hex waarde: `06639a386c9c1014217622ccbcf40908c4f1a0c33e23f8d6d68f4abf655f8f71`.

De data die wordt gebruikt voor de verificatie staat in dit bestand: [https://sebastix.dev/.well-known/nostr.json](https://sebastix.dev/.well-known/nostr.json)

```json
{
  "names": {
    "sebastian": "06639a386c9c1014217622ccbcf40908c4f1a0c33e23f8d6d68f4abf655f8f71"
  }
}
```

In de Damus app zie ik nu een gekleurd verificatie icoontje naast mijn handle @sebastix:

![Screen-Shot-2023-01-26-10-05-45.22.png](Screen-Shot-2023-01-26-10-05-45.22.png)

## **Een eigen relay server**

Aangezien ik graag eigenaar blijf van mijn data, leek het mij een goed idee om zelf een relay server op te zetten waarmee ik mijn Nostr account kan verbinden samen andere relays.

> Voor Mastodon heb ik hetzelfde gedaan,   
> zie mijn blog [ActivityPub relay server](https://sebastix.nl/blog/activitypub-relay-server/).

Het opzetten van een eigen relay server kostte mij een uurtje werk. De relay heb ik nu gekoppeld aan mijn Nostr internet identifier:

```json
{
  "names": {
    "sebastian": "06639a386c9c1014217622ccbcf40908c4f1a0c33e23f8d6d68f4abf655f8f71"
  },
    "relays": {
        "06639a386c9c1014217622ccbcf40908c4f1a0c33e23f8d6d68f4abf655f8f71": ["wss://nostr.sebastix.dev"]
    }
}
```

### Wil je ook een relay server opzetten?

Volgens mij is dit meest gebruikte setup voor het opzetten van een Nostr Relay: [A complete step by step setup guide for Nostr relay based on nostr-rs-relay](https://github.com/BlockChainCaffe/Nostr-Relay-Setup-Guide) en [nostr-rs-relay](https://github.com/scsibug/nostr-rs-relay.git). Deze is echter niet zo uitgebreid qua NIP features als deze: [nostream](https://github.com/Cameri/nostream). Daar kun je deze how-to bij gebruiken: [set up a Nostr relay server in under 5 minutes](https://andreneves.xyz/p/set-up-a-nostr-relay-server-in-under)

Ik gebruik nu een Docker configuratie. Met deze setup liep ik even vast op deze error op mijn CentOS 7 server:

```bash
FATAL:  could not map anonymous shared memory: Cannot allocate memory
HINT:  This error usually means that PostgreSQL's request for a shared memory segment exceeded available memory, swap space, or huge pages. To reduce the request size (currently 4438917120 bytes), reduce PostgreSQL's shared memory usage, perhaps by reducing shared_buffers or max_connections.
```

De server (VPS) die ik hiervoor gebruik, heeft een maximum van 4GB aan werkgeheugen. In `/postgresql.conf` heb ik `shared_buffers` moeten aanpassen van 4GB naar 512MB en `max_connections` van 512 naar 128. Pas daarna startte de Postgres database container goed op… Op de server draaien (te) veel andere services die deze resources nodig hebben (oa een Mastodon instance, paar Drupal sites, ActivityPub relay server, Listmonk, Whoogle, Immich)...🤓🤔

![Screen-Shot-2023-01-27-14-22-49.77.png](Screen-Shot-2023-01-27-14-22-49.77.png)

De verbinding met je Nostr relay server test kan via [websocketring](https://websocketking.com/).
Voor een uitgebreide relay debugging: [relayr](https://nostr.info/relayr/).

Mijn relay server heb ik nu op private gezet. Dat betekent dat alleen ik er nu gebruik van kan maken voor verschillende apps / clients. 

## **Welke Nostr apps kun je gebruiken?**

Op dit moment zijn er een behoorlijk aantal apps (clients) die worden ontwikkeld, zie [deze lijst](https://nostr.net) onder het kopje clients. Veel apps bieden dezelfde features als Twitter, maar met Nostr kun je veel meer realiseren dan alleen een tijdlijn met berichten. Denk bijvoorbeeld aan private en group chats, (micro)blog platforms, fora, mini-games of comment widgets. Het bestaande social media platform [Minds](https://www.minds.com/) is zelfs al bezig met een integratie. Dit nog te realiseren landschap (Nostrverse?) is misschien wel te vergelijk met die van de [Fediverse](https://axbom.com/fediverse/).

Een vergelijking van de meest populaire apps vind je hier: [List of Nostr clients](https://github.com/vishalxl/Nostr-Clients-Features-List). Hierbij een opsomming met apps waar ik kort naar gekeken heb:

- Desktop
  - macOS: Damus
  - Linux: nog geen, gebruik de browser
  - Windows: niet gevonden, gebruik de browser
- Browser
  - [https://astral.ninja/](https://astral.ninja/)
  - [https://snort.social/](https://snort.social/)
  - [https://iris.to/](https://iris.to/)
  - [https://hamstr.to/](https://hamstr.to/)
  - [https://yosup.app/](https://yosup.app/)
- iOS
  - Damus (TestFlight)
  - Iris (TestFlight)
- Android
  - [Nostros](https://github.com/KoalaSat/nostros)
  - [Daisy](https://github.com/neb-b/daisy)
  - [Amethyst](https://github.com/vitorpamplona/amethyst)
- Android ROM / deGoogled Android
  - Download een APK van bovengenoemde Android apps of zoek deze in de Aurora Store

Om maar even aan te geven hoe nieuw deze technologie nog is: in de app Damus heb je nog niet de mogelijkheid om je eigen berichten te bewerken of verwijderen. Als je dus een typefout maakt, kun je dit niet corrigeren via deze app. Via een andere app (ik probeerde het via Iris) kan je jouw bericht wel verwijderen.

## NIPS en events

Note to myself, alle NIPS doornemen om een overzicht te krijgen wat er mogelijk is met Nostr: [nostr-protocol/nips: Nostr Implementation Possibilities](https://github.com/nostr-protocol/nips).

> NIPs stand for Nostr Implementation Possibilities They exist to document what MUST, what SHOULD and what MAY be implemented by Nostr-compatible relay and client software.

Waar ik vooral in geïnteresseerd ben, is welke type interacties je kunt bouwen met Nostr. Deze kun je terugvinden in de lijst met event kinds waarin elke kind wordt aangeduid met een getal: [Event Kinds](https://github.com/nostr-protocol/nips#event-kinds)

![Screen-Shot-2023-01-27-09-35-34.56.png](Screen-Shot-2023-01-27-09-35-34.56.png)

Een voorbeeldje van de kinds / events die worden ondersteund in client [Hamstr](https://hamstr.to/):

```json
export const EventKind = {
  METADATA: 0,
  NOTE: 1,
  RELAY: 2,
  CONTACT: 3,
  DM: 4,
  DELETE: 5,
  SHARE: 6,
  REACTION: 7,
  CHATROOM: 42,
}
```
*Bron: [Event.js](https://github.com/styppo/hamstr/blob/main/src/nostr/model/Event.js)*

Deze events vormen de basis van hoe er wordt geïnteracteerd tussen clients (en dus tussen personen). Stel dat ik een eenvoudige chat app zou willen maken, dan staat dit beschreven in [NIP-28](https://github.com/nostr-protocol/nips/blob/master/28.md) (concept versie).

> This NIP defines new event kinds for public chat channels, channel messages, and basic client-side moderation.
> 
> It reserves five event kinds (40-44) for immediate use and five event kinds (45-49) for future use.
> 
> >`40 - channel create`  
> >`41 - channel metadata`  
> >`42 - channel message`  
> >`43 - hide message`  
> >`44 - mute user`  
> 
> Client-centric moderation gives client developers discretion over what types of content they want included in their apps, while imposing no additional requirements on relays.

## Lightning network integratie

Als je in de app Damus een Lightning invoice (payment request string) plaatst in een post, bijvoorbeeld:
```
lnbc1u1p3a8zf3pp5vtdgz9cz4522n8c5c4pdtngupkdezegcr6mhadh94z8l36sqj2tqdpjfehhxarjypjhsur9wf5k6etwwssxjmnkda5kxefqxgmnqvfjxvcqzpgxq97zvuqsp58an6843zn6gh0at59hgc052w7wdj67qcnehdc2fhpmatna23fd2s9qyyssqmyr7jgs54rujempe2a9aw88zvzgfgm648t776teptj5f6x4dy4n3jvgx0guzsl69k8qvpa9jgxvmavwhpekqt9907ldpar8u2m6ymhcplh3tln
``` 
dan wordt deze als volgt weergegeven (zie tweede bericht van mij): 

![Screen-Shot-2023-01-27-09-24-52.32.png](Screen-Shot-2023-01-27-13-43-49.40.png)

Zoals je kunt lezen heeft Bitcoin Bull 100 sats (dat is nog geen 2 eurocent) naar mijn Lightning wallet overgemaakt! 

## Wil je Nostr doorzoeken?

Dat kan via [nostr.band](https://nostr.band). Voorbeeldje met mijn pubkey [pubkey](https://nostr.band/?q=npub1qe3e5wrvnsgpggtkytxteaqfprz0rgxr8c3l34kk3a9t7e2l3acslezefe) of met die van [Edward Snowden](https://nostr.band/?q=npub1sn0wdenkukak0d9dfczzeacvhkrgz92ak56egt7vdgzn8pv2wfqqhrjdv9). Je kunt ook zoeken met keywords of hashtags. Probeer het zelf maar.



Er is nog genoeg te ontdekken met Nostr. Ik ben benieuwd wat er komende weken en maanden allemaal nog gaat komen. Misschien verdwijnt Nostr langzaam naar de achtergrond en blijft het (voorlopig) bij een opleving. Ik hoop het niet, want tot nu toe is Nostr is te interessant om niet te volgen. 



*Wil je nog meer weten over Nostr? Bekijk dan eens deze bronnen die ik ook heb geraadpleegd om deze blog te schrijven.*

- Podcast [Nostr als écht decentraal alternatief op Twitter | BNR Nieuwsradio](https://www.bnr.nl/podcast/cryptocast/10502074/256-b-nostr-als-echt-decentraal-alternatief-op-twitter)
- [Will Nostr put people back in control of their data?](https://bitcoinpolis.gitlab.io/posts/2023/01/20/nostr.html)
- [Hivemind Ventures over Nostr](https://hivemind.vc/nostr/)
- Video [Nostr - 10 hypotheses](https://yewtu.be/watch?v=tClZkGVLvVs)
- Heel veel links op [https://nostr-resources.com/](https://nostr-resources.com/)
- Podcast [Bitcoin Review Podcast BR018 - Nostr Deep Dive Panel with NVK ft. Jack Dorsey, Fiatjaf & William Casarin | 🎙Bitcoin.Review Podcast](https://bitcoin.review/podcast/episode-18/)
- Podcast [Decentralized social media & bitcoin with William Casarin](https://www.theinvestorspodcast.com/bitcoin-fundamentals/nostr-decentralized-social-media-william-casarin/)
