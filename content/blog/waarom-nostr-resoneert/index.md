---
layout: blog
title: Waarom Nostr resoneert
date: 2023-11-02T09:09:11.463Z
description: Tijdens de grote Twitter migration eind 2022 was ik zowel in de fediverse (met Mastodon) als met Nostr actief in de weer. Afgelopen zomer was ik er echter niet zoveel meer te vinden. Toch is er op Nostr nog een mooie groep power users aanwezig die enthousiast bouwen aan nieuwe applicaties, specificaties van het protocol verfijnen en allerlei andere technische proof-of-concept realiseren.  
categories: nostr, freedom tech
comments: "true"

---

![nostverse](0e2bc672-9841-45cc-89ba-950ab40963c6.png)

Mijn geloof blijft sterk in het potentieel wat Nostr te bieden heeft. 

Voor wie Nostr nog niet kent, hier een korte uitleg.

> Nostr is een communicatie protocol en is een afkorting die staat voor **N**otes and **O**ther **S**tuff **T**ransmitted by **R**elays.
> 
> Het is een open, onafhankelijke standaard waarmee iedereen aan de slag kan. Nostr is ontworpen met het oog op eenvoud en maakt het mogelijk om censuur bestendig, wereldwijd informatie te publiceren op het web. De data wordt verstuurd middels *events* tussen *clients* and *relays* in *plain* JSON formaat. 

In mijn beleving resoneert Nostr met vele andere trends en signalen die ik volg op het internet. Het is freedom tech wat als tegenmacht fungeert tegen de alsmaar toenemende censuur op informatie en surveillance op ons gedrag. Daarnaast sluit het protocol aan bij mijn persoonlijke waardes rondom online privacy en digitale autonomie. 

Nostr kan het internet repareren op een manier zoals bitcoin ons geldsysteem kan repareren. En wat zou er gebeuren als deze twee samensmelten?

Voor mij is het een no-brainer dat Nostr een toekomstbestendig protocol wordt. 

## Nostr clients zijn local-first

Onze apparaten waarmee we over het internet surfen zijn afgelopen jaren exponentieel krachtiger geworden. Dit betekent dat applicaties over steeds meer rekenkracht beschikken en steeds meer zelfstandig kunnen uitvoeren zonder gebruik te hoeven maken van iemand anders zijn computer (de zogenaamde cloud). Dankzij Nostr kunnen we afstappen van het klassieke, gecentraliseerde serverside ecosysteem dat door de tech-monopolies wordt gebruikt om ons steeds meer uit te buiten.

## Data is interoperabel

Data interoperabiliteit dankzij Nostr laat zich het beste uitleggen met een concreet voorbeeld. Dit is een bericht wat ik een tijdje terug op stacker.news plaatste met de optie om het te crossposten via Nostr.

![stacker.news_.png](stacker.news_.png)

https://stacker.news/items/293204

Met deze optie werd dit bericht verstuurd naar verschillende relays waardoor het ook verscheen op de volgende website die fungeren als Nostr client:

![](habla.news_u_sebastian@sebastix.dev_293204.png)

https://habla.news/u/sebastian@sebastix.dev/293204

![](blogstack.io_naddr1qqrrywfnxgcrgqg5waehxw309aex2mrp0yhxgctdw4eju6t0qgsqvcu68pkfcyq5y9mz9n9u7sys33835rpnuglc6mtg7j4lv40c7ugrqsqqqa28rf4dt4.png)

https://blogstack.io/naddr1qqrrywfnxgcrgqg5waehxw309aex2mrp0yhxgctdw4eju6t0qgsqvcu68pkfcyq5y9mz9n9u7sys33835rpnuglc6mtg7j4lv40c7ugrqsqqqa28rf4dt4

![](yakihonne.com_article_naddr1qqrrywfnxgcrgq3qqe3e5wrvnsgpggtkytxteaqfprz0rgxr8c3l34kk3a9t7e2l3acsxpqqqp65wars669.png)

https://yakihonne.com/article/naddr1qqrrywfnxgcrgq3qqe3e5wrvnsgpggtkytxteaqfprz0rgxr8c3l34kk3a9t7e2l3acsxpqqqp65wars669 

## Jij bent de eigenaar van je identiteit

Iedereen kan een set van digitale sleutels aanmaken. Deze set bevat 1 public en 1 private key (sleutel). Deze sleutels samen representeren een identiteit op het Nostr netwerk.  Jouw private key wordt gebruikt om je *events* te signeren die je verstuurd naar het Nostr netwerk. **Let op, het is belangrijk om je private key op een veilige plek te bewaren**. Je wilt niet dat iemand anders deze sleutel in handen krijgt voor mogelijke misbruik. Het is jouw verantwoordelijkheid om goed om te gaan met deze eigenaarschap van je private key en gekoppelde identiteit.  

## Privacy by default

Het signeren van events gebeurt met dezelfde sleutel-cryptografie (secp256k1) die je vindt bij bitcoin voor het ondertekenen van transacties. Nostr is, net zoals bitcoin, niet anoniem. Je identiteit is een pseudoniem. Jij bepaalt wat je deelt met vanuit dit pseudoniem. Als je volledige anonimiteit wilt, moet je clients gebruiken die bijvoorbeeld het Tor-netwerk gebruiken. Of een VPN gebruiken terwijl je Nostr clients gebruikt.

## Permissionless

Net als bij veel andere protocollen ben je vrij om te beginnen met bouwen. Binnen een paar uur kun je een client bouwen, verbinding maken met een aantal relays en beginnen met het ophalen van *events* met *text notes* (kind 1) om bijvoorbeeld een feed te maken met de laatst geplaatste *notes*. Al deze data is publiek beschikbaar en je hoeft aan niemand toestemming te vragen om deze te bekijken.

## Going trustless

Het Nostr protocol is geen eigendom van een derde partij of autoriteit, net zoals het http protocol voor websites of het smtp protocol voor e-mail. Je hoeft dus niet iemand te vertrouwen om met Nostr aan de slag te gaan. 

Voor volledige onafhankelijheid kun je jouw eigen client en relay gebruiken op een eigen server. Andere clients kunnen dan verbinding maken met jouw relay om de *events* op te halen die jij deelt.

## Het is eenvoudig voor developers

Elke junior developer kan binnen een paar uur een client opzetten. Er staan genoeg eenvoudige projecten (geschreven in verschillende programmeertalen) op Github die je als startpunt kunt gebruiken. Op dit moment zijn er meer dan 700 openbare project *repositories* op Github gelabeld met Nostr.

## Censuur bestendig

Nostr is bij uitstek *freedom technology*, vanwege het open en onafhankelijke karakter van het protocol. Elke client kan verbinding maken met meerdere relays om *events* te publiceren. Als een relay besluit om je te blokkeren, kun je gemakkelijk wisselen naar andere relays. Op dit moment zijn er bijna 2000 relays operationeel. 

## Agnostisch en geschikt voor meerdere type data

Het protocol kan worden gebruikt voor allerlei type data die je wilt versturen. Bekijk deze lijst met verschillende type content (er zijn er nog veel meer, dit is slechts een selectie):

| Kind  | Description                           |
| ----- | ------------------------------------- |
| 0     | Metadata                              |
| 1     | Text note                             |
| 4     | Direct message                        |
| 6     | Repost                                |
| 7     | Reaction                              |
| 1984  | Report                                |
| 9735  | Zap                                   |
| 30023 | Long-form content written in MarkDown |
| 30078 | Application specific data             |

De meeste content op het Nostr netwerk bestaat uit *notes* met kind 1. Deze zijn onveranderlijk / permanent. Dat betekent dat je deze niet kunt aanpassen of verwijderen. Long-form content (kind 30023) is dat niet, deze content kun je vervangen.

Een ander voorbeeld is kind 7 die gebruikt wordt voor comments (kind 1 wordt hier ook vaak voor gebruikt) of andere soorten reacties op content (zoals een emoticon).

## Value4value op protocol niveau

Zaps zijn zo ontzettend tof! Dankzij de integratie van bitcoin lightning network in het Nostr protocol kun je zelf bepalen hoeveel sats je naar iemand kan sturen om bijvoorbeeld jouw waardering te delen op een stuk content wat de ander deelt.  

**Ik ben groot voorstand om op social media alleen nog maar met sats te betalen in plaats van met onze persoonlijke gegevens en aandacht**. 

De value4value filosofie is echt een game-changer voor het *monetizen* van content. Nostr heeft dit model overgenomen (net als Podcasting 2.0) en laat zien hoe echte digitale waarde (ondersteund door bitcoin sats) werkt. Bijna in elke client heb je de optie om zaps in te schakelen zodat je sats kunt verzenden en ontvangen voor content die je deelt.

In de toekomst verwacht ik nog veel innovatie met zaps die tot de broodnodige disrupties zullen leiden in het huidige social media landschap. 

## Ondersteund door een sterke community

Nostr wordt nu *gebootstrapped* door de bitcoin community. Er is veel ondersteuning aanwezig dankzij early bitcoiners. Zo kan er tijd vrij worden gemaakt om te blijven werken aan Nostr. Er zijn bijvoorbeeld fondsen (OpenSats) beschikbaar voor vele soorten projecten om meer mensen te betrekken in het Nostr ecosysteem.

# Het is nog steeds heel erg vroeg voor Nostr

Fiatjaf is in 2019 begonnen met de ontwikkeling van het Nostr protocol. Nu zit het protocol midden in de *discovery phase* waarin pioniers de grenzen verleggen om te ontdekken wat er wel en niet mogelijk is. Er is veel ruimte voor try-and-error, wijzigingen worden razendsnel doorgevoerd en het bouwen vind realtime plaats in het publieke domein van Nostr. 
Zoals met de meeste open protocollen ligt alles niet rotsvast. Er is nog maar een relatief kleine groep mensen actief met Nostr, dus er kunnen nog grote veranderingen worden doorgevoerd. Alle specificaties van het protocol worden vastgelegd in zogenaamde *Nostr Implementation Possibilities* afgekort naar NIPs.

De dynamiek van Nostr maakt het volgen van alle ontwikkelingen in de *nostrverse* interessant want elke week lees je nieuwe ideeën en verschijnen er nieuwe toepassingen. Aan de andere kant ervaar je ook veel frictie en bugs als je aan de slag gaat met verschillende clients. Veel applicaties (plus relays) hebben performance problemen omdat schaalbaarheid niet door het protocol wordt opgelost. Het is soms ook een uitdaging om altijd met de juiste relays te zijn verbonden voor het snel inladen van alle content. 

Toch is het uitproberen van vele verschillende clients de manier om de ware potentie van Nostr te ervaren.  

### Mijn zoektocht naar hoe ik kan bijdragen

Als Drupal & full-stack webdeveloper ben ik op zoek naar manieren hoe ik een steentje kan bijdragen aan Nostr. Op dit moment ben ik betrokken bij de volgende projecten waar ik aan werk in mijn vrije tijd:

- [nostr-php](https://github.com/swentel/nostr-php) - a PHP helper library
- Drupal module: [Nostr internet identifier NIP-05](https://www.drupal.org/project/nostr_id_nip05)
- Drupal module: [Nostr Simple Publish](https://www.drupal.org/project/nostr_simple_publish)
- Drupal module: [Nostr long-form content NIP-23](https://www.drupal.org/project/nostr_content_nip23)

Genoeg andere (technische) ideeën poppen regelmatig op en ik hoop deze mettertijd te kunnen delen op https://nostrver.se en op mijn eigen Nostr profiel. Mijn pubkey is `npub1qe3e5wrvnsgpggtkytxteaqfprz0rgxr8c3l34kk3a9t7e2l3acslezefe` of zoek naar mijn handle `sebastian@sebastix.dev`.  

## Wil jij zelf aan de slag met Nostr?

Mocht je nog geen gebruik maken van Nostr maar wil je dat wel? Mijn advies is dan om simpel te beginnen met 1 client namelijk [Primal](https://primal.net). Zij bieden als een van de weinigen een webapp én mobiele app (iOS en Android) aan.
**Onthoud goed dat je jouw *private key* (`nsec`) goed bewaard en geheim houdt net zoals je dat doet met jouw *seed*.**

#### Volgtips

- [Nostr Nederland volglijst](https://nostr.band/npub1n0strrg4pc30g4zjf8kdl69e8d6efr896vrrkqyk080amf84vfksd74jz0)

- [Matt Odell](https://nostr.band/npub1qny3tkh0acurzla8x3zy4nhrjz5zd8l9sy9jys09umwng00manysew95gx)

- [The NostReport](https://nostr.band/npub19mduaf5569jx9xz555jcx3v06mvktvtpu0zgk47n4lcpjsz43zzqhj6vzk)

- [Lyn Alden](https://nostr.band/npub1a2cww4kn9wqte4ry70vyfwqyqvpswksna27rtxd8vty6c74era8sdcw83a)

- [Edward Snowden](https://nostr.band/npub1sn0wdenkukak0d9dfczzeacvhkrgz92ak56egt7vdgzn8pv2wfqqhrjdv9)

- [Derek Ross](https://nostr.band/npub18ams6ewn5aj2n3wt2qawzglx9mr4nzksxhvrdc4gzrecw7n5tvjqctp424)

- [Fiatjaf](https://nostr.band/npub180cvv07tjdrrgpa0j7j7tmnyl2yr6yr7l8j4s3evf6u64th6gkwsyjh6w6)

- [PABLOF7z](https://nostr.band/npub1l2vyh47mk2p0qlsku7hg0vn29faehy9hy34ygaclpn66ukqp3afqutajft)

- [Gigi](https://nostr.band/npub1dergggklka99wwrs92yz8wdjs952h2ux2ha2ed598ngwu9w7a6fsh9xzpc)

#### Relay tips

- wss://relay.damus.io

- wss://nos.lol 

- wss://relay.nostr.band

- wss://purplepag.es 

- wss://nostr.wine (werkt met een subscription)

- wss://nostr.mutinywallet.com 

- wss://relay.nostr.vet

- wss://relay.nostr.nu

- wss://relay.cheeserobot.org

- wss://relay.primal.net 

- wss://nostr.orangepill.dev

Tip! Verbind met 5 a 10 relays voor een zo optimaal mogelijke ervaring.

#### Andere Nostr resources

- [Social Media is broken. Can we fix it? - Invidious](https://yewtu.be/watch?v=aA-jiiepOrE) (25min video)
* [nostr.how](https://nostr.how/)

* [A Vision for a Value-Enabled Web](https://dergigi.com/2022/12/18/a-vision-for-a-value-enabled-web/)

* [List with NIPS, event kinds, message types and tags](https://nostr-nips.com/)

* [Why Nostr Matters](https://blog.lopp.net/why-nostr-matters/)

* [Why I'm betting big on Nostr](https://hivemind.vc/nostr/) 

* [Nostr, love at first sight](https://dri.es/nostr-love-at-first-sight) 
- [Feature matrix for Nostr clients](https://nostorg.github.io/clients/) 

- https://usenostr.org/ 

- https://www.nostrapps.com/ 

- https://nostr.band 

*Dit is een Nederlandse vertaling van een eerdere blog Why Nostr resonates (23-10-2023).*
