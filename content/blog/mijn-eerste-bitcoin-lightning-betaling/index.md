---
title: "Mijn eerste Bitcoin Lightning betaling"
date: "2020-12-08T11:13:13.000Z"
description: "Een donatie aan Satoshi Radio die nog wordt voorgedragen ook, check het fragment in de video!"
categories: [bitcoin, lightning]
comments: true
---
Op 20 november deed ik mijn eerste Bitcoin betaling via het Lightning netwerk vanaf mijn eigen node. In totaal 6562 satoshis (met een waarde van 1 euro op dat moment) ofwel 0.00006562 bitcoin. Ben heel benieuwd hoeveel euro dat waard is over een paar jaar.  
Volgen er nog vele andere betalingen in de toekomst, of zal het hierbij blijven? 
Hoe dan ook, de heren van [Satoshi Radio](https://satoshiradio.nl/) verdienen het en ze waren er blij mee. Check het fragment in de video hieronder.

https://youtu.be/1qyTrCIyoyc?t=130