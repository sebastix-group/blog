---
title: "Jouw personal graph, next big thing?"
date: "2011-10-03T08:40:26.000Z"
description: ""
categories: internet, social media, personal graph
comments: true
---

  
In het vorige artikel ‘[Persoonlijke data zijn vogelvrij](../persoonlijke-data-zijn-vogelvrij)’ staat beschreven dat iedereen in principe over een *personal graph* beschikt. Vanuit bestaande graphs zoals de *link*, *social* en *interest graph* kan een *personal graph* worden opgebouwd. In dit artikel zal Sebastian Hagens nader toelichten wat een personal graph precies inhoudt, welke data het kan bevatten, welke belangen je ermee kunt behartigen en hoe je nu al kunt beginnen met het aanleggen van je eigen personal graph. Vervolgens biedt hij een visie op de plek van persoonlijke data binnen volgende generaties internet.  
  

## Relaties
In principe kunnen allerlei data tot de personal graph behoren. De enige en vanzelfsprekende voorwaarde is dat de data een relatie hebben met jou als persoon. Dit kunnen verschillende relaties zijn. Een paar voorbeelden: van een tekst op een internetpagina kun je bijvoorbeeld de auteur zijn of je kunt het als lezer hebben gevonden via Google. Tot een persoon kun je een familierelatie hebben of het is je collega, of je werkgever etc. Deze relaties tussen jou en data bepalen de context. Dit zijn slechts een paar eenvoudige voorbeelden. Onderstaande infographic (bron: Personal Data Ecosystem) laat zien dat een individuele personal graph nog veel meer gegevens digitaal kan ontsluiten.  
  
![](personalDataEcosystem-data1.jpg)  
  
## Data ‘vrij’? 
  
 Ik (Sebastian Hagens) ben een voorstander van het concept ‘[data wants to be free’](http://en.wikipedia.org/wiki/Information_wants_to_be_free) binnen systemen en grote netwerken. Wanneer data vrij zijn, levert dat een aantal voordelen op. Nagenoeg exact dezelfde gegevens zijn vindbaar op verschillende locaties en zijn zo dus min of meer locatie onafhankelijk. Ook al nemen deze data enorme proporties aan in systemen –dat is een nadeel- past locatie onafhankelijkhe  
  
 id wel bij het idee dat data vrij willen zijn. Dit botst tegelijk wel met het gegeven dat data ook een financiële waarde vertegenwoordigen.  
  
 Veel van onze data die we vandaag de dag achterlaten op internet zijn namelijk niet vrij. Ze zitten opgesloten in systemen en het kost veel moeite om ze eruit te halen als we dat zouden willen. Binnen de systemen vertegenwoordigen data veel waarde voor de beheerders, maar nauwelijks voor de eindgebruikers die de bron zijn van al die data. Ik voorspel dat dit gegeven steeds meer spanning gaat opleveren. Eindgebruikers zullen gefrustreerd raken, omdat het bijna onmogelijk wordt (zeker voor de niet-specialist) om al die gegevens te overzien en te beheren. Het feit dat we elke keer opnieuw moeten inloggen om onszelf te registreren voor internetsessies neemt kostbare tijd in beslag en levert nogal wat ergernis op. Met een personal graph zou je dit probleem kunnen oplossen. In die personal graph hoef je in principe maar eenmalig bepaalde data toe te voegen, waarna je eenvoudig selecteert wat je ermee wilt doen. Bijvoorbeeld een set foto’s delen op Facebook en Twitter. Of je adresgegevens aanpassen in drie webshops wanneer je bent verhuist.  
  
## Opbouwen van een personal graph als centrale bron  
  
 Data in de personal graph moeten eenvoudig herbruikbaar zijn (zie ook ’[The DataPortability Project](http://dataportability.org/)’). Het aantal nieuwe internet services groeit razendsnel en elke keer moet je weer dezelfde gegevens invullen voor je aan de slag kunt. Wanneer zo’n nieuwe service vraagt om bepaalde informatie kun je ervoor kiezen om deze uit jouw personal graph te laten halen zonder dat je alles opnieuw hoeft in te voeren. Dit is bijvoorbeeld wat [1Password](https://agilebits.com/products/1Password) en [LastPass](https://lastpass.com/) doen met alle persoonlijke inloggegevens op websites.  
 Om een personal graph op te bouwen is het handig om te starten bij de verschillende social media services. Deze hebben vaak een API (application programmable interface) waarmee vrij eenvoudig ruwe data kunnen worden opgehaald. Onderstaande applicaties maken hier slim gebruik van om alle persoonlijke data op te halen en te bewaren als back-up.

|                                                                                                                                                                                  |                                                                                                                                                                           |
|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ![](backupify-logo.jpg)Backupify maakt back-ups van al jouw social data in de cloud. Dit kun je doen met je Twitter, LinkedIn, Gmail, Facebook, Google contacts, Flickr en meer. | ![](socialsafe_logo_full1.jpg)Met SocialSafe haal je al jouw Facebook en Twitter gegevens op. Deze worden lokaal opgeslagen waarna er digitaal dagboek van wordt gemaakt. |
| ![](hvsbackup-logo.jpg)HVSbackup is een eenvoudig te gebruiken applicatie die al jouw Hyves gegevens download en opslaat op je computer.(HVSbackup is een applicatie van de auteur van dit artikel)| ![](greplin-logo1-400x400-150x150.jpg)Met Greplin kun je jouw digitaal leven doorzoeken. Dit doe je door verschillende profielen van bijvoorbeeld je email, Dropbox of Facebook aan Greplin te koppelen.|

[Klik hier](http://root.sebastix.nl/internet-backup-tips-voor-jouw-social-media-leven) voor meer tools die inzicht geven in de data die je hebt achtergelaten op internet en hoe je deze veilig kunt stellen.  
  
# Opslag van eigen data in de ‘cloud’  
  
 Wanneer je begint met het ophalen van data uit verschillende services, zul je moeten bepalen waar je deze gaat bewaren. Het ligt het meest voor de hand om te kiezen voor de harde schijf van je computer, DVD of externe harde schijf want:  
  
* Deze objecten zijn ook daadwerkelijk jouw eigendom
* Ze zijn alleen toegankelijk voor jezelf 
* Ze bevinden zich maar op één locatie en zijn daarom lastig te benaderen voor derden
* Je hebt volledige controle wat je met deze objecten doet
* Data zijn niet opgesloten omdat je alle rechten hebt om er iets mee te doen

 Wel moet hier gezegd worden dat apparaten voor fysieke opslag enigszins op de achtergrond raken en steeds meer data in de ‘cloud’ worden geplaatst. Dit bevat het risico dat we steeds meer controle en privacy opgeven in ruil voor die opslag. Het roept tegelijk de vraag op waarom er nog geen opslag in de ‘cloud’ bestaat die dezelfde garanties levert als wanneer je die data zou opslaan op eerder genoemde fysieke opslagapparaten. Zo’n decentrale locatie in de ‘cloud’ zou op individueel niveau alle gegevens, de personal graph, moeten huisvesten en tegelijk de eigen beheersbaarheid en privacy moeten waarborgen.  
  
## Digitale kluis  
  
Het [Personal Data Ecosystem Consortium](http://personaldataecosystem.org/) heeft bijzondere belangstelling voor deze materie. Zij maken de kwestie inzichtelijk en stellen hierbij de eindgebruiker centraal samen met zijn persoonlijke data. Maar de ontwikkeling staat vanwege complexiteit nog in de kinderschoenen. Hieronder staat een aantal initiatieven die pionieren binnen dit *data* *ecosystem*. Ze bieden ieder verschillende oplossingen, maar de eindgebruiker staat hierin altijd centraal en heeft de mogelijkheid om zijn persoonlijke gegevens te gebruiken volgens de principes die passen bij het [Personal Data Ecosystem](http://www3.weforum.org/docs/WEF_ITTC_PersonalDataNewAsset_Report_2011.pdf). Je kunt het een [*personal data store*](http://vimeo.com/14061238), een digitale kluis of een *digital vault* noemen. Alle zijn in staat om de personal graph te ontsluiten.  

![](locker_project_logo.png)
Dit project bevindt zich nog in een experimentele fase waaraan velen meewerken. Een Locker is een soort digitale kluis waarin jij als eindgebruiker volledig in controle bent over je persoonlijke data. Je kunt je eigen datacollecties beheren, kiezen welke verbindingen je opzet met services en applicaties installeren die gebruik maken van je data. Jouw Locker heeft ook een eigen API.
![](QIY_LOGO_RGB.png)
Al enkele jaren bouwt dit onafhankelijk Nederlands initiatief in Boxtel aan een robuuste, veilige oplossing om elke gebruiker een eigen Qiy te geven. Qiy is onafhankelijk 'trusted framework' waarin je data worden opgeslagen. Binnen dit eigen domein bepaal jij wat een organisatie kan doen met deze informatie. Jij bepaalt wat erin je Qiy komt en wat eruit gaat.
![](logo-azigo-45.png)
Azigo is je digitale data portemonnee. In plaats van betalen met geld, betaal je met eigen data in ruil voor gepersonaliseerde informatie, diensten of aanbiedingen.
![](personal-logo-1.png)
Personal biedt een omgeving waarin je alle digitale gegevens kunt bewaren en categoriseren. Wat je hier vervolgens allemaal mee kunt doen, is vooralsnog niet duidelijk.
![](unhosted.png)
Dit project positioneert zichzelf als een meta-dienst. Dat wil zeggen dat Unhosted een service/protocol is, net zoals e-mail. Unhosted heeft als doel om de applicatie en de data van elkaar gescheiden te houden. Applicaties kunnen met behulp van de Unhosted service praten met locaties waar de data staan van eindgebruikers.
![](owncloud-logo-300x148.png)
OwnCloud is je eigen cloud die je installeert op een server (net zoals je een Wordpress blog installeert). Dit lijkt heel erg op wat The Locker Project ook voor ogen heeft.

## Personal graph wordt heilige graal 
Of een van bovenstaande toepassingen de oplossing voor de toekomst gaat zijn, kan nog niet worden vastgesteld. Wel ben ik ervan overtuigd dat ze bestaansrecht hebben binnen een volgende fase van internet ontwikkeling (web 4.0?). Komende jaren zal iedereen steeds meer gaan beseffen dat onze persoonlijke gegevens steeds waardevoller worden. Het is dus van belang dat we hier controle en inzicht over blijven behouden. Vanuit die positie zal de eindgebruiker veel beter in staat zijn om de juiste gegevens te gebruiken die passen bij zijn wensen en behoeftes. Voor marketeers en reclame makers betekent dat ook zij zich anders moeten opstellen. Ze zullen duidelijk moeten aangeven wat ze kunnen bieden en welke gegevens ze daarvoor nodig hebben. De personal graph wordt hiermee de heilige graal.  
  
![](personal-graph-extra.jpg)  
  
 Deze ontwikkeling zal ook leiden tot een nieuwe generatie applicaties. Applicaties die vertrekken vanuit de eindgebruiker en zijn personal graph. Hieronder staat een voorbeeld van zo’n applicatie: [RootApp](http://www.rootapp.net) (dit is een applicatie van de auteur van dit artikel).  
  
![](rootapp-screenshot.png)  
  
Met deze applicatie maak je verbinding met elk profiel die je hebt op een service en haalt al jouw persoonlijke informatie op. Vanuit dit overzicht kun je eenvoudig al deze gegevens beheren.  
  
De toepassingen die passen bij het web 3.0 krijgen ![](web3.0tools.jpg) hierdoor ook meer bestaansrecht en worden minder afhankelijk van de grote internet services zoals Google, Facebook, Twitter etc.  
  
Het bestaan van het internet is in gevaar door de dominante centralisering van data en commerciële belangen van de grote internetgiganten. Als deze trend dominant blijft dan zitten we in de toekomst gevangen in de systemen van gigantische organisaties. Zij zullen zich meer en meer ontpoppen als Big Brother op het internet. Het is onrealistisch om te stellen dat zij onze wensen en behoeften automatisch gaan bevredigen, hoe prachtig hun producten ook zullen zijn. De belangen van hun businessmodel liggen gewoon ergens anders. Het is daarom aan ons om te waken over persoonlijke data en hier zoveel mogelijk de regie bij onszelf te houden. Het personal data ecosystem is een gedachte die ons helpt bij deze doelstellingen. Ondanks dat zal het nog wel even zal duren, maar een nieuw type web met gebruikers en hun personal graphs komt eraan.
