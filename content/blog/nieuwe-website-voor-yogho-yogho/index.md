---
layout: blog
title: Nieuwe website voor Yogho!Yogho!
date: 2022-01-05T15:51:16.070Z
description: Sinds november 2021 ligt Yogho!Yogho! weer in de winkelschappen.
  Enkele weken voor de lancering van het merk en de producten ontving ik de
  vraag of ik een nieuwe website kon realiseren. Over het resultaat lees je meer
  in deze blog.
categories: website, nuxtjs, yoghoyogho
comments: "true"
---
### Website: <https://www.yoghoyogho.nl>

<video controls>
    <source src="/blog/video/yy_screencast.mp4" type="video/mp4"></source>
</video>

# Design

Het uitwerken van het design voor de website heb ik uitbesteed aan Peter van [Groowup](https://www.groowup.nl). Ondanks de tijdsdruk had Peter binnen enkele dagen een fris, speels ontwerp uitgewerkt voor zowel desktop als mobiel.

![](design_preview.png)

# Absurditeit

Een van de doelstellingen bij het realiseren van de website, was dat er een vorm van absurditeit in terug zou komen. Hierbij past bijvoorbeeld dat je bij elke klik een ander stemmetje hoort die Yogho! roept. Een ander voorbeeld: als je de website bekijkt op een desktop computer, dan is je een muiscursor een opblaasflamingo. Klik je ergens met de flamingo, dan beweegt hij met zijn hoofd naar voren.

![](screen-recording-2022-01-05-16-34-03.gif)

# Tech stack

Nadat het design was goedgekeurd door Yogho!Yogho!, ben ik aan de slag gegaan met de technische uitwerking van de site. Deze site is een zogenaamde single-page application (SPA), wat betekent dat de site bestaat uit 1 statische pagina waarop alle content staat. Hiervoor heb ik gebruik gemaakt van [NuxtJS](https://nuxtjs.org/). Dit is een framework gebaseerd op [Vue.js](https://vuejs.org/), een *progressive* Javascript framework.

Welke technische onderdelen heb ik nog meer verwerkt in de site?

* [TailwindCSS](https://tailwindcss.com/) als CSS framework
* [SASS](https://sass-lang.com/) voor custom CSS styling
* [Motion One](https://www.npmjs.com/package/motion) voor animaties
* [Google Fonts](https://google-fonts.nuxtjs.org/) voor het inladen van lettertypes
* [howler.js](https://howlerjs.com) voor het afspelen van korte geluiden
* [PWA](https://pwa.nuxtjs.org) zodat je de site als progressive web app kunt installeren
* [Use motion](https://github.com/Tahul/nuxt-use-motion) voor animaties op enkele afbeeldingen zodra deze bijna in beeld komen
* [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/) voor het automatisch bijwerken van wijzigigen op de website
* [Matomo](https://sebastix.nl/blog/terug-naar-eenvoudige-statistieken-met-matomo-bye-bye-google-analytics/) voor het bijhouden van statistieken
* [Image optimizer](https://image.nuxtjs.org/) voor geoptimaliseerde afbeeldingen
* [Kinesis](https://www.aminerman.com/kinesis/#/) voor interactieve animaties

# [Klik hier om de website yoghoyogho.nl te bekijken](https://www.yoghoyogho.nl)

<video controls>
    <source src="/blog/video/yy_screencast_mobile.mp4" type="video/mp4"></source>
</video>