---
layout: blog
title: Een oude bekende over BigTech en de grote reset
date: 2021-02-02T07:29:38.986Z
description: "Afgelopen weekend kwam ik een oude bekende tegen in de podcast
  Eindbazen: Yuri van Geest. Net als ik uit hij zijn zorgen over de BigTech en
  wat zij (al dan niet bewust of onbewust) maatschappelijk met ons doen."
categories: Massa surveillaince
comments: "true"
---
Het is niet dat Yuri en ik elkaar persoonlijk kennen, maar in 2011 maakte ik voor het eerst kennis met hem als spreker. Op 13 april 2011 bij een Seats2meet bijeenkomst sprak hij over Mobile Health en dat intigreerde mij. Tijdens zijn presentatie heb ik zelfs even een notitie naar mijzelf gemaild dat ik later meer wilde weten over hem (gevonden nadat ik mijn mailbox doorzocht met zijn naam):

![](schermafbeelding-2021-02-01-om-13.30.09.png)

> Hi, I'm Yuri van Geest, coming from the interaction of technology, strategy, innovation, organization and digitization and focusing on technological, organizational and personal transformation/innovation, I help people globally to embrace exponential thinking and doing. "Reality is a permanent museum." - <https://yurivangeest.com/>

Yuri is een uitgesproken persoon met een kritische blik. Na het zien van onderstaand interview samen met [Wiggert Meerman](https://www.wiggertmeerman.com/), komt hij tot mij ook over als een spiritueel persoon die heel bewust in het leven staat. Hij heeft verschillende visies op diverse globalistische processen, maar daar wil ik het hier in dit bericht niet over hebben. Op veel vlakken ben ik het niet met hem eens. Echter zitten er ook constanteringen in het interview waar ik het wél mee eens ben. 1 daarvan wil ik hier uitlichten, omdat deze aansluit op mijn eerdere blog '[Duwt massa surveillance ons naar een totalitair systeem?](< https://sebastix.nl/blog/duwt-massa-surveillance-ons-naar-een-totalitair-systeem/>)'. Ja, ik doe even nonchalant aan *cherry picking* om deze blog extra kracht bij te zetten betreft mijn kritische houding op BigTech. Net als ik maakt Yuri zich zorgen over wat er gaande is omtrent Big Tech. **Yuri maakt ons hier onder andere attent het ongezonde systeem van lobbyisme van die partijen.** Hij noemt ook een aantal oplossingen om de macht van hen meer aan banden te leggen.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/X1cwywO8lLI?start=6617" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

> ***Technologie maakt decentralisatie en makkelijker, goedkoper en meer mogelijk***

Laten we dat onthouden!