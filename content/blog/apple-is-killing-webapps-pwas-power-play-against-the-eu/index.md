---
layout: blog
title: Apple is killing WebApps and PWAs in their power-play against the EU
date: 2024-02-16T10:17:35.672Z
description: Apple is going all-in with a fight against the EU by disabling all WebApp and PWA (progressive web app) features only in EU countries.
categories: open web, apple, webapp, pwa
comments: "true"

---

Apple made a decision:

> iOS, Safari, and the App Store are part of an integrated, end-to-end system that Apple has designed to help protect the safety, security, and privacy of our users, and provide a simple and intuitive user experience. We strive to earn users’ trust by promptly resolving issues with apps, purchases, or web browsing through App Review, AppleCare customer support, and more.
> 
> **The DMA requires changes to this system that bring greater risks to users and developers. This includes new avenues for malware, fraud and scams, illicit and harmful content, and other privacy and security threats. These changes also compromise Apple’s ability to detect, prevent, and take action against malicious apps on iOS and to support users impacted by issues with apps downloaded outside of the App Store.**

*Source: [https://developer.apple.com/support/dma-and-apps-in-the-eu#dev-qaa](https://developer.apple.com/support/dma-and-apps-in-the-eu#dev-qaa)*

💩 To me this is just a piece of bullshit 💩

Apple is not an open web advocate. They always opposed implementing open standards around [WebApps](https://www.w3.org/TR/appmanifest/) and PWA’s in their Webkit browser engine. They never made it easy to use a PWA on iOS devices. For example, they ignored the integration of push notifications for a long time, which was implemented very late in iOS 16.4 ([March 2023](https://webkit.org/blog/13878/web-push-for-web-apps-on-ios-and-ipados/)). 

Now within one year all this will be disabled in their next iOS update - version 17.4 - in EU countries!

### What is at stake is Apple’s revenue stream - their 15-30% (!) cut from any app purchase - from app developers in their walled garden called the App Store. 

From a business perspective it’s pretty obvious it’s worth a fight for them. As in most cases, just follow the money to get a better view on the incentives.

WebApps and PWAs are providing a fantastic way to build beautiful apps in the open without the need of any permission. This toolset is getting more traction and adoption among developers and Apple know this is a threat for their business model.

## **Who’s the real victim?**

I am one with many others. Others who love to build with open web technology in open systems. We are so-called open web advocates. Many of them are app developers who love to build sites and apps with the modern technology what is at hand. Developers who are preserving digital autonomy by choosing for open, independent and permissionless technologies.

With BigTech out there, the fight for an open web is a never ending fight. I think you should always fight back, otherwise the web will be less open in the future.

## Let’s unite to fight back

**Fill in this form if you’re affected in any means by this decision from Apple:**

[https://docs.google.com/forms/d/e/1FAIpQLSfNgzepH4lwmWf2kaKC4EpKPdfi69jUHFM8kf4-TBsAyWU1BA/viewform](https://docs.google.com/forms/d/e/1FAIpQLSfNgzepH4lwmWf2kaKC4EpKPdfi69jUHFM8kf4-TBsAyWU1BA/viewform)

Initiated by the Open Web Advocacy group.

> The [Open Web Advocacy](https://open-web-advocacy.org/) (OWA) is a group of software engineers from all over that world who provide regulators, legislators and policy makers the technical details to understand anti-competitive issues like Apple not allowing real third-party browsers on iOS so web apps cannot compete with native apps. 

*Source: https://modern-web-weekly.ghost.io/your-single-page-app-may-soon-be-a-polyfill*

I’m sure there is more that can be done, so please let me know your suggestions!

**Resources**

- [Meet Web Push for Safari - WWDC22 - Videos - Apple Developer](https://developer.apple.com/videos/play/wwdc2022/10098/)
- [Apple Confirms iOS 17.4 Disables Home Screen Web Apps in the European Union - MacRumors](https://www.macrumors.com/2024/02/15/ios-17-4-web-apps-removed-apple/)
- [Apple confirms iOS 17.4 removes Home Screen web apps in the EU, here’s why - 9to5Mac](https://9to5mac.com/2024/02/15/ios-17-4-web-apps-european-union/)
- [https://open-web-advocacy.org/](https://open-web-advocacy.org/)
- [iOS 17.4 Beta (21E5184k) - REGRESSION: PWA added to Home Screen are forced to open in Safari](https://bugs.webkit.org/show_bug.cgi?id=268643)