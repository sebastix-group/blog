---
layout: blog
title: Beste vijf podcasts van 3e kwartaal 2023
date: 2023-10-09T13:33:36.823Z
description: Dit zijn de beste vijf podcasts die ik in het 3e kwartaal 2023 heb beluisterd.
categories: 
comments: "true"
---

## [Nassim N. Taleb & Scott Patterson — How Traders Make Billions in The New Age of Crisis, Defending Against Silent Risks, Personal Independence, Skepticism Where It (Really) Counts, The Bishop and The Economist, and Much More](https://overcast.fm/+Kebt2a-HQ/06:25)

## [Finding Purpose, Dealing With Burnout, 85% Rule & The Music Mindset](https://overcast.fm/+HFHWsNPHQ)

## [Beste Privacy Smartphone?](https://welkom.keuzevrijbijmij.nl/podcast/podcast-11-beste-privacy-smartphone-wesley-feijth/)

## [Why You're Always Tired & Exhausted](https://yewtu.be/watch?v=15fHZpy3bx0)

## [Cal Newport On Kids And Smartphones](https://yewtu.be/watch?v=VN5lrKMeAOs)