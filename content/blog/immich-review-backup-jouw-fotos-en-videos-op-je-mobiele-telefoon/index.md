---
layout: blog
title: Immich review - backup jouw foto’s en video’s op je mobiele telefoon
date: 2022-11-23T11:08:31.328Z
description: Maak jij gebruik van iOS of Android in combinatie met iCloud of
  Google Photos om jouw foto's te back-uppen? Als je veel foto's en video's
  maakt zul je misschien herkennen dat deze backup aanzienlijk groot is en dat je
  opslagcapaciteit vrij snel vol zit. De oplossing is dan om meer opslag te
  kopen bij Apple of Google. Het kan ook anders met bijvoorbeeld Immich of Nextcloud.
categories: backup
comments: "true"

---

Immich is een selfhosted alternatief voor Google Photos en Apple iCloud Photos waarmee je automatisch jouw foto’s en video’s opslaat op jouw eigen server. Het is een backup tool voor al je waardevolle herinneringen die op je telefoon staan.

Allereerst begin ik met deze disclaimer van de Immich software: 

> ⚠️ The project is under very active development.  
> ⚠️ Expect bugs and breaking changes.  
> ⚠️ Do not use the app as the only way to store your photos and videos!

De software wordt ontwikkeld door [Alex](https://github.com/alextran1502). De reden waarom hij Immich heeft gebouwd is voor mij herkenbaar als vader van twee kleine dochters.

> We were lying in bed with our newborn, and my wife said, "We are starting to accumulate a lot of photos and videos of our baby, and I don't want to pay for App-Which-Must-Not-Be-Named anymore. You always want to build something for me, so why don't you build me an app which can do that?

Meer dan de helft van alle foto's en video's zijn van onze kinderen en staan op onze telefoons. Stuk voor stuk waardevolle herinneringen. Herinneringen die wij graag op een veilige plek wil bewaren voor de toekomst. 

## De server

Om gebruik te maken van Immich, dien je eerst de backend app te installeren op een server. De meest eenvoudige (en aanbevolen) manier is om dit met Docker te doen: https://immich.app/docs/installation/recommended-installation

Op dit moment heb ik de app geïnstalleerd op een VPS die ik gebruik voor verschillende testdoeleinden. 

*Als het je niet lukt om Immich te installeren, stuur me dan een bericht. Ik help je graag op weg.* 

Eenmaal geïnstalleerd, dan kun je Immich ook via je browser openen:

![Immich in de browser](scherm­afbeelding-2022-11-23-om-11.48.07.png)

## De mobiele app

Hier kun je de mobiele app van Immich downloaden: https://immich.app/docs/mobile-app-beta-program
De app zorgt ervoor dat nieuwe foto's en video's automatisch worden opgeslagen op jouw server. In de app kun je zelf kiezen welke mappen er automatisch gebackupt moeten worden. 
Let op! Op iOS werkt de achtergrondfunctie om automatisch te uploaden niet. Dat betekent dat het uploaden alleen werkt als je de app opent. Ik heb mezelf aangeleerd om regelmatig de Immich app te openen om alle nieuwe foto's en video's te uploaden.

![Automatisch backup](background-foreground-backup-f449b45677e4c0848eb87c729dfbf780.png)

## Oplossing voor mijn vriendin

De app heb ik ook geïnstalleerd op mijn vriendin haar OnePlus telefoon met Android. Zowel haar telefoon als haar Google account zitten vol qua opslag. Nadat Immich initieel alle foto's en video's van haar telefoon had opgeslagen, heb ik haar uitgelegd dat ze nu oud beeldmateriaal veilig kan verwijderen om ruimte vrij te maken. 

<video src="https://sebastix.nl/blog/video/immich.mp4" controls="controls" style="max-width: 400px;">
</video>

## Alternatief

Voorlopig gebruik ik Immich niet als enige backup tool voor mijn camera roll. Hiervoor is de app nog niet af genoeg waardoor er een kleine kans is dat ik alles kwijtraak (ook al maak ik overal backups van). Een andere tool die ik ernaast gebruik, is Nextcloud. In de mobiele app vind je daar de optie voor het automatisch opslaan van je mobiele foto's en video's:

![Nextcloud automatisch uploaden](img_87ccfe76b0b3-1.jpeg)

**Als je met Immich aan de slag gaat, dan is het mijn advies om deze tool voorlopig te gebruiken naast andere backups tools zoals Nextcloud**

## Tech stack

Als developer heb ik veel bewondering voor de technische uitwerking van Immich. Deze maakt namelijk gebruik van enkele nieuwe web-technologieën waar ik zelf nog geen ervaring mee heb.  

* Backend
  
  * **Next.js**
  * **PostgreSQL**
  * **Redis**
  * **Nginx**

* Frontend
  
  * **Flutter** voor de mobiele app 
  * **SvelteKit** voor de web app

![](app-architecture-a5473ac0496e25f911b2519dfd3b5c8b.png)

## Docker setup met Nginx reverse proxy

Dit is mijn docker-compose.yml op mijn server:

```dockerfile
version: "3.8"

services:
  immich-server:
    image: altran1502/immich-server:release
    entrypoint: ["/bin/sh", "./start-server.sh"]
    volumes:
      - ${UPLOAD_LOCATION}:/usr/src/app/upload
    env_file:
      - .env
    environment:
      - NODE_ENV=production
    depends_on:
      - redis
      - database
    restart: always

  immich-microservices:
    image: altran1502/immich-server:release
    entrypoint: ["/bin/sh", "./start-microservices.sh"]
    volumes:
      - ${UPLOAD_LOCATION}:/usr/src/app/upload
    env_file:
      - .env
    environment:
      - NODE_ENV=production
    depends_on:
      - redis
      - database
    restart: always

  immich-machine-learning:
    image: altran1502/immich-machine-learning:release
    entrypoint: ["/bin/sh", "./entrypoint.sh"]
    volumes:
      - ${UPLOAD_LOCATION}:/usr/src/app/upload
    env_file:
      - .env
    environment:
      - NODE_ENV=production
    depends_on:
      - database
    restart: always

  immich-web:
    image: altran1502/immich-web:release
    entrypoint: ["/bin/sh", "./entrypoint.sh"]
    env_file:
      - .env
    #environment:
      # Rename these values for svelte public interface
      #- PUBLIC_IMMICH_SERVER_URL=${IMMICH_SERVER_URL}
    restart: always

  redis:
    container_name: immich_redis
    image: redis:6.2
    restart: always

  database:
    container_name: immich_postgres
    image: postgres:14
    env_file:
      - .env
    environment:
      POSTGRES_PASSWORD: ${DB_PASSWORD}
      POSTGRES_USER: ${DB_USERNAME}
      POSTGRES_DB: ${DB_DATABASE_NAME}
      PG_DATA: /var/lib/postgresql/data
    volumes:
      - pgdata:/var/lib/postgresql/data
    restart: always

  immich-proxy:
    container_name: immich_proxy
    image: altran1502/immich-proxy:release
    #environment:
      # Make sure these values get passed through from the env file
    #  - IMMICH_SERVER_URL
    #  - IMMICH_WEB_URL
    ports:
      - 2283:8080
    logging:
      driver: none
    depends_on:
      - immich-server
    restart: always

volumes:
  pgdata:
```

Dit is mijn Nginx configuratie:

```nginx
server {
  listen 80;
  listen [::]:80;

  server_name immich.sebastix.domain;

  include snippets/letsencrypt.conf;
  # Uncomment these lines when SSL certificates are generated by CertBot
  return 301 https://$host$request_uri;
}

upstream immich {
  server 127.0.0.1:2283;
}

server {

  server_name immich.sebastix.domain;

  location / {
    proxy_redirect off;
    proxy_pass http://immich;
    proxy_set_header  Host                $host;
    proxy_set_header  X-Real-IP           $remote_addr;
    proxy_set_header  X-Forwarded-Ssl     on;
    proxy_set_header  X-Forwarded-For     $proxy_add_x_forwarded_for;
    proxy_set_header  X-Forwarded-Proto   $scheme;
    proxy_set_header  X-Frame-Options     SAMEORIGIN;
  }

  listen 443 ssl;
  listen [::]:443 ssl;
  client_max_body_size 500M;

  access_log /var/log/nginx/immich.sebastix.domain.access.log;
  error_log /var/log/nginx/immich.sebastix.domain.error.log;

  ssl_certificate /etc/letsencrypt/live/immich.sebastix.domain/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/immich.sebastix.domain/privkey.pem;
  ssl_trusted_certificate /etc/letsencrypt/live/immich.sebastix.domain/chain.pem;
  include snippets/ssl.conf;
  include snippets/letsencrypt.conf;
}
```

## Updaten

Alex, de maker van Immich, werkt dagelijks aan de app en brengt elke week wel een nieuwe update uit. Het updaten van je mobiele app werkt hetzelfde als andere apps via de Appstore of Playstore. Het updaten van de backend app op je server via Docker voer je uit middels dit commando: `docker compose stop && docker compose pull && docker compose up -d`.

Mocht je ook gebruik willen maken van Immich, maar je weet niet hoe? Neem dan contact met me op en ik help je graag op weg.

### Andere reviews

* [Review / Immich - a self-hosted Google Photos alternative](https://geek-cookbook.funkypenguin.co.nz/review/immich/) 
* [Backup your Android and iPhone photos and videos with Immich](https://medevel.com/immich/)