---
title: Beste vijf podcasts van 2e kwartaal 2023
date: 2023-07-20T20:14:13.000Z
description: Dit zijn de beste vijf podcasts die ik in het 2e kwartaal 2023 heb beluisterd.
categories: podcasts
comments: true
---

## [Wie je bent zit in je lichaam, niet in je hoofd. Met HendrikJan Houthoff](https://overcast.fm/+rRoQU4DZo)

## [Let the Platforms Burn: The Opposite of Good Fires is Wildfires](https://overcast.fm/+C7J76GxU)

## [Amazon aanklagen en de zoektocht naar vergetelheid met Julia Janssen](https://overcast.fm/+IriTX0Nf0)

## [Nita Farahany, author, “The Battle for Your Brain”](https://overcast.fm/+KXY9EQEuk)

## [Hoe we van de grote dromen van Marcuse over vrijheid iets kunnen waarmaken](https://soundcloud.com/vrijnederland/vn-voorgelezen-hoe-we-van-de-grote-dromen-van-marcuse-over-vrijheid-iets-kunnen-waarmaken?si=14aef31d98e046e3a013cabb4d3b376a&utm_source=clipboard)

Als bonus nog deze podcast:
# [Talking Value 4 Value with the Podfather Adam Curry](https://overcast.fm/+KiHptigIw)