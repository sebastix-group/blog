---
title: "Pitch 'siteBuzz'"
date: "2010-10-27T16:40:07.000Z"
description: "
Vandaag heb ik een pitch gehouden voor de meest innovatieve bedrijven uit Brabant uit de Innovatie Top 100 van Syntens..."
categories: [internet]
comments: true
---

<div><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="475" id="prezi_vxqwmoihshvp" width="625"><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="flashvars" value="prezi_id=vxqwmoihshvp&lock_to_path=0&color=ffffff&autoplay=no&autohide_ctrls=0"></param><param name="src" value="http://prezi.com/bin/preziloader.swf"></param><embed allowfullscreen="true" allowscriptaccess="always" flashvars="prezi_id=vxqwmoihshvp&lock_to_path=0&color=ffffff&autoplay=no&autohide_ctrls=0" height="475" id="prezi_vxqwmoihshvp" src="http://prezi.com/bin/preziloader.swf" type="application/x-shockwave-flash" width="625"></embed></object></div>  
 Vandaag heb ik een pitch gehouden voor de meest innovatieve bedrijven uit Brabant uit de [Innovatie Top 100 van Syntens](http://www.syntens.nl/innovatietop100/top-100-2010/top-100-2010.aspx). In de pitch heb ik de 'siteBuzz' gepresenteerd; een website notificatie buzzer. De siteBuzz is te koppelen aan een handeling van uw website bezoeker. Bijvoorbeeld wanneer die een bestelling heeft geplaatst in uw webshop, kleurt de siteBuzz bijvoorbeeld groen. Of wanneer er bezoeker aanwezig is, geeft de siteBuzz een lichtblauw signaal.  
 De siteBuzz is niet groot en daardoor eenvoudig als gadget te plaatsen op kantoor op uw bureau. Bekijk de prezi presentatie hierboven die ik heb gebruikt tijdens de pitch voor beeldmateriaal en enkele technische specificaties.  
  
 Het is nu nog wachten of ik de siteBuzz ook daadwerkelijk mag gaan leveren, de bedrijven die vandaag uitgenodigd waren moeten namelijk een keuze maken uit een aantal andere ideeën die ze als cadeau krijgen vanuit de Provincie Noord-Brabant. Spannend!
