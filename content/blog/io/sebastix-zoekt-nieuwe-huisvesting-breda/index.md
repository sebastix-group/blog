---
title: "Sebastix zoekt nieuwe huisvesting"
date: "2012-10-22T11:59:02.000Z"
description: "Sebastix is op zoek naar een nieuwe bezoek- en werklocatie. Op dit moment zit hij in een gedeelde ruimte van Starterslif..."
categories: [internet]
comments: true
---

Sebastix is op zoek naar een nieuwe bezoek- en werklocatie. Op dit moment zit hij in een gedeelde ruimte van Starterslift in het Blushuis op het [Triple O Campus](http://www.redconcepts.nl/projecten/triple_o) in Breda. In februari 2013 vertrekt [Starterslift](http://www.starterslift.nl) uit het [Blushuis](http://www.blushuis.nl). Er is genoeg aanbod van diverse locaties, toch is het lastig om een keuze te maken.  
  
 Wat zoekt Sebastix bij de locatie?

  
- Voorkeur stad is Breda
  
- Afsluitbare ruimte / unit voor een vaste werkplek (spullen moeten veilig staan)
  
- Parkeervoorziening
  
- Minimaal ~20m2 oppervlakte
  
- 250-350 euro per maand inclusief
  

  
 Sebastix staat open voor diverse oplossingen. Als eenmansbedrijf kan hij zich flexibel opstellen en is juist daarom op zoek naar een flexibele oplossing.  
  
 Heb je tips voor huisvesting? Hieronder kun je reageren of neemt direct contact op met Sebastix via info@sebastix.nl of 06 41712778.
