---
title: "Panopticon - jouw online privacy onder de loep"
date: "2012-10-27T13:45:18.000Z"
description: "Neem even de tijd en bekijk deze documentaire die gaat over privacy in het systeem waarin jij leeft. Het systeem ver..."
categories: [internet]
comments: true
---

<iframe frameborder="0" height="362" src="http://player.vimeo.com/video/52165457?badge=0" width="640"></iframe>

Neem even de tijd en bekijk deze documentaire die gaat over privacy in het systeem waarin jij leeft. Het systeem verandert door digitale technologie en neemt verschillende belangen van organisaties kritisch onder de loep. Luister en wees je bewust van deze technologie om je heen en wat deze doet. Wat je nu achterlaat aan digitale sporen kan nu misschien geen kwaad, maar niemand kan voorspellen wat de toekomst zal brengen. **Wees je bewust van de keuzes die je maakt als je bepaalde technologie toelaat in je leven.**
