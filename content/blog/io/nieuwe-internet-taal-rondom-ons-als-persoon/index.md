---
title: "Nieuwe internet taal rondom ons als persoon"
date: "2011-07-14T13:14:51.000Z"
description: "

Gary Thompson is bezig met een nieuwe internet taal (net zoals HTML een taal is om websites mee te realiseren) die o..."
categories: [internet]
comments: true
---

<object height="375" width="625"><param name="movie" value="http://www.youtube.com/v/afMjZgvtsp8?version=3&hl=nl_NL"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed allowfullscreen="true" allowscriptaccess="always" height="375" src="http://www.youtube.com/v/afMjZgvtsp8?version=3&hl=nl_NL" type="application/x-shockwave-flash" width="625"></embed></object>  
  
 Gary Thompson is bezig met een nieuwe internet taal (net zoals HTML een taal is om websites mee te realiseren) die om ons als personen draait. Dat doet hij vanuit de organisatie [CloudInc](http://www.cloudinc.org). Bekijk bovenstaande video om een beter idee van dit ideologisch verhaal te krijgen.  
  
 Het is een soortgelijk gedachtegoed die ook in Root:\\ terug komt. Het sluit ook erg goed aan bij [The web is people](http://www.sebastix.nl/blog/the-web-is-people) van Jay Rosen. Ik kan me heel goed een wereld voorstellen waar het op internet volledig om mijzelf draait, waarin ik zelf de controle heb over mijn persoonlijke data en hoe zich deze verplaatst tussen verschillende locaties/personen. Echter is het nog een lange weg te gaan voordat het zover is. Hoe denk jij hierover?
