---
title: "Carage - social network als applicatie voor autoliefhebbers"
date: "2012-10-24T10:21:08.000Z"
description: "Al jaren heeft Sebastix een concept voor een social network voor autoliefhebbers op de plank liggen. Sinds mijn 18e bezo..."
categories: [internet]
comments: true
---

Al jaren heeft Sebastix een concept voor een social network voor autoliefhebbers op de plank liggen. Sinds mijn 18e bezoek ik vele fora op internet die met auto's te maken hebben, met name Honda. Deze fora hebben een hoge aantrekkingskracht, vooral als je zelf ook iets gaat bijdragen. Op auto fora ligt het voor de hand dat je naast iets over jezelf, met name iets over je eigen auto deelt. Auto's kunnen op vele manieren een passie vormen bij mensen en wat is er mooier dan deze passie met elkaar te kunnen delen? Het internet biedt hier verschillende mogelijkheden voor.  
 Al snel had ook ik hier een eigen idee voor in mijn hoofd: jouw auto een profiel geven binnen een sociale omgeving. In 2007 heb ik dan ook zelf een extensie voor phpBB2 uitgewerkt waar leden op het forum van Civic Club Holland een autoprofiel creëerden. Helaas is deze extensie min of meer verloren gegaan na de komst van een nieuwer phpBB3 forum.  
  
 Toch ben ik blijven zoeken naar een andere manier om mijn idee uit te werken. Daar wil ik natuurlijk zoveel mogelijk autoliefhebbers mee bereiken. In deze tijd ligt het voor de hand dat ik daar Facebook voor moet gebruiken. Hieruit is een applicatie voor Facebook ontstaan: Carage.

<table>  
<tbody>  
<tr>  
<td>[![](../../../images/2012/10/13-277x300.png "13")](../../../images/2012/10/13.png)</td>  
<td>[![](../../../images/2012/10/10-300x247.png "10")](../../../images/2012/10/10.png)</td>  
</tr>  
<tr>  
<td> [![](../../../images/2012/10/09.png "09")](../../../images/2012/10/09.png)</td>  
<td> [![](../../../images/2012/10/131.png "13")](../../../images/2012/10/131.png)</td>  
</tr>  
</tbody>  
</table>

  
  
  
 De huidige Carage applicatie is slechts een begin van een groter geheel. We missen bijvoorbeeld nog de mogelijkheid om de gebruikers op de hoogte te brengen van reacties die ze ontvangen op hun foto's en modificaties. Facebook weet dit ook en biedt sinds kort deze functie ook (weer) aan via de [notifications API](https://developers.facebook.com/blog/post/2012/08/31/reach-users-1-1-with-the-notifications-api/). Bovendien biedt het de mogelijkheid om gebruik te maken van vooraf gedefinieerde actions en objects uit de Open Graph. Dankzij deze integratie krijgt Carage een eigen timeline waarin diverse handelingen van de gebruiker in de applicatie zichtbaar zijn. Bijvoorbeeld welke foto's hij heeft toegevoegd van zijn auto.  
[![](../../../images/2012/10/timeline-facebook-1024x927.png "Carage timeline op Facebook")](../../../images/2012/10/timeline-facebook.png)

  
 Als je iets toevoegt aan je autoprofiel op Carage, wordt dit zichtbaar op je Carage timeline en gedeeld op Facebook met je vrienden. Ook kun je deze content zowel op Facebook als op Carage ‘liken’.  
<table>  
<tbody>  
<tr>  
<td>[![](../../../images/2012/10/Google-Chrome-3.png "Google Chrome 3")](../../../images/2012/10/Google-Chrome-3.png)</td>  
<td>[![](../../../images/2012/10/Google-Chrome-5.png "Google Chrome 5")](../../../images/2012/10/Google-Chrome-5.png)</td>  
</tr>  
<tr>  
<td> [![](../../../images/2012/10/Google-Chrome-2.png "Google Chrome 2")](../../../images/2012/10/Google-Chrome-2.png)</td>  
<td>[![](../../../images/2012/10/Google-Chrome.png "Google Chrome")](../../../images/2012/10/Google-Chrome.png)</td>  
</tr>  
</tbody>  
</table>

  
### Mobile is next

  
 Met de nieuwe kennis die ik sinds kort in huis heb over het ontwerpen en ontwikkelen van mobiele applicaties, zal Carage ook als mobiele applicatie gaan verschijnen. Grote kans dat dit nog succesvoller zal worden dan de huidige desktop versie, want zo'n 75% van de doelgroep 'Facebookt' mobiel. Voor Carage is het dan ook vanzelfsprekend om deze applicatie via mobiel beschikbaar te maken. Omdat autoliefhebbers vaak onderweg zijn, zullen zij juist met een mobiele applicatie in staat zijn om realtime hun profiel te vullen met foto’s en modificaties én deze direct te delen met hun vrienden.  
  
 Maak kennis met de desktop versie van Carage: <https://apps.facebook.com/carage-beta>  
  
 Volg Carage op Facebook: <http://www.facebook.com/carage.me>  
  
 Volg Carage op Twitter: <http://www.twitter.com/carageapp>
