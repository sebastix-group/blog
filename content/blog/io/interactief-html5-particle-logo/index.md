---
title: "Interactief particle logo"
date: "2011-09-27T09:24:35.000Z"
description: "Samen met Bela Zsigmond is Sebastix begin dit jaar een experimentele uitdaging aangegaan: het ontwerpen en realiseren v..."
categories: [internet]
comments: true
---

Samen met [Bela Zsigmond](http://www.belazsigmond.com/) is Sebastix begin dit jaar een experimentele uitdaging aangegaan: het ontwerpen en realiseren van een interactief logo / website met een particle system voor [Architectune](http://www.architectune.net/). Doel van dit experiment was om een aantal proof-of-concepts uit te werken in een webomgeving. Het concept waaruit we zijn vertrokken bestaat uit een interventie tussen een grafisch gedeelte (particles), audio én de interactie tussen de kijker/bezoeker en logo/website.  
  
  
  
**Vertrekpunt**  
 In zo'n uitdaging ga je op zoek naar bestaande uitwerkingen met interactieve particle systems. Zelf heeft Sebastix al ervaring in [Processing.org](http://www.processing.org) met de realisatie van een interactieve installatie [ATTACK](http://interactieontwerpen.sebastix.nl/attack-interactive-installation/). Andere bronnen die we hebben geraadpleegd:  
[MTV ID 2009 logo](http://dmtr.org/mtv_rewind/)  
[Google interactive bubble logo](http://www.wait-till-i.com/2010/09/07/google-goes-bubbly-interactive-logo-today-on-the-uk-homepage-plus-source/)  
[Particle system in HTML5 canvas](http://www.mrspeaker.net/dev/parcycle/)  
[Javascript Particle demo](http://nettutsplus.s3.amazonaws.com/63_particleSystem/demos/demo2/index.html)  
[Processing.js particle system](http://aim.johnkeston.com/im2490/p5jsDemo/shiffman_particles.html)  
[Interactive particles HTML5 canvas](http://hakim.se/experiments/html5/particles/01/)  
[Flash particles systems](http://flintparticles.org/examples)  
  
**Experiment**  
[![](../../../images/2011/09/particleSystem3.A-212x300.jpg "particleSystem3.A")](../../../images/2011/09/particleSystem3.A.jpg)[![](../../../images/2011/09/particleSystem2.A-212x300.jpg "particleSystem2.A")](../../../images/2011/09/particleSystem2.A.jpg)  
<iframe frameborder="0" height="540" src="http://player.vimeo.com/video/29652602?title=0&byline=0&portrait=0" width="300"></iframe> <iframe frameborder="0" height="540" src="http://player.vimeo.com/video/29652716?title=0&byline=0&portrait=0" width="300"></iframe>  
<iframe frameborder="0" height="540" src="http://player.vimeo.com/video/29653023?title=0&byline=0&portrait=0" width="300"></iframe> <iframe frameborder="0" height="540" src="http://player.vimeo.com/video/29653324?title=0&byline=0&portrait=0" width="300"></iframe>  
  
 Het bovenstaande in actie op een webpagina ([Processing.js](http://www.processingjs.org) framework)  
<iframe frameborder="0" height="400" src="http://sebastix.nl/html5/particles-logo/archicoustics7.html" style="background: #FFFFFF;" width="740"></iframe>  
[ Klik hier voor nog een demo](http://sebastix.nl/html5/particles-logo/archicoustics4.html), [nog een](http://sebastix.nl/html5/particles-logo/archicoustics5.html) en [nog een](http://sebastix.nl/html5/particles-logo/archicoustics6.html) (heavy!)  
[ Klik hier voor een HTML5 canvas ripple demo](http://sebastix.nl/html5/ripple2.html)  
  
**Realisatie** Vanaf het begin heeft Sebastix bewust gekozen voor de nieuwste webtechnologieën om het geheel in te ontwikkelen. Technologie die klaar is voor de toekomst en werkt op verschillende apparaten (mobiel, tablets, applicaties). Hieronder staat een demo (in ontwikkeling) waarin je met je muis over het grid gaat en de punten groeien. Ook zijn er drie knoppen (strategy, theory, history) die een moment een geluid activeren. Deze knoppen zijn niet klikbaar, maar dit is wel de bedoeling om vervolgens een nieuwe pagina te openen.  
  
<iframe frameborder="3" height="580" src="http://sebastix.nl/html5/demo3.html" style="background: #FFFFFF;" width="740"></iframe>  
 In de doorontwikkeling zou het de bedoeling zijn dat de knoppen niet zichtbaar zijn (maar wel hoorbaar, afhankelijk van hoever je met je muis verwijderd bent van een knop), het grid een herkenbare A-vorm krijgt, het grid niet zichtbaar is wanneer je niets doet met je muis, het grid bijna volledig zichtbaar is als je tussen alle knoppen in staat, de punten lichtjes at-random van positie veranderen en nog aantal andere kleine details. Tevens is het de bedoeling om het geheel volledig in het HTML5 canvas element te realiseren (in plaats van met losse HTML elementen zoals het nu werkt) om performance problemen te voorkomen op de wat oudere computers.  
  
 Een nieuwe type technisch web staat al een tijdje voor de deur. Bovenstaande experimenten zijn slechts een kleine greep uit wat er mogelijk is vanuit die techniek. Hebben jullie nog inspirerende voorbeelden uit de praktijk of misschien juist vragen over hoe andere toepassingen werken?
