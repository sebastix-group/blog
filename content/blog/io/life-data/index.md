---
title: "LIFE = DATA"
date: "2011-03-10T13:02:53.000Z"
description: "Openingsfilm van de Big Brother Awards die gisterenavond hebben plaatsgevonden. Film is een creatie van Suzero en la..."
categories: [internet]
comments: true
---

<iframe allowfullscreen="" frameborder="0" height="380" src="http://www.youtube.com/embed/F4T4ZuOpgds" title="YouTube video player" width="625"></iframe>  

Openingsfilm van de [Big Brother Awards](https://www.bigbrotherawards.nl/) die gisterenavond hebben plaatsgevonden. Film is een creatie van [Suzero](http://www.suzero.com/suzero/) en laat zien dat we continue een stroom aan data achterlaten in ons dagelijks leven. Het uitgewerkt scenario gaat over een directe relatie tussen twee mensen die op verschillende manieren ook indirect met heel veel anderen in contact staan. Beseffen we dat vandaag de dag eigenlijk wel?
