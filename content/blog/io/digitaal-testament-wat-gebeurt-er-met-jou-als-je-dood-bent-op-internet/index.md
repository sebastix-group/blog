---
title: "Digitaal testament, wat gebeurt er met jou als je dood bent op internet?"
date: "2011-10-21T10:13:25.000Z"
description: "Op 18 oktober verscheen er een persbericht 'Nederlanders willen geen eeuwig leven op internet' gepubliceerd door Nuvema..."
categories: [internet]
comments: true
---

Op 18 oktober verscheen er een [persbericht 'Nederlanders willen geen eeuwig leven op internet'](http://www.perssupport.nl/apssite/permalink/59276) gepubliceerd door [Nuvema uitvaartverzekering](http://www.nuvema.nl/). Het door hen uitgewerkte [social media testament](http://www.nuvema.nl/social-media-testament) had nieuwswaarde en verscheen op verschillende media (zoals [hier](http://www.nu.nl/internet/2645267/nederlanders-willen-sociale-media-verwijderd-hebben-dood.html), [hier](http://www.marketingfacts.nl/berichten/20111018_wat_moet_er_gebeuren_met_jouw_sociale_netwerken_na_je_dood/), [hier](http://www.hyped.nl/details/20111018_nederlanders_willen_geen_eeuwig_leven_op_social_media) en [meer](http://twitter.com/#!/search/realtime/social%20media%20testament)). Wat leidend was in het persbericht waren de onderzoeksresultaten onder 1000 mensen met de volgende belangrijke resultaten:

  
- 68,5% heeft nog nooit nagedacht wat er moet gebeuren met zijn/haar online profielen als hij/zij is overleden
  
- 61% wilt dat na overlijden zijn/haar online profielen wordt verwijder
  

  
 Nuvema speelt hierop in met een aantal oplossingen:  
<div>  
  
1. mensen aansporen om een [social media testament](http://www.nuvema.nl/docs/socialmediatestament.pdf) in te vullen
  
2. [handleidingen](http://www.nuvema.nl/handleiding-social-media-overledene-verwijderen-wijzigen) voor nabestaanden die uitleggen hoe je social media profielen van een overleden persoon kunt verwijderen of wijzigen
  

  
 Oplossing 1 van Novuma is veel te summier en slecht uitgewerkt. Diensten zoals [Digizeker](http://digizeker.nl/) of[ Ziggur](http://www.ziggur.me/nl/home.aspx) die hetzelfde doel voor ogen hebben, hebben dit vele malen beter uitgewerkt.  
 Bij oplossing 2 lees je hoe je als nabestaande een online profiel kunt verwijderen of wijzigen van een overleden persoon (andere handleidingen zijn ook [hier](https://www.abine.com/deleteme/), [hier](http://www.accountkiller.com/nl/) en [hier](http://www.howtodelete.me/) te vinden). *ik schreef er [hier](http://www.sebastix.nl/blog/deleteme-social-media-profielen-verwijderen/) ook al eerder over op mijn blog* Er wordt iets belangrijks vergeten (overgeslagen?) in het (rouw)proces waarin de nabestaande zit. Het is namelijk goed voor te stellen dat je als nabestaande niet direct alles wilt verwijderen van je dierbare die je verloren bent. De kans is heel groot dat de nabestaanden eerst alle gegevens (ontvangen berichten, foto's, video's, geschreven teksten, etc) van die persoon veilig willen bewaren op een andere plek dan op de social media. Pas als deze gegevens zijn veilig gesteld, kun je met een gerust gevoel zijn of haar online profiel verwijderen of wijzigen.  
 Voor het bewaren van al deze gegevens heeft Sebastix een oplossing. Voor Hyves is dat namelijk [HVSbackup](http://www.hvsbackup.nl), een applicatie waarmee je met de inlog gegevens volledig het profiel kunt downloaden naar je computer. Voor andere social media is Sebastix met soortgelijke oplossingen bezig. Het uiteindelijke doel is om een oplossing te creëren, waarmee je al jouw digitale sporen op internet kunt veilig stellen.  
  
</div>
