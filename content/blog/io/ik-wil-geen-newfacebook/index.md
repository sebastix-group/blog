---
title: "Ik wil geen #newFacebook"
date: "2011-09-22T10:08:23.000Z"
description: "

Nog voordat Facebook haar grote F8 conferentie houdt vanavond, krijgt ze nu al een flinke lading kritiek over zich h..."
categories: [internet]
comments: true
---

![](../../../images/2011/09/Google-Chrome-300x249.png "Google Chrome")  
  
 Nog voordat Facebook haar grote F8 conferentie houdt vanavond, krijgt ze nu al een flinke lading kritiek over zich heen als reactie op allerlei nieuwe toeters en bellen in de interface. Ook ik werd gisteren getroffen door een aangepaste interface van Facebook met bijzonder complexe functies en opties. Ik ga ze niet allemaal 1 voor 1 benoemen (dat laat ik aan de andere experts over die er hun blogs mee kunnen vullen komende dagen)...  
  
*Disclaimer bij deze afbeelding; indien iemand problemen heeft met de content die hier openbaar staat, stuur even een berichtje naar info@sebastix.nl ;-)*  
  
  
  
 Nu kan ik een heel pleidooi gaan houden waarom dit geen juiste werkwijze is van Facebook om op deze manier allerlei nieuwe functies en mogelijkheden te integreren. Facebook lijkt zich niet bewust van haar eigen krachtige principes. In mijn eerste indruk van wat ik nu zie, verdwijnen deze principes steeds meer naar de achtergrond in datgene wat iedereen nu ziet op haar scherm (interface). Ik heb een poging gedaan om lijsten aan te maken, daar vrienden in te zetten, de opties van de lijsten te doorgronden... Ik begrijp het concept achter lijsten met vrienden (Google Circles) maar de uitvoering is zodanig complex dat dit geen kans van slagen heeft. Facebook, blijf bij de basis!? Mensen gebruiken Facebook om op de hoogte te blijven wat hun vrienden doen en wat ze willen delen met deze vrienden. Dat je de controle moet hebben wat je met wie deelt is een logisch concept, maar dit kost op dit moment teveel moeite en energie. Plus dat ik het overzicht ook volledig kwijt ben over wat iedereen met mij deelt, Facebook neemt hier de controle over en beslist wat belangrijk voor me is (waar is de knop gebleven die me gewoon chronologisch laat zien wat iedereen deelt!?). Ja...ik ben de weg kwijt op Facebook... De weg naar de basis mogelijkheden wordt steeds complexer.  
  
[![](../../../images/2011/09/upAh.Screen-shot-2011-09-21-at-21-18-13.png "upAh.Screen shot 2011-09-21 at 21-18-13")](../../../images/2011/09/upAh.Screen-shot-2011-09-21-at-21-18-13.png)  
  
 Als spreek voor mezelf: ik wil geen [\#newFacebook](http://twitter.com/#!/search/%23newfacebook)! Misschien is straks Hyves gewoon [weer winning](http://www.emerce.nl/nieuws/nieuwe-strategie-hyves-vandaag-start)! Ik wil een knop bovenaan dat ik terug ga naar de oude versie (net zoals Google dat als optie biedt wanneer zij een nieuwe interface bieden voor een product). En ik ben [niet de enige](http://infegy.com/buzzstudy/facebook-redesign/) (ook [hier](http://www.readwriteweb.com/archives/facebook_newspaper.php), en [hier](http://mashable.com/2011/09/21/prepare-for-the-new-facebook/)), of denk jij er toch anders over?
