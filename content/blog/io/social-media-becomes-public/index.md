---
title: "Social media becomes public"
date: "2011-09-26T22:12:12.000Z"
description: "Hoeveel invloed gaan social media nog meer krijgen? En hoeveel invloed krijgen de gebruikers van deze social media? ..."
categories: [internet]
comments: true
---
<iframe frameborder="0" height="360" src="http://player.vimeo.com/video/29577782" width="625"></iframe>

Hoeveel invloed gaan social media nog meer krijgen? En hoeveel invloed krijgen de gebruikers van deze social media? Zijn we klaar voor een toekomst waarin steeds meer belangen een stem krijgen? Zo ja, wie gaat deze dan vormgeven? De social media of wij -de gebruikers- ?
