---
title: "The Locker Project - een introductie"
date: "2011-12-11T13:45:53.000Z"
description: "

This introduction of The Locker Project is written in Dutch, please use Google Translate to translate this page to g..."
categories: [internet]
comments: true
---

<iframe frameborder="0" height="360" src="http://www.youtube.com/embed/pTNO5npNq28" width="640"></iframe>  
  
*This introduction of The Locker Project is written in Dutch, please use [Google Translate to translate this page](http://translate.google.com/translate?hl=nl&sl=auto&tl=en&u=http%3A%2F%2Fwww.sebastix.nl%2Fblog%2Fthe-locker-project-een-introductie%2F) to get a really rubbish translation &gt;\_&lt; Please feel free to ask my anything on [Twitter](http://www.twitter.com/sebastix) of email (info@sebastix.nl).*  
  
 Al enige tijd volg ik actief [The Locker Project ](http://lockerproject.org/)welke door [Singly](http://singly.com/) wordt gesponsord. Voordat je verder leest, bekijk eerst de bovenstaande video!![](https://singly.com/images/singly-logo_300.png "Singly")  
> A Locker is a container for personal data, which gives the owner the ability to control how it's protected and shared. It retrieves and consolidates data from multiple sources, to create a single collection of the things you see and do online: the photos you take, the places you visit, the links you share, contact details for the people you communicate with, and much more. It also provides flexible APIs for developers to build rich applications with access to all of this information.

  
 Zelf heb ik ook een eigen Locker lokaal draaien op mijn Macbook. Mijn Locker haalt op dit moment verschillende social data op uit Google, Facebook, Twitter, Foursquare en Github. Op dit moment kan ik nog vrij weinig met mijn eigen Locker. Het is in ieder geval al ontzettend handig om al jouw versplinterde data op internet op deze manier te aggregeren. Mijn Locker bevat momenteel 787 foto's, 2900 contacten, 3262 links, 382 locaties. Deze data wordt lokaal opgeslagen in een database (MongoDB).  
  
 Het is een kwestie van tijd tot er straks meerdere connectors bijkomen (hiermee maak je verbinding met services zoals Google) en applicaties die iets doen met jouw persoonlijke data in je Locker. [Enkele applicaties](http://blog.singly.com/2011/12/05/making-things-together/) zijn er al ontwikkeld (welke nog in kinderschoenen staan) tijdens enkele developer hack sessions.  
<div style="text-align: center; display: block; width: 720px; height: 165px; clear: both;">[![](../../../images/2011/12/screenshot-locker1-150x150.jpg "screenshot-locker1")](../../../images/2011/12/screenshot-locker1.jpg)[![](../../../images/2011/12/screenshot-locker2-150x150.jpg "screenshot-locker2")](../../../images/2011/12/screenshot-locker2.jpg)[![](../../../images/2011/12/screenshot-locker3-150x150.jpg "screenshot-locker3")](../../../images/2011/12/screenshot-locker3.jpg)</div>  
[![](../../../images/2011/09/rootapp-screenshot-300x135.png "rootapp-screenshot")](../../../images/2011/09/rootapp-screenshot.png)Een Locker biedt ontzettend mogelijkheden, het is jouw verzameling (een eigen platform met jouw gegevens) van verschillende data die je op verschillende manieren kunt hergebruiken. Een Locker biedt namelijk al een API waarmee '/me' data kan worden opgehaald, net zoals jij vraagt aan Google om data op te halen. Voor mezelf zou het een mooie uitdaging zijn om een Hyves connector te bouwen waarmee ook je Hyves data kan worden opgehaald (wat [HVSbackup](http://www.hvsbackup.nl) ook doet). Tevens zou ook [Rootapp](http://www.rootapp.net) een mooie applicatie zijn voor een Locker. Als ik de tijd kan vinden, ga ik hier snel aan de slag. Het mooie van The Locker Project is dat alles (een connector of een app) volledig gerealiseerd kan worden met HTML, CSS en Javascript (ook server-side Javascript dankzij NodeJS). The Locker Project heeft in ieder geval nog een lange weg te gaan! Binnen het '[Personal Data Ecosystem](http://personaldataecosystem.org/)' zijn nog vele nieuwe wegen te ontdekken en bewandelen. Jouw '[Personal Graph](http://lifehacking.nl/algemeen/jouw-personal-graph/)' is de heilige graal van de toekomst. To be continued...  
 [![](../../../images/2011/09/personal-graph-extra-1024x801.jpg "personal-graph-extra")](../../../images/2011/09/personal-graph-extra.jpg)
