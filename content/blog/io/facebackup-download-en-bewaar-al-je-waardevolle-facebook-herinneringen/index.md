---
title: "Facebackup, download en bewaar al je waardevolle Facebook herinneringen"
date: "2012-10-28T09:11:06.000Z"
description: "In navolging van HVSbackup is het logisch dat er ook een applicatie voor Facebook volgt: Facebackup.



Met Faceback..."
categories: [internet]
comments: true
---

In navolging van [HVSbackup](http://www.hvsbackup.nl "HVSbackup") is het logisch dat er ook een applicatie voor Facebook volgt: Facebackup.  
  
[![](http://facebackup.net/img/logo-facebackup.1.png "Facebackup")](http://www.facebackup.net)  
  
 Met Facebackup is het mogelijk om al je vrienden, foto's (inclusief foto's waarin je getagd bent), updates, likes, privé berichten te downloaden naar je eigen computer. Nu Facebook zegt ze meer dan 1 miljard actieve gebruikers heeft, zullen er ongetwijfeld velen zijn die ook al hun digitale eigendommen weer terug willen die ze ooit op Facebook hebben geplaatst. Waarom? Ze stoppen met Facebook, maar ze willen niet alles kwijtraken wat erop staat.  
  
 Stoppen mensen dan met Facebook?  
 Ja.  
 Waarom?  
 Lastig om concreet te benoemen, maar het komt erop neer dat mensen worden Facebook moe worden. Hoe vaker ze Facebook checken, hoe groter ook de kans wordt dat ze uiteindelijk niets lezen wat ze ook interesseert. Er ontstaat een zekere verveling van wat je van je digitale vrienden leest. Dit wordt mede veroorzaakt door Facebook zelf die met hun ‘edge rank’ bepalen welk nieuws van je vrienden voor jou interessant is. De groep mensen die met Facebook stopt, groeit. De groep mensen die Facebook steeds ‘vervelender’ beginnen te worden is nog groter. Tussen het moment dat je jezelf Facebook-moe begint te voelen en het moment dat je ook echt stopt met Facebook zit veel tijd. Naarmate er straks meer mensen besluiten te stoppen met Facebook, zal deze tijd korter gaan worden. Facebackup kan je straks helpen met het stoppen van Facebook door in ieder geval al jouw Facebook data veilig te stellen. Je krijgt de zekerheid niets te hoeven verliezen wat je ooit op op Facebook hebt geplaatst.  
  
 Waar zal Facebackup een kopie van maken naar je eigen computer?

  
- Jouw foto’s
  
- Foto’s waarin je getagd bent
  
- Jouw vrienden
  
- Jouw updates
  
- Ontvangen likes op je foto’s en berichten
  
- Reacties van anderen op je foto’s en berichten
  
- Jouw privé berichten
  
- Jouw Facebook groepen
  
- Jouw Facebook pagina’s
  

  
 Het is mogelijk om nog meer data op te halen. Ik ben daarom ook benieuwd welke data voor jou het meest waard is die op Facebook staat en je absoluut wilt bewaren voor de toekomst.
