---
title: "Envoy geeft je het eeuwige leven"
date: "2011-11-10T11:14:12.000Z"
description: "

Gisteren tijdens de IDentity.Next conferentie toonde Gerrit Bloem van Ziggur  in zijn presentatie over de digitale ..."
categories: [internet]
comments: true
---

<iframe frameborder="0" height="424" src="http://player.vimeo.com/video/24658276?title=0&byline=0&portrait=0&autoplay=0" width="640"></iframe>  
  
 Gisteren tijdens de [IDentity.Next ](http://www.identitynext.eu)conferentie toonde Gerrit Bloem van [Ziggur](http://www.ziggur.me) in zijn presentatie over de digitale dood bovenstaande video. [Envoy](http://maxbatt.com/envoy/) is een applicatie die jou het eeuwige leven geeft via jouw digitale identiteiten. Je naasten kunnen altijd met je blijven communiceren via diverse kanalen zoals Facebook. In een digitale wereld hoef je nooit dood te gaan!  
 In theorie en ook vanuit technisch oogpunt is een zo'n applicatie te realiseren. Zou je dit willen als nabestaande? Zou je het willen als jij er zelf niet meer bent in de fysieke wereld? Denk je überhaupt wel eens na wat er zou moeten gebeuren met jouw leven die je deelt op het internet met vrienden, collega's, familie en kennissen?  
  
 Morgen is het Digital Death Day in Europa, mocht je hierin geïnteresseerd zijn zou je morgen zeker naar het Tropenmuseum in Amsterdam moeten komen. Meer informatie: <http://digitaldeathday.com/>
