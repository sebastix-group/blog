---
title: "Inzage verzoek Rabobank (deel I)"
date: "2011-12-29T17:25:22.000Z"
description: "Vorig jaar heb ik enkele rekeningafschriften opgevraagd van één van mijn rekeningen bij de Rabobank. Omdat een aantal ..."
categories: [internet]
comments: true
---

Vorig jaar heb ik enkele rekeningafschriften opgevraagd van één van mijn rekeningen bij de Rabobank. Omdat een aantal afschriften ouder waren 15 maanden en deze normaal alleen digitaal te downloaden zijn, waren ze niet meer beschikbaar via de digitale weg en werd er €5,- per afschrift in rekening gebracht waarna ik deze per post op papier heb ontvangen. 5 euro voor 1 A4...ik moest toen even een flink betalen. Dit kostenbeleid is echter in strijd met de wet.  
  
  
  
 Aangezien ik graag al mijn rekeningafschriften wil ontvangen voor oa mijn zakelijke boekhouding heb ik op 12 december 2011 een inzage verzoek ingediend met de hulp van [Privacy Inzage Machine](https://pim.bof.nl/) van [Bits of Freedom](https://www.bof.nl/) en Koen Vermissen (die [hier](http://koenversmissen.tumblr.com/post/12320895050/inzage-bij-asn-bank-zo-kan-het-dus-ook) uitlegt dat hij van zijn ASN bank wél alle afschriften heeft gekregen). Dit verzoek zag er als volgt uit:

> *Rabobank*  
> *Postbus 17100*  
> *3500 HG Utrecht*  
> *Nederland*  
>   
> *Hoeven, 12 december 2011*  
>   
> ***Betreft: Inzageverzoek op grond van de Wet bescherming persoonsgegevens (Wbp)***  
>   
> *Geachte heer, mevrouw,*  
>   
> *Onder verwijzing naar artikel 35 lid 1 en lid 2 Wet bescherming persoonsgegevens verzoek ik* *u om mij een transactieoverzicht te doen toekomen voor mijn Rabobank 1) directrekening 1225.xx.xxx, 2) internetspaarrekening 1225.xxx.xx, 3) ondernemersrekening 1546.xx.xxx, 4) bedrijfsspaarrekening 3631.xx.xx voor de periode vanaf het openen van de rekeningen tot en met 1 oktober 2011.*  
>   
> *Wellicht ten overvloede vermeld ik dat in lid 1 van het wetsartikel een termijn van vier weken wordt genoemd waarin u aan dit verzoek moet voldoen.*  
>   
> *Graag wijs ik u ook op het volgende bericht op internet waarin hetzelfde verzoek bij de ASN Bank is gedaan: <span style="text-decoration: underline;">http://koenversmissen.tumblr.com/post/12320895050/inzage-bij-asn-bank-zo-kan-het-dus-ook</span>.*  
>   
> *Een kopie van mijn identiteitsbewijs is als bijlage opgenomen.*  
>   
> *Hoogachtend,*  
> *BAG Hagens*

  
 Op 23 december heb ik deze reactie ontvangen op dit verzoek:  
> *Beste meneer Hagens,*  
> *Op 23 december jl. hebben wij de volgende vraag van u ontvangen:*  
> *\[kopie van bovenstaande brief\]*  *Allereerst excuses voor het feit dat we zo laat reactie geven aan uw verzoek. De reden hiervan, is dat uw verzoek eerst is binnen gekomen bij Rabobank Nederland en daarna pas is doorgestuurd naar de lokale bank.* *Graag wil ik uw verzoek in behandeling nemen. Begrijp ik uit uw verzoek dat u vraagt om de rekeningafschriften van bovengenoemde rekeningen? Als dat het geval is dan kunt u de rekeningafschriften voor de volgende rekeningen terugvinden in uw InternetBankieren.*  
>  Directrekening 1225.xx.xxx  
> *InternetSparen 1225.xxx.xx*  
> *OndernemersPakket 1546.xx.xxx*  
>   
> *De rekeningafschriften van uw BedrijfSpaarrekening zijn al per post aangeleverd. Deze ontvangt u maandelijks indien er transcaties plaatsvinden op de rekening.*  
> *Mocht u bepaalde rekeningafschriften toch opnieuw willen laten aanmaken dan wil ik u kenbaar dat de kosten hiervoor €5,- per rekeningafschrift bedragen. Een rekeningafschrift is een maand.*  
>   
> *Hopende u hiermee voldoende geïnformeerd te hebben. Mocht u nog vragen hebben dan kunt u mailen of bellen op [(076) 513 44 44](tel:%28076%29%20513%2044%2044).*  
> *Met vriendelijke groet,*  
> *\[naam van medewerker\]*  
> *Verkoop &amp; Service Adviseur*

  
 Gezien het feit dat ik al mijn hele leven lang bij de Rabobank klant ben, is het voor te stellen dat de kosten door de duizend euro bedragen als alle afschriften opvraag via InternetBankieren. Er is echter wettelijk bepaald dat de Rabobank niet meer dan €4,50 of €0,23 per pagina of €22,50 indien het overzicht meer uit 100 pagina's bestaat, in rekening mag brengen als tegemoetkoming. Dit staat in besluit kostenvergoeding Wpb (<http://wetten.overheid.nl/BWBR0012565>) die al van kracht is vanaf 2001 en valt onder de Wet bescherming persoonsgegevens (<http://wetten.overheid.nl/BWBR0011468/>). Dit is een behoorlijk verschil in euro's om aan mijn rekeningafschriften te komen! Met deze kennis heb ik de volgende reactie per mail op 26 december 2011 teruggestuurd:  
> Beste \[...\],  
>   
>  Graag ontvang ik nog steeds een transactieoverzicht van mijn Rabobank:  
>  1) directrekening 1225.xxx  
>  2) internetspaarrekening 1225.xxx  
>  3) ondernemersrekening 1546.xxx  
>   
>  Met het niet ontvangen van het transactieoverzicht van mijn Bedrijfsspaarrekening ga ik akkoord met de door u aangedragen argumenten dat deze overzichten al worden verstuurd per post indien er een transactie plaatsgevonden.  
>   
>  Ik wil u benadrukken dat ik niet akkoord ga met de kosten van €5,- per rekeningafschrift voor het ontvangen van de bovengenoemde transactieoverzichten.  
>  Ik wil u wijzen op het besluit kostenvergoeding Wpb (http://wetten.overheid.nl/BWBR0012565). Ik wil en hoef niet meer dan €4,50 of €0,23 per pagina of €22,50 indien het overzicht meer uit 100 pagina's bestaat, te betalen.  
>   
>  Met vriendelijke groet,  
>  Sebastian Hagens

  
 Waarop ik de volgende reactie op 27 december 2011 heb gekregen van een andere Rabobank medewerker:  
> Beste heer Hagens,  
>   
>  Als antwoordt op u mail stuur ik u bijgevoegde voorwaarden en onderstaande uitleg.  
>   
>  Via internetbankieren ontvangt u digitaal de rekeningafschriften van een uw Rabo DirectRekening. Vanaf het ontstaan van deze rekening heeft de Rabobank u elke maand een rekeningafschrift gestuurd. De klant is zelf verantwoordelijk om deze rekeningafschriften te archiveren. Daarnaast verwijs is ik u naar de Algemene voorwaarden voor betaalrekeningen en betaaldiensten van de Rabobank. In het tarievenblad (kop Rekeningafschriften) en paragraaf 3.3 t/m 3.5 van de algemene voorwaarden vindt u alle informatie en voorwaarden voor het in rekening brengen van bepaalde kosten. De kosten voor heraanvraag rekeningafschriften vallen onder de kosten van onderzoek in onze administratie, daar wij rekeningafschriften tot 15 maanden terug kunnen inzien en heraanmaken.  
>  Ook verwijs ik u naar de algemene voorwaarden van uw Rabo IntenetSpaarrekening. U ontvangt van Rabo InternetSparen geen afschriften. Via Rabo Internetbankieren kunt u het actuele saldo tot 15 maanden terug raadplegen. Daarnaast ontvangt u elk jaar een financieel jaaroverzicht.  
>   
>  Ik verneem van u of u de rekeningafschriften van uw Rabo Directrekening wilt heraanmaken. De kosten bedragen, zoals eerder genoemd, 5 euro per rekeningafschrift.  
>   
>  Hopende u hiermee voldoende te hebben geïnformeerd. Mocht u nog vragen hebben dan kunt u mailen of bellen op (076) 513 44 44.  
>   
>  Met vriendelijke groet,  
>   
>  \[naam medewerker\]  
>  Service en Advies Team

  
 In de bijlage van de mail trof ik ook drie documenten aan: 1) tarievenblad 2) Algemene vw betaalrekening 3) AV spaarrekening. Mijn reactie op 27 december 2011:  
> Beste \[...\],  
>   
>  De wet Wpb (http://wetten.overheid.nl/BWBR0011468/) bestaat uit voorschriften die als dwingend recht gelden. Ik benadruk nogmaals dat ik niet akkoord ga met de kosten van €5,- per rekeningafschrift. In het besluit kostenvergoeding Wpb (http://wetten.overheid.nl/BWBR0012565) is bepaald (die valt onder de wet Wbp) dat de verantwoordelijke (Rabobank) €4,50 of €0,23 per pagina of €22,50 indien het overzicht meer uit 100 pagina's bestaat, in rekening brengen als redelijke vergoeding.  
>   
>  Met vriendelijke groet,  
>  Sebastian Hagens

  
 Zoals je ziet ga ik nu in de herhaling vallen en krijg ik de indruk dat er niet naar me wordt geluisterd. Dat werd wederom bevestigd in deze reactie die daarop kort volgde:  
> Beste heer Hagens,  
>   
>  Ik verwijs u nogmaals naar onderstaande mail die ik u eerder heb verstuurd. Het beleid van Rabobank Nederland is, dat wij kosten in rekening brengen voor het heraanmaken van rekeningafschriften.  
>  Wij wijken niet van dit beleid af.  
>   
>  Met vriendelijke groet,  
>   
>  \[naam medewerker\]  
>  Service en Advies Team

  
 Na deze email heb ik de medewerker van Rabobank telefonisch toegelicht waarom ik geen €5,- per rekeningafschrift ga betalen. Ik kreeg echter het verwijt dat dit mijn mening was, terwijl ik toch mijn argumenten hard maak met wettelijke bepalingen. De medewerker bevestigde ook dat zij de bepalingen niet had gelezen die in Wbp staan en tevens ook van mening was dat rekeningafschriften geen persoonlijke gegevens bevatten. Met stomheid geslagen en grote verbazing werd ik doorverwezen naar het klachtencentrum van Rabobank Nederland. Maar ik heb geen klacht, maar een verzoek... Bij het klachtencentrum Rabobank Nederland vertelde ze me dat ik dit toch echt moet oplossen mijn mijn lokale bank (Rabobank Etten-Leur) en ze mij een klachtenformulier zouden toesturen. Inmiddels heb ik dat klachtenformulier ontvangen, maar dit gaat mij niet opleveren waar ik om heb gevraagd in mijn brief. Het feit is dat de Rabobank een beleid voert die in strijd is met de wet. Dit heb ik ondertussen ook al doorgegeven met een signaal aan College Bescherming Persoonsgegevens waar ik nog geen reactie op heb ontvangen.  
  
 Goed, dit is de huidige stand van zaken en ik zie het wel zitten om een gang naar de rechter voor te bereiden. Echter ben ik geen jurist en zoek hierbij de nodige hulp. Als iemand me kan helpen hoor ik dat graag. Iedereen gaat dit tenslotte aan, rekeningafschriften worden in de toekomst alleen maar digitaal verstrekt en elke bank die zulke hoge kosten in rekening brengt om jóuw afschriften te verstrekken is in strijd met de wet. Tevens kan ieder dit enkele honderden euro's gaan besparen in de toekomst! Er moet dus iets veranderd worden, het beleid moet aangepast worden op basis wat er in de wet is bepaald.
