---
title: "Facebook Timeline & Een digitale levenslijn"
date: "2011-09-23T09:03:19.000Z"
description: "Eind 2009 was bezig met het schrijven van een scriptie in mijn afstudeerfase aan de AKV|St.Joost. In februari was deze k..."
categories: [internet]
comments: true
---

Eind 2009 was bezig met het schrijven van een scriptie in mijn afstudeerfase aan de AKV|St.Joost. In februari was deze klaar was met de titel 'Een digitale levenslijn'. Je kunt heel simpel stellen dat mijn scriptie stevig concept was die al jouw digitale sporen op het internet zou samenvoegen en op diverse manieren kunt visualiseren. Bijvoorbeeld over een tijdlijn.  
  
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="450" style="width: 600; height: 450;" width="600"><param name="allowFullScreen" value="true"></param><param name="src" value="http://static.issuu.com/webembed/viewers/style1/v1/IssuuViewer.swf"></param><param name="allowfullscreen" value="true"></param><param name="flashvars" value="mode=embed&documentId=100326093018-29af3fe8796d46bd9b2febfbd366af14&documentUsername=Sebastix&documentName=scriptie-digitalelevenslijn-sebastianhagens&layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Fcolor%2Flayout.xml&backgroundColor=000000&showFlipBtn=true"></param><embed allowfullscreen="true" flashvars="mode=embed&documentId=100326093018-29af3fe8796d46bd9b2febfbd366af14&documentUsername=Sebastix&documentName=scriptie-digitalelevenslijn-sebastianhagens&layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Fcolor%2Flayout.xml&backgroundColor=000000&showFlipBtn=true" height="450" src="http://static.issuu.com/webembed/viewers/style1/v1/IssuuViewer.swf" style="width: 600; height: 450;" type="application/x-shockwave-flash" width="600"></embed></object>  
  
 Deze scriptie ligt ook aan de basis rondom het gedachtegoed wat ik heb uitgewerkt onder [Root:\\](http://www.myroot.me). Binnen Root:\\ heb ik bijvoorbeeld een aantal visualisaties uitgewerkt die passen bij een 'digitale levenslijn' applicatie:  
  
  
  
[![](../../../images/2011/09/visual-jaartal-300x199.jpg "visual-jaartal")](../../../images/2011/09/visual-jaartal.jpg)  
  
![](../../../images/2011/09/visual-moment-300x197.jpg "visual-moment")  
  
   
  
   
  
   
  
   
  
   
  
[![](../../../images/2011/09/flow-zooming-443x1024.jpg "flow-zooming")](../../../images/2011/09/flow-zooming.jpg)  
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
 Een aantal grafische ontwerpen (destijds uitgewerkt in samenwerking met [Something New](http://www.something-new.nl)):  
  
[![](../../../images/2011/09/Ontwerp_2-1-300x187.jpg "Ontwerp_2-1")](../../../images/2011/09/Ontwerp_2-1.jpg)[![](../../../images/2011/09/Ontwerp_2-2-300x187.jpg "Ontwerp_2-1")](../../../images/2011/09/Ontwerp_2-2.jpg)  
  
   
  
   
  
   
  
   
  
   
  
   
  
[![](../../../images/2011/09/Ontwerp_2-31-300x187.jpg "Ontwerp_2-3")](../../../images/2011/09/Ontwerp_2-31.jpg)  
  
[![](../../../images/2011/09/new_root_bekijken_3_1-300x177.jpg "new_root_bekijken_3_1")](../../../images/2011/09/new_root_bekijken_3_1.jpg)  
  
   
  
   
  
   
  
   
  
   
  
   
  
[![](../../../images/2011/09/roadtrip2009_view13aug_1-300x177.jpg "roadtrip2009_view13aug_1")](../../../images/2011/09/roadtrip2009_view13aug_1.jpg)  
  
[![](../../../images/2011/09/roadtrip2009_view13aug_2-300x176.jpg "roadtrip2009_view13aug_2")](../../../images/2011/09/roadtrip2009_view13aug_2.jpg)  
  
   
  
   
  
   
  
   
  
   
  
   
  
 Gisteren heeft Facebook een aantal veelbelovende nieuwe mogelijkheden aangekondigd, waaronder Timeline. Timeline is de verzameling van al jouw Facebook actitiveiten van afgelopen jaren en zijn op een aantrekkelijke manier onder elkaar gezet. Gebaseerd op hetzelfde concept waar ik ook mijn scriptie omheen heb geschreven.  
 Hieronder een screenshot van mijn Timeline op Facebook (het begin):  
  
[![](../../../images/2011/09/Google-Chrome1-1024x813.png "Google Chrome")](../../../images/2011/09/Google-Chrome1.png)  
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  
 Natuurlijk is het concept niet uniek, kijk maar eens naar [Memolane](http://memolane.com/site/), [Lifepath.me](http://lifepath.me/) en [Tiki-Toki](http://www.tiki-toki.com/).  
  
[![](../../../images/2011/09/lane1-300x176.png "lane1")](../../../images/2011/09/lane1.png)  
  
[![](../../../images/2011/09/The-Fight-for-Democracy-in-the-Middle-East-300x192.png "The Fight for Democracy in the Middle East")](../../../images/2011/09/The-Fight-for-Democracy-in-the-Middle-East.png)  
  
   
  
   
  
   
  
   
  
   
  
   
  
 Ik ben benieuwd wat de nieuwe Facebook updates teweeg gaan brengen. Wat denk jij?
