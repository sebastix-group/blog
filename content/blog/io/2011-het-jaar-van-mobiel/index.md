---
title: "2011: hét jaar van mobiel"
date: "2011-12-21T11:13:13.000Z"
description: "2011 is absoluut het jaar waarin mobiel doorgebroken is als apparaat die vaker en vaker wordt gebruikt om het internet t..."
categories: [internet]
comments: true
---

2011 is absoluut het jaar waarin mobiel doorgebroken is als apparaat die vaker en vaker wordt gebruikt om het internet te besurfen. De video hieronder geeft dat ook op een prachtige manier weer:  
  
<iframe allowfullscreen="" frameborder="0" height="350" src="http://www.youtube.com/embed/aKAIzU90zA8" width="622"></iframe>  
  
 In mijn eigen praktijk zie ik ook steeds meer mobiel gebruik, waaronder in de mailings die ik verstuurde afgelopen jaar. Bijna 50% van de mensen die de mail openen doen dit met hun mobiele apparaat! Binnen een jaar tijd heb ik dit zien stijgen met 15%. Dat dwingt je dus ook anders na te denken over de content die je plaatst in de email en hoe je mogelijke conversie eruit ziet.

[![](../../../images/2011/12/2011mobile-email-300x160.png "2011mobile-email")](../../../images/2011/12/2011mobile-email.png)

  
 Ook websites worden vaker bezocht met een mobiel apparaat. Hieronder een screenshot uit Google Analytics van het mobiele bezoek op een website met dagelijks ~4000 bezoekers. Zoals je kunt zien is in 2011 het mobiel bezoek significant toegenomen en is de huidige situatie zelfs zo dat het mobiele bezoek bijna 25% van het totale bezoek inneemt. Komt er een moment dat er meer mobiel bezoek is dan bezoek via de reguliere computer en browser? Zo ja, wanneer?  
  
[![](../../../images/2011/12/2011mobile-website-300x42.png "2011mobile-website")](../../../images/2011/12/2011mobile-website.png)  
  
 Als ik even in de spiegel kijk, zie ik dat ik nog veel te weinig doe en nadenk over de mogelijkheden met mobiel. En dat terwijl ik wel heel goed weet welke tools er zoal beschikbaar zijn om efficient dingen te bouwen voor mobiel. Kijk bijvoorbeeld naar [jQuery Mobile](http://jquerymobile.com/), [PhoneGap](http://phonegap.com/), [Appcelerator](http://www.appcelerator.com/), [KendoUI](http://www.kendoui.com/), [Sencha](http://www.sencha.com/) (meer [hier](http://www.diigo.com/user/sebastix/mobile)). De groei voor mobiel is er ook nog lang niet uit en ik verwacht dat mobiele web (let's create some more!) waarop we surfen uiteindelijk ook echt door de massa zal worden omarmt.
