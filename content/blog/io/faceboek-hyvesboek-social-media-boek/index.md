---
title: "Faceboek, Hyvesboek, social media boek"
date: "2010-11-24T12:52:04.000Z"
description: "Afgelopen zondag trok deze tweet van watvindikvanjou (tip!) mijn aandacht. Namelijk een boek van jouw Facebook:



I..."
categories: [internet]
comments: true
---

Afgelopen zondag trok deze [tweet](http://twitter.com/watvindikvanjou/status/6320483398057984) van [watvindikvanjou](http://www.watvindikvanjou.nl) (tip!) mijn aandacht. Namelijk een boek van jouw Facebook:  
  
<iframe frameborder="0" height="340" src="http://player.vimeo.com/video/16889815?portrait=0" width="625"></iframe>  
  
 Interessant idee en via [IN10](http://www.in10.nl) zie ik zelfs dat [William van Ommen](http://williamvanommen.nl/) ook al een [faceboek](http://issuu.com/williamvanommen/docs/faceboek_william) als concept had uitgewerkt.  
 Wil je een eigen faceboek? Dat kan met [My Egobook](http://apps.facebook.com/my-egobook/) of [Book of Fame](http://www.bookoffame.net/).  
  
 Ondertussen ontwikkel ik [HVSbackup](http://www.hvsbackup.nl) verder en zal ook goed nadenken hoe je dit idee kunt vertalen in een boek van jouw Hyves vrienden, krabbels, blogs, foto's en www's ([www.hvsboek.nl](http://www.hvsboek.nl)? [www.hyvesboek.nl](http://www.hyvesboek.nl)?). En waarom niet met je tweets, mails, Flickr foto's, linkedin etc? =)
