---
title: "Volkstyle: magazine & website voor de VAG community"
date: "2012-11-06T08:47:26.000Z"
description: "Een goede klant 402automotive van Sebastix heeft een een nieuw concept VolkStyle voor VAG autoliefhebbers geïntroduceer..."
categories: [internet]
comments: true
---

Een goede klant [402automotive](http://www.402automotive.com) van Sebastix heeft een een nieuw concept VolkStyle voor VAG autoliefhebbers geïntroduceerd. VAG staat voor Volkswagen Group en tot deze groep behoren de volgende automerken: Volkswagen, Audi, Bentley, Bugatti, Lamborghini, Porsche, SEAT en Skoda.  
  
 Met een splinternieuw magazine die om de twee maanden verschijnt maakt 402automotive zichtbaar hoe innovatief en gevarieerd deze auto’s kunnen zijn die worden aangepakt door tuners. Verschillende stijlen zijn goed zichtbaar en dat maakt deze scene aantrekkelijk om te zien.

[![](../../../images/2012/08/LittleSnapper.png "LittleSnapper")](../../../images/2012/08/LittleSnapper.png)

  
Sebastix heeft voor Volkstyle meegedacht in de online strategie. De doelgroep is online erg actief en op diverse locaties terug te vinden, waarvan Facebook een van de grootste is. De [Volkstyle Facebook pagina](http://www.facebook.com/volkstyle.eu) had daarom in een aantal maanden al snel enkele duizenden fans (6000+) en de betrokkenheid van deze fans is groot. Dagelijks worden er ongeveer 10 tot 15 berichten geplaatst die veel likes en reacties ontvangen van VolkStyle fans.  
 Echter met een succesvolle Facebook pagina heb je nog geen succesvolle community én business. Het is een uitdaging om de conversie te maken van Facebook naar directe sales. Hiervoor kun je verschillende middelen inzetten. Voor VolkStyle lag het voor de hand om hier in ieder geval een website voor op te zetten waar je onder andere het magazine snel en eenvoudig kunt kopen. Middels een koppeling Ogone kan er via verschillende betaalmethodes betaald worden vanuit verschillende landen.

  
  
<table>  
<tbody>  
<tr>  
<td>[![](../../../images/2012/10/Google-Chrome-4.png "Google Chrome 4")](../../../images/2012/10/Google-Chrome-4.png)</td>  
<td>[![](../../../images/2012/10/Google-Chrome-31.png "Google Chrome 3")](../../../images/2012/10/Google-Chrome-31.png)</td>  
<td>[![](../../../images/2012/10/Google-Chrome-21.png "Google Chrome 2")](../../../images/2012/10/Google-Chrome-21.png)</td>  
</tr>  
</tbody>  
</table>

  
 Meten is weten en daarom ontwikkelen we door in een iteratief proces om de conversie verder te optimaliseren. Concreet betekent dit dat we binnenkort op de website ook gaan werken met een uitgebreid winkelmandje om in de shop verschillende producten te kunnen aanbieden. Daarnaast maakt VolksStyle uit van een pakket van nog 14 andere websites. As we speak worden deze op dit moment gerealiseerd en zullen in december gelanceerd worden. Ik hou jullie op de hoogte.
