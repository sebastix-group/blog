---
title: "Nieuwe weblog!"
date: "2011-09-21T18:28:29.000Z"
description: "Je leest en ziet het goed: een nieuwe weblog! Voorheen schreef ik met enige regelmaat artikelen op interactieontwerpen.s..."
categories: [internet]
comments: true
---

Je leest en ziet het goed: een nieuwe weblog! Voorheen schreef ik met enige regelmaat artikelen op [interactieontwerpen.sebastix.nl](http://interactieontwerpen.sebastix.nl) en [myroot.me](http://www.myroot.me). Beide blogs waren niet altijd geschikt om te delen waar Sebastix mee bezig is/was. De nodige content is overgezet en voorlopig blijven de 'oude' weblogs ook online staan. Op myroot.me zal op de langere termijn ook een nieuwe website komen die het concept van [Root:\\](http://www.myroot.me) beter zal uitleggen.  
  
 Met deze nieuwe weblog is het de bedoeling om regelmatig terug te blikken op wat Sebastix drijft en met welke zaken we bezig zijn. Onderwerpen die centraal zullen staan: wat gebeurt er met jouw persoonlijke data, online privacy, social media, vernieuwende online concepten, nieuwe (eigen) toepassingen/tools, (eigen) experimenten met nieuwe web-technologie, afgeronde projecten voor klanten &amp; meer. Deze content zal ook regelmatig in de nieuwsbrief van Sebastix worden samengevat (schrijf je in onderaan deze pagina). Of volg Sebastix op [Twitter](http://www.twitter.com/sebastix), [LinkedIn](http://nl.linkedin.com/in/sebastianhagens), [Google+](http://plus.google.com/103702145593626870060/) of [Facebook](http://www.facebook.com/interactie.ontwerper).  
  
 Wees vrij om te reageren, ik hoor graag wat jij meer zou willen zien/weten van Sebastix!  
  
[![](../../../images/2011/09/sebastix-photo-fora.jpg "sebastix-photo-fora")](../../../images/2011/09/sebastix-photo-fora.jpg)  
 Sebastian Hagens
