---
title: "Big data is watching you"
date: "2011-02-13T13:00:27.000Z"
description: "Al enige tijd hou ik in de gaten wat er zoal wordt gedaan met onze persoonsgegevens die we vaak onbewust en onwetend m..."
categories: [internet]
comments: true
---

![](http://root.sebastix.nl/images/bigdata-cover.jpg)  
 Al enige tijd hou ik in de gaten wat er zoal wordt gedaan met onze persoonsgegevens die we vaak onbewust en onwetend met derden delen, maar met bovenstaande titel op het voorblad van Management Team van 4 februari 2011 gaan mijn haren recht overeind staan. En het wordt nog erger als je het gehele artikel doorleest...  

 Het artikel telt 5 pagina's die je hieronder kunt bekijken. Klik op een afbeelding om het origineel te kunnen bekijken, het is op die manier goed leesbaar. Hou vooral goed in je achterhoofd vanuit welk perspectief wij (diegenen die alles weggeven) worden uitgelicht.  
 Het artikel deed me ook direct denken aan een artikel op Frankwatching uit 2009: [Data is de nieuwe olie](http://www.frankwatching.com/archive/2009/11/02/data-is-de-nieuwe-olie/).  
  
[![](http://root.sebastix.nl/images/bigdata-blad1.jpg)](http://root.sebastix.nl/images/bigdata-blad1.jpg)  
[![](http://root.sebastix.nl/images/bigdata-blad2.jpg)](http://root.sebastix.nl/images/bigdata-blad2.jpg)  
[![](http://root.sebastix.nl/images/bigdata-blad3.jpg)](http://root.sebastix.nl/images/bigdata-blad3.jpg)  
[![](http://root.sebastix.nl/images/bigdata-blad4.jpg)](http://root.sebastix.nl/images/bigdata-blad4.jpg)  
[![](http://root.sebastix.nl/images/bigdata-blad5.jpg)](http://root.sebastix.nl/images/bigdata-blad5.jpg)  
  
 Na het lezen van dit artikel hoop ik dat ik niet de enige ben die enigszins een rare smaak krijgt bij deze ontwikkelingen. Het is een ontwikkeling waarvan we de illusie niet moeten hebben dat we hem kunnen stoppen, nee...deze is al heel lang geleden ingezet waar geen einde aan zal komen. De uitspraak 'kennis is macht' gaat goed op binnen deze context, echter is het een kennis die slechts bij deze derden aanwezig is. Het bevestigd voor mij wederom dat er snel tools moeten komen waarmee wij inzicht kunnen krijgen in die kennis die anderen over ons en van ons hebben. Het bevestigt voor mij wederom waarom ik Root:\\ heb uitgewerkt en waar ik me nog altijd hard voor wil maken.  
 Dat het een grootse uitdaging is, wordt steeds helderder na mijn afstuderen. Maar het is geen onmogelijke missie.  
 Nu hoor ik heel graag hoe jij tegenover deze ontwikkelingen staat. En waar je behoefte aan hebt. Als ik namens mezelf spreek; ik zou het prachtig vinden als ik op een of andere manier inzichtelijk zou hebben wat derden allemaal over en van mij weten. Zelfkennis is machtiger!
