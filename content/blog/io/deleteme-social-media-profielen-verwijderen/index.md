---
title: "DeleteMe - social media profielen verwijderen"
date: "2010-12-21T12:54:39.000Z"
description: "In Amerika is al een tijdje een nieuwe dienst actief waarmee je jouw online profielen en andere digitale sporen kunt..."
categories: [internet]
comments: true
---

<object height="420" width="625"><param name="movie" value="http://www.youtube.com/v/f6Wn_5Ik5t0?fs=1&hl=nl_NL"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed allowfullscreen="true" allowscriptaccess="always" height="420" src="http://www.youtube.com/v/f6Wn_5Ik5t0?fs=1&hl=nl_NL" type="application/x-shockwave-flash" width="625"></embed></object>  
  
 In Amerika is al een tijdje een nieuwe dienst actief waarmee je jouw online profielen en andere digitale sporen kunt laten verwijderen. [DeleteMe](https://www.abine.com/deleteme/) biedt de garantie dat deze dan ook echt verwijderd worden op het internet. In veel gevallen is het zo dat als je jouw account ergens verwijderd, veel informatie nog altijd wel op internet blijven staan. DeleteMe lijkt kwa functionaliteiten erg op het initiatief [SuicideMachine](http://suicidemachine.org/) waarmee je jouw complete Facebook account kon verwijderen. Echter moest SuicideMachine haar tool stopzetten wegens juridische stappen vanuit Facebook.
