---
layout: blog
title: Why social networks should be based on commons
date: 2024-01-03T13:13:32.76Z
description: This seems like a obvious no-brainer, but the current social media platforms do no feed our best interests based on commons. It's your attention that's being harvested for their own interests. Let me outline why enjoyable social networking should work based on commons.
categories: social media, nostr
comments: "true"
---

> commonality - the fact of sharing interests, experiences or other characteristics with someone or something

For social networking I would like to separate commons in the following three kinds:

1. **Interests**  
   This can be any topic or hobby you have in common.
2. **Problems**  
   A question or issue you have in common.
3. **Geo-located / physical**  
   This can be a certain place or language you have in common.

In real-life you have different type of friends, colleagues, family, business clients etc around certain commons. Based on those contexts, you act different with separate roles. 
When you translate those commons to a digital experience, it's not always obvious to separate those roles to all these people. If you're sharing content on Instagram, it can be seen by almost anyone.
This could result in disagreement with someone on an unshared common. A slowly growing problem which the current big social media platforms are squeezing out with their machine powered algorithms for their own profits.  

Based on commons you share with someone, maybe you would like to choose a social network application build around one specific common.

## Niche communities

In 2022 I wrote ‘Rather a fediverse than a metaverse’ (in [Dutch](/liever-een-fediverse-dan-een-metaverse)) when I was researching the fediverse. Let me point out some personal commons of mine.

### Child-friendly apps

As a father of two little daughters I would like to use real child-friendly apps when they ask me to view some video for example.  
As far as I know, YouTube Kids is the least bad app. The app only works with Google services and a Google account (that's evil). And when in use it's obvious to see the dark-pattern design tricks nudging my kids.
Educative content publishers for children should be aware of this and take the lead with providing content through child-friendly apps. It should be a clear goal for them to make BigTech absolute when it comes to raising our kids.  
Another (maybe better) way is to create a community with parents where we can aggregate and curate content for our kids through an app.

### Honda Civic

This is a country-level hobby with other Honda car enthusiasts. 
In the early 2000s I was maintaining a Dutch forum with more than 3000 visitors each day. Those were all Honda car lovers and there were many evening we had slow-chats in 'offtopic'.
People who used to be active on fora, moved to Facebook groups and then moved to Instagram where they share their car content and WhatsApp to communicate with each-other.
Why can't we use an app where we can do both?

![](Screen-Shot-2023-12-21-13-58-37.38.png)
In 2022, I've built CCHS.social trying to re-invent an online community by aggregating Instagram links with Honda car profiles. Further development on this project staggered from my side.

### Cycling and gravel

This is a local-level hobby where I use a common messaging group chats (WhatsApp urggh!) and Strava. And there is also a club website where you can find the agenda with the next rides.
Can't we aggregate all these content within one app?

### Drupal

The Drupal community is scattered around on different places like Slack, the drupal.org website, mailing-lists and the drupal.community Mastodon instance.
So when I try to find someone to connect with, it's always pretty hard to stay connected because all of those great developers are on a place which seems always far away.

## Niche relays

I know this topic is often discussed by others, but I think there is a real use-case here for niche communities.

With relays filtering specific events on different criteria’s, we can build niche focused apps / clients. Like the niches I mentioned above. I could turn the website CCHS.social into a Nostr client for example. Together with a CCHS relay only accepting content kinds (or npubs) used by the CCHS client.

![](app_screens.png)
Or have a look at the [Proudwheels](https://proudwheels.com) online community mobile app for petrolheads. This is a client-project I’ve worked on as a product owner during the initial development phase in 2021/2022.

## Nostr enables the use of different apps with just one social graph

Last year a lot of Nostr clients are copy-cats build around features. But when you look closer to the client that’s being copied, you could argue when they started it was around one common before they pivoted into a medium used by the mass.

With Nostr, applications can be built around commons and their specific content kinds. This can be built without to the need to rebuild your social connections, because these connections are interoperable.
With those different apps, you have to worry less about sharing content to someone which is not outlined with their commons.
This a somewhat contra-dictional approach, because the most-used Nostr apps are trying to embed all different content kinds nowadays. 



**It's still very, very early for Nostr. Could we build apps more around commons with certain content kinds? What do you think?**



**Resources**

* https://thenewpress.com/books/whos-raising-kids
* https://www.linkedin.com/posts/woutervannoort_nou-dan-lossen-we-het-maar-sámen-op-activity-7129049056584032256-dkWh (Dutch)