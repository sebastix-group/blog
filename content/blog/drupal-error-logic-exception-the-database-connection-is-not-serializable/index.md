---
layout: blog
title: "Drupal error LogicException: The database connection is not serializable"
date: 2023-12-07T14:47:11.382Z
description: "This week I ran into this error on my Drupal sandbox project." 
categories: Drupal
comments: "true"
---

```
LogicException: The database connection is not serializable. 
This probably means you are serializing an object that has an indirect reference to the database connection. Adjust your code so that is not necessary. 
Alternatively, look at DependencySerializationTrait as a temporary solution. in Drupal\Core\Database\Connection->__sleep() (line 1712 of core/lib/Drupal/Core/Database/Connection.php).
```

This error is returned when a file is being uploaded from the CMS. This is done with a AJAX request. So what code is causing this error? If I look at the stack trace, it looks like no specific module is causing this error. 

When I [search]([drupal LogicException: The database connection is not serializable. - Whoogle Search](https://whoogle.sebastix.social/search?q=drupal+LogicException%3A+The+database+connection+is+not+serializable.)) for this error the results are quite random and most of the time there is a module causing this error. In my case is doesn't look like a module is the cause for this error.

After some advanced searching for a solution, I've found these comments: [Ajax error on [Add exception] button if &quot;seasons&quot; is activated [#3399054] | Drupal.org](https://www.drupal.org/project/office_hours/issues/3399054#comment-15307090) where a fellow Dutch Drupal developer found a possible cause of the problem. 

After I uninstalled the last activated modules back to a situation where the error disappeared, I found out a custom module was causing the error.  In this module a class is used to store data in a database table. 

As the exception error suggest, a temporary solution is to use DependencySerializationTrait. After I added `use DependencySerializationTrait;` to the storage class and the class which is calling this storage class, the error was fixed. 

With this temporary fix, I still don't know what exactly is causing this error. What data is being serialized in a wrong way according to the Drupal standards? 
