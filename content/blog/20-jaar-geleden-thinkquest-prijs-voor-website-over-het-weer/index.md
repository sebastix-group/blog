---
layout: blog
title: "20 jaar geleden: Thinkquest prijs voor website over 'het weer'"
date: 2021-04-19T19:41:48.045Z
description: Als 14-jarige heb ik in 2001 samen met Rob van Kooten en Wouter van
  Lent mijn eerste grote website gemaakt in de webstrijd ThinkQuest.
categories: Website, ThinkQuest
comments: "true"
---
In februari 2010 schreef ik er al eens [een blog](https://sebastix.nl/blog/interactieontwerpen/uit-den-ouden-doosch-thinkquest-minister-hermans/) over. In 2001 wonnen we de derde prijs ter waarde van ƒ5000,- bij ThinkQuest in de categorie Voor Leerlingen 12 t/m 15 jaar.

De website kun je hier bekijken: <http://thinkquest-lla022.sebastix.nl/>

Op de weblog van Sjaak Jansen staat er ook een leuk bericht over onze website en ThinkQuest: <https://www.sjaakjansen.nl/2014/01/20/tq-2001-het-weer-en-meer/>. Met onder andere deze video:

https://www.youtube.com/watch?v=aYAIn4FZ7Xc

Het volledige team:

![Frans van Bommel, Rob van Kooten, Wouter van Lent, Sjaak Jansen en Sebastian Hagens](2001weer.jpg)

In de lokale krant DeStem:

![](4381477327_4bc3520352_o.jpg)

Het is inspirerend om dit nog eens zo terug te zien na 20 jaar. In die 20 jaar is er veel veranderd, maar het werken met digitale techniek staat nog altijd centraal in het werk wat ik doe voor anderen.