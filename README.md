# Sebastix blog

Made with Gatsby with the Leonids template  

Live: **[https://sebastix.nl/blog](https://sebastix.nl/blog)**

### Development

```bash
gatsby develop
```

### Build

```bash
gatsby build --prefix-paths
```

### Update

```bash
npm update
```

### Manually build and deploy
```bash
npm run build
rsync -avz --delete --exclude-from='gitlab-ci/rsync-excludes.txt' -e ssh public/. sebastix@sebastix.nl:/var/www/vhosts/sebastix.nl/domains/sebastix.nl/private_html/blog
```