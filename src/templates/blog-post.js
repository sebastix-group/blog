import React from "react"
import { Link, graphql } from "gatsby"

import Bio from "../components/bio"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Alert from "../components/alert"
import { rhythm, scale } from "../utils/typography"
import { format } from "date-fns"
import { nl } from 'date-fns/locale'

const BlogPostTemplate = ({ data, pageContext, location }) => {
  const post = data.markdownRemark
  // const siteTitle = data.site.siteMetadata.websiteTitle
  const { previous, next } = pageContext

  var alert;

  if (location.pathname.startsWith('/blog/io/')) {
    alert = <Alert text={'bron io.sebastix.nl'} />;
  }
  if (location.pathname.startsWith('/blog/interactieontwerpen/')) {
    alert = <Alert text={'bron interactieontwerpen.sebastix.nl (studie blog)'} />;
  }

  return (
    <Layout location={location} title="Blog">
      <SEO
        canonical={location.pathname}
        title={post.frontmatter.title}
        description={post.frontmatter.description || post.excerpt}
      />
      <article>
        <header>
          {alert}
          <h1
            style={{
              marginBottom: rhythm(1 / 4),
            }}
          >
            {post.frontmatter.title}
          </h1>

          <p
            style={{
              ...scale(-1 / 5),
              display: `block`,
              marginBottom: rhythm(1),
            }}
          >
            { format(new Date(post.frontmatter.date), "eeee d MMMM yyyy", { locale: nl }) } - {post.fields.readingTime.words} woorden, {post.fields.readingTime.text}
          </p>
          <p
            style={{
              fontWeight: `bold`,
              fontSize: `110%`,
              lineHeight: `1.9`
            }}
          >
            {post.frontmatter.description}
          </p>
        </header>
        <section dangerouslySetInnerHTML={{ __html: post.html }} />
        <hr
          style={{
            marginBottom: rhythm(1),
          }}
        />
        <footer>
          <Bio />
        </footer>
      </article>

      <nav>
        <ul
          style={{
            display: `flex`,
            flexWrap: `wrap`,
            justifyContent: `space-between`,
            listStyle: `none`,
            padding: 0,
          }}
        >
          <li>
            {previous && (
              <Link to={previous.fields.slug} rel="prev">
                ← {previous.frontmatter.title}
              </Link>
            )}
          </li>
          <li>
            {next && (
              <Link to={next.fields.slug} rel="next">
                {next.frontmatter.title} →
              </Link>
            )}
          </li>
        </ul>
      </nav>
    </Layout>
  )
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        websiteTitle  
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      html
      frontmatter {
        title
        date
        description
      }
      fields {
        readingTime {
            text
            words
        }  
      }  
    }
  }
`
