import React from "react"

const Alert = ({ text }) => {

  return (
    <div className="bg-gray-100 max-w-7xl mx-auto mt-6 mb-0 py-3 px-4 text-xs">
      <div className="flex items-center justify-between flex-wrap">
        <div className="w-0 flex-1 flex items-center">
          <span className="flex">
            <svg className={'h-6 w-6'} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
              <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd" />
            </svg>
          </span>
          <p className={'ml-3 my-0 italic'}>
            {text} - dit artikel bevat mogelijk verouderde content
          </p>
        </div>
      </div>
    </div>
  )
}

export default Alert