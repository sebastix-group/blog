module.exports = {
  pathPrefix: "/blog",
  siteMetadata: {
    title: `Blog`,
    websiteTitle: `Sebastian Hagens - creative webdeveloper & tech consultant`,
    author: {
      name: `Sebastian Hagens`,
      summary: `creative webdeveloper & tech consultant`,
    },
    description: `Blog Sebastix.nl`,
    siteUrl: `https://sebastix.nl/blog`,
    canonicalUrl: `https://sebastix.nl`,
    social: {
      twitter: `sebastix`,
      linkedin: `sebastianhagens`,
      gitlab: `sebastix`,
      github: `sebastix`,
      drupal: `sebastian-hagens`,
      stackoverflow: `1269858/sebastian-hagens`,
      mastodon: `https://nwb.social/@sebastian`,
      nostr: `https://nostr.com/npub1qe3e5wrvnsgpggtkytxteaqfprz0rgxr8c3l34kk3a9t7e2l3acslezefe`
    },
    defaultImage: "images/bg.jpeg",
  },
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/blog`,
        name: `blog`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/assets`,
        name: `assets`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/images`,
        name: `images`,
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 560,
              withWebp: true,
            },
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
            options: {
              wrapperStyle: `margin-bottom: 1.0725rem`,
            },
          },
          `gatsby-remark-prismjs`,
          {
            resolve: `gatsby-remark-copy-linked-files`,
            options: {
              destinationDir: `images`,
            }
          },
          `gatsby-remark-smartypants`,
          `gatsby-remark-external-links`,
          `gatsby-remark-embedder`,
          `gatsby-remark-reading-time`,
        ],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-feed`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Sebastix`,
        short_name: `Sebastix`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#f05222`,
        display: `minimal-ui`,
        icon: `content/assets/gatsby-icon.png`,
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
    `gatsby-plugin-postcss`,
    `gatsby-plugin-twitter`,
    'gatsby-plugin-sitemap',
    {
      resolve: `gatsby-plugin-netlify-cms`,
      options: {
        publicPath: `admin`,
        enableIdentityWidget: true,
      }
    },
    {
      resolve: 'gatsby-plugin-matomo',
      options: {
        siteId: '1',
        matomoUrl: 'https://matomo.sebastix.nl',
        siteUrl: 'https://sebastix.nl/blog'
      }
    }
  ],
}
